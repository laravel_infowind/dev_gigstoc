<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Response;
use App\User;
use DataTables;
use Validator;
use Redirect;
use Mail;
use Illuminate\Support\Facades\Hash ;
use Illuminate\Support\Str;
use DB;
use Illuminate\Support\Facades\Input;
use App\Models\WithdrawalRequest;
use App\Models\UserWallet;
use App\Models\Notification;
use Auth;


class WithdrawalController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    

    public function index(Request $request){
        return view('admin::withdrawal.index');
    }

    public function requestList(Request $request)
    {
        try{
			
            if ($request->ajax()) {
                $data = WithdrawalRequest::latest();
                if (!empty($request->get('date_between'))) {
                    $date = explode('-', $request->get('date_between'));
                    $fromdate = date('Y-m-d', strtotime($date[0]));
                    $todate = date('Y-m-d', strtotime($date[1]));
                    $data->whereDate('created_at', '>=', $fromdate)->whereDate('created_at', '<=', $todate);
                }
                if (!empty($request->get('status'))) {
					$status = $request->get('status');
                    $data->where('status', $status);
                }
				$data = $data->latest()->get();
                return Datatables::of($data)
                        ->addIndexColumn()
						 ->addColumn('username', function($row){
                         		$btn = $row->user->name;
                                return $btn;
                         })
					    ->editColumn('created_at', function($row){
                         	    return changeDateformat($row->created_at);
                         })
						 ->editColumn('amount', function($row){
                         	    return '$'.number_format($row->amount,2);
                         })
						  ->editColumn('selected_method', function($row){
                         	    return ucfirst($row->selected_method);
                         })
						  ->editColumn('status', function($row){
								if($row->status == 'decline'){
									$status = '<span id="sta_'.$row->id.'">Declined</span>';
								}elseif($row->status == 'complete'){
									$status = '<span id="sta_'.$row->id.'">Approved</span>';
								}elseif($row->status == 'processing'){
									$status = '<span id="sta_'.$row->id.'">Processing</span>';
								}else{
									$status = '<span id="sta_'.$row->id.'">Pending</span>';
								}
                         	    return ucfirst($status);
                         })
                        ->addColumn('action', function($row){
                            $viewUrl = url('admin/view-request/'.$row->id);
                            $btn = '<div class="dropdown">';
									if($row->status == 'pending'){
								  $btn .='<button class="btn btn-primary " type="button" data-toggle="dropdown">Action
									<span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu">' ; 
								  if($row->status == 'pending'){
								  $btn .='<li><a onclick="changeRequestStatus(1,'.$row->id.')" href="javascript:void(0)">Processing</a></li>';
								  }
								  if($row->status == 'pending'){
								  $btn .= '<li><a onclick="changeRequestStatus(2,'.$row->id.')" href="javascript:void(0)">Decline</a></li>';
								  }
								  if($row->status == 'pending' || $row->status == 'processing'){
								  $btn .= '<li><a onclick="changeRequestStatus(3,'.$row->id.')" href="javascript:void(0)">Approve</a></li>';
								  }
								  $btn .= '</ul>';
									}
								  $btn .='<a href="'.$viewUrl.'" class="edit btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
								</div>';
		                        return $btn;
                        })
						->addColumn('payout', function($row){
                            $viewUrl = url('admin/payout');
							$btn ='-';
							if($row->selected_method == 'paypal' && $row->status == 'pending'){
                           
								$btn ='<form action="'.$viewUrl.'" method="get">
                                <div class="row">
                                 <div class="col-md-2">
								 <input type="hidden" name="user_id" value="'.$row->user_id.'">
                                  <button class="form-control btn btn-success" type="submit">Payout </button>
                                 </div>
                                 </div>
                                </form>';
							}elseif($row->selected_method == 'paypal' && $row->status != 'pending'){
								$btn ='Transferred';
							}	
							return $btn;
                        })
                        ->rawColumns(['action','username','status','payout'])
                        ->make(true);
            }
        }catch(Exception $ex){
            print_r($ex->getMessage());die;
        }
    }
	
	public function requestDetail($id){
		$withdraRequest = WithdrawalRequest::where(['id'=>$id])->first();
		return view('admin::withdrawal.detail',['withdraRequest'=>$withdraRequest]);
	}

	public function changeStatus(Request $request){
		if($request->type == 1){
			$status = 'processing';
		}elseif($request->type == 2){
			$status = 'decline';
		}else{
			$status = 'complete';
		}
	
		$withdraRequest = WithdrawalRequest::where(['id'=>$request->id])->first();
		if($request->type == 3 && !empty($withdraRequest)){
			$walletObj = new UserWallet();
			$walletObj->site_transaction_id = 0;
			$walletObj->user_id = $withdraRequest->user_id;
			$walletObj->credit = 0;
			$walletObj->debit = 0;
			$walletObj->amount = $withdraRequest->amount;
			$walletObj->current_balance = getExistingBlanace($withdraRequest->user_id)-$withdraRequest->amount;
			$walletObj->is_withdrawal = 1;
			$walletObj->save();
		}
		$withdraRequest->status = $status;
		$withdraRequest->decline_reason = ($request->status=='decline')?$request->reason:null;
		if($withdraRequest->save()){
			$message = '';
			$jobUrl = 'my-withdrawal?type=history';
			if($request->type == 1){
				$message = 'Your Withdrawal Request is under processing.';
				$type = 'request_process_notification';
			}elseif($request->type == 2){
				$message = 'Your Withdrawal Request has been declined due to '.$request->reason.'.';
				$type = 'request_decline_notification';
			}else{
				$message = 'Your Withdrawal Request has been approved.';
				$type = 'request_approve_notification';
			}
			saveNotification($message,$jobUrl,$withdraRequest->user_id,Auth::user()->id);
			
		}
		return Response::json(['success'=>true,'message' => 'Status Updated Successfully.','request'=>$withdraRequest]);
	}
	
    public function addCoupon(){
        return view('admin::coupon.create');
    }

    public function editCoupon($id){
        $coupon = Coupon::where(['id'=>$id])->first();
        return view('admin::coupon.edit',['coupon'=>$coupon]);
    }

    public function saveCoupon(AddCouponRequest $request){
        try{
                if(!empty($request->id)){
                    $coupon =   Coupon::where(['id'=>$request->id])->first();
                }else{
                    $coupon =  new Coupon();
                }
                $coupon->coupon = $request->coupon;
				$coupon->amount = $request->amount;
				$coupon->type = $request->type;
				$coupon->total_usage = $request->total_usage;
				$coupon->start_date = date('Y-m-d',strtotime($request->start_date));
				$coupon->end_date = date('Y-m-d',strtotime($request->end_date));
                $coupon->save();
                if(!empty($request->id)){
                    return Response::json(['success'=>true,'message' => 'Coupon Updated Successfully.']);
                }else{
                    return Response::json(['success'=>true,'message' => 'Coupon Added Successfully.']);
                }
          
        }catch(Exception $ex){
            return Response::json(['success'=>false,'message' => $ex->getMessage()]);
            print_r($ex->getMessage());die;
        }
    }
	public function destroy($id)
    {
        $res =  Coupon::where('id', $id)->delete();
        if ($res) {
            return Response::json(['success'=>true,'message' => 'FAQ Deleted Successfully.']);
        }
    }
}
