<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use DataTables;
use File;
use App\Models\Page;
use App\Models\AboutUs;
use App\Http\Requests\Admin\AddPageRequest;
use Response;



class PageController extends Controller
{
    protected $admin;
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ConfigSetting::all();
        $settings = [];
        foreach ($data as $key => $value) {

            $settings[$value->meta_key] = $value->meta_value;
        }

        $settings = (object) $settings;
        return view('admin::settings.index', compact('settings'));
    }

    public function pages(Request $request)
    {
        return view('admin::pages.index');
    }

    public function PageList(Request $request)
    {
        try{
            if ($request->ajax()) {
                        $data = Page::latest();

                        if (!empty($request->get('status'))) {
                            $status = ($request->get('status')=='active')?1:0;
                            $data->where('is_active',$status);
                        }
                        $data  = $data->get();
                    
                return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('created_at',function($date){
                            return date('m-d-Y',strtotime($date->created_at));
                        })
                        ->addColumn('active',function($active){

                            if ($active->is_active == 0) {
                                $status = '<label class="switch"><input id="row'.$active->id.'" onclick="updateStatus('.$active->id.')" type="checkbox"   class="checked change_user_status" data-status="'.$active->is_active.'" data-table="data-table" data-url="'.url('admin/update-page-status').'" ><span class="slider round"></span></label>';
                            } else {
                                $status = '<label class="switch"><input id="row'.$active->id.'" onclick="updateStatus('.$active->id.')" type="checkbox" checked  class="checked change_user_status" data-status="'.$active->is_active.'" data-table="data-table" data-url="'.url('admin/update-page-status').'" ><span class="slider round"></span></label>';

                            }
                            return $status;
                        })
                        ->addColumn('action', function($row){
                            $editUrl = url('admin/edit-page/'.$row->id);
                            $btn = '<a href="'.$editUrl.'" class="edit btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i></a><a id="rowremove'.$row->id.'" onclick="deleteRow('.$row->id.')" href="javascript:void(0)" class="btn btn-danger btn-xs" data-table="page_table" data-url="'.url('admin/delete-page').'" ><i class="fa fa-trash-o"></i></a>';
                             
        
                                return $btn;
                        })
                        ->rawColumns(['action','created_at','active'])
                        ->make(true);
            }
        }catch(Exception $ex){
            print_r($ex->getMessage());die;
        }
    }

    public function addPage(Request $request)
    {
        return view('admin::pages.create');
    }

    public function savePage(AddPageRequest $request)
    {
        // echo "<pre>";
        // print_r($request->all());die;
   
        try{
            if(!empty($request->id)){
                $page =   Page::where(['id'=>$request->id])->first();
            }else{
                $page =  new Page();
            }
            
                $page->name = $request->name;
                $page->slug = str_slug($request->name, '-');
                $page->content = $request->content;
                $page->meta_title = $request->meta_title;
                $page->meta_desc = $request->meta_desc;
                $page->meta_keywords = $request->meta_keywords;
                $page->is_active = 1;
                $page->is_visible = (isset($request->is_visible))?$request->is_visible:0;
                $page->save();
                if(empty($request->id) && (str_slug($request->name, '-')=='about-us')){
                    $about =  new AboutUs();
                    $about->name = $request->name;
                    $about->slug = str_slug($request->name, '-');
                    $about->header_content = 'test header content';
                    $about->middle_content = 'test middle content';
                    $about->footer_header = 'footer header';
                    $about->footer_content = 'footer content';
                    $about->save();
                }

                if(!empty($request->id)){
                    return Response::json(['success'=>true,'message' => 'Page Updated Successfully.']);
                }else{
                    return Response::json(['success'=>true,'message' => 'Page Added Successfully.']);
                }
          
        }catch(Exception $ex){
            return Response::json(['success'=>false,'message' => $ex->getMessage()]);
            print_r($ex->getMessage());die;
        }
    }

    public function saveAboutUs(Request $request)
    {

        try{
                $about =   AboutUs::first();
                if(!empty($about)){
                    $about = $about;    
                }else{
                    $about =  new AboutUs();
                }
               if ($request->hasFile('image')) {
                    $image = $request->file('image');
                    $name = $image->getClientOriginalName();
                    $destinationPath = public_path('/images');
                    $image->move($destinationPath, $name);
                    $about->image = $name;
                }
                $about->name = $request->name;
                $about->slug = str_slug($request->name, '-');
                // $about->main_header = $request->main_header;
                $about->header_content = $request->header_content;
                $about->middle_content = $request->middle_content;
                $about->footer_header = $request->footer_header;
                $about->footer_content = $request->footer_content;
                $about->save();

                $page =   Page::where(['id'=>$request->id])->first();
                $page->name = $request->name;
                $page->slug = str_slug($request->name, '-');
                $page->meta_title = $request->meta_title;
                $page->meta_desc = $request->meta_desc;
                $page->meta_keywords = $request->meta_keywords;
                $page->is_active = 1;
                $page->is_visible = (isset($request->is_visible))?$request->is_visible:0;
                $page->save();
                return Response::json(['success'=>true,'message' => 'About Us Updated successfully.']);
          
        }catch(Exception $ex){
            return Response::json(['success'=>false,'message' => $ex->getMessage()]);
            print_r($ex->getMessage());die;
        }
    }

    public function editPage($id)
    {
        $page = Page::where(['id'=>$id])->first();
        if($page->slug == 'about-us'){
            $about  = AboutUs::first();
            return view('admin::pages.edit-about-us',['page'=>$page,'about'=>$about]);
        }else{
            return view('admin::pages.edit_page',['page'=>$page]);
        }
        
    }
   

    public function deletePage($id)
    {
       $res =  Page::where('id',$id)->delete();
       if($res){
       return Response::json(['success'=>true,'message' => 'Page Deleted Successfully.']);
       }
    }


    public function changePageStatus(Request $request)
    {

        if(!empty($request->id)){
            $page =   Page::where(['id'=>$request->id])->first();
        }

        $status = 0;

        if($request->status == 1){
            $status = 0;
        }else if($request->status == 0){
            $status = 1;
        }else{
            $status = 0;
        }

        $page->is_active = $status;
        $page->save();

        return Response::json(['success'=>true,'message' => 'Page Status Successfully']);
    }


    // Upload ckeditor image
    public function upload(Request $request)
    {
        // Image upload
        if($request->hasFile('upload')){
            $image = $request->file('upload');
            
            $filename = time().'.'.$image->getClientOriginalExtension();
            
            $destinationPath = public_path('images/page');

            $image->move($destinationPath, $filename);

            $CKEditorFuncNum = $request->get('CKEditorFuncNum');
            $url = url('public/images/page/'.$filename); 
            $msg = 'Image uploaded successfully'; 
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
               
            @header('Content-type: text/html; charset=utf-8'); 
            echo $response;
        }
    }

    
}
