<?php

namespace Modules\Admin\Http\Controllers;

use App\Http\Requests\Admin\AddBlogCategoryRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BlogCategory;
use DataTables;
use Response;
use Auth;
use DB;
use App\Models\BlogPost;
use App\Models\BlogPostCategory;

class BlogCategoryController extends Controller
{
    protected $admin;
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin::blog-category.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddBlogCategoryRequest $request)
    {
        try {
            if (!empty($request->id)) {
                $category =   BlogCategory::where(['id'=>$request->id])->first();
            } else {
                $category =  new BlogCategory();
            }
            $category->category_name        = trim($request->category_name);
            $category->category_description = trim($request->description);
            $category->slug                 = str_slug($request->category_name, '-');
            $category->created_by           = Auth::user()->id;
            $category->save();
            if (!empty($request->id)) {
                return Response::json(
                    [
                        'success'=>true,
                        'message' => 'Blog Category Updated Successfully.'
                    ]
                );
            } else {
                return Response::json(
                    [
                        'success'=>true,
                        'message' => 'Blog Category Added Successfully.'
                    ]
                );
            }
        } catch(Exception $ex) {
            return Response::json(
                [
                    'success'=>false,
                    'message' => $ex->getMessage()
                ]
            );
            print_r($ex->getMessage());die;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = BlogCategory::where(['id' => $id])->first();
        return view('admin::blog-category.edit_category', [ 'category' => $category ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		   $blogIds = [];
		   $blogCategory = BlogPostCategory::where(['blog_category_id'=>$id])->get();
		   foreach($blogCategory as $val){
			   $blogIds[] = $val->blog_post_id;
		   }
		   foreach($blogIds as $id){
	       BlogPost::where(['id'=>$id])->delete();
		   }
           $res =  BlogCategory::where('id', $id)->delete();
		
        if ($res) {
            return Response::json(
                [
                    'success' => true,
                    'message' => 'Blog Category Deleted Successfully.'
                ]
            );
        } else {
            return Response::json(
                [
                    'success' => false,
                    'message' => 'Something was wrong.'
                ]
            );
        }
    }

    public function blogCategoryList(Request $request)
    {
        try{
            if ($request->ajax()) {
                $data = BlogCategory::latest();
                if (!empty($request->get('status'))) {
                    $status = ($request->get('status') == 'active')?1:0;
                    $data->where('is_active',$status);
                }
                $data  = $data->latest()->get();
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                            $editUrl = url('admin/edit-blog-category/'.$row->id);
                            $btn = '
                            <a href="'.$editUrl.'" class="edit btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i></a>
                            <a onclick="deleteBlogCategory('.$row->id.')" href="javascript:void(0)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> </a>';
                            return $btn;
                        })
                    ->addColumn('status', function($row){
                      
                            if ($row->is_active == 0) {
                                $status = '<label class="switch"><input id="row'.$row->id.'" onclick="updateStatus('.$row->id.')" type="checkbox"   class="checked change_user_status" data-status="'.$row->is_active.'" data-table="data-table" data-url="'.url('admin/update-status').'" ><span class="slider round"></span></label>';
                            } else {
                                $status = '<label class="switch"><input id="row'.$row->id.'" onclick="updateStatus('.$row->id.')" type="checkbox" checked  class="checked change_user_status" data-status="'.$row->is_active.'" data-table="data-table" data-url="'.url('admin/update-status').'" ><span class="slider round"></span></label>';

                            }
                            return $status;
                        })
                    ->rawColumns(['action', 'status'])
                    ->make(true);
            }
        } catch(Exception $ex) {
            print_r($ex->getMessage());
            die;
        }
    }

    /**
     *
     *
     */
    public function addBlogCategory(Request $request)
    {
        return view('admin::blog-category.create');
    }

    /**
     *
     *
     */
    public function updateStatus(Request $request)
    {
        try {
            if ($request->status == 0) {
               $res =  BlogCategory::where('id', '=', $request->id)->update(array('is_active' => 1));   
            } else {
                $res =  BlogCategory::where('id', '=', $request->id)->update(array('is_active' => 0));
            }
            if ($res) {
                return Response::json(
                    [
                        'success' => true,
                        'message' => 'Blog category status change successfully.'
                    ]
                );
            } else {
                return Response::json(
                    [
                        'success' => false,
                        'message' => 'Something was wrong.'
                    ]
                );
            }
        } catch(Exception $ex) {
            print_r($ex->getMessage());
            die;
        }
    }
}
