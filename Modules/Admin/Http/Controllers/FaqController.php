<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use App\User;
use App\Models\Faq;
use Validator;
use DB;
use File;
use DataTables;
use Response;
use App\Http\Requests\Admin\AddFaqRequest;

class FaqController extends Controller
{
    protected $per_page = 10;
	public function __construct()
	{
      
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        return view('admin::faq.index');
    }

    public function list(Request $request) {
       
        try {
            if ($request->ajax()) {
                $slider = Faq::latest()->get();

                return Datatables::of($slider)
                    ->addIndexColumn()
                    ->addColumn('status', function($row){
                        if ($row->is_active == 0) {
                            $status = '<label class="switch"><input id="row'.$row->id.'" onclick="updateStatus('.$row->id.')" type="checkbox"   class="checked change_user_status" data-status="'.$row->is_active.'" data-table="data-table" data-url="'.url('admin/update-faq-status').'" ><span class="slider round"></span></label>';
                        } else {
                            $status = '<label class="switch"><input id="row'.$row->id.'" onclick="updateStatus('.$row->id.')" type="checkbox" checked  class="checked change_user_status" data-status="'.$row->is_active.'" data-table="data-table" data-url="'.url('admin/update-faq-status').'" ><span class="slider round"></span></label>';

                        }
                        return $status;
                    })
                    ->addColumn('action', function ($row) {
                        $editUrl = url('admin/faq/edit/' . $row->id);
                        $btn = '<a href="' . $editUrl . '" class="edit btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i></a><a id="rowremove'.$row->id.'" onclick="deleteRow('.$row->id.')" href="javascript:void(0)" class="btn btn-danger btn-sm" data-table="data-table" data-url="'.url('admin/faq/destroy').'" ><i class="fa fa-trash-o"></i></a></div>';

                        return $btn;
                    })
                    ->rawColumns(['action', 'status'])
                    ->make(true);
            }
        } catch (Exception $ex) {
            print_r($ex->getMessage());die;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin::faq.create');
    }

    public function edit($id){
        $faq = Faq::where('id',$id)->first();
        return view('admin::faq.edit',['faq' => $faq]);
    }

    public function store(AddFaqRequest $request)
    {   
        try {
            if(!empty($request->id)){
                $faq = Faq::where('id',$request->id)->first();
            }else{
                $faq = new Faq();
            }
           
            $faq->question = $request->question;
            $faq->answer = $request->answer;
            $faq->save();
            $message = (!empty($request->id))? 'FAQ updated Successfully' : 'FAQ created Successfully.';
            return Response::json(['success'=>true,'message' =>$message]);
        } catch (Exception $ex) {
            return Response::json(['success'=>false,'message' => $ex->getMessage()]);
            print_r($ex->getMessage());die;
        }
    }

 
    public function destroy($id)
    {
        $res =  Faq::where('id', $id)->delete();
        if ($res) {
            return Response::json(['success'=>true,'message' => 'FAQ Deleted Successfully.']);
        }
    }

  public function updateStatus(Request $request)
  {
    $faq = Faq::where(['id'=>$request->id])->first();
    $faq->is_active = ($request->status==1) ? 0 : 1;
    $faq->save();
    return Response::json(['success'=>true,'message' => 'FAQ Changed status Successfully.']);
  }

}
