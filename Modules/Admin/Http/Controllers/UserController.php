<?php

namespace Modules\Admin\Http\Controllers;

use App\Exports\UserExport;
// use Illuminate\Http\Response;
use App\Http\Requests\Admin\AddUserRequest;
use App\Models\UserInRole;
use App\User;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Response;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function user(Request $request)
    {
        return view('admin::user.index');
    }

    public function userList(Request $request)
    {
        try {
            if ($request->ajax()) {
                $data = User::with(['userInRole.role']);
                $data = $data->whereHas('userInRole.role', function ($q) {
                    $q->where('name', '!=', 'admin');
                });

                if (!empty($request->get('date_between'))) {
                    $date = explode('-', $request->get('date_between'));
                    $fromdate = date('Y-m-d', strtotime($date[0]));
                    $todate = date('Y-m-d', strtotime($date[1]));
                    $data->whereDate('created_at', '>=', $fromdate)->whereDate('created_at', '<=', $todate);
                }
                if (!empty($request->get('status'))) {
                    $status = ($request->get('status') == 'active') ? 1 : 0;
                    $data->where('is_active', $status);
                }
				
                $data = $data->latest()->get();
                
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('created_at', function ($row) {
                        return changeDateformat($row->created_at);
                    })
                    ->addColumn('role', function ($row) {

                        return ($row->userInRole) ? $row->userInRole[0]->role['name'] : '-';
                    })
                    ->addColumn('total_orders', function ($row) {
                        $url = url('admin/user-order/?user_id=' . $row->id);
                        return '<a class="btn btn-primary" href="' . $url . '">Orders (' . getUserOrderCount($row->id) . ')</a>';
                    })
                    ->addColumn('transactions', function ($row) {
                        $url = url('admin/user-transaction/?user_id=' . $row->id);
                        return '<a class="btn btn-primary" href="' . $url . '">Transactions</a>';
                    })
                    ->addColumn('user_wallet', function ($row) {
                        return '$' . number_format(getExistingBlanace($row->id),2);
                    })
                    ->addColumn('status', function ($row) {
                        if ($row->is_active == 0) {
                            $status = '<label class="switch"><input id="row' . $row->id . '" onclick="updateStatus(' . $row->id . ')" type="checkbox"   class="checked change_user_status" data-status="' . $row->is_active . '" data-table="data-table" data-url="' . url('admin/update-user-status') . '" ><span class="slider round"></span></label>';
                        } else {
                            $status = '<label class="switch"><input id="row' . $row->id . '" onclick="updateStatus(' . $row->id . ')" type="checkbox" checked  class="checked change_user_status" data-status="' . $row->is_active . '" data-table="data-table" data-url="' . url('admin/update-user-status') . '" ><span class="slider round"></span></label>';

                        }
                        return $status;
                    })
                    ->addColumn('action', function ($row) {
                        $editUrl = url('admin/edit-user/' . $row->id);
                        $btn = '<a href="' . $editUrl . '" class="edit btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i></a>';

                        return $btn;
                    })
                    ->rawColumns(['action', 'status', 'role', 'transactions', 'total_orders'])
                    ->make(true);
            }
        } catch (Exception $ex) {
            print_r($ex->getMessage());die;
        }
    }

    public function addUser()
    {
        return view('admin::user.create');
    }

    public function editUser($id)
    {
        $user = User::where(['id' => $id])->first();
        return view('admin::user.edit-user', ['user' => $user]);
    }

    public function saveUser(AddUserRequest $request)
    {

        $dir = url('public/images/profile_pic');
        if (is_dir($dir) === false) {
            mkdir($dir);
        }
        if (!empty($request->id)) {
            $user = User::where(['id' => $request->id])->first();
        } else {
            $user = new User();
        }
        $password = generateRandomString(8);
        $data = [];
        if ($request->hasFile('profile_pic')) {
            $image = $request->file('profile_pic');
            $name = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/images/profile_pic/');
            $image->move($destinationPath, $name);
            $user->profile_pic = $name;
        }
        $user->email = $request->email;
        $user->fullname = $request->fullname;
        $user->username = $request->username;
        $user->password = Hash::make($password);
        $user->is_verified = 1;
        if (!empty($request->id)) {
            $user->save();
            $userRole = UserInRole::where(['id' => $user->userInRole[0]->id])->first();
            $userRole->user_id = $user->id;
            $userRole->role_id = $request->role_id;
            $userRole->save();
            return Response::json(['success' => true, 'message' => 'User Updated Successfully.']);
        } else {
            if ($user->save()) {
                $userRole = new UserInRole();
                $userRole->user_id = $user->id;
                $userRole->role_id = $request->role_id;
                $userRole->save();
                $data['username'] = $request->username;
                $data['email'] = $request->email;
                $data['password'] = $password;
                $data['subject'] = 'Welcome Mail';
                $data['to_email'] = $request->email;
                sendMail($data, 'createUser');
            }
            return Response::json(['success' => true, 'message' => 'User Added Successfully.']);
        }

    }

    public function changeUserStatus(Request $request)
    {
        $user = User::where(['id' => $request->id])->first();
        $user->is_active = ($request->status == 1) ? 0 : 1;
        if($user->save()){
			$data['username'] = $user->name;
			$data['subject'] = 'Account status mail';
			$data['to_email'] = $user->email;
			$data['status'] = $user->is_active;
			sendMail($data,'user-account-status-mail');
		}
        return Response::json(['success' => true, 'message' => 'User Updated Successfully.']);
    }

    public function export_users(Request $request)
    {
        return Excel::download(new UserExport($request), 'users.xlsx');
    }

    public static function allUsersExportData($request)
    {

        $post = $request->all();
        $data = [];

        $users = User::with(['userInRole.role', 'userInRole'])->latest();
        $users->whereHas('userInRole.role', function ($q) {
            $q->where('name', '!=', 'admin');
        });

        if (!empty($request->get('date'))) {
            $date = explode('-', $request->get('date'));
            $fromdate = date('Y-m-d', strtotime($date[0]));
            $todate = date('Y-m-d', strtotime($date[1]));
            $users->whereDate('created_at', '>=', $fromdate)->whereDate('created_at', '<=', $todate);
        }
        if (!empty($request->get('status'))) {
            $status = ($request->get('status') == 'active') ? 1 : 0;
            $users->where('is_active', $status);
        }
        if (!empty($request->get('search'))) {
            $users->where(function ($query) use ($post, $request) {
                $search = $request->get('search');
                $query->where('fullname', 'LIKE', "%" . $search . "%")
                    ->orWhere('email', 'LIKE', "%" . $search . "%");
            });

        }
        $users = $users->get();

        foreach ($users as $value) {
            $row = [];
            $row['full_name'] = $value['fullname'];
            $row['email'] = $value['email'];
            $row['role'] = (!empty($value->userInRole)) ? $value->userInRole[0]->role['name'] : '-';
            $row['created_at'] = date('Y-m-d', strtotime($value['created_at']));
            $row['status'] = ($value['is_active'] == 1) ? 'active' : 'inactive';
            $data[] = $row;
        }
        return $data;
    }

    public function userOrder(Request $request)
    {
        return view('admin::user.user-order');
    }

    public function userTransaction()
    {
        return view('admin::user.user-transaction');
    }

}
