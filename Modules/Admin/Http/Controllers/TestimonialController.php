<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use App\Common;
use App\User;
use App\Models\Testimonial;
use App\Models\FindingCost;
use App\Models\KeyFeature;
use Validator;
use DB;
use File;
use DataTables;
use Response;
use App\Http\Requests\Admin\AddTestimonialRequest;
use Spatie\Image\Image;

class TestimonialController extends Controller
{
	protected $common_model;
    protected $per_page = 10;
	public function __construct(User $user_model, Common $common_model)
	{
	    $this->common = $common_model;
      
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        return view('admin::testimonial.index');
    }

    public function list(Request $request) {
       
        try {
            if ($request->ajax()) {
                $slider = Testimonial::latest()->get();

                return Datatables::of($slider)
                    ->addIndexColumn()
                    ->addColumn('image', function($row){
                        $editUrl = url('public/images/testimonial/'.$row->image);
                        $slider_img = '
                        <img src="'.$editUrl.'" hight="80" width="80">';
                        return $slider_img;
                    })
                    ->editColumn('rating',function($row){
                        return getratingStar($row->rating);
                    })
                    ->addColumn('status', function($row){
                        if ($row->is_active == 0) {
                            $status = '<label class="switch"><input id="row'.$row->id.'" onclick="updateStatus('.$row->id.')" type="checkbox"   class="checked change_user_status" data-status="'.$row->is_active.'" data-table="data-table" data-url="'.url('admin/update-testimonial-status').'" ><span class="slider round"></span></label>';
                        } else {
                            $status = '<label class="switch"><input id="row'.$row->id.'" onclick="updateStatus('.$row->id.')" type="checkbox" checked  class="checked change_user_status" data-status="'.$row->is_active.'" data-table="data-table" data-url="'.url('admin/update-testimonial-status').'" ><span class="slider round"></span></label>';

                        }
                        return $status;
                    })
                    ->addColumn('action', function ($row) {
                        $editUrl = url('admin/testimonial/edit/' . $row->id);
                        $btn = '<a href="' . $editUrl . '" class="edit btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i></a><a id="rowremove'.$row->id.'" onclick="deleteRow('.$row->id.')" href="javascript:void(0)" class="btn btn-danger btn-sm" data-table="data-table" data-url="'.url('admin/testimonial/destroy').'" ><i class="fa fa-trash-o"></i></a></div>';

                        return $btn;
                    })
                    ->rawColumns(['action', 'image','status','rating'])
                    ->make(true);
            }
        } catch (Exception $ex) {
            print_r($ex->getMessage());die;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin::testimonial.create');
    }

    public function edit($id){
        $testimonial = Testimonial::where('id',$id)->first();
        return view('admin::testimonial.edit',['testimonial'=>$testimonial]);
    }

    public function store(AddTestimonialRequest $request)
    {   
        
        try {

            $dir = url('public/images/testimonial');
            if( is_dir($dir) === false )
            {
                mkdir($dir);
            }

            if(!empty($request->id)){
                $testimonial = Testimonial::where('id',$request->id)->first();
            }else{
                $testimonial = new Testimonial();
            }
            if ($request->hasFile('image')) {
                $image              = $request->file('image');
                $imageUploaded      = $image->getClientOriginalName();
                $destinationPath    = public_path('images/testimonial');
                $image->move($destinationPath, $imageUploaded);
                $testimonial->image = $imageUploaded;
                
                // Image::load($destinationPath.'/'.$image)
                // ->width(88)     
                // ->height(88)     
                // ->optimize()
                // ->save($destinationPath.'/'.$imageUploaded);
            }
            $testimonial->author = $request->author;
            $testimonial->comment = $request->comment;
            $testimonial->rating = $request->rating;
            $testimonial->save();
            $message = (!empty($request->id))?'Testimonial updated Successfully.':'Testimonial created Successfully.';
            return Response::json(['success'=>true,'message' =>$message]);
        } catch (Exception $ex) {
            return Response::json(['success'=>false,'message' => $ex->getMessage()]);
            print_r($ex->getMessage());die;
        }

    }

 
    public function destroy($id)
    {
       
        $res =  Testimonial::where('id', $id)->delete();
        if($res){
        return Response::json(['success'=>true,'message' => 'Testimonial Deleted Successfully.']);
        }
    }

  public function updateStatus(Request $request){
    $user = Testimonial::where(['id'=>$request->id])->first();
    $user->is_active = ($request->status==1)?0:1;
    $user->save();
    return Response::json(['success'=>true,'message' => 'Testiminoal Updated Successfully.']);
  }

}
