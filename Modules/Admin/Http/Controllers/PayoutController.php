<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Session;
use Auth;
use App\Models\UserWallet;
use App\Models\UserPaymentMethod;
use App\PayPal;
use App\User;
use Validator;
use File;
use Illuminate\Support\Facades\Input;
use App\Exports\UserExport;
use Maatwebsite\Excel\Facades\Excel;
use Response;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Api\Paypalpayment;
use DB;
use DataTables;
use App\Models\PayoutTransaction;
use App\Models\WithdrawalRequest;
// use App\Models\



class PayoutController extends Controller
{
	protected $payout_model;
	protected $per_page = 10;
    private $_api_context;

	public function __construct(UserWallet $payout_model)
	{
	    $this->payout = $payout_model;
        $paypal_conf = \Config::get('paypal_api');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::with(['userInRole.role'])->latest();
        $users->whereHas('userInRole.role', function($q)
        {
            $q->where('name','!=', 'admin');    
        });
        $users = $users->get();
       return view('admin::payout.index',['users'=>$users,'user_id'=>($request->user_id)?$request->user_id:'']);
    }

    public function payoutTransaction(Request $request)
    {
        $users = User::with(['userInRole.role'])->latest();
        $users->whereHas('userInRole.role', function($q)
        {
            $q->where('name','!=', 'admin');    
        });
        $users = $users->get();
       return view('admin::payout.payout-transaction',['users'=>$users]);
    }

    public function transactionView($id)
    {
       return view('admin::payout.payout-transaction-view',['user_id'=>$id]);
    }
    

    public function payoutList(Request $request)
    {

        try{
            $userIds = [];
            if(!empty($request->userId)){
                $users = User::where('id', '=', $request->userId)->get();
                foreach($users as $user){
                    $userIds[] = $user['id'];
                }
            }
        
            if ($request->ajax()) {
                        $data = WithdrawalRequest::with(['user.userpaymentMethod'])
						->where(['status'=>'pending','selected_method'=>'paypal'])
						->whereHas('user.userpaymentMethod', function($q){
							$q->where('payment_type', '=', 2);
						})
                        ->orderBy('id', 'desc');
						
                        if(!empty($request->user_id)){
						 $data->where(['user_id'=>$request->user_id]);
						}
                        if(count($userIds)>0){
                            $data = $data->whereIn('user_id',$userIds)->latest()->get();
                        }else{
                            $data = $data->orderBy('id','desc')->latest()->get();
                        }
                return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('usename', function($row){
                                return $row->user->fullname;
                        })
						
                        ->addColumn('checkbox', function($row){
                            $userId = $row->user_id;
                            $btn = '<input id="user_'.$userId.'"  onclick="removeFromArray('.$userId.')" type="checkbox" value="'.$userId.'" class="cb-element" />';
                            
                                return $btn;
                        })
						 ->editColumn('amount', function($row){
                            return '$'.$row->amount;
                         })
                        ->addColumn('preferred_method', function($row){
                            $method = getUserPrefereMethod($row['user_id']);
                            $str='';
                            $url = url('public/images/payment_03.png');
							$str = '<img src="'.$url.'" alt="Paypal" width="80px">';
                            return $str;
                        })
                        ->rawColumns(['checkbox','preferred_method'])
                        ->make(true);
            }
        }catch(Exception $ex){
            print_r($ex->getMessage());die;
        }

    }

    public function payoutTransactionList(Request $request)
    {
        
        $data = PayoutTransaction::with(['reciever'])
        ->whereIn('id', function($query) {
            $query->selectRaw('max(`id`)')
            ->from('payout_transactions')
            ->groupBy('receiver_id');
        })
        ->orderBy('id', 'desc')
        ->latest();
        if (!empty($request->get('userId'))) {
            $data->whereHas('reciever', function($q)use($request)
            {
                $q->where('id','=', $request->userId);    
            });
        }
        if (!empty($request->get('date_between'))) {
            $date = explode('-',$request->get('date_between'));
            $fromdate = date('Y-m-d',strtotime($date[0]));
            $todate = date('Y-m-d',strtotime($date[1]));
            $data->whereDate('created_at', '>=', $fromdate)->whereDate('created_at', '<=', $todate);
        }

        if(!empty($request->payment_method)){
            $data->where('payment_type', '=', $request->payment_method);
        }
        $data = $data->orderBy('id','desc')->get();
       if ($request->ajax()) {
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('usename', function($row){
                        return $row->reciever->fullname;
                     })
                      ->editColumn('created_at', function($row){
                         	    return changeDateformat($row->created_at);
                      })
					 ->editColumn('amount', function($row){
                            return '$'.number_format($row->amount,2);
                         })
                    ->editColumn('payment_type', function($row){
                        if($row->payment_type == 'stripe'){ 
                            $url = url('public/images/payment_07.png');
                            $str = '<img src="'.$url.'" alt="Stripe" width="80px">';
                        }else{
                            $url = url('public/images/payment_03.png');
                            $str = '<img src="'.$url.'" alt="Paypal" width="80px">';
                        }
                        return $str;
                    })
                    ->addColumn('action', function($row){
                        $viewUrl = url('admin/payout/transaction/'.$row->receiver_id);
                        $btn = '<a href="'.$viewUrl.'" class="edit btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';
                         
    
                            return $btn;
                    })
                    ->rawColumns(['action','payment_type'])
                    ->make(true);
            }
}

    public function payoutUserTransactionList(Request $request)
    {
        $transations = PayoutTransaction::where(['receiver_id'=>$request->user_id])->orderBy('id','desc')->get();
        if ($request->ajax()) {
            return Datatables::of($transations)
                    ->addIndexColumn()
                    ->addColumn('usename', function($row){
                        return $row->reciever->fullname;
                     })
                     ->editColumn('created_at', function($row){
                        return date('y-m-d',strtotime($row->created_at));
                     })
					 ->editColumn('amount', function($row){
                            return '$'.$row->amount;
                         })
                    ->editColumn('payment_type', function($row){
                        if($row->payment_type == 'stripe'){ 
                            $url = url('public/images/payment_03.png');
                            $str = '<img src="'.$url.'" alt="Stripe" width="80px">';
                        }else{
                            $url = url('public/images/payment_07.png');
                            $str = '<img src="'.$url.'" alt="Paypal" width="80px">';
                        }
                        return $str;
                    })
                    ->rawColumns(['payment_type'])
                    ->make(true);
            }
    }

    public function arrayPaginator($array, $request)
    {
        $page = Input::get('page', 1);
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;
        return new \Illuminate\Pagination\LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
            ['path' => $request->url(), 'query' => $request->query()]);
    }

    public function sendPayOut(Request $request)
    {

        try {
            $post  = $request->all();
            if($post['type']=='all'){
                $customers = UserWallet::groupBy('user_id')->get();
            }else{
                $customers = UserWallet::whereIn('user_id',$post['ids'])->groupBy('user_id')->get();  
            }
            foreach($customers as $value){
                $amount = getPayPalWithDrawalBlanace($value->user_id);
                $method =(array) DB::table('user_payment_methods')->where('user_id', $value['user_id'])->first();
                if(!empty($method) && $method['payment_type']==2){
                    $user = User::where(['id'=>$value['user_id']])->first();
                    $respnse =  self::payPalPayout('anay.sequester-buyer@gmail.com',$amount,$value->user_id);
                }else{
                    //insert data into db for those users who has not added payment method
                    $inputs  = array(
                        'user_id' => $value->user_id,
                        'type' => 'payment',
                        'created_at'=>date('Y-m-d H:i:s'),
                    );
                    $this->common->storeData('mail_queue', $inputs); 
                    $summary = "You haven't add the payment type in your settings...Go to settings and add the payment method type so that we can credit your amount";
                    sendNotification($summary, 'payment mail', 1, $value->user_id);
                    $respnse = json_encode(['success'=>true,'message'=>'success all payout transaction.']);
                }
                $respnse = (array)json_decode($respnse);
                if(!$respnse['success']){
                    return json_encode($respnse);
                }
            }
            return json_encode(['success'=>true,'message'=>'success all payout transaction.']);
        } catch (Exception $ex) {
            return json_encode(['success'=>false,'message'=>$ex->getMessage()]);
            echo "<pre>";
            print_r($ex->getMessage());die;
        }
     
    }

   // paypal payout 
    public static function payPalPayout($emailId="anay.sequester-buyer@gmail.com",$amount=1.0,$userId)
    {

        try {
            $payouts = new \PayPal\Api\Payout();
            $senderBatchHeader = new \PayPal\Api\PayoutSenderBatchHeader();
            $senderBatchHeader->setSenderBatchId(uniqid().microtime(true))
                ->setEmailSubject("You have a payment");
                    $senderItem = new \PayPal\Api\PayoutItem();
                    $senderItem->setRecipientType('Email')
                        ->setNote('Thanks you.')
                        ->setReceiver($emailId)
                        ->setSenderItemId("item_1" . uniqid().microtime('true'))
                        ->setAmount(new \PayPal\Api\Currency('{
                    "value":"'.$amount.'",
                    "currency":"USD"
                }'));
                    $payouts->setSenderBatchHeader($senderBatchHeader)->addItem($senderItem);
            $request = clone $payouts;
            $apiContext = new \PayPal\Rest\ApiContext(
                new \PayPal\Auth\OAuthTokenCredential(
                    env('PAYPAL_CLIENT_ID'),
                    env('PAYPAL_SECRET')
                )
            );
           $output = $payouts->create(array('sync_mode' => 'false'), $apiContext);
           if(!empty($output)){
               
                $payoutObj =  new UserWallet();
                $payoutObj->site_transaction_id = $output->batch_header->payout_batch_id;
                $payoutObj->user_id = $userId;
                $payoutObj->credit = 0;
                $payoutObj->debit = 0;
                $payoutObj->current_balance  = getExistingBlanace($userId)-$amount;
                $payoutObj->is_withdrawal = 1;
                if($payoutObj->save()){
					$request = WithdrawalRequest::where(['user_id'=>$userId ,'status'=>'pending','selected_method'=>'paypal'])->first();
					$request->status = 'complete';
					$request->save();
				}
                $data = [];
                // $output->batch_header->batch_status;
                $payoutTransacrtion = new PayoutTransaction();
                $data['receiver_id'] = $userId;
                $data['sender_id'] = Auth::user()->id;
                $data['transaction_id'] = $output->batch_header->payout_batch_id;
                $data['amount'] = $amount;
                $data['payment_type'] = 'paypal';
                $data['status'] = "SUCCESS";
                $payoutTransacrtion->create($data);

           }
           return json_encode(['success'=>true,'message'=>'','data'=>$output]);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            return json_encode(['success'=>false,'message'=>$ex->getMessage()]);
        }catch (\Exception $ex){
            return json_encode(['success'=>false,'message'=>$ex->getMessage()]);
        }
   
           
    }

// stripe payout amount transfer
    public static function stripePayout($accounId='', $amount=1, $userId)
    {
       
        try {
    
            $res =   \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $transResponse =  \Stripe\Transfer::create([
            'amount' => $amount*100,
            'currency' => 'usd',
            'destination' => $accounId
            ]);
          
            if(!empty($transResponse)){
                $payoutObj =  new UserWallet();
                $payoutObj->site_transaction_id = $transResponse['id'];
                $payoutObj->user_id = $userId;
                $payoutObj->credit = 0;
                $payoutObj->debit = $amount;
                $payoutObj->current_balance  = 0;
                $payoutObj->is_withdrawal = 1;
                $payoutObj->save();

                $data = [];
                $payoutTransacrtion = new PayoutTransaction();
                $data['receiver_id'] = $userId;
                $data['sender_id'] = Auth::user()->id;
                $data['transaction_id'] = $transResponse['id'];
                $data['amount'] = $transResponse['amount']/100;
                $data['payment_type'] = 'stripe';
                $data['status'] = 'SUCCESS';
                $payoutTransacrtion->create($data);
            }
            return json_encode(['success'=>true,'message'=>'','data'=>$transResponse]);
          } catch(\Stripe\Exception\CardException $ex) {
            return json_encode(['success'=>false,'message'=>$ex->getMessage()]);
          } catch (\Stripe\Exception\RateLimitException $ex) {
            return json_encode(['success'=>false,'message'=>$ex->getMessage()]);
          } catch (\Stripe\Exception\InvalidRequestException $ex) {
            return json_encode(['success'=>false,'message'=>$ex->getMessage()]);
          } catch (\Stripe\Exception\AuthenticationException $ex) {
            return json_encode(['success'=>false,'message'=>$ex->getMessage()]);
          } catch (\Stripe\Exception\ApiConnectionException $ex) {
            return json_encode(['success'=>false,'message'=>$ex->getMessage()]);
          } catch (\Stripe\Exception\ApiErrorException $ex) {
            return json_encode(['success'=>false,'message'=>$ex->getMessage()]);
          } catch (Exception $ex) {
            return json_encode(['success'=>false,'message'=>$ex->getMessage()]);
          }
 
       
    }

    public function getTotalBalance(){
        try {
            $data = [];
            // stripe balance
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
			$stripeBalance = \Stripe\Balance::retrieve();
			$data['stripe_balance'] = ($stripeBalance->available[0]->amount/100);
			// paypal balance
            $paypal = new Paypal(env('PAYPAL_SANDBOX_API_USERNAME'),env('PAYPAL_SANDBOX_API_PASSWORD'),env('PAYPAL_SANDBOX_API_SECRET'));
            $paypalBalance = $paypal->call('GetBalance');
            $data['paypal_balance'] = $paypalBalance['L_AMT0'];
		    return json_encode(['success'=>true,'message'=>'','data'=>$data]);
        } catch (Exception $ex) {
            return json_encode(['success'=>false,'message'=>$ex->getMessage()]);
        }
    }


    public function getTransferringAmount(Request $request){

        try {
            return json_encode(['success'=>true,'message'=>'','data'=>getPayPalTransferringAmount($request)]);
        } catch (Exception $ex) {
            return json_encode(['success'=>false,'message'=>$ex->getMessage()]);
        }
    }

    public function checkPayPalAccount(Request $request){

        $paypal_email='anay.sequester-buyer@gmail.com';
        $paypal_username='';
        $paypal_password=$email_pay->password;
        $paypal_appid=$email_pay->appid;
        $paypal_signature=$email_pay->signature;
        $mode=0;
        if($mode==0)
        {
            $API_Endpoint = "https://svcs.sandbox.paypal.com/AdaptiveAccounts/GetVerifiedStatus";
        }
        else
        {
            $API_Endpoint = "https://svcs.paypal.com/AdaptiveAccounts/GetVerifiedStatus";
        }

        $ret['error_new']='';   
        $payLoad["emailAddress"]=$_POST['paypal_email'];
        $payLoad["matchCriteria"]="NONE";       $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER,  array(
            'X-PAYPAL-REQUEST-DATA-FORMAT: JSON',
            'X-PAYPAL-RESPONSE-DATA-FORMAT: JSON',
            'X-PAYPAL-SECURITY-USERID: '. $paypal_username,
            'X-PAYPAL-SECURITY-PASSWORD: '. $paypal_password,
            'X-PAYPAL-SECURITY-SIGNATURE: '. $paypal_signature,
            'X-PAYPAL-APPLICATION-ID: '. $paypal_appid
        ));  
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payLoad));
        $response = curl_exec($ch);
        $response = json_decode($response, 1); 
        if(empty($response['error'])){
            echo "Account is valid";die;
        }
        else{
            $ret['error_new']='Please enter valid paypal email id.';    
        }

       
        
        echo json_encode($ret);
    }



    
}
