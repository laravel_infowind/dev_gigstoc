<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Mail\ForgetPassword;
use Response;
use App\User;
use DataTables;
use Validator;
use Redirect;
use Mail;
use Illuminate\Support\Facades\Hash ;
use Illuminate\Support\Str;
use DB;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UserExport;
use App\Models\Tag;
use App\Http\Requests\Admin\AddTagRequest;


class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    

    public function index(Request $request){
        return view('admin::tag.index');
    }

    public function tagList(Request $request)
    {
        try{
            if ($request->ajax()) {
                    $data = Tag::latest()->get();
                   
                return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function($row){
                            $editUrl = url('admin/edit-tag/'.$row->id);
                            $btn = '<a href="'.$editUrl.'" class="edit btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i></a>';
        
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
            }
        }catch(Exception $ex){
            print_r($ex->getMessage());die;
        }
    }

    public function addTag(){
        return view('admin::tag.create');
    }

    public function editTag($id){
        $tag = Tag::where(['id'=>$id])->first();
        return view('admin::tag.edit',['tag'=>$tag]);
    }

    public function saveTag(AddTagRequest $request){
        try{
         
                if(!empty($request->id)){
                    $tag =   Tag::where(['id'=>$request->id])->first();
                }else{
                    $tag =  new Tag();
                }
                $tag->name = $request->name;
                $tag->save();
                if(!empty($request->id)){
                    return Response::json(['success'=>true,'message' => 'Tag Updated Successfully.']);
                }else{
                    return Response::json(['success'=>true,'message' => 'Tag Added Successfully.']);
                }
          
        }catch(Exception $ex){
            return Response::json(['success'=>false,'message' => $ex->getMessage()]);
            print_r($ex->getMessage());die;
        }
    }
}
