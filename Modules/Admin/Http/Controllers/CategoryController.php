<?php

namespace Modules\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AddCategoryRequest;
use App\Models\Category;
use Auth;
use DataTables;
use File;
use Illuminate\Http\Request;
use Response;

class CategoryController extends Controller
{
    protected $admin;
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ConfigSetting::all();
        $settings = [];
        foreach ($data as $key => $value) {

            $settings[$value->meta_key] = $value->meta_value;
        }

        $settings = (object) $settings;
        return view('admin::settings.index', compact('settings'));
    }

    public function category(Request $request)
    {
        return view('admin::category.index');
    }

    public function categoryList(Request $request)
    {
        try {
            if ($request->ajax()) {

                        $data = Category::with('categories');
                        if (!empty($request->get('parent_category'))) {
                            $data->where('category_id',$request->get('parent_category'))->orWhere('id',$request->get('parent_category'));
                        }
                        if (!empty($request->get('status'))) {
                            $status = ($request->get('status')=='active')?1:0;
                            $data->where('is_active',$status);
                        }
                        $data  = $data->latest()->get();
                    
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) {
                        $editUrl = url('admin/edit-category/' . $row->id);
                        $btn = '<a href="' . $editUrl . '" class="edit btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i></a>';
                        // <a id="row'.$row->id.'" onclick="deleteRow('.$row->id.')" href="javascript:void(0)" class="btn btn-danger btn-xs" data-table="category_table" data-url="'.url('admin/delete-category').'" ><i class="fa fa-trash-o"></i></a></div>';

                        return $btn;
                    })
                    ->addColumn('status', function ($row) {
                        if ($row->is_active == 0) {
                            $status = '<label class="switch"><input id="row' . $row->id . '" onclick="updateStatus(' . $row->id . ')" type="checkbox"   class="checked change_user_status" data-status="' . $row->is_active . '" data-table="data-table" data-url="' . url('admin/update-category-status') . '" ><span class="slider round"></span></label>';
                        } else {
                            $status = '<label class="switch"><input id="row' . $row->id . '" onclick="updateStatus(' . $row->id . ')" type="checkbox" checked  class="checked change_user_status" data-status="' . $row->is_active . '" data-table="data-table" data-url="' . url('admin/update-category-status') . '" ><span class="slider round"></span></label>';

                        }
                        return $status;
                    })
                    ->addColumn('parent_category', function ($row) {
                        return $parentCatery = getParentCategoryName($row->category_id);
                    })
                    ->rawColumns(['action', 'status'])
                    ->make(true);
            }
        } catch (Exception $ex) {
            print_r($ex->getMessage());die;
        }
    }

    public function addCategory(Request $request)
    {
        return view('admin::category.create');
    }
    public function saveCategory(AddCategoryRequest $request)
    {
        try {
            $dir = url('public/images/category');
            if (is_dir($dir) === false) {
                mkdir($dir);
            }
            if (!empty($request->id)) {
                $category = Category::where(['id' => $request->id])->first();
            } else {
                $category = new Category();
            }

            if ($request->hasFile('cat_image')) {
                $image = $request->file('cat_image');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/images/category/');
                $image->move($destinationPath, $name);
                $category->cat_image = $name;
            }
            $category->name = $request->name;
            $category->description = $request->description;
            $category->slug = str_slug($request->name, '-');
            if (!empty($request->id) && $request->parent == 0) {
                $category->category_id = null;
            } else {
                $category->category_id = (isset($request->category_id)) ? $request->category_id : null;
            }
            $category->created_by = Auth::user()->id;
            $category->is_featured = (isset($request->is_featured)) ? $request->is_featured : 0;
            $category->visible_in_menu = (isset($request->visible_in_menu)) ? $request->visible_in_menu : 0;
            $category->save();
            if (!empty($request->id)) {
                return Response::json(['success' => true, 'message' => 'Category Updated Successfully.']);
            } else {
                return Response::json(['success' => true, 'message' => 'Category Added Successfully.']);
            }

        } catch (Exception $ex) {
            return Response::json(['success' => false, 'message' => $ex->getMessage()]);
            print_r($ex->getMessage());die;
        }
    }

    public function getParentCategory(Request $request)
    {

        $list = [];
        $categorys = Category::where('category_id', '=', null)->get();
        $list[] = "<option value=''>Select Category</option>";
        foreach ($categorys as $cat) {
            $option = "<option value='" . $cat->id . "' >$cat->name</option>";
            if (!empty($request->id) && $cat->id == $request->id) {
                $option = "<option value='" . $cat->id . "' selected='selected'>$cat->name</option>";
            }
            $list[] = $option;
        }
        return Response::json(['success' => true, 'message' => '', 'list' => $list]);

    }

    public function editCategory($id)
    {
        $category = Category::where(['id' => $id])->first();
        return view('admin::category.edit_category', ['category' => $category]);
    }

    public function deleteCategory($id)
    {
        $res = Category::where('id', $id)->delete();
        if ($res) {
            return Response::json(['success' => true, 'message' => 'Category Deleted Successfully.']);
        }
    }

    public function changeStatus(Request $request)
    {
        $user = Category::where(['id' => $request->id])->first();
        $user->is_active = ($request->status == 1) ? 0 : 1;
        $user->save();
        return Response::json(['success' => true, 'message' => 'User Updated Successfully.']);
    }

}
