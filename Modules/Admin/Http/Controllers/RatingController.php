<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Response;
use App\Models\SellerRating;
use App\Models\ServiceRating;
use App\Models\Service;
use DataTables;
use Validator;
use Redirect;
use Mail;
use Illuminate\Support\Facades\Hash ;
use Illuminate\Support\Str;
use DB;
use App\User;


class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    

    public function index(Request $request){
        $users = User::with(['userInRole.role'])->latest();
        $users->whereHas('userInRole.role', function($q)
        {
            $q->where('name','!=', 'admin');    
        });
        $users = $users->get();
        return view('admin::rating.index',['users'=>$users]);
    }

    public function sellerRatingList(Request $request)
    {
        try{
            if ($request->ajax()) {
                        $data = SellerRating::with(['fromUser','toUser']);
                       
                        if (!empty($request->get('userId'))) {
                            $userId = explode('-',$request->get('userId'));
                            $data->where(function($q)use($userId)
                            {
                                $q->where('from_id','=', $userId)->orWhere('to_id','=', $userId);    
                            });
                        }
                        if (!empty($request->get('status'))) {
                            $status = ($request->get('status')=='publish')?1:0;
                            $data->where('is_published',$status);
                        }
                        $data = $data->latest()->get();
                    
                return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('fromUser', function($row){
                           
                            return ($row->fromUser)?$row->fromUser->fullname:'-';
                        })
                       ->addColumn('toUser', function($row){
                        return ($row->toUser)?$row->toUser->fullname :'-';
                        })
                        ->addColumn('created_at', function($row){
                           return changeDateformat($row->created_at);
                        })
                        ->addColumn('rating', function($row){
                            return getratingStar($row->rating);
                        })
                        ->addColumn('status', function($row){
                            if ($row->is_published == 0) {
                                $status = '<label class="switch"><input id="row'.$row->id.'" onclick="updateStatus('.$row->id.')" type="checkbox"   class="checked change_user_status" data-status="'.$row->is_published.'" data-table="data-table" data-url="'.url('admin/update-rating-status').'" ><span class="slider round"></span></label>';
                            } else {
                                $status = '<label class="switch"><input id="row'.$row->id.'" onclick="updateStatus('.$row->id.')" type="checkbox" checked  class="checked change_user_status" data-status="'.$row->is_published.'" data-table="data-table" data-url="'.url('admin/update-rating-status').'" ><span class="slider round"></span></label>';

                            }
                            return $status;
                        })
                        ->rawColumns(['status','rating'])
                        ->make(true);
            }
        }catch(Exception $ex){
            print_r($ex->getMessage());die;
        }
    }

    public function updateStatus(Request $request){
        $user = SellerRating::where(['id'=>$request->id])->first();
        $user->is_published = ($request->status==1)?0:1;
        $user->save();
        return Response::json(['success'=>true,'message' => 'Publish Status Updated Successfully.']);
    }

    public function service(){

        $users = User::with(['userInRole.role'])->latest();
        $users->whereHas('userInRole.role', function($q)
        {
            $q->where('name','!=', 'admin');    
        });
        $users = $users->get();
        $services = Service::get();
        return view('admin::rating.service',['users'=>$users,'services'=>$services]);  

    }

    public function serviceRatingList(Request $request)
    {
        try{
            if ($request->ajax()) {
                        $data = ServiceRating::with(['service']);
                        if (!empty($request->get('userId'))) {
                            $userId = explode('-',$request->get('userId'));
                            $data->where(function($q)use($userId)
                            {
                                $q->where('user_id','=', $userId);    
                            });
                        }
                        if (!empty($request->get('service'))) {
                            $data->whereHas('service', function($q) use($request)
                            {
                                $q->where('id','=', $request->get('service'));    
                            });
                        }
                        if (!empty($request->get('status'))) {
                            $status = ($request->get('status')=='publish')?1:0;
                            $data->where('is_published',$status);
                        }
                        $data = $data->latest()->get();
                    
                   return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('service', function($row){
                            return ($row->service)?$row->service->title:'-';
                        })
                       ->addColumn('user', function($row){
                        return ($row->user)?$row->user->fullname :'-';
                        })
                        ->addColumn('created_at', function($row){
                            return changeDateformat($row->created_at);
                        })
                        ->addColumn('status', function($row){
                            if ($row->is_published == 0) {
                                $status = '<label class="switch"><input id="row'.$row->id.'" onclick="updateStatus('.$row->id.')" type="checkbox"   class="checked change_user_status" data-status="'.$row->is_published.'" data-table="data-table" data-url="'.url('admin/update-service-status').'" ><span class="slider round"></span></label>';
                            } else {
                                $status = '<label class="switch"><input id="row'.$row->id.'" onclick="updateStatus('.$row->id.')" type="checkbox" checked  class="checked change_user_status" data-status="'.$row->is_published.'" data-table="data-table" data-url="'.url('admin/update-service-status').'" ><span class="slider round"></span></label>';

                            }
                            return $status;
                        })
                        ->addColumn('rating', function($row){
                            return getratingStar($row->rating);
                        })
                        ->rawColumns(['status','rating'])
                        ->make(true);
            }
        }catch(Exception $ex){
            print_r($ex->getMessage());die;
        }
     
    }

    public function updatServiceStatus(Request $request){
        $user = ServiceRating::where(['id'=>$request->id])->first();
        $user->is_published = ($request->status==1)?0:1;
        $user->save();
        return Response::json(['success'=>true,'message' => 'Publish Status Updated Successfully.']);
    }

}
