<?php

namespace Modules\Admin\Http\Controllers;

use App\Models\JobRequest;
use App\User;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Response;
use App\Models\Category;
use App\Models\Tag;
use App\Models\TagInJobRequest;
use App\Http\Requests\Admin\EditJobRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\JobsExport;
use App\Models\OffersOnJob;


class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function index(Request $request)
    {
        return view('admin::jobs.index');
    }

    function list(Request $request) {
        
        try {
            if ($request->ajax()) {
                $data = JobRequest::with(['category', 'user'])->where('is_custom','=',0);
             
                if (!empty($request->get('category_id'))) {
                    $data->where('category_id', '=',  $request->get('category_id'));
                }

                if (!empty($request->get('date_between'))) {
                    $date = explode('-', $request->get('date_between'));
                    $fromdate = date('Y-m-d', strtotime($date[0]));
                    $todate = date('Y-m-d', strtotime($date[1]));
                    $data->whereBetween('created_at', [$fromdate, $todate]);
                }

                if (!empty($request->get('status'))) {
                    $status = ($request->get('status') == 'active') ? 1 : 0;
                    $data->where('is_active', $status);
                }
                $data = $data->latest()->get();

                return Datatables::of($data)
                    ->addIndexColumn()
                    ->editColumn('created_at', function ($row) {
                        return changeDateformat($row->created_at);
                    })
                    ->editColumn('price', function ($row) {
                        return '$' . $row->budget;
                    })
                    ->addColumn('category', function ($row) {
                        return ($row->category) ? ucwords($row->category->name) : '';
                    })
                    ->addColumn('created_by', function ($row) {

                        return ($row->user) ? ucwords($row->user->fullname) : '';
                    })
					->addColumn('offer', function ($row) {
                        $url = url('admin/jobs/offer/' . $row->id);
                        return '<a class="btn btn-primary" href="' . $url . '">Offers (' . getTotalOffers($row->id) . ')</a>';
                    })
                    ->addColumn('status', function($row){
                        if ($row->is_active == 0) {
                            $status = '<label class="switch"><input id="row'.$row->id.'" onclick="updateStatus('.$row->id.')" type="checkbox"   class="checked change_user_status" data-status="'.$row->is_active.'" data-table="data-table" data-url="'.url('admin/job/status').'" ><span class="slider round"></span></label>';
                        } else {
                            $status = '<label class="switch"><input id="row'.$row->id.'" onclick="updateStatus('.$row->id.')" type="checkbox" checked  class="checked change_user_status" data-status="'.$row->is_active.'" data-table="data-table" data-url="'.url('admin/job/status').'" ><span class="slider round"></span></label>';

                        }
                        return $status;
                    })
                    ->addColumn('action', function ($row) {
                        $editUrl = url('admin/job/edit/' . $row->id);
                        $btn = '<a href="' . $editUrl . '" class="edit btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i></a>';

                        return $btn;
                    })
                    ->rawColumns(['action', 'status', 'category', 'created_by','offer'])
                    ->make(true);
            }
        } catch (Exception $ex) {
            print_r($ex->getMessage());die;
        }
    }

    public function edit($id)
    {
        $job = JobRequest::with(['tags'])->where('id', $id)->first();
        $categories = Category::where('is_active', '=', 1)->get();
        $tags = Tag::all();
        
        return view('admin::jobs.edit', compact('job', 'categories', 'tags'));
    }
   

    public function changeStatus(Request $request)
    {
        $user = JobRequest::where(['id' => $request->id])->first();
        $user->is_active = ($request->status == 1) ? 0 : 1;
        $user->save();
        return Response::json(['success' => true, 'message' => 'User Updated Successfully.']);
    }

    public function getCategories(Request $request)
    {
        $list = [];
        $categorys = Category::where('is_active', '=', 1)->get();
        $list[] = "<option value=''>Select Category</option>";
        foreach ($categorys as $cat) {
            $option = "<option value='" . $cat->id . "' >$cat->name</option>";
            if (!empty($request->id) && $cat->id == $request->id) {
                $option = "<option value='" . $cat->id . "' selected='selected'>$cat->name</option>";
            }
            $list[] = $option;
        }
        return Response::json(['success' => true, 'message' => '', 'list' => $list]);
    }

    public function update(EditJobRequest $request)
    {

        try {
                $jobRequest = JobRequest::where(['id'=>$request->id])->first();
                $jobRequest->title = $request->title;
                $jobRequest->budget = $request->budget;
                $jobRequest->deadline = $request->deadline;
                $jobRequest->category_id = $request->category_id;
                $jobRequest->description = $request->description;
                if($jobRequest->save())
                {
                    $deResponse = TagInJobRequest::where(['job_request_id'=>$request->id])->delete();
                    if(!empty($request->tag_id)){
                    foreach($request->tag_id as $tagId) {
                        $tagJob = new TagInJobRequest();
                        $tagJob->job_request_id = $request->id;
                        $tagJob->tag_id = $tagId;
                        $tagJob->save();
                    }
                   }
                }
                return Response::json(['success' => true, 'message' => 'Job Updated Successfully.']);
            } catch (Exception $ex) {
                return Response::json(['success' => false, 'message' => $ex->getMessage()]);
            }
    }
	public function exportJobs(Request $request)
	{
	 return Excel::download(new JobsExport($request), 'jobs.xlsx');
    }
	
	public static function allJobsExportData($request)
	{
		$post = $request->all();
        $data = [];

		
            $jobs = JobRequest::with(['user','category'])->where('is_custom','=',0)->latest();
			
            if (!empty($request->get('date'))) {
                $date = explode('-',$request->get('date'));
                $fromdate = date('Y-m-d',strtotime($date[0]));
                $todate = date('Y-m-d',strtotime($date[1]));
                $orders->whereDate('created_at', '>=', $fromdate)->whereDate('created_at', '<=', $todate);
            }

            if (!empty($request->get('userId'))) {
				 $user = $request->get('userId');
			     $orders->where('user_id', '=', $user);
            }
			if (!empty($request->get('payment_type'))) {
				 $payment_type = $request->get('payment_type');
			     $orders->where('payment_type', '=', $payment_type);
            }
			
			if (!empty($request->get('search'))) {
				 $search = $request->get('search');
				 $search =str_replace('#','',$search);
				 $orders->where(function($query) use ($search,$request) {
                        $query->where('order_id', 'LIKE', "%".$search ."%")
                        ->orWhere('transaction_id', 'LIKE', "%".$search ."%");
                });
		    }
			if(!empty($request->get('user_id'))){
				 $orders->where('user_id',$request->user_id);	
			}
            $jobs = $jobs->orderBy('id','desc')->get();
			
            foreach($jobs as $value){
                $row = [];
                $row['title'] = $value->title;
                $row['budget'] = '$'.$value->budget;
                $row['deadline'] = $value->deadline.'(days)';
				$row['category'] =  $value->category->name;
				$row['created_by'] =  $value->user->name;
                $row['created_date'] = changeDateformat($value->created_at);
                $data[] = $row;
            }
            return $data;
	}
}


