<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Requests\Admin\AdminLoginPostRequest;
use Illuminate\Support\Facades\Auth;
use App\Mail\ForgetPassword;
use Response;
use App\User;
use DataTables;
use Validator;
use Redirect;
use Mail;
use App\Http\Requests\Admin\ChangePasswordRequest;
use App\Http\Requests\Admin\ForgetPasswordRequest;
use App\Http\Requests\Admin\ForGotPasswordRequest;
use App\Http\Requests\Admin\ResetPasswordRequest;
use App\Http\Requests\Admin\ProfileRequest;
use Illuminate\Support\Facades\Hash ;
use App\Models\SiteTransaction;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\TransactionExport;
use App\PayPal;
use Stripe\Stripe;
use Stripe\Balance;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::index');
    }

    public function dashboard()
    {
	    \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $stripeBalance = \Stripe\Balance::retrieve();
        $data['stripe_balance'] = ($stripeBalance->available[0]->amount/100);
        // paypal balance
        $paypal = new Paypal(env('PAYPAL_SANDBOX_API_USERNAME'),env('PAYPAL_SANDBOX_API_PASSWORD'),env('PAYPAL_SANDBOX_API_SECRET'));
        $paypalBalance = $paypal->call('GetBalance');
        $data['paypal_balance'] = $paypalBalance['L_AMT0'];

        $paypalTotal = $data['paypal_balance'];
       
        $transferrinfAmount = getTransferringAmount(['type'=>'all']);

        $paypalTransfer = $transferrinfAmount['paypal_amount'];

        $adminTotalEarning = getAdminTotalEraning();
        $adminCurrentYearEarning = adminCurrentYearEarning();
		
        $getCurrentYearEarning = getCurrentYearEarning();
		return view('admin::dashboard.index',compact('paypalTotal','paypalTransfer','adminTotalEarning','adminCurrentYearEarning'));
    }

    public function login(AdminLoginPostRequest $request)
    {
        
        $user = User::where(['email'=>trim($request->email)])->first();
        $credentials = ['email'=>trim($request->email),'password'=>$request->password];
        $remember_me = $request->get('remember')==1 ? true : false;
        if (!empty($user) && ($user->userInRole[0]->role->name=='admin') && Auth::attempt($credentials)) {
            if ($remember_me) {
                setcookie("email", "$request->email", time()+7 * 24 * 60 * 60);  
                setcookie("password", "$request->password", time()+7 * 24 * 60 * 60);
            }else{
                setcookie("email", " ");  
                setcookie("password", " "); 
            }
            return Response::json(['success'=>true,'message' => 'Login successfully.']);
        }
        return Response::json(['success'=>false,'message' => 'Invalid Credential.']);
    }

    public function logout()
    {
        Auth::logout();
        \Session::flush();
        return redirect('/admin/login');
    }

    public function user(Request $request){
        return view('admin::user.index');
    }
	
	public function forgotForm(){
		return view('admin::forget-password');
	}
    public function userList(Request $request)
    {
        try{
            if ($request->ajax()) {
                        $data = User::with(['userInRole.role'])->latest();
                        $data->whereHas('userInRole.role', function($q)
                        {
                            $q->where('name','!=', 'admin');    
                        });
                        $data = $data->get();
                    
                return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function($row){
                            $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">View</a>';
        
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
            }
        }catch(Exception $ex){
            print_r($ex->getMessage());die;
        }
    }

    public function changePassword() {
        return view('admin::change-password');
    }

    public function getResetPassword(){
        return view('admin::reset-password');
    }

    public function updatePassword(ChangePasswordRequest $request){

        try{
            $admin =  User::where(['id'=>Auth::user()->id])->first();
            if ($admin) {
                $admin->password = Hash::make($request->new_password);
                $admin->save();
                return Response::json(['success'=>true,'message' => 'Password changed successfully.']);
            }
        }catch(Exception $ex){
            return Response::json(['success'=>false,'message' => $ex->getMessage()]);
            print_r($ex->getMessage());die;
        }
    }

    public function editProfile(){
        return view('admin::edit-profile',['user_detail'=>Auth::user()]);
       
    }

    public function updateProfile(ProfileRequest $request){
        
        try{
            $dir = url('public/images/profile_pic');
            if( is_dir($dir) === false )
            {
                mkdir($dir);
            }
            $admin =  User::where(['id'=>Auth::user()->id])->first();
            if ($request->hasFile('profile_pic')) {
                $image = $request->file('profile_pic');
                $name = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/images/profile_pic/');
                $image->move($destinationPath, $name);
                $admin->profile_pic = $name;
              }
            if ($admin) {
                $admin->fullname = $request->fullname;
                $admin->save();
                return Response::json(['success'=>true,'message' => 'Profile Updated Successfully.']);
            }
        }catch(Exception $ex){
            return Response::json(['success'=>false,'message' => $ex->getMessage()]);
            print_r($ex->getMessage());die;
        }
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     *
     *
     */
    public function forgetPassword(ForGotPasswordRequest $request)
    {
       
        try {
            $requestUser = User::where('email', $request->email)->first();
            if (!empty($requestUser) && ($requestUser->userInRole[0]->role->name=='admin')) {
                $otp = str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT);
                $requestUser->otp = $otp;
                $requestUser->save();
                Mail::to($requestUser['email'])->send(new ForgetPassword($requestUser));
                return Response::json(['success'=>true,'message' => 'Please Check your email and reset password.']);
               
            } else {
                return Response::json(['success'=>false,'message' => 'Entered email is not exit in my record.']);
            }
        } catch(Exception $ex){
            return Response::json(['success'=>false,'message' => $ex->getMessage()]);
        }
    }


     /**
     *
     *
     *
     */
    public function resetPassword(ResetPasswordRequest $request)
    {
       
        try {
            $user = User::where('otp', $request->otp)->first();
            if ($user) {
                $updatePassword = User::findOrFail($user->id);
                $updatePassword->password = Hash::make(trim($request->password));
                $updatePassword->otp = '';
                $updatePassword->save();
                return Response::json(
                    [
                        'success' => true,
                        'message' => 'Password has been changed successfully.'
                    ]
                );
            } else {
                return Response::json(
                    [
                        'success' => false,
                        'message' => 'Please enter correct OTP.'
                    ]
                );
            }
        } catch(Exception $ex){
            return Response::json(['success'=>false,'message' => $ex->getMessage()]);
            print_r($ex->getMessage());die;
        }
    }
	public function getEarning(){
        return Response::json(['success'=>true,'message'=>'','data'=>getCurrentYearEarning()]);
    }
    public function  getAllTransaction()
	{
        $users = User::with(['userInRole.role'])->latest();
        $users->whereHas('userInRole.role', function($q)
        {
            $q->where('name','!=', 'admin');    
        });
        $users = $users->get();
        return view('admin::all_transactions.index',['users'=>$users]);
    }

    public function allTransactionsList(Request $request)
	{
        try {
            $data = SiteTransaction::with(['payer','receiver','order']);
            if (!empty($request->get('date_between')))
			{
                $date = explode('-',$request->get('date_between'));
                $fromdate = date('Y-m-d',strtotime($date[0]));
                $todate = date('Y-m-d',strtotime($date[1]));
                $data->whereDate('created_at', '>=', $fromdate)->whereDate('created_at', '<=', $todate);
            }
			if (!empty($request->get('userId'))) 
			{
				 $user = $request->get('userId');
			     $data->where('sender_id', '=', $user)
                         ->orWhere('receiver_id', '=',  $user);
               
            }
			if(!empty($request->get('user_id')))
			{
				 $data->where('sender_id',$request->user_id);	
			}
            $data = $data->latest()->get();
            if ($request->ajax()) {
                return Datatables::of($data)
                        ->addIndexColumn()
                        ->editColumn('created_at', function($row){
                            return changeDateformat($row->created_at);
                         })
						  ->editColumn('user_received_amount', function($row){
                            return '$'.number_format($row->user_received_amount, 2);
                         })
						  ->editColumn('amount', function($row){
                            return '$'.number_format($row->amount, 2);
                         })
                         ->addColumn('payer', function($row){
                            return $row->payer->name;
                         })
                         ->addColumn('receiver', function($row){
                            return $row->receiver->name;
                         })
						 ->addColumn('admin_earning', function($row){
							 $admin_earning = ((($row->amount * $row->sc)/100) + (($row->amount * $row->cr)/100));
                            return '$'.number_format($admin_earning, 2);
                         })
						 
                         ->addColumn('orderId', function($row){
							 $viewUrl = url('admin/view-order/'.$row->order->id);
                            $btn = '<a style = "color:#087ccf" href="'.$viewUrl.'" >#'.$row->order->order_id.'</a>
							';
                            return $btn;
                         })
                         ->addColumn('service_chage', function($row){
                            return $row->sc.'%';
                         })
                         ->addColumn('commission', function($row){
                            return $row->cr.'%';
                         })
						 ->rawColumns(['orderId'])
                        ->make(true);
                }
             }  
            catch (Exception $ex) {
                return json_encode(['success'=>false,'message'=>$ex->getMessage()]);
                    echo "<pre>";
                    print_r($ex->getMessage());die;
                }
 }
		  
 public function exportTransaction(Request $request){
	 return Excel::download(new TransactionExport($request), 'transaction.xlsx');
 }
 
 public static function allTransactionExportData($request)
 {

        $post = $request->all();
        $data = [];

            $transactions = SiteTransaction::with(['payer','receiver','order']);
			
            if (!empty($request->get('date'))) {
                $date = explode('-',$request->get('date'));
                $fromdate = date('Y-m-d',strtotime($date[0]));
                $todate = date('Y-m-d',strtotime($date[1]));
                $transactions->whereDate('created_at', '>=', $fromdate)->whereDate('created_at', '<=', $todate);
            }

            if (!empty($request->get('userId'))) {
				 $user = $request->get('userId');
			     $transactions->where('sender_id', '=', $user)
                         ->orWhere('receiver_id', '=',  $user);
               
            }
			 if (!empty($request->get('search'))) {
                $transactions->where(function($query) use ($post,$request) {
                $search = $request->get('search');
                $query->where('transaction_id', 'LIKE', "%".$search ."%")
                        ->orWhere('user_received_amount', 'LIKE', "%".$search ."%");
                })->orWhereHas('order', function ($query) use ($request) {
					  $search = $request->get('search');
                      $query->where('order_id', 'like',"%".$search ."%");
                });
		    }
			if(!empty($request->get('user_id'))){
				 $transactions->where('sender_id',$request->user_id);	
			}
			
            $transactions = $transactions->latest();
			if(count($transactions)==0){
			return Redirect('admin/all-transactions')->with( ['message' => 'There is no data to export.','type'=>'error'] );
			}
            foreach($transactions as $value){
                $row = [];
                $row['payer'] = $value->payer->fullname;
                $row['receiver'] = $value->receiver->fullname;
                $row['user_received_amount'] = "$". $value->user_received_amount;
                $row['orderId'] = $value->order->order_id;
                $row['amount'] = "$".$value->amount;
				$row['service_chage'] = "$".$value->sc;
				$row['commission'] = "$".$value->cr;
				$row['transaction_id'] = $value->transaction_id;
				$row['created_at'] = changeDateformat($value->created_at);
                $data[] = $row;
            }
            return $data;
    }
}
