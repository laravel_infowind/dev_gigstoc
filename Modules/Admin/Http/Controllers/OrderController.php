<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Mail\ForgetPassword;
use Response;
use App\User;
use DataTables;
use Validator;
use Redirect;
use Mail;
use Illuminate\Support\Str;
use DB;
use Illuminate\Support\Facades\Input;
use App\Models\Order;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\OrderExport;



class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    

    public function index(Request $request){
		$users = User::with(['userInRole.role'])->latest();
        $users->whereHas('userInRole.role', function($q)
        {
            $q->where('name','!=', 'admin');    
        });
        $users = $users->get();
        return view('admin::order.index',['users'=>$users]);
    }

    public function orderList(Request $request)
    {

        try{
            if ($request->ajax()) {
                 $data = Order::with('user');
				 if (!empty($request->get('userId'))) {
				$data->whereHas('user', function($q)use($request)
				{
					$q->where('id','=', $request->userId);    
				});
				}
				 if (!empty($request->payment_method)) {
				$data->where('payment_type',$request->payment_method);
				}
				if (!empty($request->get('date_between'))) {
					$date = explode('-',$request->get('date_between'));
					$fromdate = date('Y-m-d',strtotime($date[0]));
					$todate = date('Y-m-d',strtotime($date[1]));
					$data->whereDate('created_at', '>=', $fromdate)->whereDate('created_at', '<=', $todate);
				}
				if(!empty($request->get('user_id'))){
				 $data->where('user_id',$request->user_id);	
				}
				$data = $data->latest()->get();
                return Datatables::of($data)
                        ->addIndexColumn()
						 ->addColumn('username', function($row){
                           return ($row->user)?$row->user->name:'';
                        })
						 ->editColumn('created_at', function($row){
                           return changeDateformat($row->created_at);
                        })
						 ->addColumn('orderId', function($row){
							$viewUrl = url('admin/view-order/'.$row->id);
                            $btn = '<a style = "color:#087ccf" href="'.$viewUrl.'" >#'.$row->order_id.'</a>
							';
                            return $btn;
                        })
						
						 ->editColumn('total', function($row){
                           return  $str = "$".number_format($row->subtotal, 2);
                        })
						 ->editColumn('fees', function($row){
                           return  $str = "$".number_format($row->tax, 2);
                        })
						
						 ->editColumn('paid', function($row){
                           return  $str = "$".number_format($row->total, 2);
                        })
						->addColumn('coupon_code', function($row){
                           return  ($row->orderCoupon)?$row->orderCoupon->coupon->coupon:'-';
                        })
						->addColumn('discount', function($row){
                           return  '$'.number_format($row->discount, 2);
                        })
						 ->editColumn('status', function($row){
							 if($row->status == 0){
								 $status = "Pending";
							 }elseif($row->status == 1){
								 $status = "Complete";
							 }else{
								 $status = "Cancelled";
							 }
                             return $status;
                        })
						 ->editColumn('order_id', function($row){
                           return  $str = "#".$row->order_id;
                        })
						 ->editColumn('payment_type', function($row){
                            $str='';
                            if($row->payment_type == 'stripe'){ 
                                $url = url('public/images/payment_07.png');
                                $str = '<img src="'.$url.'" alt="Stripe" width="80px">';
                            }elseif($row->payment_type == 'paypal') {
                                $url = url('public/images/payment_03.png');
                                $str = '<img src="'.$url.'" alt="Paypal" width="80px">';
                            }else{
                                $str = 'Wallet';
                            }
                               return $str;
                        })
                        ->addColumn('action', function($row){
                            $viewUrl = url('admin/view-order/'.$row->id);
                            $btn = '<a href="'.$viewUrl.'" class="edit btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
							';
                            return $btn;
                        })
                        ->rawColumns(['action','username','payment_type','orderId','coupon_code','discount'])
                        ->make(true);
            }
        }catch(Exception $ex){
            print_r($ex->getMessage());die;
        }
    }

  
	public function orderDetail($id)
    {
        $order =  Order::where('id', $id)->first();
		return view('admin::order.detail',['orders'=>$order]);
    }
	
	public function exportOrders(Request $request)
	{
	 return Excel::download(new OrderExport($request), 'order.xlsx');
    }
	
	public static function allOrderExportData($request)
	{
		$post = $request->all();
        $data = [];

		
            $orders = Order::with(['user','detail','orderCoupon']);
			
            if (!empty($request->get('date'))) {
                $date = explode('-',$request->get('date'));
                $fromdate = date('Y-m-d',strtotime($date[0]));
                $todate = date('Y-m-d',strtotime($date[1]));
                $orders->whereDate('created_at', '>=', $fromdate)->whereDate('created_at', '<=', $todate);
            }

            if (!empty($request->get('userId'))) {
				 $user = $request->get('userId');
			     $orders->where('user_id', '=', $user);
            }
			if (!empty($request->get('payment_type'))) {
				 $payment_type = $request->get('payment_type');
			     $orders->where('payment_type', '=', $payment_type);
            }
			
			if (!empty($request->get('search'))) {
				 $search = $request->get('search');
				 $search =str_replace('#','',$search);
				 $orders->where(function($query) use ($search,$request) {
                        $query->where('order_id', 'LIKE', "%".$search ."%")
                        ->orWhere('transaction_id', 'LIKE', "%".$search ."%");
                });
		    }
			if(!empty($request->get('user_id'))){
				 $orders->where('user_id',$request->user_id);	
			}
            $orders = $orders->orderBy('id','desc')->get();
			
            foreach($orders as $value){
                $row = [];
                $row['order_id'] = '#'.$value->order_id;
                $row['user_name'] = $value->user->fullname;
                $row['amount'] = "$". $value->subtotal;
				$row['service_charge'] = "$". $value->tax;
				$row['paid_amount'] = "$". $value->total;
                $row['discount'] = $value->discount;
                $row['applied_coupon'] = ($value->orderCoupon)?$value->orderCoupon->coupon->coupon:'-';
				$row['transaction_id'] = $value->transaction_id;
				$row['order_date'] = changeDateformat($value->created_at);
                $data[] = $row;
            }
            return $data;
	}
}
