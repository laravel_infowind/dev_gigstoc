<?php

namespace Modules\Admin\Http\Controllers;

use App\Http\Requests\Admin\AddBlogPostRequest;
use App\Http\Controllers\Controller;
use App\Models\BlogPostCategory;
use App\Models\BlogCategory;
use Illuminate\Http\Request;
use App\Models\BlogPost;
use DataTables;
use Response;
use Auth;
use Image;
use DB;

class BlogPostController extends Controller
{
    protected $admin;
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogCategory =   BlogCategory::where('is_active', '1')->get();
        return view('admin::blog-post.index',['blog_category'=>$blogCategory]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddBlogPostRequest $request)
    {
        $dir = url('public/images/blog-post/thumbnail');
        if( is_dir($dir) === false )
        {
            mkdir($dir);
        }
       
        if ($request->hasFile('image')) {
            $image              = $request->file('image');
            $imageUploaded      = time().'.'.$image->getClientOriginalExtension();
            $destinationPath    = public_path('images/blog-post');
            $thumb_img          = Image::make($image->getRealPath())->resize(350, 233);
            $image->move($destinationPath, $imageUploaded);
            $thumb_img->save($destinationPath.'/thumbnail/'.$imageUploaded);
          
        } else { 
            $imageUploaded = '';
        }
        $slug = str_slug($request->title, '-');
        $blogPost                   = new BlogPost();
        $blogPost->user_id          = Auth::user()->id;
        $blogPost->title            = $request->title;
        $blogPost->slug             = $slug;
        $blogPost->short_description = $request->short_description;
        $blogPost->post_body        = $request->post_body;
        $blogPost->posted_at        = date("Y-m-d H:i:s");
        $blogPost->image            = $imageUploaded;
        $blogPost->thumbnail        = $imageUploaded;
        $blogPost->meta_title       = $request->meta_title;
        $blogPost->meta_desc        = $request->meta_desc;
        $blogPost->meta_keywords    = $request->meta_keywords;
        $blogPost->is_active        = '1';
        $blogPost->created_at       = date('Y-m-d H:i:s');
        $res                        = $blogPost->save();
        if ($res) {
            foreach ($request->categories as $key => $cat) {
                $blogPostCategory                   = new BlogPostCategory();
                $blogPostCategory->blog_post_id     = $blogPost->id;
                $blogPostCategory->blog_category_id = $cat;
                $catResponse                        = $blogPostCategory->save();
            }
            if ($catResponse) {
                return Response::json(
                    [
                        'success'   => true,
                        'message'   => 'Blog has been posted Successfully.'
                    ],
                    200
                );
            } else {
                return Response::json(
                    [
                        'success'   => false,
                        'message'   => 'Something wrong in blog posting.'
                    ],
                    200
                );
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(AddBlogPostRequest $request)
    {
       
        $updateImage = false;
        if ($request->hasFile('image')) {
            $updateImage = true;
            $image              = $request->file('image');
            $imageUploaded      = time().'.'.$image->getClientOriginalExtension();
            $destinationPath    = public_path('images/blog-post');
            $thumb_img          = Image::make($image->getRealPath())->resize(350, 233);
            $thumb_img->save($destinationPath.'/thumbnail/'.$imageUploaded);
            $image->move($destinationPath, $imageUploaded);
        }
        $slug = str_slug($request->title, '-');
        $blogPost                   = BlogPost::findOrFail($request->id);
        if ($blogPost) {
            $blogPost->user_id          = Auth::user()->id;
            $blogPost->title            = $request->title;
            $blogPost->slug             = $slug;
            $blogPost->short_description = $request->short_description;
            $blogPost->post_body        = $request->post_body;
            $blogPost->posted_at        = date("Y-m-d H:i:s");
            if ($updateImage) {
                $blogPost->image            = $imageUploaded;
                $blogPost->thumbnail        = $imageUploaded;
            }
            $blogPost->meta_title       = $request->meta_title;
            $blogPost->meta_desc        = $request->meta_desc;
            $blogPost->meta_keywords    = $request->meta_keywords;
            $blogPost->is_active        = '1';
            $blogPost->created_at       = date('Y-m-d H:i:s');
            $res                        = $blogPost->save();
            if ($res) {
               
                BlogPostCategory::where('blog_post_id', $blogPost->id)->delete();
                foreach ($request->categories as $key => $cat) {
                    $blogPostCategory                   = new BlogPostCategory();
                    $blogPostCategory->blog_post_id     = $blogPost->id;
                    $blogPostCategory->blog_category_id = $cat;
                    $catResponse                        = $blogPostCategory->save();
                }
                if ($catResponse) {
                    return Response::json(
                        [
                            'success'   => true,
                            'message'   => 'Blog has been update Successfully.'
                        ],
                        200
                    );
                } else {
                    return Response::json(
                        [
                            'success'   => false,
                            'message'   => 'Something wrong in blog updating.'
                        ],
                        200
                    );
                }
            }
        } else {
            return Response::json(
                [
                    'success'   => false,
                    'message'   => 'Blog post can not found.'
                ],
                200
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BlogCategory  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $blogPost = BlogPost::where(['id' => $id])->first();
        return view('admin::blog-post.edit', [ 'blogPost' => $blogPost ] );
    }

    /**
     *
     * @param  \App\BlogCategory  $request
     * @return \Illuminate\Http\Response
     */
    public function getPostList(Request $request)
    {
        
       try{
            if ($request->ajax()) {
                $data = BlogPost::with(['blogPostCategory.blogCategory']);

            
                if (!empty($request->get('category'))) {
                    $catId = $request->get('category');
                    $data->whereHas('blogPostCategory.blogCategory', function($q) use ($catId)
                    {
                        $q->where('id', '=', $catId);    
                    });
                }
                if (!empty($request->get('date_between'))) {
                    $date = explode('-',$request->get('date_between'));
                    $fromdate = date('Y-m-d',strtotime($date[0]));
                    $todate = date('Y-m-d',strtotime($date[1]));
                    $data->whereDate('created_at', '>=', $fromdate)->whereDate('created_at', '<=', $todate);
                }
                if (!empty($request->get('status'))) {
                    $status = ($request->get('status')=='active')?1:0;
                    $data->where('is_active',$status);
                }
                $data = $data->orderBy('id','desc')->get();

                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                            $editUrl = url('admin/edit-blog-post/'.$row->id);
                            $btn = '
                            <a href="'.$editUrl.'" class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"></i></a>';
                            return $btn;
                        })
                    ->addColumn('createdAt', function($row){
                            $createdAt = date('Y-m-d',strtotime($row->created_at));
                            return $createdAt;
                        })
                    ->addColumn('postImage', function($row){
                            $editUrl = url('public/images/blog-post/thumbnail/'.$row->thumbnail);
                            $postImage = '
                            <img src="'.$editUrl.'" hight="50" width="50">';
                            return $postImage;
                        })
                    ->addColumn('postedBy', function($row){
                            $postedBy = $row->user->fullname;
                            return $postedBy;
                        })
                    ->addColumn('status', function($row){
                            // if ($row->is_active == 0) {
                            //     $status = '<label class="switch"><input type="checkbox"  data-id="'. $row->id .'" class="checked change_status" data-status="'.$row->is_active.'"><span class="slider round"></span></label>';
                            // } else {
                            //     $status = '<label class="switch"><input type="checkbox" checked  data-id="'. $row->id .'" class="checked change_status" data-status="'.$row->is_active.'"><span class="slider round"></span></label>';

                            // }
                            // return $status;
                            if ($row->is_active == 0) {
                                $status = '<label class="switch"><input id="row'.$row->id.'" onclick="updateStatus('.$row->id.')" type="checkbox"   class="checked change_user_status" data-status="'.$row->is_active.'" data-table="data-table" data-url="'.url('admin/update-post-status').'" ><span class="slider round"></span></label>';
                            } else {
                                $status = '<label class="switch"><input id="row'.$row->id.'" onclick="updateStatus('.$row->id.')" type="checkbox" checked  class="checked change_user_status" data-status="'.$row->is_active.'" data-table="data-table" data-url="'.url('admin/update-post-status').'" ><span class="slider round"></span></label>';

                            }
                            return $status;
                        })
                    ->addColumn('categoryNames', function($row){
                            $categoryNames = '';
                            foreach ($row->categories as $key => $cat) {
                                if ($key != 0) {
                                    $categoryNames .= ', '.$cat->category_name;
                                } else {
                                    $categoryNames .= $cat->category_name;
                                }
                            }
                            return $categoryNames;
                        })
                    ->rawColumns(['action', 'status', 'postImage', 'postedBy', 'categoryNames'])
                    ->make(true);
            }
        } catch(Exception $ex) {
            print_r($ex->getMessage());
            die;
        }
    }

    /**
     *
     *
     */
    public function addBlogPost(Request $request)
    {
        return view('admin::blog-post.create');
    }

    /**
     *
     *
     */
    public function updateStatus(Request $request)
    {
        try {
            if ($request->status == 0) {
               $res =  BlogPost::where('id', '=', $request->id)->update(array('is_active' => 1));   
            } else {
                $res =  BlogPost::where('id', '=', $request->id)->update(array('is_active' => 0));
            }
            if ($res) {
                return Response::json(
                    [
                        'success' => true,
                        'message' => 'Blog post status change successfully.'
                    ]
                );
            } else {
                return Response::json(
                    [
                        'success' => false,
                        'message' => 'Something was wrong.'
                    ]
                );
            }
        } catch(Exception $ex) {
            print_r($ex->getMessage());
            die;
        }
        
    }

    /**
     *
     *
     */
    public function getBlogCategory(Request $request)
    {
        $list = [];
        $categorys  =  BlogCategory::where('is_active', '1')->get();
        $list[] = "<option value=''>Select Category</option>";
        if (!empty($request->get('ids'))) {
            foreach($categorys as $cat){
                $option = "<option value='".$cat->id."' >$cat->category_name</option>";
                $list[] = $option;
            }
        } else {
            foreach($categorys as $cat){
                $option = "<option value='".$cat->id."' >$cat->category_name</option>";
                $list[] = $option;
            }
        }
        return Response::json(
            [
                'success'   => true,
                'list'      => $list
            ]
        );
    }
}
