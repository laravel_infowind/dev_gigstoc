<?php

namespace Modules\Admin\Http\Controllers;

use App\Models\JobRequest;
use App\User;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Response;
use App\Models\OffersOnJob;


class JobOfferController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function index($id)
    {
        return view('admin::job-offer.index',['id'=>$id]);
    }

    function list(Request $request) 
    {
        
        try {
            if ($request->ajax()) {
                $data = OffersOnJob::where(['job_request_id'=>$request->request_id]);
             
                if (!empty($request->get('date_between'))) {
                    $date = explode('-', $request->get('date_between'));
                    $fromdate = date('Y-m-d', strtotime($date[0]));
                    $todate = date('Y-m-d', strtotime($date[1]));
                    $data->whereBetween('created_at', [$fromdate, $todate]);
                }

                if (!empty($request->get('status'))) {
                    $status = ($request->get('status') == 'pending') ? 0 : $request->get('status');
                    $data->where('is_accepted', $status);
                }
                $data = $data->latest()->get();

                return Datatables::of($data)
                    ->addIndexColumn()
                    ->editColumn('created_at', function ($row) {
                        return changeDateformat($row->created_at);
                    })
                    ->editColumn('job_title', function ($row) {
                        return ($row->jobDetail)?$row->jobDetail->title:'-';
                    })
                    ->editColumn('service_tittle', function ($row) {
                        return ($row->serviceDetail)?$row->serviceDetail->title:'-';
                    })
                    ->addColumn('created_by', function ($row) {

                        return ($row->requestedUser) ? ucwords($row->requestedUser->fullname) : '';
                    })
					/*
                    ->addColumn('status', function($row){
                        if ($row->is_accepted == 0) {
                            $status = '<span class="newtag">Pending</span>';
                        } elseif($row->is_accepted == 1) {
                            $status = '<span class="newtagto">Accepted</span>';
                        }else{
                            $status = '<span class="newtag">Rejected</span>';
                        }
                        return $status;
                    })*/
                    
                    ->rawColumns(['status'])
                    ->make(true);
            }
        } catch (Exception $ex) {
            print_r($ex->getMessage());die;
        }
    }

}


