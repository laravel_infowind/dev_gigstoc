<?php

namespace Modules\Admin\Http\Controllers;

use App\Models\Service;
use App\User;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Response;
use App\Models\Category;
use App\Models\Tag;
use App\Models\ImageInService;
use App\Models\TagInService;
use App\Models\ExtraInService;
use Validator;
use Auth;
use File;
use Image;
use App\Http\Requests\Admin\EditServiceRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ServiceExport;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function index(Request $request)
    {
        return view('admin::services.index');
    }

    function list(Request $request) {
        //print_r($request->all());die;
        try {
            if ($request->ajax()) {
                $data = Service::with(['category', 'user'])->where('is_custom','=',0);
                // $data->whereHas('category', function ($q) {
                //     $q->where('id', '=', 'category_id');
                // });

                if (!empty($request->get('category_id'))) {
                    $data->where('category_id', '=',  $request->get('category_id'));
                }

                if (!empty($request->get('date_between'))) {
                    $date = explode('-', $request->get('date_between'));
                    $fromdate = date('Y-m-d', strtotime($date[0]));
                    $todate = date('Y-m-d', strtotime($date[1]));
                    $data->whereBetween('created_at', [$fromdate, $todate]);
                }

                if (!empty($request->get('status'))) {
                    $status = ($request->get('status') == 'active') ? 1 : 0;
                    $data->where('is_active', $status);
                }
                $data = $data->latest()->get();

                return Datatables::of($data)
                    ->addIndexColumn()
                    ->editColumn('created_at', function ($row) {
                        return changeDateformat($row->created_at);
                    })
                    ->editColumn('price', function ($row) {
                        return '$' . $row->price;
                    })
                    ->addColumn('category', function ($row) {

                        return ($row->category) ? ucwords($row->category->name) : '';
                    })
                    ->addColumn('created_by', function ($row) {

                        return ($row->user) ? ucwords($row->user->fullname) : '';
                    })
					->addColumn('view_count', function ($row) {
						$count = getViewCount($row->id);
                        return $count;
                    })
					
                    ->addColumn('status', function($row){
                        if ($row->is_active == 0) {
                            $status = '<label class="switch"><input id="row'.$row->id.'" onclick="updateStatus('.$row->id.')" type="checkbox"   class="checked change_user_status" data-status="'.$row->is_active.'" data-table="data-table" data-url="'.url('admin/service/status').'" ><span class="slider round"></span></label>';
                        } else {
                            $status = '<label class="switch"><input id="row'.$row->id.'" onclick="updateStatus('.$row->id.')" type="checkbox" checked  class="checked change_user_status" data-status="'.$row->is_active.'" data-table="data-table" data-url="'.url('admin/service/status').'" ><span class="slider round"></span></label>';

                        }
                        return $status;
                    })
                    ->addColumn('action', function ($row) {
                        $editUrl = url('admin/service/edit/' . $row->id);
                        $btn = '<a href="' . $editUrl . '" class="edit btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i></a>';

                        return $btn;
                    })
                    ->rawColumns(['action', 'status', 'category', 'created_by','view_count'])
                    ->make(true);
            }
        } catch (Exception $ex) {
            print_r($ex->getMessage());die;
        }
    }

    public function edit($id)
    {
        $service = Service::with(['extras', 'images', 'tags'])->where('id', $id)->first();
        $categories = Category::where('is_active', '=', 1)->get();
        $tags = Tag::all();
        //echo '<pre>';print_r($service);die;
        return view('admin::services.edit', compact('service', 'categories', 'tags'));
    }

    public function update(EditServiceRequest $request)
    {
        //echo '<pre>';
        //print_r($request->all());die;

        try {
            $video_url = $request->video;
            $is_featured = $request->is_featured;
            $service = Service::where(['id' => $request->id])->first();
            $service->title = $request->title;
            $service->price = $request->price;
            $service->deadline = $request->deadline;
            $service->category_id = $request->category_id;
            $service->description = $request->description;

            if (!empty($video_url)) {
                
                $video_type = determineVideoUrlType($video_url);
                
                if (!$video_type['video_type']) {
                    return Response::json(['success' => false, 'video_error' => true, 'message' => 'The vedio type is not valid']);
                }
                
                $service->is_video = 1;
                $service->video_type = $video_type['video_type'];
                $service->video_link = $video_url;
            }
            
            $service->opening_message = $request->opening_message;
            //print_r($service);die;
            if($service->save()) {
                //tags in services
                if (!empty($request->tag_id)) {
                    $deResponse = TagInService::where(['service_id' => $request->id])->delete();

                    foreach($request->tag_id as $value) {
                        $tag_in_service = new TagInService();
                        $tag_in_service->service_id = $request->id;
                        $tag_in_service->tag_id = $value;
                        $tag_in_service->save();
                    }
                }

                //extras in services
                if (!empty($request->extras)) {
                    $deResponse = ExtraInService::where(['service_id' => $request->id])->delete();

                    foreach($request->extras as $value) {
                        if (!empty($value['service_name']) && !empty($value['service_price'])) {
                            $extra_in_service = new ExtraInService();
                            $extra_in_service->service_id = $request->id;
                            $extra_in_service->service_name = $value['service_name'];
                            $extra_in_service->price = $value['service_price'];
                            $extra_in_service->save();
                        }
                    }
                }

                // update featured image
                ImageInService::where('service_id', '=', $request->id)->update(['is_featured' => 0]);
                ImageInService::findOrFail($is_featured)->update(['is_featured' => 1]);
            }
            return Response::json(['success' => true, 'message' => 'Service Updated Successfully.']);
        } catch (Exception $ex) {
            return Response::json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }

    public function imageUpload(Request $request)
    {
        //print_r($request->all());die;
        $data = [];
        $service_id = $request->service_id;
        if ($request->hasFile('images')) {
                
            $rules = [
                'images.*' => 'mimes:gif,jpg,jpeg,png|max:20000'
            ];

            $messages = [
                'images.*.mimes' => 'Only jpg, jpeg and png images are allowed',
                'images.*.max' => 'Sorry! Maximum allowed size for an image is 20MB',
            ];
    
            $validator = Validator::make($request->all(), $rules, $messages);
           
            if ($validator->fails()) {
                return Response::json(['success' => false, 'message' => $validator->errors()->all()]);
            }

            foreach ($request->file('images') as $key => $value) {
                $imageName = time(). $key . '.' . $value->getClientOriginalExtension();
    
                $destinationPath = public_path('images/services');

                $thumb_img = Image::make($value->getRealPath())->resize(350, 233);

                $thumb_img->save($destinationPath.'/thumbnail/'.$imageName);

                $value->move($destinationPath, $imageName);

                $image_in_service = new ImageInService();
                $image_in_service->service_id = $service_id;
                $image_in_service->thumbnail = $imageName;
                $image_in_service->image = $imageName;
                $image_in_service->save();

                // update featured image
                ImageInService::where('service_id', '=', $service_id)->update(['is_featured' => 0]);
                ImageInService::findOrFail($image_in_service->id)->update(['is_featured' => 1]);

                $data[$image_in_service->id] = url('public/images/services').'/thumbnail/'.$imageName;
            }
        }

        return Response::json(['success' => true, 'data' => $data]);
    }

    public function deleteImage($imageId)
    {   
        $image =  ImageInService::where('id', $imageId)->first();
        $directoryPath = public_path().'/images/services/'.$image['image'];
        $directoryPathThumbnail = public_path().'/images/services/thumbnail/'.$image['thumbnail'];

        if (file_exists($directoryPath)) {
            File::delete($directoryPath);
        }

        if (file_exists($directoryPathThumbnail)) {
            File::delete($directoryPathThumbnail);
        }

        $res =  ImageInService::where('id', $imageId)->delete();

        if ($res) {
            return Response::json(
                [
                    'success' => true,
                    'message' => 'The image has been removed.'
                ]
            );
        } else {
            return Response::json(
                [
                    'success' => false,
                    'message' => 'Some problem occurred, please try again.'
                ]
            );
        }
    }

    public function changeStatus(Request $request)
    {
		
        $service = Service::where(['id' => $request->id])->first();
        $service->is_active = ($request->status == 1) ? 0 : 1;
        if($service->save()){
			$data['username'] = $service->user->name;
			$data['service'] = $service->title;
			$data['service_link'] = 'service/'.$service->id;
			$data['subject'] = 'Service status updated';
			$data['to_email'] = $service->user->email;
			$data['status'] = ($service->is_active==1)?'Activated':'Inactivated';
			sendMail($data,'owner-service-publish-unpublish');
		}
        return Response::json(['success' => true, 'message' => 'Service status changed successfully.']);
    }

    public function getCategories(Request $request)
    {
        $list = [];
        $categorys = Category::where('is_active', '=', 1)->get();
        $list[] = "<option value=''>Select Category</option>";
        foreach ($categorys as $cat) {
            $option = "<option value='" . $cat->id . "' >$cat->name</option>";
            if (!empty($request->id) && $cat->id == $request->id) {
                $option = "<option value='" . $cat->id . "' selected='selected'>$cat->name</option>";
            }
            $list[] = $option;
        }
        return Response::json(['success' => true, 'message' => '', 'list' => $list]);
    }
	
	public function exportServices(Request $request){
		 return Excel::download(new ServiceExport($request), 'service.xlsx');
	}
	
	public static function allServiceExportData($request)
	{
		$post = $request->all();
        $data = [];

		
            $services = Service::with(['category', 'user'])->where('is_custom','=',0)->latest();
			
            if (!empty($request->get('date'))) {
                $date = explode('-',$request->get('date'));
                $fromdate = date('Y-m-d',strtotime($date[0]));
                $todate = date('Y-m-d',strtotime($date[1]));
                $services->whereDate('created_at', '>=', $fromdate)->whereDate('created_at', '<=', $todate);
            }

            if (!empty($request->get('userId'))) {
				 $user = $request->get('userId');
			     $services->where('user_id', '=', $user);
            }
			if (!empty($request->get('payment_type'))) {
				 $payment_type = $request->get('payment_type');
			     $services->where('payment_type', '=', $payment_type);
            }
			
			if (!empty($request->get('search'))) {
				 $search = $request->get('search');
				 $search =str_replace('#','',$search);
				 $services->where(function($query) use ($search,$request) {
                        $query->where('order_id', 'LIKE', "%".$search ."%")
                        ->orWhere('transaction_id', 'LIKE', "%".$search ."%");
                });
		    }
			if(!empty($request->get('user_id'))){
				 $services->where('user_id',$request->user_id);	
			}
            $services = $services->get();
			
            foreach($services as $value){
                $row = [];
                $row['title'] = $value->title;
                $row['price'] = '$' .$value->price;
                $row['deadline'] = $value->deadline;
				$row['view_count'] = getViewCount($value->id);
				$row['category'] = $value->category->name;
                $row['created_by'] = $value->user->fullname;
                $row['created_date'] = changeDateformat($value->created_at);
			    $data[] = $row;
            }
            return $data;
	}

}
