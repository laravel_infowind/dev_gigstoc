<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Session;
use App\Models\ConfigSetting;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use App\Common;
use Validator;
use DataTables;
use File;
use App\Http\Requests\Admin\SettingRequest;



class SettingController extends Controller
{
    protected $admin;
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ConfigSetting::all();
        $settings = [];
        foreach ($data as $key => $value) {

            $settings[$value->meta_key] = $value->meta_value;
        }

        $settings = (object) $settings;
        return view('admin::settings.index', compact('settings'));
    }

    public function create()
    {
        return view('admin::settings.create');
    }

    public function store(SettingRequest $request)
    {   //echo '<pre>'; 
        //print_r($request->all());die;
          $dir = url('public/images/setting');
            if( is_dir($dir) === false )
            {
                mkdir($dir);
            }
        foreach ($request->except('_token') as $key => $value) {
            $setting = ConfigSetting::firstOrCreate(['meta_key' => $key]);

            $setting->meta_key     =   $key;
            $setting->meta_value   =   $value;
            $setting->save();


            if ($request->file($key)) {
                $photo           = $request->file($key);
                $destinationPath = public_path('images/setting');
                $photo->move($destinationPath, time() . $photo->getClientOriginalName());
                $banner_image = time() . $photo->getClientOriginalName();

                $setting = ConfigSetting::firstOrCreate(['meta_key' => $key]);

                $setting->meta_key     =   $key;
                $setting->meta_value   =   $banner_image;
                $setting->save();
            }
        }

        return Redirect::back()->with('flash_alert_notice', 'Site settings saved successfully !!');
    }
}
