<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use App\Common;
use App\User;
use App\Models\Slider;
use App\Models\HomeManagement;
use App\Models\FindingCost;
use App\Models\KeyFeature;
use Validator;
use DB;
use File;
use DataTables;
use Response;
use Spatie\Image\Image;

class SliderController extends Controller
{
	protected $common_model;
    protected $per_page = 10;
	public function __construct(User $user_model, Common $common_model)
	{
	    $this->common = $common_model;
        $this->slider = new Slider;
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        return view('admin::slider.index');
    }

    public function list(Request $request) {
        //print_r($request->all());die;
        try {
            if ($request->ajax()) {
                $slider = Slider::latest()->get();

                return Datatables::of($slider)
                    ->addIndexColumn()
                    ->editColumn('created_at', function ($row) {
                        return date('Y-m-d', strtotime($row->created_at));
                    })
                    ->addColumn('slider_img', function($row){
                        $editUrl = url('public/images/slider/'.$row->slider_img);
                        if($row->mime_type == 'video/mp4') {
                            $slider_img = '
                            <video width="80" height="80">
                            <source src="'.$editUrl.'" type="video/mp4">
                        </video>';
                        } else {
                            $slider_img = '
                        <img src="'.$editUrl.'" hight="80" width="80">';
                        }
                        
                        return $slider_img;
                    })
                    ->addColumn('action', function ($row) {
                        $editUrl = url('admin/slider/edit/' . $row->id);
                        $btn = '<a href="' . $editUrl . '" class="edit btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i></a><a id="rowremove'.$row->id.'" onclick="deleteRow('.$row->id.')" href="javascript:void(0)" class="btn btn-danger btn-sm" data-table="data-table" data-url="'.url('admin/slider/destroy').'" ><i class="fa fa-trash-o"></i></a></div>';

                        return $btn;
                    })
                    ->rawColumns(['action', 'slider_img'])
                    ->make(true);
            }
        } catch (Exception $ex) {
            print_r($ex->getMessage());die;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     
        return view('admin::slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //print_r($request->all());die;
        // Validate request
        $rules = [
            'title'=>'required',
            'description'=>'required',
            'slider_img'=>'required|mimes:jpeg,jpg,png,mp4|max:20000'
		];

		$messages = [
            'slider_img.dimensions' => 'Image should be of minimum 1366px width and 590px height.'
        ];

		$validator = Validator::make($request->all(), $rules, $messages);

		if ($validator->fails()) {
		    return back()->withInput()->withErrors($validator->messages());
		}

        // Image upload
        if($request->hasFile('slider_img')){
            $image = $request->file('slider_img');
            
            $slider_img = $image->getClientOriginalName();

            $mime_type = $image->getClientMimeType();

            $destinationPath = public_path('images/slider');

            $pathToImage = $image->move($destinationPath, $slider_img);

            // Image::load($destinationPath.'/'.$slider_img)
            // ->width(1366)     
            // ->optimize()
            // ->save($destinationPath.'/'.$slider_img);

        }
        else{ 
            $slider_img = '';
        }

        $desc = trim(preg_replace('/(&nbsp;)+|\s\K\s+/','',$request->get('description')));
        if (empty($desc) || $desc == '') {
            return back()->withInput()->withErrors(['description'=>'The description field is required']);
        }
        

        // Prepare data for store
        $slider_data = [
            'title' => $request->get('title'),
            'description' => $desc,
            'slider_img' => $slider_img,
            'mime_type' => $mime_type,
            'created_at' => date('Y-m-d h:i:s')
        ];

        $store = $this->common->storeData('sliders', $slider_data);

        return redirect('admin/sliders')->with('success', 'Slider added successfully !!');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $where = array('id'=>$id);
        $slider = uniqueData('sliders',$where);

        return view('admin::slider.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->get('id');
       // Validate request
        $rules = [
            'title'=>'required',
            'description'=>'required',
            'slider_img'=>'mimes:jpeg,jpg,png,mp4|max:20000'
        ];

        $messages = [];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        // Image upload
        if($request->hasFile('slider_img')){
            $image = $request->file('slider_img');
            
            $slider_img = $image->getClientOriginalName();

            $mime_type = $image->getClientMimeType();
            
            $destinationPath = public_path('images/slider');

            $image->move($destinationPath, $slider_img);

            // Image::load($destinationPath.'/'.$slider_img)
            // ->width(1366)     
            // ->optimize()
            // ->save($destinationPath.'/'.$slider_img);

        }
        else{ 
            $slider_img = $request->get('old_img');
        }
        
        $desc = trim(preg_replace('/(&nbsp;)+|\s\K\s+/','',$request->get('description')));
        if (empty($desc) || $desc == '') {
            return back()->withInput()->withErrors(['description'=>'The description field is required']);
        }

        // Prepare data for update
        if($request->hasFile('slider_img')){
            $slider_data = [
                'title' => $request->get('title'),
                'description' => $desc,
                'slider_img' => $slider_img,
                'mime_type' => $mime_type
            ];
        } else {
            $slider_data = [
                'title' => $request->get('title'),
                'description' => $desc,
                'slider_img' => $slider_img,
            ];
        }


        $page_updated = $this->common->updateData('sliders', $slider_data, ['id' => $id]);
        
        return redirect('admin/sliders')->with('success', 'page updated successfully !!');
    }

    public function destroy($id)
    {
        $this->delete_image('sliders', 'slider_img', $id);
        $res =  Slider::where('id', $id)->delete();
        if($res){
        return Response::json(['success'=>true,'message' => 'Slide Deleted Successfully.']);
        }
    }

    public function delete_image($table, $column, $deletedId)
    {
        $whereCluase = array('id' => $deletedId);
        $blogs = $this->common->getSingleRow($table, $column, $whereCluase);
        $directoryPath = public_path().'/images/slider/'.$blogs->$column;

        if (file_exists($directoryPath)) {
            File::delete($directoryPath);
        }

        return true;
    }

    public function content(Request $request)
    {   
        return view('admin::home.index');
    }

    public function content_list(Request $request) {
        try {
            if ($request->ajax()) {
                $content = HomeManagement::with(['keyFeatures', 'findingCosts'])->latest()->get();

                return Datatables::of($content)
                    ->addIndexColumn()
                    ->editColumn('updated_at', function ($row) {
                        return date('Y-m-d', strtotime($row->created_at));
                    })
                    
                    ->addColumn('action', function ($row) {
                        $editUrl = url('admin/content/edit/' . $row->id);
                        $btn = '<a href="' . $editUrl . '" class="edit btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i></a>';

                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            }
        } catch (Exception $ex) {
            print_r($ex->getMessage());die;
        }
    }

    public function content_edit($id)
    {
        //$where = array('id'=> $id);
        //$data = uniqueData('home_management', $where);
        $data = HomeManagement::with(['keyFeatures', 'findingCosts'])->find($id);

        return view('admin::home.edit', compact('data'));
    }

    public function content_update(Request $request)
    {
        $id = $request->get('id');
		
        // Validate request
        if ($id == 6) {
            $rules = [
                'sub_heading'=>'required',
                'heading'=>'required',
                'description'=>'required',
                'image'=>'mimes:jpeg,jpg,png|max:4096',
            ];
        } elseif ($id == 5 || $id == 7 || $id == 8 || $id == 11 || $id == 13) {
            $rules = [
                'sub_heading'=>'required'
            ];
        } else if($id == 9 || $id == 10) {
            $rules = [
                'sub_heading'=>'required',
                'heading.*'=>'required',
                'description.*'=>'required',
                'image.*'=>'mimes:jpeg,jpg,png|max:4096',
            ];
        } else {
            $rules = [
                'sub_heading'=>'required',
                'description'=>'required',
                'image'=>'mimes:jpeg,jpg,png|max:4096',
            ];
        }
       

        $messages = [];

        if($id == 9 || $id == 10) {
            if ($request->has('image')) {
                foreach ($request->file('image') as $key => $val) {
                    $messages['image.' . $key . '.mimes'] = 'The image must be a file of type: jpeg, jpg, png.';
                    $messages['image.' . $key . '.max'] = 'The image may not be greater than :max KB';
                }
            }
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        
        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }
        
        if($id == 9 || $id == 10) {
            // Image upload
            $heading = $request->get('heading');
            $description = $request->get('description');
			
			$oldImages = $request->get('old_img');
			  // echo "<pre>";
				//print_r($request->all());die;
            $images = [];
            $files = $request->file('image');
            //if (!empty($files)) {
                $keyf_id = $request->get('keyf_id');
                
                foreach ($oldImages as $key => $val) 
                {
					if (isset($files[$key]) && !empty($files[$key])) {
                        
                        $image_name = $files[$key]->getClientOriginalName();
                
                        $destinationPath = public_path('images');

                        $files[$key]->move($destinationPath, $image_name);

                        Image::load($destinationPath.'/'.$slider_img)
                        ->optimize()
                        ->save($destinationPath.'/'.$image_name);
                        
                    } else {
                        $image_name = $val;
                    }

                    $data_key_feature = [
                        'home_management_id' => $request->get('id'),
                        'heading' => $heading[$key],
                        'description' => $description[$key],
                        'image' => $image_name
                    ];
                    
                    if ($id == 9) {
                        KeyFeature::where('id', '=', $keyf_id[$key])->update($data_key_feature);
                    } else {
                        FindingCost::where('id', '=', $keyf_id[$key])->update($data_key_feature);
                    }
                }
          //  }
        }

        // Prepare data for update
        if ($id == 6) {
            // Image upload
            if($request->hasFile('image')){
                $photo = $request->file('image');
                
                $image_name = $photo->getClientOriginalName();
                
                $destination = public_path('images').'/';

                $photo->move($destination, $image_name);
            }
            else{ 
                $image_name = $request->get('old_img');
            }
            
            $content = [
                'heading' => $request->get('heading'),
                'sub_heading' => $request->get('sub_heading'),
                'description' => $request->get('description'),
                'image' => $image_name
            ];
        } else if($id == 5 || $id == 7 || $id == 8 ||  $id == 9 || $id == 10 || $id == 11 || $id == 13) {
            $content = [
                'sub_heading' => $request->get('sub_heading')
            ];
        } else {
            // Image upload
            if($request->hasFile('image')){
                $image = $request->file('image');
                
                $image_name = $image->getClientOriginalName();
                
                $destinationPath = public_path('images');

                $image->move($destinationPath, $image_name);
            }
            else{ 
                $image_name = $request->get('old_img');
            }

            $content = [
                'sub_heading' => $request->get('sub_heading'),
                'description' => $request->get('description'),
                'image' => $image_name
            ];
        }
	
        $Updated = HomeManagement::where('id', '=', $id)->update($content);
        
        return redirect('admin/content')->with(['type' => 'success', 'message' => 'Home page content updated successfully !!']);
    }
}
