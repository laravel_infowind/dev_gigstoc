<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Mail\ForgetPassword;
use Response;
use App\User;
use DataTables;
use Validator;
use Redirect;
use Mail;
use Illuminate\Support\Facades\Hash ;
use Illuminate\Support\Str;
use DB;
use Illuminate\Support\Facades\Input;
use App\Models\Coupon;
use App\Http\Requests\Admin\AddCouponRequest;


class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    

    public function index(Request $request){
        return view('admin::coupon.index');
    }

    public function couponList(Request $request)
    {
        try{
            if ($request->ajax()) {
                    $data = Coupon::latest()->get();
                   
                return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function($row){
                            $editUrl = url('admin/edit-coupon/'.$row->id);
                            $btn = '<a href="'.$editUrl.'" class="edit btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i></a>
							<a id="rowremove'.$row->id.'" onclick="deleteRow('.$row->id.')" href="javascript:void(0)" class="btn btn-danger btn-sm" data-table="data-table" data-url="'.url('admin/coupon/destroy').'" ><i class="fa fa-trash-o"></i></a>';
        
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
            }
        }catch(Exception $ex){
            print_r($ex->getMessage());die;
        }
    }

    public function addCoupon(){
        return view('admin::coupon.create');
    }

    public function editCoupon($id){
        $coupon = Coupon::where(['id'=>$id])->first();
        return view('admin::coupon.edit',['coupon'=>$coupon]);
    }

    public function saveCoupon(AddCouponRequest $request){
        try{
                if(!empty($request->id)){
                    $coupon =   Coupon::where(['id'=>$request->id])->first();
                }else{
                    $coupon =  new Coupon();
                }
                $coupon->coupon = $request->coupon;
				$coupon->amount = $request->amount;
				$coupon->type = $request->type;
				$coupon->total_usage = $request->total_usage;
				$coupon->start_date = date('Y-m-d',strtotime($request->start_date));
				$coupon->end_date = date('Y-m-d',strtotime($request->end_date));
                $coupon->save();
                if(!empty($request->id)){
                    return Response::json(['success'=>true,'message' => 'Coupon Updated Successfully.']);
                }else{
                    return Response::json(['success'=>true,'message' => 'Coupon Added Successfully.']);
                }
          
        }catch(Exception $ex){
            return Response::json(['success'=>false,'message' => $ex->getMessage()]);
            print_r($ex->getMessage());die;
        }
    }
	public function destroy($id)
    {
        $res =  Coupon::where('id', $id)->delete();
        if ($res) {
            return Response::json(['success'=>true,'message' => 'FAQ Deleted Successfully.']);
        }
    }
}
