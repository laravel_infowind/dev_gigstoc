<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'admin', 'middleware' => 'guest'], function () {
    
    Route::get('/login', 'AdminController@index');
    Route::post('/login', 'AdminController@login')->name('admin.login');
	Route::get('/forget-password-form', 'AdminController@forgotForm');
    Route::post('/forget-password', 'AdminController@forgetPassword')->name('forget.password');
    Route::get('/reset-password', 'AdminController@getResetPassword');
    Route::post('/reset-password', 'AdminController@resetPassword')->name('reset.password');
});
    
Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
    Route::get('/logout', 'AdminController@logout');

    Route::get('/change-password', 'AdminController@changePassword');
    Route::post('/update-password', 'AdminController@updatePassword');
    Route::get('/edit-profile', 'AdminController@editProfile');
    Route::post('/update-profile', 'AdminController@updateProfile');

    Route::get('/dashboard', 'AdminController@dashboard');
    Route::get('/get-earning', 'AdminController@getEarning');
	
    Route::get('settings', 'SettingController@index')->name('settings');
    Route::post('setting/store', 'SettingController@store')->name('setting/store');

    // users routes
    Route::post('export_users', 'UserController@export_users');
    Route::get('/users', 'UserController@user');
    Route::get('/user-list', 'UserController@userList');
    Route::get('/add-user', 'UserController@addUser');
    Route::post('/save-user', 'UserController@saveUser');
    Route::get('/edit-user/{id}', 'UserController@editUser');
    Route::post('/update-user', 'UserController@saveUser');
    Route::get('/update-user-status', 'UserController@changeUserStatus');
	Route::get('/user-order', 'UserController@userOrder');
	Route::get('/user-transaction', 'UserController@userTransaction');

    // Home management
    Route::get('sliders', 'SliderController@index')->name('sliders');
    Route::get('slider/list', 'SliderController@list')->name('slider/list');
    Route::get('slider/create', 'SliderController@create')->name('slider/create');
    Route::post('slider/store', 'SliderController@store')->name('slider/store');

    Route::get('slider/edit/{page_id}', 'SliderController@edit')->name('slider/edit');
    Route::post('slider/update', 'SliderController@update')->name('slider/update');
    Route::get('slider/destroy/{slide_id}', 'SliderController@destroy')->name('slider/destroy');
    Route::get('content', 'SliderController@content')->name('content');
    Route::get('content/list', 'SliderController@content_list')->name('content/list');
    Route::get('content/edit/{section_id}', 'SliderController@content_edit')->name('content/edit');
    Route::post('content/update', 'SliderController@content_update')->name('content/update');
    
    //testimonial routes
    Route::get('testimonial', 'TestimonialController@index');
    Route::get('testimonial/list', 'TestimonialController@list');
    Route::get('testimonial/create', 'TestimonialController@create')->name('testimonial/create');
    Route::post('testimonial/store', 'TestimonialController@store');
    Route::get('testimonial/edit/{page_id}', 'TestimonialController@edit');
    Route::get('testimonial/destroy/{id}', 'TestimonialController@destroy');
    Route::get('update-testimonial-status', 'TestimonialController@updateStatus');
    
    
    // category routes
    Route::get('/category', 'CategoryController@category');
    Route::get('/category-list', 'CategoryController@categoryList');
    Route::get('/add-category', 'CategoryController@addCategory');
    Route::post('/add-category', 'CategoryController@saveCategory');
    Route::get('/get-parent-category', 'CategoryController@getParentCategory');
    Route::get('/edit-category/{id}', 'CategoryController@editCategory');
    Route::get('/delete-category/{id}', 'CategoryController@deleteCategory');
    Route::post('/edit-category', 'CategoryController@saveCategory');
    Route::get('/update-category-status', 'CategoryController@changeStatus');
    

    // page routes
    Route::get('/pages', 'PageController@pages');
    Route::get('/page-list', 'PageController@pageList');
    Route::get('/add-page', 'PageController@addPage');
    Route::post('/add-page', 'PageController@savePage');
    Route::get('/edit-page/{id}', 'PageController@editPage');
    Route::get('/delete-page/{id}', 'PageController@deletePage');
    Route::post('/edit-page', 'PageController@savePage');
    Route::post('/about-us', 'PageController@saveAboutUs');
    Route::get('/update-page-status', 'PageController@changePageStatus');
    Route::post('/ckeditor/upload', 'PageController@upload');

    // services routes
    Route::get('services', 'ServiceController@index');
    Route::get('service/list', 'ServiceController@list');
    Route::get('service/edit/{id}', 'ServiceController@edit');
    Route::post('service/update', 'ServiceController@update');
    Route::get('service/status', 'ServiceController@changeStatus');
    Route::get('service/categories', 'ServiceController@getCategories');
    Route::get('service/delete-image/{image_id}', 'ServiceController@deleteImage');
    Route::post('service/image-upload', 'ServiceController@imageUpload');
    Route::post('export/services', 'ServiceController@exportServices');
	
	
    // jobs routes
    Route::get('jobs', 'JobController@index');
    Route::get('job/list', 'JobController@list');
    Route::get('job/edit/{id}', 'JobController@edit');
    Route::post('job/update', 'JobController@update');
    Route::get('job/status', 'JobController@changeStatus');
    Route::post('export_jobs', 'JobController@exportJobs');
	
    // jobs offer routes
    Route::get('jobs/offer/{id?}', 'JobOfferController@index');
    Route::get('offer/list', 'JobOfferController@list');
	
    
    // payout routes
    Route::get('payout/{user_id?}', 'PayoutController@index');
    Route::get('payout-list', 'PayoutController@payoutList');
    Route::get('payout-transaction', 'PayoutController@payoutTransaction');
    Route::get('payout-transaction-list', 'PayoutController@payoutTransactionList');
    Route::get('payout/transaction/{id}', 'PayoutController@transactionView');
    Route::get('payout-user-transaction-list', 'PayoutController@payoutUserTransactionList');
    Route::get('get-total-balance', 'PayoutController@getTotalBalance');
    Route::get('get-transferring-amount', 'PayoutController@getTransferringAmount');
    Route::get('send-payout', 'PayoutController@sendPayOut');
    
    // blog category routes
    Route::get('/blog-category', 'BlogCategoryController@index');
    Route::get('/blog-category-list', 'BlogCategoryController@blogCategoryList');
    Route::get('/add-blog-category', 'BlogCategoryController@addBlogCategory');
    Route::post('/add-blog-category', 'BlogCategoryController@store');
    Route::get('/edit-blog-category/{id}', 'BlogCategoryController@edit');
    Route::post('/update-blog-category', 'BlogCategoryController@store');
    Route::get('/delete-blog-category/{id}', 'BlogCategoryController@destroy');
    Route::get('/update-status', 'BlogCategoryController@updateStatus');

    // blog post routes
    Route::get('/blog-post', 'BlogPostController@index');
    Route::get('/blog-post-list', 'BlogPostController@getPostList');
    Route::get('/add-new-post', 'BlogPostController@addBlogPost');
    Route::post('/add-new-post', 'BlogPostController@store');
    Route::get('/get-blog-category', 'BlogPostController@getBlogCategory');
    Route::get('/edit-blog-post/{id}', 'BlogPostController@edit');
    Route::post('/update-blog-post', 'BlogPostController@update');
    Route::get('/update-post-status', 'BlogPostController@updateStatus');

    // rating route
    Route::get('/seller-rating', 'RatingController@index');
    Route::get('/seller-rating-list', 'RatingController@sellerRatingList');
    Route::get('/update-rating-status', 'RatingController@updateStatus');

    // service route
    Route::get('/service-rating', 'RatingController@service');
    Route::get('/service-rating-list', 'RatingController@serviceRatingList');
    Route::get('/update-service-status', 'RatingController@updatServiceStatus');

    //order routes
	Route::get('/orders', 'OrderController@index');
    Route::get('/order-list', 'OrderController@orderList');
	Route::get('/view-order/{id}', 'OrderController@orderDetail');
	Route::post('export_order', 'OrderController@exportOrders');
	
    // rating route
    Route::get('/tags', 'TagController@index');
    Route::get('/tags-list', 'TagController@tagList');
    Route::get('/add-tag', 'TagController@addTag');
    Route::post('/save-tag', 'TagController@saveTag');
    Route::get('/edit-tag/{id}', 'TagController@editTag');

    Route::get('all-transactions', 'AdminController@getAllTransaction');
    Route::get('all-transactions-list', 'AdminController@allTransactionsList');
	Route::post('export_transaction', 'AdminController@exportTransaction');
    

    //faq routes
    Route::get('faqs', 'FaqController@index');
    Route::get('faq/list', 'FaqController@list');
    Route::get('faq/create', 'FaqController@create')->name('faq/create');
    Route::post('faq/store', 'FaqController@store');
    Route::get('faq/edit/{page_id}', 'FaqController@edit');
    Route::get('faq/destroy/{id}', 'FaqController@destroy');
    Route::get('update-faq-status', 'FaqController@updateStatus');
	
	//coupon routes
	Route::get('/coupons', 'CouponController@index');
    Route::get('/coupon-list', 'CouponController@couponList');
    Route::get('/add-coupon', 'CouponController@addCoupon');
    Route::post('/save-coupon', 'CouponController@saveCoupon');
    Route::get('/edit-coupon/{id}', 'CouponController@editCoupon');
	Route::get('coupon/destroy/{id}', 'CouponController@destroy');
	
	//withdrawal routes
	Route::get('/withdrawal-request', 'WithdrawalController@index');
	Route::get('/withdrawal-request-list', 'WithdrawalController@requestList');
	Route::get('/view-request/{id}', 'WithdrawalController@requestDetail');
	Route::get('/change-request-status', 'WithdrawalController@changeStatus');
	

});
