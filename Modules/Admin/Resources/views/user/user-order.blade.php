@extends('admin::layouts.inner-master')
@section('content')

<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>User's Order List</h2>
                        <div class="clearfix"></div>
						<h2 style="float: right;">
                            <a class="btn btn-primary" href="{{url('admin/users')}}">Back</a>
                        </h2>
                    </div>
                    <div class="x_content">
                    
                        <div class="filter-list">
                            <div class="">
                                <form method="get" action="" id="payout-filter">
                                <input type="hidden" id="column" value="" name="column">
                                <div class="row">
                                
                                    <div class="col-md-3">
                                        <select class="form-control" onchange="getOrderList()" id="payment_method">
                                            <option value="">Payment Method</option>
                                            <option  value="stripe">Stripe</option>
                                            <option  value="paypal">Paypal</option>
                                        </select>
                                    </div>
                                  
                                    <input type ="hidden" id="sort" name="sort" value="desc"/>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                          <div class="controls">
                                            <div class="input-prepend input-group">
                                                <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                <input type="text" name="date_range" id="filter-date-range" placeholder="Date range" class="form-control" value="" autocomplete="off"/>
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <input class="form-control btn btn-primary" onclick="getOrderList()" type="button" name="filter" value="Filter">
                                    </div>
                                    <div class="col-md-1">
                                    <span class="date-select-btn">
                                        <button onclick="refresh()" class="btn btn-default" type="button">
                                                <i class="fa fa-repeat"></i>
                                       </button>
                                     </span>
                                   </div>
								   	    <?php $url = 'admin/export_order'; ?>
									
                                   </div>
                                </form>
								<form action="{{ url($url) }}" method="post">
									 <div class="row">
									 <div class="col-md-2">
										  @csrf
										  <input type="hidden" name="payment_type" id="ex_payment">
										  <input type="hidden" name="date" id="ex_date">
										  <input type="hidden" name="userId" id="ex_user">
										  <input type="hidden" name="search" id="ex_search">
										  <input type="hidden" name="user_id" id="ex_user_id">
										 <button class="form-control btn btn-success" type ="submit">Export </button>
									 </div>
									 </div>
								</form>
                                
                            </div>
                      
                        </div>
                      
                        <div class="table-responsive"id="payout-list" >
                        <table class="table jambo_table table-striped table-bordered data-table">
                        <thead>
                                <tr class="headings">
                                    <th>S. No.</th>
									<th>Order Id</th>
                                    <th>User Name</th>
                                    <th>Amount</th>
									<th>Service Charge</th>
									<th>Paid Amount</th>
									<th >Discount</th>
									<th >Applied Coupon</th>
                                    <th>TransactionId</th>
                                    <th>Payment Type</th>
                                    <th>Order Date</th>
									<th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<script>

function getOrderList(){
	
     $('#ex_date').val($('#filter-date-range').val());
	 $('#ex_user').val($('#userSearch').val());
	 $('#ex_payment').val($('#payment_method').val());
	 $('#ex_user_id').val("{{request()->get('user_id')}}");
	
     var table = $('.data-table').DataTable({
         processing: true,
         serverSide: true,
         destroy: true,
         oLanguage: {
            sZeroRecords: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sEmptyTable: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sProcessing:'<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
         },
         ajax: {
             url:"{{url('admin/order-list')}}",
             data: function (d) {
                 d.userId = $('#userSearch').val(),
                 d.payment_method = $('#payment_method').val(),
                 d.date_between = $('#filter-date-range').val(),
				 d.user_id = "{{request()->get('user_id')}}"
             }},
         columns: [
             {data: 'DT_RowIndex', name: 'DT_RowIndex'},
             {data: 'orderId', name: 'orderId', searchable: true},
			 {data: 'username', name: 'username', searchable: true},
             {data: 'total', name: 'total', searchable: true},
			 {data: 'fees', name: 'fees', searchable: true},
			 {data: 'paid', name: 'paid', searchable: true},
			 {data: 'discount', name: 'discount', searchable: false},
			 {data: 'coupon_code', name: 'coupon_code', searchable: false},
             {data: 'transaction_id', name: 'transaction_id', searchable: true},
             {data: 'payment_type', name: 'payment_type', searchable: false,orderable:false},
             {data: 'created_at', name: 'created_at', searchable: false},
			 {data: 'status', name: 'status', searchable: false},
             {data: 'action', name: 'action', searchable: false,orderable:false},
         ],
         "order": [[ 1, 'asc' ]]
     });
     table.on( 'order.dt search.dt', function () {
         table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
             cell.innerHTML = i+1;
         } );
     } ).draw();
      
 }

function refresh(){
    $('#payout-filter')[0].reset();
    $("#userSearch").select2("destroy");
    $('#userSearch').select2();
    getOrderList();
}
$(document).ready(function() {
    getOrderList(); 
	 $("input[type='search']").keyup(function(e){
         $('#ex_search').val($(this).val());
        });
});

</script>
@endsection