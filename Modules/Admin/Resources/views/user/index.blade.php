@extends('admin::layouts.inner-master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h1>Users</h1>
                        <h2 style="float: right;">
                            <a class="btn btn-primary" href="{{url('admin/add-user')}}">Add User</a>
                        </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <div class="filter-list">
                            <div class="">
                                <form method="get" action="" id="user_filter">


                                    <div class="row">
                                        <div class="col-md-3">
                                            <select class="form-control" onchange="getUserList()" id="status">
                                                <option value="">Select Status</option>
                                                <option value="active">Active</option>
                                                <option value="inacrive">Inactive</option>
                                            </select>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="control-group">
                                                <div class="controls">
                                                    <div class="input-prepend input-group">
                                                        <span class="add-on input-group-addon"><i
                                                                class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                        <input type="text" name="date_range" id="filter-date-range"
                                                            placeholder="Date range" class="form-control"
                                                            autocomplete="off" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <input class="form-control btn btn-primary" type="button"
                                                onclick="getUserList()" name="filter" value="Filter">
                                        </div>
                                     
                                        <div class="col-md-1">
                                            <span class="date-select-btn">
                                                <button onclick="refreshFilter()" class="btn btn-default" type="button">
                                                    <i class="fa fa-repeat"></i>
                                                </button>
                                            </span>
                                        </div>


                                    </div>

                                </form>

                                <?php $url = 'admin/export_users'.$_SERVER['QUERY_STRING']; ?>
                                <form action="{{ url($url) }}" method="post">
                                <div class="row">
                                 <div class="col-md-2">
                                      @csrf
                                      <input type="hidden" name="status" id="ex_status">
                                      <input type="hidden" name="date" id="ex_date">
                                      <input type="hidden" name="search" id="ex_search">
                                     <button class="form-control btn btn-success" type ="submit">Export </button>
                                 </div>
                                 </div>
                                </form>

                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table jambo_table table-bordered data-table">
                                <thead>
                                    <tr class="headings">
                                        <th>S.No.</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Created Date</th>
                                        <th>Role</th>
										<th>Current Wallet</th>
                                        <th width="100px">Status</th>
										<th width="100px">Total Orders</th>
										<th width="100px">Transactions</th>
                                        <th width="100px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>



<script type="text/javascript">
    $(function () {
        getUserList();
        $("input[type='search']").keyup(function(e){
         $('#ex_search').val($(this).val());
        });

    });

    function getUserList() {

        $('#ex_status').val($('#status').val());
        $('#ex_date').val($('#filter-date-range').val());

        var table = $('.data-table').DataTable({
			
			processing: true,
            serverSide: true,
            destroy: true,
            oLanguage: {
                sZeroRecords: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
                sEmptyTable: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
                sProcessing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            ajax: {
                url: "{{ url('admin/user-list') }}",
                data: function (d) {
                    d.status = $('#status').val(),
                        d.date_between = $('#filter-date-range').val()
                }
            },
            columns: [
			    {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'fullname',
                    name: 'fullname',
                    searchable: true
                },
                {
                    data: 'email',
                    name: 'email',
                    searchable: true
                },
                {
                    data: 'created_at',
                    name: 'created_at',
                    searchable: true
                },
                {
                    data: 'role',
                    name: 'role',
                    searchable: true,
                    orderable: false,
                },
				{
                    data: 'user_wallet',
                    name: 'user_wallet',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'status',
                    name: 'status',
                    orderable: false,
                    searchable: false
                },
				 {
                    data: 'total_orders',
                    name: 'total_orders',
                    orderable: false,
                    searchable: false
                },
				 {
                    data: 'transactions',
                    name: 'transactions',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
         
        });
        table.on('order.dt search.dt', function () {
            table.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    }

    function refreshFilter() {
        $('#user_filter')[0].reset();
        getUserList();
    }

</script>

@endsection
