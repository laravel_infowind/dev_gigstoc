@extends('admin::layouts.inner-master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Edit User</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                        <form method="post" action="{{ url('admin/update-user') }}" id="edit_user" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                                {{ csrf_field() }}
                               <input type="hidden" name="id" value="{{$user->id}}">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fullname">Fullname <span class="required" autocomplete="off">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="plan_title" name="fullname" class="form-control col-md-7 col-xs-12" placeholder="Enter fullname here" value="{{$user->fullname}}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username <span class="required" value="{{$user->fullname}}" autocomplete="off">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="email"  name="username" required="required" class="form-control col-md-7 col-xs-12" placeholder="Enter email here" value="{{$user->username}}" autocomplete="off" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required" value="{{$user->fullname}}" autocomplete="off">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="email"  name="email" required="required" class="form-control col-md-7 col-xs-12" placeholder="Enter email here" value="{{$user->email}}" autocomplete="off" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="assign-role">Assign role <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="role_id"  class="form-control col-md-7 col-xs-12" id="roles">
                                          <?php
                                           $roleId = $user->userInRole[0]->role_id;
                                          ?>
                                          <option value="" >Select Role</option>
                                          <option value="2" @if($roleId == 2) selected="selected" @endif  >User</option>
                                          <option value="3" @if($roleId == 3) selected="selected" @endif >Staff</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Profile Photo <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <img src="{{getUserProfileImage($user->id)}}" width="80px" height="80px">
                                        <input type="file" name="profile_pic">
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                                    <span><button type="button" class="btn btn-success">Submit</button></span>
                                    <span><a href="{{ url('admin/users') }}" class="btn btn-warning">Cancel</a></span>
                                    </div>
                                </div>
                            </form>
                         {!! JsValidator::formRequest('App\Http\Requests\Admin\AddUserRequest','#edit_user') !!}
        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

$("#edit_user .btn-success").on('click   ',function(e) {
    e.preventDefault(); 
    var formData = new FormData($('#edit_user')[0]);
   
    if ($("#edit_user").valid()) {
        $('#blogCategoryFormButton').prop('disabled',true);
        $('#blogCategoryFormLoader').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: "{{url('admin/update-user')}}",
            contentType: false,
            processData: false,
            data: formData, 
            dataType:'json',
            success: function(data) {
                if (data.success) {
                    toastr.success(data.message); 
                    setTimeout(function() {
                        window.location.href = "{{url('admin/users')}}";
                    }, 1000);
                 
                } else {
                    toastr.error(data.message); 
                }
                $('#blogCategoryFormLoader').hide();
                $('#blogCategoryFormButton').prop('disabled',false);
            }
        });
    }
});
</script>
@endsection