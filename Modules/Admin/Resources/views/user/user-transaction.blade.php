@extends('admin::layouts.inner-master')
@section('content')

<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>User's Transactions</h2>
                        <div class="clearfix"></div>
						<h2 style="float: right;">
                            <a class="btn btn-primary" href="{{url('admin/users')}}">Back</a>
                        </h2>
                    </div>
                    <div class="x_content">
                    
                        <div class="filter-list">
                            <div class="">
                                <form method="get" action="" id="filter_form">
                                <input type="hidden" id="column" value="" name="column">
                                <div class="row">
                                    <input type ="hidden" id="sort" name="sort" value="desc"/>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                          <div class="controls">
                                            <div class="input-prepend input-group">
                                                <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                <input type="text" name="date_range" id="filter-date-range" placeholder="Date range" class="form-control" value="" autocomplete="off"/>
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <input class="form-control btn btn-primary" onclick="getPayoutTransactionList()" type="button" name="filter" value="Filter">
                                    </div>
                                    <div class="col-md-1">
                                    <span class="date-select-btn">
                                        <button onclick="refresh()" class="btn btn-default" type="button">
                                                <i class="fa fa-repeat"></i>
                                       </button>
                                     </span>
								   </div>
								   
							
                                   </div>
                                </form>
									    <?php $url = 'admin/export_transaction' ?>
									<form action="{{ url($url) }}" method="post">
									 <div class="row">
									 <div class="col-md-2">
										  @csrf
										  <input type="hidden" name="date" id="ex_date">
										  <input type="hidden" name="userId" id="ex_user">
										  <input type="hidden" name="search" id="ex_search">
										  <input type="hidden" name="user_id" id="ex_search" value="{{request()->get('user_id')}}">
										 <button class="form-control btn btn-success" type ="submit">Export </button>
									 </div>
									 </div>
									</form>
                                
                            </div>
                      
                        </div>
                      
                        <div class="table-responsive"id="payout-list" >
                        <table class="table jambo_table table-striped table-bordered data-table">
                        <thead>
                        <tr class="headings">
                                <th>S. NO.</th>
                                        <th >Payer</th>
                                        <th >Receiver</th>
                                        <th >Recieved Amount
                                        </th>
                                        <th>Order ID</th>
                                        <th>Paid Amount
                                        </th>
                                        <th>Service Charge</th>
                                        <th>Commission</th>
										<th>Admin Earning</th>
                                        <th>Transaction ID</th>
                                        <th>
                                        Created Date 
                                        </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<script>

function getPayoutTransactionList(){
     $('#ex_date').val($('#filter-date-range').val());
	 $('#ex_user').val($('#userSearch').val());
     var table = $('.data-table').DataTable({
         processing: true,
         serverSide: true,
         destroy: true,
         oLanguage: {
            sZeroRecords: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sEmptyTable: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sProcessing:'<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
         },
         ajax: {
             url:"{{ url('admin/all-transactions-list') }}",
             data: function (d) {
                 d.userId = $('#userSearch').val(),
                 d.payment_method = $('#payment_method').val(),
                 d.date_between = $('#filter-date-range').val(),
				 d.user_id = "{{request()->get('user_id')}}"
             }},
         columns: [
             {data: 'DT_RowIndex', name: 'DT_RowIndex'},
             {data: 'payer', name: 'payer', searchable: true},
             {data: 'receiver', name: 'receiver', searchable: true},
             {data: 'user_received_amount', name: 'user_received_amount', searchable: true},
             {data: 'orderId', name: 'orderId', searchable: true},
             {data: 'amount', name: 'amount', searchable: true,orderable:true},
             {data: 'service_chage', name: 'service_chage', searchable: false,orderable:true},
             {data: 'commission', name: 'commission', searchable: false,orderable:true},
			 {data: 'admin_earning', name: 'admin_earning', searchable: false,orderable:true},
             {data: 'transaction_id', name: 'transaction_id', searchable: true},
             {data: 'created_at', name: 'created_at', searchable: false},
         ],
         "order": [[ 1, 'asc' ]]
     });
     table.on( 'order.dt search.dt', function () {
         table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
             cell.innerHTML = i+1;
         } );
     } ).draw();
      
 }

function refresh(){
    $('#filter_form')[0].reset();
    $("#userSearch").select2("destroy");
    $('#userSearch').select2();
    getPayoutTransactionList();
}
$(document).ready(function() {
    getPayoutTransactionList(); 
	 $("input[type='search']").keyup(function(e){
         $('#ex_search').val($(this).val());
     });
});

</script>
@endsection