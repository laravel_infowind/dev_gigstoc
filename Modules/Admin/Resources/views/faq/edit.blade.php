@extends('admin::layouts.inner-master')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Edit FAQ</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form action="{{ url('admin/faq/store') }}" method="post" id="faq_form" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"> Question <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="hidden" name="id" value="{{$faq->id}}">
                                    <input type="text" name="question"  value="{{$faq->question}}" placeholder="Enter Question"  class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"> Answer <span class="required">*</span>
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <textarea  name="answer" rows="5" placeholder="Enter Answer"  class="form-control col-md-7 col-xs-12"  >{{$faq->answer}}</textarea>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                                    <span><button type="button" class="btn btn-success">Submit
                                    <i id="form_loader" class="fa fa-spinner fa-spin" style="display:none"></i>
                                    </button></span>
                                    <span><a href="{{ url('admin/faq') }}" class="btn btn-warning">Cancel</a></span>
                                </div>
                            </div>
                        </form>
                        {!! JsValidator::formRequest('App\Http\Requests\Admin\AddFaqRequest','#faq_form') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>


$("#faq_form .btn-success").on('click',function(e) {
     e.preventDefault(); 
        
        var formData = new FormData($('#faq_form')[0]);
        if ($("#faq_form").valid()) {
            $('.btn-success').prop('disabled',true);
            $('#form_loader').show();
            $.ajax({
            headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: "{{url('admin/faq/store')}}",
            contentType: false,
            processData: false,
            data: formData, 
            dataType:'json',
            success: function(data)
            {
                if (data.success){
                    toastr.success(data.message); 
                    setTimeout(function(){
                        window.location.href = "{{url('admin/faqs')}}";
                    },1000);
                 
                } else {
                    toastr.error(data.message); 
                }
                $('#form_loader').hide();
                $('.btn-success').prop('disabled',false);
            }
            });
        }
});
</script>
@endsection