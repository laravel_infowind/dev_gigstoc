<!DOCTYPE html>
<html lang="en">
    <head>
    
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="{{ asset('public/images/favicon.png') }}" />
        <title>Gigstoc Admin</title>
        <link href="{{url('public/admin/css/bootstrap.min.css')}}" rel="stylesheet" id="bootstrap-css">
        <script src="{{url('public/admin/js/jquery.min.js')}}"></script>
        <script src="{{url('public/admin/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('public/vendor/jsvalidation/js/jsvalidation.js')}}"></script>
        <link href="{{ asset('public/admin/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/admin/css/admin-style.css') }}" type="text/css" rel="stylesheet"/>

    </head>
    <body>
        @yield('content')

        {{-- Laravel Mix - JS File --}}
        {{-- <script src="{{ mix('js/admin.js') }}"></script> --}}

        @include('admin::common.message')
    </body>
</html>


