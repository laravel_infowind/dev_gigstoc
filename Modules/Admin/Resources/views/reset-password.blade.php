@extends('admin::layouts.master')

@section('content')
<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-7 login-sec">
            <div class="login-head-logo">
                <img src=""></a>
            </div>
            <div class="card">
                <div class="card-header">Reset password</div>
                <div class="card-body">
                    <form action="{{url('admin/reset-password')}}" method="POST" id="reset_password_form">
                        @csrf
                        <div class="form-group">
                            <input type="test" name="otp" class="form-control" placeholder="Enter your OTP" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Enter your password" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <input type="password" name="confirm_password" class="form-control" placeholder="Re-enter your password" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-primary" id="reset_password_button">
                            Submit
                            <i id="form_loader" class="fa fa-spinner fa-spin" style="display:none"></i>
                            </button>
                        </div>
                       
                    </form>
                    {!! JsValidator::formRequest('App\Http\Requests\Admin\ResetPasswordRequest','#reset_password_form') !!}
                </div>
            </div>
        </div>
    </div>
</div>

<script>

$("#reset_password_button").on('click',function(e) {
    e.preventDefault(); 
    if ($("#reset_password_form").valid()) {
        $('#reset_password_button').prop('disabled',true);
        $('#form_loader').show();
        $.ajax({
            headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: "{{url('admin/reset-password')}}",
            data: $('#reset_password_form').serialize(), 
            dataType:'json',
            success: function(data){
                if (data.success){
                    toastr.success(data.message); 
                    setTimeout(function(){
                        window.location.href = "{{url('admin')}}";
                    },2000)
                } else {
                    toastr.error(data.message); 
                }
                $('#form_loader').hide();
                $('#reset_password_button').prop('disabled',false);
            }
        });
    }
});

</script>
@endsection
