@extends('admin::layouts.inner-master')
@section('content')
<div class="right_col" role="main">
<div class="">
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Update Profile</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
					
                         <form method="post" action="{{ url('admin/update-profile') }}" id="edit_profile" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="user_id" value="{{ $user_detail->id }}">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Full Name <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="plan_title" name="fullname" class="form-control col-md-7 col-xs-12" placeholder="Enter first name here" autocomplete="off" value="{{ $user_detail->fullname }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="email"  name="email" required="required" class="form-control col-md-7 col-xs-12" placeholder="Enter email here" autocomplete="off" value="{{ $user_detail->email }}" disabled>
                                    </div>
                                </div>
                             
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Profile Photo <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <img src="{{getUserProfileImage($user_detail->id)}}" width="80px" height="80px">
                                        <input type="file" name="profile_pic">
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                                    <span><button id="edit_profile_button" type="submit" class="btn btn-success">Submit
                                    <i id="edit_profile_loader" class="fa fa-spinner fa-spin" style="display:none"></i>
                                    </button></span>
                                    <span><a href="{{ url('admin/dashboard') }}" class="btn btn-warning">Cancel</a></span>
                                    </div>
                                </div>
                            </form>
                            {!! JsValidator::formRequest('App\Http\Requests\Admin\ProfileRequest','#edit_profile') !!}
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
   
   

<script>


$("#edit_profile").submit(function(e) {
     e.preventDefault(); 
        var form = $(this);
        var formData = new FormData(this);
        var url = form.attr('action');
        if ($("#edit_profile").valid()) {
            $('#edit_profile_button').prop('disabled',true);
            $('#edit_profile_loader').show();
            $.ajax({
            headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: url,
            contentType: false,
            processData: false,
            data: formData, 
            dataType:'json',
            success: function(data)
            {
                if (data.success){
                    toastr.success(data.message); 
                    setTimeout(function(){
                        location.reload();
                    },1000);
                 
                } else {
                    toastr.error(data.message); 
                }
                $('#edit_profile_loader').hide();
                $('#edit_profile_button').prop('disabled',false);
            }
            });
        }
});

</script>

  @endsection