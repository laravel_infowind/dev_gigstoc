<footer>
          <div class="pull-right">
            <?php
           $copyright_text =  App\Models\ConfigSetting::where(['meta_key'=>'copyright_text'])->first();
            ?>
            {{($copyright_text)?$copyright_text->meta_value:'-'}}
          </div>
          <div class="clearfix"></div>
 </footer>

 <!-- FastClick -->

 <script src="{{ asset('public/admin/vendors/fastclick/lib/fastclick.js') }}"></script>

<!-- NProgress -->

<script src="{{ asset('public/admin/vendors/nprogress/nprogress.js') }}"></script>

<!-- bootstrap-daterangepicker -->

<script src="{{ asset('public/admin/vendors/moment/min/moment.min.js') }}"></script>

<script src="{{ asset('public/admin/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<!-- bootstrap-datetimepicker -->    

<script src="{{ asset('public/admin/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

<!-- Switchery -->
<script src="{{ asset('public/admin/vendors/switchery/dist/switchery.min.js') }}"></script>

<!-- Chart.js -->

<script src="{{ asset('public/admin/vendors/Chart.js/dist/Chart.min.js') }}"></script>

<!-- gauge.js -->

<script src="{{ asset('public/admin/vendors/gauge.js/dist/gauge.min.js') }}"></script>

<!-- bootstrap-progressbar -->

<script src="{{ asset('public/admin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>

<!-- iCheck -->

<script src="{{ asset('public/admin/vendors/iCheck/icheck.min.js') }}"></script>



<!-- Datatables -->

<script src="{{ asset('public/admin/js/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('public/admin/js/dataTables.bootstrap.min.js') }}"></script>    



<!-- Skycons -->

<script src="{{ asset('public/admin/vendors/skycons/skycons.js') }}"></script>

<!-- Flot -->

<script src="{{ asset('public/admin/vendors/Flot/jquery.flot.js') }}"></script>

<script src="{{ asset('public/admin/vendors/Flot/jquery.flot.pie.js') }}"></script>

<script src="{{ asset('public/admin/vendors/Flot/jquery.flot.time.js') }}"></script>

<script src="{{ asset('public/admin/vendors/Flot/jquery.flot.stack.js') }}"></script>

<script src="{{ asset('public/admin/vendors/Flot/jquery.flot.resize.js') }}"></script>

<!-- Flot plugins -->

<script src="{{ asset('public/admin/vendors/flot.orderbars/js/jquery.flot.orderBars.js') }}"></script>

<script src="{{ asset('public/admin/vendors/flot-spline/js/jquery.flot.spline.min.js') }}"></script>

<script src="{{ asset('public/admin/vendors/flot.curvedlines/curvedLines.js') }}"></script>

<!-- DateJS -->

<script src="{{ asset('public/admin/vendors/DateJS/build/date.js') }}"></script>

<!-- JQVMap -->

<script src="{{ asset('public/admin/vendors/jqvmap/dist/jquery.vmap.js') }}"></script>

<script src="{{ asset('public/admin/vendors/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>

<script src="{{ asset('public/admin/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js') }}"></script>

<!-- bootstrap-daterangepicker -->

<script src="{{ asset('public/admin/vendors/moment/min/moment.min.js') }}"></script>

<script src="{{ asset('public/admin/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>



<!-- Custom Theme Scripts -->

<!-- <script src="{{ asset('public/admin/js/editor.js') }}"></script> -->

<script src="{{ asset('public/admin/js/nicEdit-latest.js') }}"></script>

<script src="{{ asset('public/admin/build/js/custom.min.js') }}"></script>

<script src="{{ asset('public/admin/js/bootstrap-datepicker.min.js') }}"></script>

<script src="{{ asset('public/admin/js/custom.js') }}"></script>

<!-- <script src="{{ asset('public/admin/vendors/jquery/dist/jquery.min.js') }}"></script> -->



<!-- Modal HTML -->
<input type="hidden" id="rowId" name="id" value="">
<div id="deleteModal" class="modal fade">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header">
                              
                <h4 class="modal-title">Are you sure?</h4>  
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <p>Do you really want to delete these records? This process cannot be undone.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                <a type="button" href="javascript:void(0)" onclick="deleteRecord()" class="btn btn-danger user-delete">Delete</a>
            </div>
        </div>
    </div>
</div>

<div id="statusModal" class="modal fade">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="icon-box">
                    <i class="fa fa-times"></i>
                </div>              
                <h4 class="modal-title">Are you sure?</h4>  
                <button type="button" class="close" onclick="resetStatus()" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <p>Do you really want to change these status?.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" onclick="resetStatus()" data-dismiss="modal">Cancel</button>
                <a type="button" href="javascript:void(0)" onclick="changeStatus()" class="btn btn-danger user-delete">Change<i id="form_loader" style="display:none" class="fa fa-spinner fa-spin"></i>
				</a>
            </div>
        </div>
    </div>
</div>

<script>
    
    function deleteRow(id){
        $('#rowId').val(id);
        $('#deleteModal').modal('show');  
    }

   

    function deleteRecord(){
        var  id = $('#rowId').val();
        var table = $('#rowremove'+id).data('table');
        var url = $('#rowremove'+id).data('url');
        $.ajax({
        type: "get",
        url: url+'/'+id,
        dataType:'json',
        success: function(data)
        {
            if (data.success){
                $('#deleteModal').modal('hide');
               toastr.success(data.message);
               $('.'+table).DataTable().clear().draw();
            }
        }
        });
        
    }

    function updateStatus(id){
        $('#rowId').val(id);
        $('#statusModal').modal('show');
    }

    function changeStatus(){
        var  id = $('#rowId').val();
        var url = $('#row'+id).data('url');
        var status = $('#row'+id).data('status');
        var table = $('#row'+id).data('table');
		$('#form_loader').show();
        $.ajax({
        type: "get",
        url: url,
        data: {
            id: id,
            status: status
         },
        dataType:'json',
        success: function(data)
        {
            if (data.success){
                $('#statusModal').modal('hide');
               toastr.success(data.message);
               $('.'+table).DataTable().clear().draw();
			   $('#form_loader').hide();
            }
        }
        });
        
    }

    function resetStatus(){
        var  id = $('#rowId').val();
        var status = $('#row'+id).data('status');
        if(status){
            $('#row'+id).prop('checked',true);
        }else{
            $('#row'+id).prop('checked',false);
        }
    }
    </script>