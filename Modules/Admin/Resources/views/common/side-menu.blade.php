<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-home"></i> Dashboard <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{url('admin/dashboard')}}">dashboard</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-users"></i> Users <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{url('admin/users')}}">users</a></li>
                </ul>
            </li>
            <li>
                <a><i class="fa fa-hdd-o"></i> Home Management <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ url('admin/sliders') }}">slider</a></li>
                    <li><a href="{{ url('admin/testimonial') }}">testimonial</a></li>
                    <li><a href="{{ url('admin/content') }}">content</a></li>
                </ul> 
            </li>
            <li><a><i class="fa fa-sitemap"></i> Categories <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{url('admin/category')}}">categories</a></li>
                </ul>
            </li>
            <li>
              <a><i class="fa fa-clone"></i> Pages <span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                  <li><a href="{{ url('admin/pages') }}">pages</a></li>
              </ul>
            </li>
            <li><a><i class="fa fa-sitemap"></i> Services <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{url('admin/services')}}">services</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-question"></i> FAQs <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{url('admin/faqs')}}">faq</a></li>
                </ul>
            </li>
			<li><a><i class="fa fa-inbox"></i> Job Request <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{url('admin/jobs')}}">job request</a></li>
                </ul>
            </li>
			<!---
            <li><a><i class="fa fa-sitemap"></i> Job Offers <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{url('admin/jobs/offer')}}">job offres</a></li>
                </ul>
            </li>
			-->
            
            <li><a><i class="fa fa-shopping-cart"></i> Orders<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{url('admin/orders')}}">order</a></li>
                </ul>
            </li>
            <li>
                <a>
                    <i class="fa fa-th"></i> Blogs <span class="fa fa-chevron-down"></span>
                </a>
                <ul class="nav child_menu">
                <li><a href="{{url('admin/blog-category')}}">categories </a></li>
                    <li><a href="{{url('admin/blog-post')}}">posts</a></li>
                </ul>
            </li>
            <li>
                <a>
                    <i class="fa fa-star"></i> Ratings <span class="fa fa-chevron-down"></span>
                </a>
                <ul class="nav child_menu">
                    <li><a href="{{url('admin/seller-rating')}}">seller ratings</a></li>
                    <li><a href="{{url('admin/service-rating')}}">service ratings</a></li>
                </ul>
            
            </li>
            <li>
                <a>
                    <i class="fa fa-tags"></i> Tags <span class="fa fa-chevron-down"></span>
                </a>
                <ul class="nav child_menu">
                    <li><a href="{{url('admin/tags')}}">tags</a></li>
                </ul>
            
            </li>
			<li>
                <a>
                    <i class="fa fa-gift"></i> Coupon Management <span class="fa fa-chevron-down"></span>
                </a>
                <ul class="nav child_menu">
                    <li><a href="{{url('admin/coupons')}}">Coupon</a></li>
                </ul>
            
            </li>
			<li>
                <a>
                    <i class="fa fa-files-o"></i> Withdrawal Request <span class="fa fa-chevron-down"></span>
                </a>
                <ul class="nav child_menu">
                    <li><a href="{{url('admin/withdrawal-request')}}">request</a></li>
                </ul>
            </li>
			
			<li><a><i class="fa fa-exchange"></i> Payouts<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{url('admin/payout')}}">payouts</a></li>
                    <li><a href="{{url('admin/payout-transaction')}}">payout transactions</a></li>
                </ul>
            </li>
			
            <li>
                <a>
                    <i class="fa fa-money"></i> All Transactions <span class="fa fa-chevron-down"></span>
                </a>
                <ul class="nav child_menu">
                    <li><a href="{{url('admin/all-transactions')}}">all transactions</a></li>
                </ul>
            </li>
            
        </ul>
    </div>
</div>
