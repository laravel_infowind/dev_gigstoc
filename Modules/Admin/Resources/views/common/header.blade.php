<!DOCTYPE html>
<html lang="en">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ env('APP_NAME') }}</title>
        <!-- Bootstrap -->
        <link rel="shortcut icon" href="{{ asset('public/images/favicon.png') }}" />
        <link href="{{ asset('public/admin/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{ asset('public/admin/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <!-- NProgress -->
        <link href="{{ asset('public/admin/vendors/nprogress/nprogress.css') }}" rel="stylesheet">
        <!-- iCheck -->
        <link href="{{ asset('public/admin/vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet">
        <!-- Datatables -->
        <link href="{{ asset('public/admin/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
        <!-- Switchery -->
        <link href="{{ asset('public/admin/vendors/switchery/dist/switchery.min.css') }}" rel="stylesheet">
        <!-- bootstrap-progressbar -->
        <link href="{{ asset('public/admin/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet">
        <!-- JQVMap -->
        <link href="{{ asset('public/admin/vendors/jqvmap/dist/jqvmap.min.css') }}" rel="stylesheet"/>
        <!-- bootstrap-daterangepicker -->
        <link href="{{ asset('public/admin/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
        <!-- bootstrap-datetimepicker -->
        <link href="{{ asset('public/admin/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="{{ asset('public/admin/build/css/custom.css') }}" rel="stylesheet">
        <!-- Editor css -->
        <link href="{{ asset('public/admin/css/editor.css') }}" type="text/css" rel="stylesheet"/>
        <!-- bootstrap-datepicker -->
        <link href="{{ asset('public/admin/css/bootstrap-datepicker3.css') }}" type="text/css" rel="stylesheet"/>
        <!-- admin-style -->
        <link href="{{ asset('public/admin/css/admin-style.css') }}" type="text/css" rel="stylesheet"/>
        <!-- Multiselect css-->
        <link href="{{ asset('public/admin/css/jquery.multiselect.css') }}" rel="stylesheet" />
        <!-- jQuery -->
        <script src="{{ asset('public/admin/vendors/jquery/dist/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/vendor/jsvalidation/js/jsvalidation.js')}}"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <!-- Bootstrap -->
        <script src="{{ asset('public/admin/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <!-- Multiselect JS -->
        <script src="{{ asset('public/admin/js/jquery.multiselect.js') }}"></script>
        <!---- CKeditor----->
        <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
        <!---- select box------>
        
        <script src="{{ asset('public/admin/dist/js/select2.min.js') }}"></script>
        <link href="{{ asset('public/admin/dist/css/select2.min.css') }}" rel="stylesheet" />
        
        <script type="text/javascript">
            var APP_URL = "{{ url('/admin') }}";
            var APP_TOKEN = "{{ csrf_token() }}";
            var STRIPE_KEY = "{{ env('STRIPE_KEY') }}";
        </script>
  </head>


  