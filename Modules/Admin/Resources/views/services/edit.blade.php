@extends('admin::layouts.inner-master')
@section('content')

<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Edit Service</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <form method="post" action="{{ url('admin/service/update') }}" id="edit_form" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $service->id }}">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Title <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="title" class="form-control col-md-7 col-xs-12" placeholder="Enter title here" autocomplete="off" value="{!! $service->title !!}" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Price <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="price" class="form-control col-md-7 col-xs-12" placeholder="Enter price here" autocomplete="off" value="{!! $service->price !!}" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="deadline">Deadline (Day)<span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="deadline" class="form-control col-md-7 col-xs-12" placeholder="Enter deadline here" autocomplete="off" value="{!! $service->deadline !!}" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category">Category <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="service_category_id" name="category_id" class="form-control col-md-7 col-xs-12">
                                                <option value="">Select Catgory</option>
                                                @foreach($categories as $val)
                                                    <option value="{{ $val->id }}" {{ (!empty($service->category_id) && ($service->category_id == $val->id)) ? 'selected' : ''  }}>{{ $val->name }}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Description <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                      <textarea id="editor1" cols="50" name="description" rows="3"  class="form-control col-md-7 col-xs-12" data-sample-short >{{ $service->description }}</textarea>
                                      <p  id="body_error" style="display:none" class="invalid-feedback alert-danger">The Description field is required.</p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Gallery</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input service-id="{{ $service->id }}" type="file" class="form-control col-md-7 col-xs-12" id="images" name="images[]" multiple/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                    <div id="image_preview" class="col-md-6 col-sm-6 col-xs-12">
                                    @if(!empty($service->images))
                                    @foreach($service->images as $key => $value)
                                        <div class='col-md-3'><img class='img-responsive' src="{{ url('public/images/services/thumbnail/'.$value->thumbnail) }}"><div class="featured_image"><input type='radio' name='is_featured' value="{{ $value->id }}" {{ ($value->is_featured == 1) ? 'checked' : '' }}></div>
                                        <i class='del_img fa fa-remove remove_photo_{{ $value->id }}' onclick='deleteImage({{ $value->id }})'></i><i id="login_loader_{{ $value->id }}" class="fa fa-spinner fa-spin" style="display:none"></i></div>
                                    @endforeach
                                    @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="video">Video</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="video" class="form-control col-md-7 col-xs-12" placeholder="Add link from Youtube, Vimeo or .MP4 " autocomplete="off" value="{!! $service->video !!}" >
                                        <p id="video_link" style="display:none" class="invalid-feedback alert-danger"></p>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-12"></div>
                               
                                <div class="main-wrapper col-md-6 col-sm-6 col-xs-12">

                                    <div class="ex-ser">
                                    <label class="control-label">Extra service</label>
                                    </div>

                                    @php ($count = 0)
                                    @if(!empty($service->extras))
                                    @foreach($service->extras as $key => $value)
                                    <div class="field_wrapper_{{ $count }}">
                                        <div>
                                            <div class="form-group repeat">
                                                <div class="ser-des">
                                                    <input id="{{ $count }}" required="required" class="s_name form-control col-md-7 col-xs-12" type="text" value="{{ $value->service_name }}" name="extras[{{$count}}][service_name]">
                                                    <p style="display:none" class="invalid-feedback alert-danger" id="service_name_{{ $count }}">Enter Extra Service</p>
                                                </div>
                                                <div class="Pr-usd">
                                                    <span>Price ($)</span>
                                                    <input id="{{ $count }}" required="required" class="s_price form-control col-md-7 col-xs-12" type="text" value="{{ $value->price }}" name="extras[{{$count}}][service_price]">
                                                    <p style="display:none" class="invalid-feedback alert-danger" id="service_price_{{ $count }}">Enter Price</p>
                                                </div>
                                                <div class="Pr-plus"></div>
                                                <i class="remove_button fa fa-minus" title="Remove"></i>
                                            </div>
                                        </div>
                                    </div>
                                    @php ($count++)
                                    @endforeach
                                    @endif

                                    <div data-count="{{ $count }}" class="extra_count field_wrapper_{{ $count }}">
                                        <div>
                                            <div class="form-group repeat">
                                                <div class="ser-des">
                                                    <input class="form-control col-md-7 col-xs-12" type="text" name="extras[{{$count}}][service_name]">
                                                </div>
                                                <div class="Pr-usd">
                                                    <span>Price ($)</span>
                                                    <input required="required" class="form-control col-md-7 col-xs-12" type="text" name="extras[{{$count}}][service_price]">
                                                </div>
                                                <div class="Pr-plus">
                                                <i class="add_button fa fa-plus" title="Add More"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category">Tags</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="tag_id[]" multiple="multiple" id="tags" class="form-control col-md-7 col-xs-12">
                                                @foreach($tags as $key => $val)
                                                    <?php $found = findInCollection($service->tags, 'id', $val->id);?>
                                                    <option value="{{ $val->id }}" <?php echo ($found) ? 'selected' : ''; ?>>{{ $val->name }}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_desc">Opening Message
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea cols="50" name="opening_message" rows="2"  class="form-control col-md-7 col-xs-12" data-sample-short >{{ $service->opening_message }}</textarea>
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                                    <span><button type="submit" id="subBtn1" class="btn btn-success">Submit</button></span>
                                    <span><a href="{{ url('admin/services') }}" class="btn btn-warning">Cancel</a></span>
                                    </div>
                                </div>
                            </form>
                            {!! JsValidator::formRequest('App\Http\Requests\Admin\EditServiceRequest','#edit_form') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    var x =  $('.extra_count').data('count'); //Initial field counter is 0
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper_' + x); //Input field wrapper
    var fieldHTML = '<div><div class="form-group repeat"><div class="ser-des"><input class="form-control col-md-7 col-xs-12" type="text" name="extras['+ x +'][' + 'service_name'+']"></div> <div class="Pr-usd"><span>Price ($)</span><input class="form-control col-md-7 col-xs-12" type="text" name="['+ x +'][' + 'service_price'+']"></div><div class="Pr-plus"></div><i class="remove_button fa fa-minus" title="Remove"></i></div><div>'; //New input field html
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });

    //Once remove button is clicked
    $(document).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});

function preview_images()
{
    var total_file=document.getElementById("images").files.length;
    for(var i=0;i<total_file;i++)
    {
        $('#image_preview').append("<div class='col-md-3'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'><a class='remove_photo' href='javascript:void(0);'>Remove</a></div>");
    }
}

$(document).on('click', '.remove_photo', function(e){
    e.preventDefault();
    $(this).parent('div').remove(); //Remove field html
});
</script>

<script>
function deleteImage(id){
    var result = confirm("Are you sure to delete?");
    if(result){
        $('#login_loader_'+id).show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "GET",
            url: "{{ url('admin/service/delete-image') }}/" + id,
            dataType:'json',
            success: function(data) {
                $('#login_loader_'+id).hide();
                if (data.success) {
                    $('.remove_photo_' + id).parent('div').remove();
                    toastr.success(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function(xhr, status, errorThrown) {
                toastr.error("Sorry, there was a problem!");
            }
        });
    }
}
</script>
<script>
$("#edit_form .btn-success").on('click', function(e) {
    e.preventDefault();
    var formData = new FormData($('#edit_form')[0]);
    var editor_data = CKEDITOR.instances.editor1.getData();
    formData.append('description', editor_data);
    var err = 0;
    if (!editor_data) {
        $('#body_error').show();
    } else {
        $('#body_error').hide();
    }

    $('.s_name').each(function() {
        var s_name_id = $(this).attr('id');
        var s_name_value = $(this).val();
        
        if(s_name_value == '') {
            $('#service_name_' + s_name_id).show();
            err++;
        } else {
            $('#service_name_' + s_name_id).hide();
        }
    });

    $('.s_price').each(function() {
        var s_price_id = $(this).attr('id');
        var s_price_value = $(this).val();
        
        if(s_price_value == '') {
            $('#service_price_' + s_price_id).show();
            err++;
        } else {
            $('#service_price_' + s_price_id).hide();
        }
    });

    if ($("#edit_form").valid() && editor_data && (err == 0)) {
        $('.btn-success').prop('disabled',true);
        $('#blogCategoryFormLoader').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: "{{url('admin/service/update')}}",
            contentType: false,
            processData: false,
            data: formData,
            dataType:'json',
            success: function(data) {
                $('#video_link').text('')
                $('#video_link').hide();
                if (data.success) {
                    
                    toastr.success(data.message);
                    setTimeout(function() {
                        window.location.href = "{{url('admin/services')}}";
                    }, 1000);

                } else if(!data.success && data.video_error) {
                    $('#video_link').text(data.message);
                    $('#video_link').show();
                } else {
                    toastr.error(data.message);
                }
                $('#blogCategoryFormLoader').hide();
                $('.btn-success').prop('disabled',false);
            },
            error: function(xhr, status, errorThrown) {
                toastr.error("Sorry, there was a problem!");
            }
        });
    }
});

function rempveError(){
    $('#tag-error').hide();
}

CKEDITOR.on('instanceCreated', function(e) {
        e.editor.on('contentDom', function() {
            e.editor.document.on('keyup', function(event) {
                $("#body_error").hide();
            });
        });
    });
</script>

<script>
$(document).on('change', '#images', function(){
    var service_id = $('#images').attr('service-id');
    var form_data = new FormData();
    var err = 0;
    form_data.append('service_id', service_id);
    // Read selected files
    var totalfiles = document.getElementById('images').files.length;
    for (var index = 0; index < totalfiles; index++) {
        var name = document.getElementById("images").files[index].name;
        form_data.append("images[]", document.getElementById('images').files[index]);

        var ext = name.split('.').pop().toLowerCase();
        if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
        {
            toastr.error("Invalid Image File");
            err++;
        }

        var size = document.getElementById("images").files[index].size;
        if(size > 20000000)
        {
            toastr.error("Image Size should be less than 20 MB");
            err++;
        }
    }

    if (err > 0) {
        return false; 
    } else {
        // AJAX request
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            url: '{{ url("admin/service/image-upload") }}', 
            type: 'post',
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (res) {
                if(res.success) {
                    $.each(res.data, function(k, v) {
                        var src = res.data[k];

                        $('#image_preview').append("<div class='col-md-3'><img class='img-responsive' src='"+ src +"'><div class='featured_image'><input type='radio' name='is_featured' value='"+ k +"'></div><i class='del_img fa fa-remove remove_photo_" + k + "' onclick='deleteImage("+k+")'></i><i id='login_loader_"+ k +"' class='fa fa-spinner fa-spin' style='display:none'></i></div>");
                    });
                } else {
                    $.each(res.message, function(i, val) {
                        toastr.error(val)
                    });
                }
            },
            error: function(xhr, status, errorThrown) {
                toastr.error("Sorry, there was a problem!");
            }
        });
    }
});

</script>
@endsection
