@extends('admin::layouts.inner-master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h1>Job Requests</h1>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <div class="filter-list">
                            <div class="">
                                <form method="get" action="" id="service_filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <select  onchange="getJobList()" class="form-control" id="categories">
                                            </select>
                                        </div>

                                        <div class="col-md-2">
                                            <select class="form-control" id="status">
                                                <option value="">Select Status</option>
                                                <option value="active">Active</option>
                                                <option value="inactive">Inactive</option>
                                            </select>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="control-group">
                                                <div class="controls">
                                                    <div class="input-prepend input-group">
                                                        <span class="add-on input-group-addon"><i
                                                                class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                        <input type="text" name="date_range" id="filter-date-range"
                                                            placeholder="Date range" class="form-control"
                                                            autocomplete="off" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <input class="form-control btn btn-primary" type="button"
                                                onclick="getJobList()" name="filter" value="Filter">
                                        </div>
                             
                                        <div class="col-md-1">
                                            <span class="date-select-btn">
                                                <button onclick="refreshFilter()" class="btn btn-default" type="button">
                                                    <i class="fa fa-repeat"></i>
                                                </button>
                                            </span>
                                        </div>
										  </form>
								  <div class="col-md-2">
                                            <?php $url = 'admin/export_jobs'; ?>
                                           <form action="{{ url($url) }}" method="post">

												  @csrf
												  <input type="hidden" name="date" id="ex_date">
												  <input type="hidden" name="userId" id="ex_user">
												  <input type="hidden" name="search" id="ex_search">
												 <button class="form-control btn btn-success" type ="submit">Export </button>
											</form>
                                 </div>
                                    </div>
                              
								
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered jambo_table data-table">
                                <thead>
                                    <tr class="headings">
                                        <th class="column-title">S.No.</th>
                                        <th class="column-title">Title</th>
                                        <th class="column-title">Budget</th>
                                        <th class="column-title">Deadline (In Days)</th>
                                        <th class="column-title">Category</th>
                                        <th class="column-title">Created By</th>
                                        <th class="column-title">Created Date</th>
										<th class="column-title" width="100px">Total Offers</th>
                                        <th class="column-title" width="100px">Status</th>
                                        <th class="column-title" width="100px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>




<script type="text/javascript">
    $(function () {
        getJobList();
        getCategories()
    });

    function getJobList() {
        var table = $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            oLanguage: {
                sZeroRecords: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
                sEmptyTable: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
                sProcessing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            ajax: {
                url: "{{ url('admin/job/list') }}",
                data: function (d) {
                    d.status = $('#status').val(),
                    d.date_between = $('#filter-date-range').val(),
                    d.category_id = $('#categories').val()
                }
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'title',
                    name: 'title',
                    searchable: true
                },
                {
                    data: 'price',
                    name: 'price',
                    searchable: true
                },
                {
                    data: 'deadline',
                    name: 'deadline',
                    searchable: true
                },
                {
                    data: 'category',
                    name: 'category',
                    searchable: true
                },
                {
                    data: 'created_by',
                    name: 'created_by',
                    searchable: true
                },
                {
                    data: 'created_at',
                    name: 'created_at',
                    searchable: true
                },
				{
                    data: 'offer',
                    name: 'offer',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'status',
                    name: 'status',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ]
        });
        table.on('order.dt search.dt', function () {
            table.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    }

    function refreshFilter()
    {
        $('#service_filter')[0].reset();
        $("#categories").select2("destroy");
        $('#categories').select2();
        getJobList();
    }

    function getCategories()
    {
        $.ajax({
            type: "get",
            url: "{{url('admin/service/categories')}}",
            dataType:'json',
            success: function(data)
            {
                if (data.success){
                    $('#categories').html(data.list);
                }
            }
        });
    }

</script>

@endsection