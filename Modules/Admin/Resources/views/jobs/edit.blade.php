@extends('admin::layouts.inner-master')
@section('content')
<style>
    .form-group.repeat div {
        display: inline-block;
    }
    .Pr-usd {
    position: relative;
}
.Pr-usd span {
    position: absolute;
    left: 6px;
    top: 7px;
    z-index: 12;
}
.Pr-usd input {
    padding-left: 45px;
}
  </style>
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Edit Job</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <form method="post" action="{{ url('admin/job/update') }}" id="edit_form" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $job->id }}">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Title <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="title" class="form-control col-md-7 col-xs-12" placeholder="Enter title here" autocomplete="off" value="{!! $job->title !!}" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Budget <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="budget" class="form-control col-md-7 col-xs-12" placeholder="Enter price here" autocomplete="off" value="{!! $job->budget !!}" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="deadline">Deadline (Day)<span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="deadline" class="form-control col-md-7 col-xs-12" placeholder="Enter deadline here" autocomplete="off" value="{!! $job->deadline !!}" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category">Category <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="category_id"  id="parent_category_list" class="form-control col-md-7 col-xs-12">
                                                <option value="">Select Catgory</option>
                                                @foreach($categories as $val)
                                                    <option value="{{ $val->id }}" {{ (!empty($job->category_id) && ($job->category_id == $val->id)) ? 'selected' : ''  }}>{{ $val->name }}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Description <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                      <textarea id="editor1" cols="50" name="description" rows="3"  class="form-control col-md-7 col-xs-12" data-sample-short >{{ $job->description }}</textarea>
                                      <p  id="body_error" style="display:none" class="invalid-feedback alert-danger">The Description body field is required.</p>
                                    </div>
                                </div>
                    
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category">Tags</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="tag_id[]" multiple="multiple" id="tags" class="form-control col-md-7 col-xs-12">
                                                @foreach($tags as $key => $val)
                                                    <?php $found = findInCollection($job->tags, 'id', $val->id);?>
                                                    <option value="{{ $val->id }}" <?php echo ($found) ? 'selected' : ''; ?>>{{ $val->name }}</option>
                                                @endforeach
                                            </select>
                                            <p  id="tag-error" style="display:none" class="invalid-feedback alert-danger">The tag is required.</p>
                                    </div>
                                </div>
                              
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                                    <span><button type="submit" id="subBtn1" class="btn btn-success">Submit</button></span>
                                    <span><a href="{{ url('admin/jobs') }}" class="btn btn-warning">Cancel</a></span>
                                    </div>
                                </div>
                            </form>
                            {!! JsValidator::formRequest('App\Http\Requests\Admin\EditJobRequest','#edit_form') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$("#edit_form .btn-success").on('click',function(e) {
    e.preventDefault();
    var formData = new FormData($('#edit_form')[0]);
    var editor_data = CKEDITOR.instances.editor1.getData();
    formData.append('description',editor_data);
    
    if(!editor_data){
        $('#body_error').show();
    }else{
        $('#body_error').hide();
    }

  
    if ($("#edit_form").valid() && editor_data) {
        $('.btn-success').prop('disabled',true);
        $('#blogCategoryFormLoader').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: "{{url('admin/job/update')}}",
            contentType: false,
            processData: false,
            data: formData,
            dataType:'json',
            success: function(data) {
                if (data.success) {
                    toastr.success(data.message);
                    setTimeout(function() {
                        window.location.href = "{{url('admin/jobs')}}";
                    }, 1000);

                } else {
                    toastr.error(data.message);
                }
                $('#blogCategoryFormLoader').hide();
                $('.btn-success').prop('disabled',false);
            }
        });
    }
});


CKEDITOR.on('instanceCreated', function(e) {
        e.editor.on('contentDom', function() {
            e.editor.document.on('keyup', function(event) {
                $("#body_error").hide();
            });
        });
    });
</script>

@endsection
