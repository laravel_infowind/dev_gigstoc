@extends('admin::layouts.inner-master')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Edit Page</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form action="{{ route('slider/update') }}" method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$slider->id}}">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"> Title <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="title" value="{{ $slider->title }}" placeholder="Enter Page title"  class="form-control col-md-7 col-xs-12">
                                    @if($errors->has('title'))
                                    <p class="input-error"> {{ $errors->first('title') }} </p>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"> Description <span class="required">*</span>
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <textarea cols="80" id="editor1" name="description" rows="10"  class="form-control col-md-7 col-xs-12" data-sample-short >{{ $slider->description }}</textarea>
                                    @if($errors->has('description'))
                                    <p class="input-error"> {{ $errors->first('description') }} </p>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Slider Image <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    @if($slider->slider_img && $slider->mime_type == 'video/mp4')
                                    <video height="80" width="100" autoplay>
                                        <source src="{{ url('public/images/slider/').'/'.$slider->slider_img}}" type="video/mp4">
                                    </video>
                                    @else
                                    <img src="{{ url('public/images/slider/').'/'.$slider->slider_img}}" height="80px" width="100px">
                                    @endif
                                    <input type="hidden" name="old_img" value="{{ $slider->slider_img}}">
                                    <input type="file" name="slider_img" value="{{ old('title') }}"  class="form-control col-md-7 col-xs-12" accept="image/*">
                                    @if($errors->has('slider_img'))
                                    <p class="input-error"> {{ $errors->first('slider_img') }} </p>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"> <span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <p class="input-error">Recommended size 1366 width * 590 height</p>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                                    <span><button type="submit" class="btn btn-success">Submit</button></span>
                                    <span><a href="{{ url('admin/sliders') }}" class="btn btn-warning">Cancel</a></span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection