@extends('admin::layouts.inner-master')
@section('content')
<div class="right_col" role="main">
<div class="">
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Change Password</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
					
                    <form method="post" id="change_password_form" action="{{ url('admin/update-password') }}" data-parsley-validate class="form-horizontal form-label-left">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="current_password">Current Password <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="password" name="current_password" class="form-control col-md-7 col-xs-12" placeholder="Enter current password here" autocomplete="off">
                                      
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="new_password">New Password <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="password" name="new_password" class="form-control col-md-7 col-xs-12" placeholder="Enter new password here" autocomplete="off" >
                                       
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="confirm_password">Confirm Password <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="password"  name="confirm_password" class="form-control col-md-7 col-xs-12" placeholder="Enter confirm password here" autocomplete="off">
                                       
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                                    <span><button id="change_password_button" type="submit" class="btn btn-success">Submit
                                    <i id="change_password_loader" class="fa fa-spinner fa-spin" style="display:none"></i>
                                    </button></span>
                                    <span><a href="{{ url('admin/dashboard') }}" class="btn btn-warning">Cancel</a></span>
                                    </div>
                                </div>
                            </form>
                            {!! JsValidator::formRequest('App\Http\Requests\Admin\ChangePasswordRequest','#change_password_form') !!}
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
   

<script>



$("#change_password_form").submit(function(e) {

        e.preventDefault(); 
        var form = $(this);
        var url = form.attr('action');
        if ($("#change_password_form").valid()) {
            $('#change_password_button').prop('disabled',true);
            $('#change_password_loader').show();
            $.ajax({
            headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: url,
            data: form.serialize(), 
            dataType:'json',
            success: function(data)
            {
                if (data.success){
                    toastr.success(data.message); 
                    $("#change_password_form")[0].reset();
                } else {
                    toastr.error(data.message); 
                }
                $('#change_password_loader').hide();
                $('#change_password_button').prop('disabled',false);
            }
            });
        }

});

</script>


  @endsection