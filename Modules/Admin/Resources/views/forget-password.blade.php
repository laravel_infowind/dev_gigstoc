@extends('admin::layouts.master')

@section('content')

<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-7 login-sec">
            <div class="login-head-logo">
                <img src=""></a>
            </div>
            <div class="card">
                <div class="card-header">Forget password</div>
                <div class="card-body">
                    <form action="{{url('admin/forget-password')}}" method="POST" id="forget_password_form">
                        {{ csrf_field() }}

                        <div class="form-group row"> 
                            <label for="email" class="col-md-4 col-form-label text-md-right">Email address</label>

                            <div class="col-md-6">
                            <input type="email" name="email" class="form-control" placeholder="Enter your email">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-4">
                                <span>
                                <button type="submit"  class="btn btn-primary forgetPassword">
                                Submit
                                <i id="form_loader" class="fa fa-spinner fa-spin" style="display:none"></i>
                                </button>
                                </span>
                                <span><a href="{{url('admin/login')}}" class="btn btn-warning">Cancel</a></span>
                            </div>  
                           
                        </div>

                    </form>
                    {!! JsValidator::formRequest('App\Http\Requests\Admin\ForGotPasswordRequest','#forget_password_form') !!}
                </div>
            </div>
        </div>
    </div>
</div>

<script>

$("#forget_password_form .forgetPassword").on('click',function(e) {
        e.preventDefault(); 
        if ($("#forget_password_form").valid()) {
            $('#forgetPassword').prop('disabled',true);
            $('#form_loader').show();
            $.ajax({
            headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: "{{url('admin/forget-password')}}",
            data: $('#forget_password_form').serialize(), 
            dataType:'json',
            success: function(data)
            {
                if (data.success){
                    toastr.success(data.message); 
                    $('#forget_password_form')[0].reset();
                } else {
                    toastr.error(data.message); 
                }
                $('#form_loader').hide();
                $('#forgetPassword').prop('disabled',false);
            }
            });
        }

});

</script>
@endsection
