@extends('admin::layouts.inner-master')
@section('content')
    <div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                    <h1>Seller Ratings</h1>
                  
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                       
                        <div class="filter-list">
                            <div class="row">
                                <form method="get" id="filter_form" action="">
                                   <div class="col-md-3">
                                        <select class="form-control" name="name" id="userSearch" onchange="getSellerRatingList()" >
                                            <option value="">Select User</option>
                                            @foreach($users as $row)
                                            <option value="{{ $row->id }}"  >{{ ucfirst($row->fullname)}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <select class="form-control" id="status" >
                                            <option value="">Select Status</option>
                                            <option  value="publish">Publish</option>
                                            <option  value="unpublish">Unpublish</option>
                                        </select>
                                    </div>

                                    <div class="col-md-2">
                                        <input class="form-control btn btn-primary" type="button" name="filter"  onclick="getSellerRatingList()" value="Filter">
                                    </div>
                                    <div class="col-md-1">
                                    <span class="date-select-btn">
                                        <button onclick="refreshFilter()" class="btn btn-default" type="button">
                                                <i class="fa fa-repeat"></i>
                                       </button>
                                     </span>
                                   </div>
                                     
                                </form>
                            </div>
                        </div>

                        <div class="table-responsive">
                        <table class="table jambo_table table-bordered data-table" id="category_table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>From User</th>
                                    <th>To User</th>
                                    <th>Rating</th>
                                    <th>Review</th>
                                    <th>Posted Date</th>
                                    <th width="200px">Publish</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        </div>
                    </div>
                   
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

   
<script type="text/javascript">

  $(function () {
    getSellerRatingList();
  });

  function getSellerRatingList(){

    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        oLanguage: {
            sZeroRecords: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sEmptyTable: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sProcessing:'<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        ajax: {
            url:"{{ url('admin/seller-rating-list') }}",
            data: function (d) {
                d.userId = $('#userSearch').val(),
                d.status = $('#status').val()
            }},
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'fromUser', name: 'fromUser', searchable: true},
            {data: 'toUser', name: 'toUser', searchable: true},
            {data: 'rating', name: 'rating', searchable: true},
            {data: 'review', name: 'review', searchable: true},
            {data: 'created_at', name: 'created_at'},
            {data: 'status', name: 'status', orderable: false,},
        ]
    });
  }


  function refreshFilter(){
    $('#filter_form')[0].reset();
    $("#userSearch").select2("destroy");
    $('#userSearch').select2();
    getSellerRatingList();
    }
    
  </script>

  @endsection