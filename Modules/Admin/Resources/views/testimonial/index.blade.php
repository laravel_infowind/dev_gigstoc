@extends('admin::layouts.inner-master')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Testimonial</h2>
                        <h2 style="float: right;">
                            <a class="btn btn-primary" href="{{ route('testimonial/create') }}">Add Testimonial</a>
                        </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action data-table">
                                <thead>
                                    <tr class="headings">
                                        <th>S.No</th>
                                        <th class="column-title">Author</th>
                                        <th class="column-title">Comment</th>
                                        <th class="column-title">Image</th>
                                        <th class="column-title">Rating</th>
                                        <th class="column-title no-link last"><span class="nobr">Status</span>
                                        <th class="column-title no-link last"><span class="nobr">Action</span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        getSlider();
    });

    function getSlider() {
        var table = $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            oLanguage: {
                sZeroRecords: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
                sEmptyTable: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
                sProcessing:'<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            ajax: {
                url: "{{ url('admin/testimonial/list') }}"
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'author',
                    name: 'author',
                    searchable: true
                },
                {
                    data: 'comment',
                    name: 'comment',
                    searchable: true
                },
                {
                    data: 'image',
                    name: 'image',
                    searchable: true
                },
                {
                    data: 'rating',
                    name: 'rating',
                    searchable: true
                },
                {
                    data: 'status',
                    name: 'status',
                    searchable: false
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
            "order": [
                [1, 'asc']
            ]
        });
        table.on('order.dt search.dt', function () {
            table.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    }

</script>
@endsection