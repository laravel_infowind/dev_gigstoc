@extends('admin::layouts.inner-master')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Edit Testimonial</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form action="{{ url('admin/testimonial/store') }}" method="post" id="testmonial_form" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"> Author <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="hidden" name="id" value="{{$testimonial->id}}">
                                    <input type="text" name="author"  value="{{$testimonial->author}}" placeholder="Enter Page author"  class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"> Comment <span class="required">*</span>
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <textarea  name="comment" rows="5"  class="form-control col-md-7 col-xs-12"  >{{$testimonial->comment}}</textarea>
                                    <p>Maximum upto 200 characters.</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"> Rating <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="rating" class="form-control col-md-7 col-xs-12">
                                            <option value="">Select Rating</option>
                                            <option value="1" @if($testimonial->rating==1)selected="selected" @endif>1</option>
                                            <option value="2" @if($testimonial->rating==2)selected="selected" @endif>2</option>
                                            <option value="3" @if($testimonial->rating==3)selected="selected" @endif>3</option>
                                            <option value="4" @if($testimonial->rating==4)selected="selected" @endif>4</option>
                                            <option value="5" @if($testimonial->rating==5)selected="selected" @endif>5</option>
                                        </select>
                                        </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Image
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php 
                                $url = url('public/images/testimonial/'.$testimonial->image);
                                ?>
                                <img src="{{$url}}" width="80px" height="80px">
                                    <input type="file" name="image"  class="form-control col-md-7 col-xs-12" accept="image/*">
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                                    <span><button type="button" class="btn btn-success">Submit
                                    <i id="form_loader" class="fa fa-spinner fa-spin" style="display:none"></i>
                                    </button></span>
                                    <span><a href="{{ url('admin/testimonial') }}" class="btn btn-warning">Cancel</a></span>
                                </div>
                            </div>
                        </form>
                        {!! JsValidator::formRequest('App\Http\Requests\Admin\AddTestimonialRequest','#testmonial_form') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>


$("#testmonial_form .btn-success").on('click',function(e) {
     e.preventDefault(); 
        
        var formData = new FormData($('#testmonial_form')[0]);
        if ($("#testmonial_form").valid()) {
            $('.btn-success').prop('disabled',true);
            $('#form_loader').show();
            $.ajax({
            headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: "{{url('admin/testimonial/store')}}",
            contentType: false,
            processData: false,
            data: formData, 
            dataType:'json',
            success: function(data)
            {
                if (data.success){
                    toastr.success(data.message); 
                    setTimeout(function(){
                        window.location.href = "{{url('admin/testimonial')}}";
                    },1000);
                 
                } else {
                    toastr.error(data.message); 
                }
                $('#form_loader').hide();
                $('.btn-success').prop('disabled',false);
            }
            });
        }
});
</script>
@endsection