@extends('admin::layouts.inner-master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add Tag</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                        <form method="post" action="{{ url('admin/save-tag') }}" id="add_tag" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                                {{ csrf_field() }}
                               
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required" autocomplete="off">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="plan_title" name="name" class="form-control col-md-7 col-xs-12" placeholder="Enter tag name" autocomplete="off">
                                    </div>
                                </div>
                             
                              
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                                    <span><button type="button" class="btn btn-success">
                                    <i id="form_loader" class="fa fa-spinner fa-spin" style="display:none"></i>
                                    Submit</button></span>
                                    <span><a href="{{ url('admin/tags') }}" class="btn btn-warning">Cancel</a></span>
                                    </div>
                                </div>
                            </form>
                         {!! JsValidator::formRequest('App\Http\Requests\Admin\AddTagRequest','#add_tag') !!}
        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

$("#add_tag .btn-success").on('click   ',function(e) {
    e.preventDefault(); 
    var formData = new FormData($('#add_tag')[0]);
      if ($("#add_tag").valid()) {
        $('.btn-success').prop('disabled',true);
        $('#form_loader').show();
        $.ajax({
            type: "POST",
            url: "{{url('admin/save-tag')}}",
            contentType: false,
            processData: false,
            data: formData, 
            dataType:'json',
            success: function(data) {
                if (data.success) {
                    toastr.success(data.message); 
                    setTimeout(function() {
                        window.location.href = "{{url('admin/tags')}}";
                    }, 1000);
                 
                } else {
                    toastr.error(data.message); 
                }
                $('#form_loader').hide();
                $('.btn-success').prop('disabled',false);
            }
        });
    }
});
</script>
@endsection