@extends('admin::layouts.inner-master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h1>Withdrawal Request</h1>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <div class="filter-list">
                            <div class="">
                                <form method="get" action="" id="service_filter">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <select class="form-control" id="status">
                                                <option value="">Select Status</option>
                                                <option value="pending">Pending</option>
                                                <option value="processing">Processing</option>
												<option value="decline">Declined</option>
                                                <option value="approve">Approved</option>
                                            </select>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="control-group">
                                                <div class="controls">
                                                    <div class="input-prepend input-group">
                                                        <span class="add-on input-group-addon"><i
                                                                class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                        <input type="text" name="date_range" id="filter-date-range"
                                                            placeholder="Date range" class="form-control"
                                                            autocomplete="off" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <input class="form-control btn btn-primary" type="button"
                                                onclick="getWithdrawalList()" name="filter" value="Filter">
                                        </div>
                                        <div class="col-md-1">
                                            <span class="date-select-btn">
                                                <button onclick="refreshFilter()" class="btn btn-default" type="button">
                                                    <i class="fa fa-repeat"></i>
                                                </button>
                                            </span>
                                        </div>


                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered jambo_table data-table">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>UserName</th>
                                        <th>Payment</th>
                                        <th>Amount</th>
										<th>Requested Date</th>
										<th width="100px">Payout</th>
                                        <th width="100px">Action</th>
										<th width="100px">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
<input type="hidden" value="" id="row_id">
<input type="hidden" value="" id="request_type">

<div id="change_status" class="modal fade">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="icon-box">
                    <i class="fa fa-times"></i>
                </div>              
                <h4 class="modal-title">Are you sure?</h4>  
                <button type="button" class="close" onclick="resetStatus()" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <p>Do you really want to change these status?.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" onclick="resetStatus()" data-dismiss="modal">Cancel</button>
                <a type="button" href="javascript:void(0)" onclick="updateRequestStatus()" class="btn btn-danger user-delete save-change">Change</a>
            </div>
        </div>
    </div>
</div>
<div id="processing_status" class="modal fade">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="icon-box">
                    <i class="fa fa-times"></i>
                </div>              
                <h4 class="modal-title">Decline Reason</h4>  
                <button type="button" class="close" onclick="resetStatus()" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
           <form>
		     <div class="modal-body mx-3">
				<div class="md-form mb-5">
				  <label data-error="wrong" data-success="right" for="">Reason</label>
				  <input type="text" name="reason" id="decline_reson" class="form-control require">
				  <span style="display:none" id="reason-error" class="invalid-feedback alert-danger">The reason field is required.</span>
				</div>
			 </div>
			
			<div class="modal-footer">
					<button type="button" class="btn btn-info" onclick="resetStatus()" data-dismiss="modal">Cancel</button>
					<a type="button" href="javascript:void(0)" onclick="updateRequestStatus()" class="btn btn-danger user-delete save-change">Change</a>
			</div>
			</form>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function () {
        getWithdrawalList();
      
    });

    function getWithdrawalList() {
        var table = $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            oLanguage: {
                sZeroRecords: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sEmptyTable: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sProcessing:'<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            ajax: {
                url: "{{ url('admin/withdrawal-request-list') }}",
                data: function (d) {
                    d.status = $('#status').val(),
                    d.date_between = $('#filter-date-range').val()
                }
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'username',
                    name: 'username',
                    searchable: true
                },
                {
                    data: 'selected_method',
                    name: 'selected_method',
                    searchable: true
                },
                {
                    data: 'amount',
                    name: 'amount',
                    searchable: true
                },
				{
                    data: 'created_at',
                    name: 'created_at',
                    searchable: true
                },
				 {
                    data: 'payout',
                    name: 'payout',
                    orderable: false,
                    searchable: false
                },
				 {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
				{
                    data: 'status',
                    name: 'status',
                    searchable: true
                },
               
            ]
        });
        table.on('order.dt search.dt', function () {
            table.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    }

    function refreshFilter()
    {
        $('#service_filter')[0].reset();
        getWithdrawalList();
    }
	
    function changeRequestStatus(type,id){
		$('#row_id').val(id);
		$('#request_type').val(type);
		if(type==2){
		  $('#processing_status').modal('show');  
		}else{
		  $('#change_status').modal('show');  
		}
    }
	
	function updateRequestStatus(){
		var flag = 1;
		if($('#request_type').val() == 2){
			if($('#decline_reson').val()==''){
				$('#reason-error').show();
				var flag = 0;
			}else{
				$('#reason-error').hide();
				var flag = 1;
			}
		}
		$('.save-change').prop('disabled',true);
		var url = "{{url('admin/change-request-status')}}";
		if(flag){
        $.ajax({
			type: "get",
			url: url,
			data:{id:$('#row_id').val(),type:$('#request_type').val(),reason:$('#decline_reson').val()},
			dataType:'json',
			success: function(data)
			{
				   if(data.request.status=='decline'){
					  $('#processing_status').modal('hide');  
					}else{
					  $('#change_status').modal('hide');  
					}
					if (data.success){
					   toastr.success(data.message);
					 }
				   $('.save-change').prop('disabled',false);
				   var status = (data.request.status=='complete')?'Approved':data.request.status;
				   $('#sta_'+data.request.id).text(status);
				   
				   
			}
        });
		}
	}


    

</script>

@endsection