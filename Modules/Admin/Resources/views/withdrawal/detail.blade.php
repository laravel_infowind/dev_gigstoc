@extends('admin::layouts.inner-master')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
               <!--  <h3>Order Detail</h3> -->
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Request Detail</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
						   <h4><strong>Payment Information</strong></h4>
							@if($withdraRequest->selected_method == 'bank')
                            <ul class="stats-overview">
								<?php 
								$accountInfo = (array)json_decode($withdraRequest->paymentMethod->account_info);
								?>
                                <li>
                                    <span class="name"> First name </span>
                                    <span class="value text-success"> {{ $accountInfo['first_name'] }} </span>
                                </li>
                                <li>
                                    <span class="name"> Middle Name </span>
                                    <span class="value text-success"> {{ $accountInfo['middle_name'] }} </span>
                                </li>
								 <li>
                                    <span class="name"> Last Name </span>
                                    <span class="value text-success"> {{ $accountInfo['last_name'] }} </span>
                                </li>
                            </ul>
							 <ul class="stats-overview">
                                <li>
                                    <span class="name"> Bank Name </span>
                                    <span class="value text-success"> {{ $accountInfo['bank_name'] }} </span>
                                </li>
                                <li>
                                    <span class="name"> Swift Code </span>
                                    <span class="value text-success"> {{ $accountInfo['swift_code'] }} </span>
                                </li>
								<li>
                                    <span class="name"> Account Number </span>
                                    <span class="value text-success"> {{ $accountInfo['account_number'] }} </span>
                                </li>
						    </ul>
							@else
							<ul class="stats-overview">
                                <li>
                                    <span class="value text-success">Paypal Email</span>
                                </li>
                                <li>
                                    <span class="name"> {{$withdraRequest->paymentMethod->account_info}} </span>
                                </li>
                            </ul>
							@endif
							</br></br></br>
							<h4><strong>Withdrawal Information</strong></h4>
							<ul class="stats-overview">
                                <li>
                                    <span class="value text-success">User Name</span>
                                </li>
                                <li>
                                    <span class="name"> {{$withdraRequest->user->name}} </span>
                                </li>
                            </ul>
							<ul class="stats-overview">
                                <li>
                                    <span class="value text-success">Amount</span>
                                </li>
                                <li>
                                    <span class="name">${{$withdraRequest->amount}} </span>
                                </li>
                            </ul>
							<ul class="stats-overview">
                                <li>
                                    <span class="value text-success"> Status </span>
                                </li>
                                <li>
                                    <span class="name"> 
									{{ucfirst($withdraRequest->status)}}
									</span>
                                </li>
                            </ul>
							<ul class="stats-overview">
                                <li>
                                    <span class="value text-success"> Requested date </span>
                                </li>
                                <li>
                                    <span class="name"> 
									{{changeDateformat($withdraRequest->created_at)}}
									</span>
                                </li>
                            </ul>
                   
                           </div> 
                        </div>
                       
                </div>
            </div>
        </div>
    </div>
</div>

@endsection