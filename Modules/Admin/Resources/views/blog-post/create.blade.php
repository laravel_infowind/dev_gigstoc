@extends('admin::layouts.inner-master')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add Blog Post</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <form method="post" action="{{url('admin/add-new-post')}}" id="blogPostform" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Title <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="title" class="form-control col-md-7 col-xs-12" placeholder="Enter title here" autocomplete="off" value="{{ old('title') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Short Description <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea cols="50" name="short_description" rows="3"  class="form-control col-md-7 col-xs-12"  data-sample-short >{{ old('short_description') }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Select Category <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" name="categories[]" onchange="rempveError()" multiple="multiple" id="blogCategory">
                                               <?php 
                                               $categorys  =  App\Models\BlogCategory::where('is_active', '1')->get();
                                               ?>
                                               @foreach($categorys as $cat)
                                                    <option value="{{ $cat->id }}">{{$cat->category_name }}</option>
                                               @endforeach
                                    </select>
                                    <p  id="categories-error" style="display:none" class="invalid-feedback alert-danger">The category is required.</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Post Body <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <textarea cols="50" id="editor1" onkeyup="checkBody()" name="post_body" rows="10"  class="form-control col-md-7 col-xs-12" data-sample-short >{{ old('post_body') }}</textarea>
                                        <p  id="body_error" style="display:none" class="invalid-feedback alert-danger">The post body field is required.</p>
                                    </div>
                                     
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Image <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" name="image" class="form-control col-md-7 col-xs-12" placeholder="Enter image here" autocomplete="off" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_title">Meta Title <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="meta_title" class="form-control col-md-7 col-xs-12" placeholder="Enter meta title here" autocomplete="off" value="{{ old('meta_title') }}" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_desc">Meta Description <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea cols="50" name="meta_desc" rows="2"  class="form-control col-md-7 col-xs-12" data-sample-short >{{ old('meta_desc') }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_desc">Meta Keywords <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea cols="50" name="meta_keywords" rows="2"  class="form-control col-md-7 col-xs-12" data-sample-short >{{ old('meta_desc') }}</textarea>
                                    
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                                    <span>
                                        <button type="submit" id="blogPostformButton" class="btn btn-success">
                                        <i id="blogPostformLoader" class="fa fa-spinner fa-spin" style="display:none"></i>
                                        Submit</button>
                                    </span>
                                    <span>
                                        <a href="{{url('admin/blog-post')}}" class="btn btn-warning">Cancel</a>
                                    </span>
                                    </div>
                                </div>
                            </form>
                            {!! JsValidator::formRequest('App\Http\Requests\Admin\AddBlogPostRequest','#blogPostform') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$("#blogPostform").submit(function(e) {
    e.preventDefault();
    var form = $(this);
    var formData = new FormData(this);
    var url = form.attr('action');
    
    if(!$('#blogCategory').val()){
        $('#categories-error').show();
    }else{
        $('#categories-error').hide();
    }
    var editor_data = CKEDITOR.instances.editor1.getData();
    if(!editor_data){
        $('#body_error').show();
    }else{
        $('#body_error').hide();
    }
    if (jQuery("#blogPostform").valid() && $('#editor1').val()) {
        jQuery('#blogPostformButton').prop('disabled',true);
        jQuery('#blogPostformLoader').show();
        jQuery.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: url,
            contentType: false,
            processData: false,
            data: formData, 
            dataType:'json',
            success: function(data) {
                if (data.success) {
                    toastr.success(data.message); 
                    setTimeout(function() {
                        window.location.href = "{{url('admin/blog-post')}}";
                    }, 1000);
                 
                } else {
                    toastr.error(data.message); 
                }
                jQuery('#blogCategoryFormLoader').hide();
                jQuery('#blogCategoryFormButton').prop('disabled',false);
            }
        });
    }
});

function rempveError(){
    $('#categories-error').hide();
}

CKEDITOR.on('instanceCreated', function(e) {
        e.editor.on('contentDom', function() {
            e.editor.document.on('keyup', function(event) {
                $("#body_error").hide();
            });
        });
    });

</script>
@endsection

