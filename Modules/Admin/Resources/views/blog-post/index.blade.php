@extends('admin::layouts.inner-master')
@section('content')
<div class="right_col" role="main">
    <div class="x_panel">
        <div class="x_title">
            <h1>Blog Posts</h1>
        </div>
        <div class="filter-list">
            <div class="row">
            <form action="" method="get" id="filter_form">
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <select class="form-control" name="category" onchange="getBlogPostList()" id="filterBlogCategory">
                            <option value=""  >Select Category </option>
                               @foreach($blog_category as $cat)
                               <option value="{{ $cat->id }}"  >{{$cat->category_name }}</option>
                               @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <select class="form-control" name="status" id="filterStatus">
                                <option value="">Select Status</option>
                                <option value="active">Active</option>
                                <option value="inactive">Inactive</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="control-group">
                        <div class="controls">
                            <div class="input-prepend input-group">
                                <span class="add-on input-group-addon"><i
                                        class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                <input type="text" name="date_range" id="filter-date-range"
                                    placeholder="Date range" class="form-control"
                                    autocomplete="off" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <button type="button" class="form-control btn btn-primary" onclick="getBlogPostList()" id="filterButton">Filter</button>
                    </div>
                </div>
                <div class="col-md-1">
                    <span class="date-select-btn">
                        <button onclick="refreshFilter()" class="btn btn-default" type="button">
                            <i class="fa fa-repeat"></i>
                        </button>
                    </span>
                </div>
            </form>
            </div>
        </div>
        <h2 style="float: right;">
            <a class="btn btn-primary" href="{{url('admin/add-new-post')}}">Add New Post</a>
        </h2>
        <div class="clearfix"></div>
        <table class="table jambo_table table-bordered data-table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Title</th>
                    <th>Image</th>
                    <th>Posted At</th>
                    <th>Added By</th>
                    <th>Categories</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
jQuery(function () {
    getBlogPostList();
});
function getBlogPostList(){
    var table = jQuery('.data-table').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        oLanguage: {
            sZeroRecords: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sEmptyTable: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sProcessing:'<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        ajax: {
                url: "{{ url('admin/blog-post-list') }}",
                data: function (d) {
                    d.status = $('#filterStatus').val(),
                    d.date_between = $('#filter-date-range').val(),
                    d.category = $('#filterBlogCategory').val()
                }
            },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'title', name: 'title', searchable: true},
            {data: 'postImage', name: 'postImage', searchable: false},
            {data: 'createdAt', name: 'createdAt', searchable: false},
            {data: 'postedBy', name: 'postedBy', searchable: false},
            {data: 'categoryNames', name: 'categoryNames', searchable: false},
            {data: 'status', name: 'status' ,orderable: false,},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
}



function refreshFilter() {
    $('#filter_form')[0].reset();
    $("#filterBlogCategory").select2("destroy");
    $('#filterBlogCategory').select2();
    getBlogPostList();
}
</script>

@endsection