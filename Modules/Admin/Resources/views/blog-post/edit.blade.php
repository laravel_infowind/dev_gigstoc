@extends('admin::layouts.inner-master')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Edit Blog Post</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <form method="post" action="{{url('admin/update-blog-post')}}" id="editBlogPostform" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $blogPost->id }}">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Title <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="title" class="form-control col-md-7 col-xs-12" placeholder="Enter title here" autocomplete="off" value="{!! $blogPost->title !!}" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Short Description <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <textarea cols="50" name="short_description" rows="3"  class="form-control col-md-7 col-xs-12" data-sample-short >{{ $blogPost->short_description }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Select Category <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" name="categories[]" multiple="multiple" id="blogCategory">
                                                <?php 
                                                $categorys  =  App\Models\BlogCategory::where('is_active', '1')->get();
                                                ?>
                                                @foreach($categorys as $cat)
                                                <?php $found = App\Models\BlogPostCategory::where(['blog_category_id'=>$cat->id ,'blog_post_id'=>$blogPost->id])->first();
                                                ?>
                                                <option value="{{ $cat->id }}"  @if($found)  selected ="selected" @endif >{{$cat->category_name }}</option>
                                               @endforeach
                                    </select>
                                    </div>
                                </div>
                                 
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Post Body <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                      <textarea cols="50" id="editor1" name="post_body" rows="10"  class="form-control col-md-7 col-xs-12" data-sample-short >{{ $blogPost->post_body }}</textarea>
                                      <p  id="body_error" style="display:none" class="invalid-feedback alert-danger">The post body field is required.</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Image <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" name="image" class="form-control col-md-7 col-xs-12" placeholder="Enter image here" autocomplete="off">
                                        <input type="hidden" name="old_image" value="{{ $blogPost->image }}">
                                        @if (!empty($blogPost->image) && file_exists( public_path() . '/images/blog-post/thumbnail/' . $blogPost->image))    
                                            <img src="{{ url('public/images/blog-post/thumbnail/').'/'.$blogPost->image }}" width="80px" height="80px" alt="">
                                        @endif
                                      
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_title">Meta Title <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="meta_title" class="form-control col-md-7 col-xs-12" placeholder="Enter meta title here" autocomplete="off" value="{{ $blogPost->meta_title }}" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_desc">Meta Description <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea cols="50" name="meta_desc" rows="2"  class="form-control col-md-7 col-xs-12" data-sample-short >{{ $blogPost->meta_desc }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_desc">Meta Keywords <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea cols="50" name="meta_keywords" rows="2"  class="form-control col-md-7 col-xs-12" data-sample-short >{!! $blogPost->meta_keywords !!}</textarea>
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                                        <span>
                                            <button type="submit" id="blogPostformButton" class="btn btn-success">
                                            <i id="blogPostformLoader" class="fa fa-spinner fa-spin" style="display:none"></i>
                                            Update</button>
                                        </span>
                                        <span>
                                            <a href="{{ url('admin/blog-post') }}" class="btn btn-warning">Cancel</a>
                                        </span>
                                    </div>
                                </div>
                            </form>
                            {!! JsValidator::formRequest('App\Http\Requests\Admin\AddBlogPostRequest','#editBlogPostform') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$("#blogPostformButton").on('click',function(e) {
    e.preventDefault();
    var form =$('#editBlogPostform')[0];
    var formData = new FormData($('#editBlogPostform')[0]);
    var editor_data = CKEDITOR.instances.editor1.getData();
    formData.append('post_body',editor_data);
  
    if(!editor_data){
        $('#body_error').show();
    }else{
        $('#body_error').hide();
    }

    if (jQuery("#editBlogPostform").valid()) {
        jQuery('#blogPostformButton').prop('disabled',true);
        jQuery('#blogPostformLoader').show();
        jQuery.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            contentType: false,
            processData: false,
            data: formData, 
            url: "{{url('admin/update-blog-post')}}",
            dataType:'json',
            success: function(data) {
                if (data.success) {
                    toastr.success(data.message); 
                    setTimeout(function() {
                        window.location.href = "{{url('admin/blog-post')}}";
                    }, 1000);
                 
                } else {
                    toastr.error(data.message); 
                }
                jQuery('#blogCategoryFormLoader').hide();
                jQuery('#blogCategoryFormButton').prop('disabled',false);
            }
        });
    }
});

catIDS = jQuery('#catIDS').val();

$(document).ready(function(){
$('#ms-list-1').addClass('ms-options-wrap ms-active ms-has-selections');
});
</script>
@endsection
