@extends('admin.layouts.adminlayouts')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add School</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <form method="post" action="{{ route('setting/store') }}" id="formSubmit" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Meta Key <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="email" id="meta_key" name="meta_key" class="form-control col-md-7 col-xs-12" placeholder="Enter Key" autocomplete="off" value="{{ old('meta_key') }}">
                                        @if($errors->has('meta_key'))
                                            <p class="input-error">{{ $errors->first('meta_key') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Meta Key <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="email" id="meta_key" name="meta_key" class="form-control col-md-7 col-xs-12" placeholder="Enter Key" autocomplete="off" value="{{ old('meta_key') }}">
                                        @if($errors->has('meta_key'))
                                            <p class="input-error">{{ $errors->first('meta_key') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Meta Key <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="email" id="meta_key" name="meta_key" class="form-control col-md-7 col-xs-12" placeholder="Enter Key" autocomplete="off" value="{{ old('meta_key') }}">
                                        @if($errors->has('meta_key'))
                                            <p class="input-error">{{ $errors->first('meta_key') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                                    <span><button type="submit" id="subBtn1" class="btn btn-success">Submit</button></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

