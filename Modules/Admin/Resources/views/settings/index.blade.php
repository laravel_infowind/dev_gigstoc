@extends('admin::layouts.inner-master')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Website Settings</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @if(Session::has('flash_alert_notice'))
                                <div class="alert alert-success alert-dismissable" style="margin:10px">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                    <i class="icon fa fa-check"></i>  
                                    {{ Session::get('flash_alert_notice') }} 
                                </div>
                            @endif
                            <form method="post" action="{{ route('setting/store') }}" id="formSubmit" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Site Email
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="email" id="site_email" name="site_email" class="form-control col-md-7 col-xs-12" autocomplete="off" value="{{ isset($settings->site_email) ? $settings->site_email : old('site_email') }}">
                                        @if($errors->has('site_email'))
                                            <p class="input-error">{{ $errors->first('site_email') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Contact Number
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="contact_number" name="contact_number" class="form-control col-md-7 col-xs-12" autocomplete="off" value="{{ isset($settings->contact_number) ? $settings->contact_number : '' }}">
                                        @if($errors->has('contact_number'))
                                            <p class="input-error">{{ $errors->first('contact_number') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Contact Address
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea type="email" id="contact_address" name="contact_address" class="form-control col-md-7 col-xs-12" autocomplete="off">{{ isset($settings->contact_address) ? $settings->contact_address : old('contact_address') }} </textarea>
                                        @if($errors->has('contact_address'))
                                            <p class="input-error">{{ $errors->first('contact_address') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Facebook URL
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="facebook_url" name="facebook_url" class="form-control col-md-7 col-xs-12" autocomplete="off" value="{{ isset($settings->facebook_url) ? $settings->facebook_url : old('facebook_url') }}">
                                        @if($errors->has('facebook_url'))
                                            <p class="input-error">{{ $errors->first('facebook_url') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Twitter URL
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="twitter_url" name="twitter_url" class="form-control col-md-7 col-xs-12" autocomplete="off" value="{{ isset($settings->twitter_url) ? $settings->twitter_url : old('twitter_url')  }}">
                                        @if($errors->has('twitter_url'))
                                            <p class="input-error">{{ $errors->first('twitter_url') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">LinkedIn URL
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="linkedin_url" name="linkedin_url" class="form-control col-md-7 col-xs-12" autocomplete="off" value="{{ isset($settings->linkedin_url) ? $settings->linkedin_url : '' }}">
                                        @if($errors->has('linkedin_url'))
                                            <p class="input-error">{{ $errors->first('linkedin_url') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Instagram URL
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="instagram_url" name="instagram_url" class="form-control col-md-7 col-xs-12" autocomplete="off" value="{{ isset($settings->instagram_url) ? $settings->instagram_url : old('instagram_url') }}">
                                        @if($errors->has('instagram_url'))
                                            <p class="input-error">{{ $errors->first('instagram_url') }}</p>
                                        @endif
                                    </div>
                                </div>
                                  <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                                        Date format
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="date_formate" class="form-control col-md-7 col-xs-12" autocomplete="off">
                                        <option value="">Select Date Format </option>
                                        <option value="Y-m-d" @if(isset($settings->date_formate) && ($settings->date_formate=='Y-m-d')) selected="selected" @endif >Y-m-d</option>
                                        <option value="m-d-Y" @if(isset($settings->date_formate) && ($settings->date_formate=='m-d-Y')) selected="selected" @endif>m-d-Y</option>
                                        <option value="d-m-Y" @if(isset($settings->date_formate) && ($settings->date_formate=='d-m-Y')) selected="selected" @endif>d-m-Y</option>
										<option value="F d, Y" @if(isset($settings->date_formate) && ($settings->date_formate=='F d, Y')) selected="selected" @endif>F d, Y</option>
                                        </select>
                                        @if($errors->has('date_format'))
                                            <p class="input-error">{{ $errors->first('date_format') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Copyright Text
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="copyright_text" name="copyright_text" class="form-control col-md-7 col-xs-12" autocomplete="off" value="{{ isset($settings->copyright_text) ? $settings->copyright_text : old('copyright_text')  }}">
                                        @if($errors->has('copyright_text'))
                                            <p class="input-error">{{ $errors->first('copyright_text') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Site Logo
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <?php
                                        if (isset($settings->site_logo)) {
                                            $logo = url('public/images/setting'). '/' .$settings->site_logo;
                                            echo '<img src="'.$logo.'" width="100px" height="100px">';
                                            echo '<input type="hidden" name="site_logo" value="'.$settings->site_logo.'">';
                                        }
                                        ?>
                                        <input type="file" id="site_logo" name="site_logo" class="form-control col-md-7 col-xs-12" autocomplete="off" value="{{ isset($settings->site_logo) ? $settings->site_logo : ''  }}">
                                        <p style="color: #ff0000">Recommended Logo Size : 244 width * 93 height</p>
                                        @if($errors->has('site_logo'))
                                            <p class="input-error">{{ $errors->first('site_logo') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Cookie Privacy Policy
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea id="cookie_privacy_policy" name="cookie_privacy_policy" class="form-control col-md-7 col-xs-12" rows="4" cols="50" autocomplete="off">{!! isset($settings->cookie_privacy_policy) ? trim($settings->cookie_privacy_policy) : old('cookie_privacy_policy')  !!}</textarea>
                                        @if($errors->has('cookie_privacy_policy'))
                                            <p class="input-error">{{ $errors->first('cookie_privacy_policy') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Serivce Charge In(%)
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="service_charge" name="service_charge" class="form-control col-md-7 col-xs-12" autocomplete="off" value="{{ isset($settings->service_charge) ? $settings->service_charge : old('service_charge')  }}">
                                        @if($errors->has('service_charge'))
                                            <p class="input-error">{{ $errors->first('service_charge') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Commission Rate In(%)
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="commission_rate" name="commission_rate" class="form-control col-md-7 col-xs-12" autocomplete="off" value="{{ isset($settings->commission_rate) ? $settings->commission_rate : old('commission_rate') }}">
                                        @if($errors->has('commission_rate'))
                                            <p class="input-error">{{ $errors->first('commission_rate') }}</p>
                                        @endif
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Minimum Withdrawal Amount
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="" name="minimum_withdrawal_amount" class="form-control col-md-7 col-xs-12" autocomplete="off" value="{{ isset($settings->minimum_withdrawal_amount) ? $settings->minimum_withdrawal_amount : old('minimum_withdrawal_amount') }}">
                                        @if($errors->has('minimum_withdrawal_amount'))
                                            <p class="input-error">{{ $errors->first('minimum_withdrawal_amount') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Banned Word List Chat
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea id="filter_bad_words" name="filter_bad_words" rows="5" cols="15" class="form-control col-md-7 col-xs-12" autocomplete="off">{{ isset($settings->filter_bad_words) ? $settings->filter_bad_words : old('filter_bad_words') }} </textarea>
                                        <p>Each word is separated by comma (,). E.g. foo, boo, too.</p>
                                        @if($errors->has('filter_bad_words'))
                                            <p class="input-error">{{ $errors->first('filter_bad_words') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Replace Bad Word
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="replace_bad_word" name="replace_bad_word" class="form-control col-md-7 col-xs-12" autocomplete="off" value="{{ isset($settings->replace_bad_word) ? $settings->replace_bad_word : old('replace_bad_word') }}">
                                        <p>Give a replacement word for all bad words.</p>
                                        @if($errors->has('replace_bad_word'))
                                            <p class="input-error">{{ $errors->first('replace_bad_word') }}</p>
                                        @endif
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Contact Address Frame
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="replace_bad_word" name="contact_address_frame" class="form-control col-md-7 col-xs-12" autocomplete="off" value="{{ isset($settings->contact_address_frame) ? $settings->contact_address_frame : old('contact_address_frame') }}">
                                        @if($errors->has('contact_address_frame'))
                                            <p class="input-error">{{ $errors->first('contact_address_frame') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                                    <span><button type="submit" id="subBtn1" class="btn btn-success">Save Settings</button></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
