@extends('admin::layouts.inner-master')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
               <!--  <h3>Order Detail</h3> -->
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Order Detail</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <ul class="stats-overview">
                                <li>
                                    <span class="name"> Order Number </span>
                                    <span class="value text-success"> #{{ $orders->order_id }} </span>
                                </li>
                                <li>
                                    <span class="name"> Transaction Id </span>
                                    <span class="value text-success"> {{ $orders->transaction_id }} </span>
                                </li>
                            </ul>
							 <ul class="stats-overview">
                                <li>
                                    <span class="name"> Price </span>
                                    <span class="value text-success"> ${{ $orders->subtotal }} </span>
                                </li>
                                <li>
                                    <span class="name"> Service Charge </span>
                                    <span class="value text-success"> ${{ $orders->tax }} </span>
                                </li>
								<li>
                                    <span class="name"> Total </span>
                                    <span class="value text-success"> ${{ $orders->total }} </span>
                                </li>
                            </ul>
							<ul class="stats-overview">
                                <li>
                                    <span class="value text-success">Discount</span>
                                </li>
                                <li>
                                    <span class="name"> {{'$'.$orders->discount}} </span>
                                </li>
                            </ul>
							<ul class="stats-overview">
                                <li>
                                    <span class="value text-success">Applied Coupon</span>
                                </li>
                                <li>
                                    <span class="name"> {{($orders->orderCoupon)?$orders->orderCoupon->coupon->coupon:'-'}} </span>
                                </li>
                            </ul>
							<ul class="stats-overview">
                                <li>
                                    <span class="value text-success">Service Title</span>
                                </li>
                                <li>
                                    <span class="name"> {{($orders->detail->service)?$orders->detail->service->title:''}} </span>
                                </li>
                            </ul>
							<!-- <ul class="stats-overview">
                                <li>
                                    <span class="value text-success">Order Detail</span>
                                </li>
                                <li>
                                    <span class="name"> {!!($orders->detail->service)?$orders->detail->service->opening_message:''!!} </span>
                                </li>
                            </ul> -->
							<ul class="stats-overview">
                                <li>
                                    <span class="value text-success"> Description </span>
                                </li>
                                <li>
                                    <span class="name"> {!! ($orders->detail->service)?$orders->detail->service->description:'' !!} </span>
                                </li>
                            </ul>
							@if(!empty($order->detail->extra_services) && count($order->detail->extra_services)>0)
							 <ul class="stats-overview">
                                <li>
                                    <span class="value text-success"> Extra Service </span>
                                </li>
								@foreach(json_decode($order->detail->extra_services) as $extra)
                                <li>
								<?php 
								$extraService = \App\Models\ExtraInService::where(['id'=>$extra->id])->first();
								?>
                                    <span class="name"> Name </span>
                                    <span class="value text-success"> {{ $extraService->name }} </span>
                                </li>
								<li>
                                    <span class="name"> Price </span>
                                    <span class="value text-success"> {{ $extra->name }} </span>
                                </li>
								@endforeach
							</ul>
							@endif
							
							<ul class="stats-overview">
                                <li>
                                    <span class="value text-success"> Time of Delivery </span>
                                </li>
                                <li>
                                    <span class="name"> {{($orders->detail->service)?$orders->detail->service->deadline:''}} Days</span>
                                </li>
                            </ul>
							<ul class="stats-overview">
                                <li>
                                    <span class="value text-success"> Status </span>
                                </li>
                                <li>
                                    <span class="name"> 
									<?php 
									
									if($orders->status == 0){
										$status = 'Pending';
									}elseif($orders->status == 1){
										$status = 'Complete';
									}else{
										$status = 'Cancelled';
									}
									echo $status;
									?>
									</span>
                                </li>
                            </ul>
					        <ul class="stats-overview">
                                <li>
                                    <span class="value text-success"> Order Date </span>
                                </li>
                                <li>
                                    <span class="name"> 
									{{changeDateformat($orders->created_at)}}
									</span>
                                </li>
                            </ul>
                           </div> 
                        </div>
                       
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
@endsection