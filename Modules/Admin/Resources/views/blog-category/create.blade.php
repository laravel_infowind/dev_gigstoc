@extends('admin::layouts.inner-master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add Category</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
        <form method="post" action="{{url('admin/add-blog-category')}}" id="categoryForm" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category_name"> Category Name <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="plan_title" name="category_name" class="form-control col-md-7 col-xs-12" placeholder="Enter Name here" autocomplete="off" value="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12"> Category Description <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea placeholder="Enter Description here" cols="50" name="description" rows="3"  class="form-control col-md-7 col-xs-12" data-sample-short ></textarea>
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                    <span>
                        <button type="submit" id="blogCategoryFormButton" class="btn btn-success">
                        <i id="blogCategoryFormLoader" class="fa fa-spinner fa-spin" style="display:none"></i>
                        Submit</button>
                    </span>
                    <span>
                        <a href="{{ url('admin/blog-category') }}" class="btn btn-warning">Cancel</a>
                    </span>
                </div>
            </div>
        </form>
        {!! JsValidator::formRequest('App\Http\Requests\Admin\AddBlogCategoryRequest','#categoryForm') !!}
        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

$("#categoryForm").submit(function(e) {
    e.preventDefault(); 
    var form = $(this);
    var formData = new FormData(this);
    var url = form.attr('action');
    if ($("#categoryForm").valid()) {
        $('#blogCategoryFormButton').prop('disabled',true);
        $('#blogCategoryFormLoader').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: url,
            contentType: false,
            processData: false,
            data: formData, 
            dataType:'json',
            success: function(data) {
                if (data.success) {
                    toastr.success(data.message); 
                    setTimeout(function() {
                        window.location.href = "{{url('admin/blog-category')}}";
                    }, 1000);
                 
                } else {
                    toastr.error(data.message); 
                }
                $('#blogCategoryFormLoader').hide();
                $('#blogCategoryFormButton').prop('disabled',false);
            }
        });
    }
});
</script>
@endsection