@extends('admin::layouts.inner-master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add Category</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
        <form method="post" action="{{url('admin/update-blog-category')}}" id="edit_Category_Form" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Category Name <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="hidden" value="{{ $category->id }}" name="id">
                    <input type="text" id="plan_title" name="category_name" class="form-control col-md-7 col-xs-12" placeholder="Enter Name here" autocomplete="off" value="{{$category->category_name}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12"> Category Description <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea placeholder="Enter Description here" cols="50" name="description" rows="3"  class="form-control col-md-7 col-xs-12" data-sample-short >{{$category->category_description}}</textarea>
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                <span><button type="submit" id="edit_Category_Form_Button" class="btn btn-success">
                 <i id="categoryFormLoader" class="fa fa-spinner fa-spin" style="display:none"></i>
                Update</button></span>
                <span><a href="{{ url('admin/blog-category') }}" class="btn btn-warning">Cancel</a></span>
                </div>
            </div>
          </form>
          {!! JsValidator::formRequest('App\Http\Requests\Admin\AddBlogCategoryRequest','#edit_Category_Form') !!}
          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
   <?php 
   $categoryId = ($category->parent_cat_id != 0)?$category->parent_cat_id:$category->id;
   ?>
<script>

$("#edit_Category_Form").submit(function(e) {
    e.stopImmediatePropagation(); 
    var form = $(this);
    var formData = new FormData(this);
    var url = form.attr('action');
    if ($("#edit_Category_Form").valid()) {
        $('#edit_Category_Form_Button').prop('disabled',true);
        $('#categoryFormLoader').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: url,
            contentType: false,
            processData: false,
            data: formData, 
            dataType:'json',
            success: function(data)
            {
                if (data.success){
                    toastr.success(data.message); 
                    setTimeout(function(){
                        window.location.href = "{{url('admin/blog-category')}}";
                    },1000);
                 
                } else {
                    toastr.error(data.message); 
                }
                jQuery('#categoryFormLoader').hide();
                jQuery('#edit_Category_Form_Button').prop('disabled',false);
            }
        });
        return false;
    }
});
</script>
@endsection