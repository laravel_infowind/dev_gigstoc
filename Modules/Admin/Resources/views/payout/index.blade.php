@extends('admin::layouts.inner-master')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Payouts</h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_items">
                        <ul>
                            <li>
                                <h3>$ <span id="paypal-balance"><i class="fa fa-spinner fa-spin total-avail-amount" style="display:none"></i></span></h3>
                                <p>Paypal Available Balance:</p>
                            </li>
                            <li>
                                <h3>$ <span id="paypal-transferring">0<i id="paypal-transferring-amount" class="fa fa-spinner fa-spin" style="display:none"></i></span></h3>
                                <p>Paypal Transferring Amount:</p>
                            </li>
                        </ul>
                    </div>

                    <div class="x_content">
                     
                        <div class="filter-list">
                            <div class="">
                                <div class="row">
								<form method="get" action="" id="payout-filter">
                                    <div class="col-md-3">
                                        <select class="form-control" name="name" id="userSearch" onchange="getPayoutList()" >
                                            <option value="">Select User</option>
                                            @foreach($users as $row)
                                            <option value="{{ $row->id }}" <?php echo (isset($_GET['posted_by']) && $_GET['posted_by'] == $row->id) ? 'selected' : ''; ?> >{{ ucfirst($row->fullname)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <input type ="hidden" id="sort" name="sort" value="desc"/>
                                    <div class="col-md-3">
                                        <input class="form-control btn btn-primary" onclick="getPayoutList()" type="button" name="filter" value="Filter">
                                    </div>
                                    <div class="col-md-1">
                                    <span class="date-select-btn">
                                        <button onclick="refresh()" class="btn btn-default" type="button">
                                                <i class="fa fa-repeat"></i>
                                       </button>
                                     </span>
                                   </div>
								    </form>
									 <div class="col-md-3">
									<button id="payout-button" onclick="payout()" class="form-control btn btn-primary">
									Pay
									<i id="payout-loader" class="fa fa-spinner fa-spin" style="display:none"></i>
										   </button>
									</div>
                                   </div>
                              
                        </div>
                      </div>
                        <div class="table-responsive"id="payout-list" >
                        <table class="table jambo_table table-bordered data-table">
                        <thead>
                                <tr class="headings">
                                <th>S. No.</th>
                                    <th>
                                    <div class="dropdown select-tab-div">
                                        <button type="button" class="dropdown-toggle" data-toggle="dropdown" role="button" title="Select">Select</button>
                                        <div class="dropdown-menu">
                                            <ul>
                                                <li>
                                                    <label>
                                                        <input type="checkbox" name="all" id="checkall" />
                                                        <span>Check All</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="checkbox" name="all" id="checkallcurrentpage" /><span>Check All current page</span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    </th>
                                   
                                    <th class="column-title">User Name</th>
                                    <th class="column-title">
                                    Amount
                                            <a href="javascript:void(0)"> <span class="count_top_icon"><i onclick="getPayoutList()" class="fa fa-sort"></i></span></a>
                                    </th>
                                    <th class="column-title">Prefered method</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>


function getTotalBalance(){
    $('.total-avail-amount').show();
    $.ajax(
        {
        url: "{{url('admin/get-total-balance')}}",
        dataType:'json',
        success: function(response){
            $('.total-avail-amount').hide();
            if(response.success){
                $("#stripe-balance").text(response.data.stripe_balance);
                $("#paypal-balance").text(response.data.paypal_balance);
            }else{
                $("#error-msg").css("display","block");
                $("#error-msg strong").text(response.message);
            }
        }});
}

function getPayoutList(){
     
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        oLanguage: {
            sZeroRecords: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sEmptyTable: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sProcessing:'<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        ajax: {
            url:"{{ url('admin/payout-list') }}",
            data: function (d) {
                d.userId = $('#userSearch').val(),
                d.payment_method = $('#payment_method').val(),
				d.user_id = "{{$user_id}}"
            }},
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'checkbox', name: 'checkbox', searchable: false,orderable: false},
            {data: 'usename', name: 'usename', searchable: true},
            {data: 'amount', name: 'amount', searchable: true},
            {data: 'preferred_method', name: 'preferred_method', searchable: true},
        ]
    });
    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
     
}


        var ids = [];
        var payOutFlage = '';
        var type = '';

// to send payout amount to all user 
    $('#checkall').change(function () {
        $('.cb-element').prop('checked',this.checked);
        ids = [];
        $('#checkallcurrentpage').prop('checked',false);
        
        ids = getUsers();
        type = (ids.lenght!=0)?'partial':'all';
        if($('#checkall').prop('checked')==true){
            type = 'all';
        }
        ids = [];
        getTransferringAmount(ids,type);
    });
// to send payout amount to current page user
    $('#checkallcurrentpage').change(function () {
        ids = [];
        $('.cb-element').prop('checked',this.checked);
        $('#checkall').prop('checked',false);
         ids = getUsers();
         type = 'partial';
        getTransferringAmount(ids,type);
    });

// to remove element from array
    function removeFromArray(id){
        if(!ids.includes(id)){
            ids.push(id);
        } else {
        if($('#user_'+id).prop('checked')==false){
            const index = ids.indexOf(id);
            if (index > -1) {
                ids.splice(index, 1);
            }
         }
        }
        ids = [];
        if ($('.cb-element:checked').length == $('.cb-element').length){
            $('#checkallcurrentpage').prop('checked',true);
            type = 'partial';
        }
        else {
            $('#checkall').prop('checked',false);
            $('#checkallcurrentpage').prop('checked',false);
        }
        ids = getUsers();
        type = (ids.lenght!=0)?'partial':'all';
        getTransferringAmount(ids,type);
        console.log(ids);
    }
// to user's array
    function getUsers(){
        $('.cb-element').each(function(i){
            if($(this).prop('checked')==true){
                ids[i] = $(this).val();
            }
        });

        return ids = ids.filter( onlyUnique );;
    }
// to remove duplicate element
    function onlyUnique(value, index, self) { 
            return self.indexOf(value) === index;
    }
// to transfer payout amount


   function payout(){
            var show_erro = '';
            var stripe_total =  $('#stripe-balance').text();
            var paypal_total =  $('#paypal-balance').text();

            var stripe_transferring_amount =  $('#stripe-transferring').text();
            var paypal_transferring_amount =  $('#paypal-transferring').text();
  
            $('#payout-button').prop('disabled',true);
            if(parseInt(stripe_transferring_amount)>parseInt(stripe_total) || parseInt(paypal_transferring_amount)>parseInt(paypal_total)){
                show_erro = 'yes';
                toastr.error('you have insufficient amount.');
            }
            $('.loader-overlay').css("display", "block");
            console.log(ids.length+'02');
            console.log(type);
            if((ids.length == 0 && type == 'partial')||(type == '')){
                show_erro = 'yes';
                toastr.error('Please select user first.');
            }
            if(($('#paypal-transferring').text()==0) && ($('#stripe-transferring').text()==0 && ids.length != 0)){
                show_erro = 'yes';
                toastr.error('Nothing to pay please select paying user.');
            }
            console.log(show_erro);
            console.log(ids);
            if(show_erro!=''){
                $('#payout-button').prop('disabled',false);
            }else{
                $('#payout-loader').show();
               $.ajax({
                type: "GET",
                url: "{{url('admin/send-payout')}}",
                data: {ids:ids,type:type},
                cache: false,
                success: function(res){
                            res = JSON.parse(res);
                            $('#payout-button').prop('disabled',false);
                            $('.loader-overlay').css("display", "none");
                            if(res.success){
                                toastr.success(res.message);
                            $('.cb-element').prop('checked',false);
                            }else{
                                toastr.error(res.message);
                            }
                            $('input[type="checkbox"]').prop('checked',false);
                            setTimeout(function(){
                                window.location.reload();
                            },5000)
                            $('#payout-loader').hide();
                        
                    }
                })
            }
            
        }
// to get total transferring amount
        function getTransferringAmount(ids,type){
            console.log(ids);
            $("#stripe-transferring").html("<i class='fa fa-spinner fa-spin'></i>");
            $("#paypal-transferring").html("<i class='fa fa-spinner fa-spin'></i>");
            $.ajax(
                {
                url: "{{url('admin/get-transferring-amount')}}",
                dataType:'json',
                data:{ids:ids,type:type},
                success: function(response){
                    if(response.success){
                        setTimeout(function(){
                        $("#stripe-transferring").text('');
                        $("#paypal-transferring").text('');
                        $("#stripe-transferring").text(response.data.stripe_amount);
                        $("#paypal-transferring").text(response.data.paypal_amount);
                        },1000);
                    }else{
                        $("#error-msg").css("display","block");
                        $("#error-msg strong").text(response.message);
                    }
                }});   
        }

function refresh(){
    $('#payout-filter')[0].reset();
   window.location = "{{url('admin/payout')}}";
}

$(document).ready(function() {
    getPayoutList(); 
    getTotalBalance();
});

</script>
@endsection