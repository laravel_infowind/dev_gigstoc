@extends('admin::layouts.inner-master')
@section('content')

<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>User Transactions</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if(Session::has('success'))
                        <div class="alert alert-success" >
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        {{ Session::get('success') }}</div>
                        @endif
                        <div class="alert alert-success alert-dismissible" id="msg" style="display: none">
                        <strong></strong>
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </div>
                        <div class="alert alert-danger alert-dismissible" id="error-msg" style="display: none">
                        <strong></strong>
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </div>
                        <br />
                      
                        <div class="table-responsive"id="transaction-list" >
                        <table class="table jambo_table table-striped table-bordered data-table">
                        <thead>
                                <tr class="headings">
                                    <th>S. No.</th>
                                    <th>User Name</th>
                                    <th>Amount</th>
                                    <th>TransactionId</th>
                                    <th>Payment Type</th>
                                    <th>Payment Date</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<script>


$(document).ready(function() {
    getUserTransactionList(); 
});

function getUserTransactionList(){
     
     var table = $('.data-table').DataTable({
         processing: true,
         serverSide: true,
         destroy: true,
         oLanguage: {
            sZeroRecords: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sEmptyTable: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sProcessing:'<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
         },
         ajax: {
             url:"{{ url('admin/payout-user-transaction-list') }}",
             data: function (d) {
                 d.userId = $('#userSearch').val(),
                 d.payment_method = $('#payment_method').val(),
                 d.date_between = $('#filter-date-range').val(),
                 d.user_id= "{{$user_id}}"
             }},
         columns: [
             {data: 'DT_RowIndex', name: 'DT_RowIndex'},
             {data: 'usename', name: 'usename', searchable: true},
             {data: 'amount', name: 'amount', searchable: true},
             {data: 'transaction_id', name: 'transaction_id', searchable: true},
             {data: 'payment_type', name: 'payment_type', searchable: false},
             {data: 'created_at', name: 'created_at', searchable: false},
         ],
         "order": [[ 1, 'asc' ]]
     });
     table.on( 'order.dt search.dt', function () {
         table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
             cell.innerHTML = i+1;
         } );
     } ).draw();
      
 }

function refresh(){
    $('#payout-filter')[0].reset();
    $("#userSearch").select2("destroy");
    $('#userSearch').select2();
    getPayoutTransactionList();
}

</script>
@endsection