@extends('admin::layouts.inner-master')
@section('content')

<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Payouts Transactions</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    
                        <div class="filter-list">
                            <div class="">
                                <form method="get" action="" id="payout-filter">
                                <input type="hidden" id="column" value="" name="column">
                                <div class="row">
                                    <div class="col-md-3">
                                        <select class="form-control" name="name" id="userSearch" onchange="getPayoutTransactionList()" >
                                            <option value="">Select User</option>
                                            @foreach($users as $row)
                                            <option value="{{ $row->id }}"  >{{ ucfirst($row->fullname) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="form-control" onchange="getPayoutTransactionList()" id="payment_method">
                                            <option value="">Payment Method</option>
                                            <option  value="stripe">Stripe</option>
                                            <option  value="paypal">Paypal</option>
                                        </select>
                                    </div>
                                  
                                    <input type ="hidden" id="sort" name="sort" value="desc"/>
                                    <div class="col-md-3">
                                        <div class="control-group">
                                          <div class="controls">
                                            <div class="input-prepend input-group">
                                                <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                <input type="text" name="date_range" id="filter-date-range" placeholder="Date range" class="form-control" value="" autocomplete="off"/>
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <input class="form-control btn btn-primary" onclick="getPayoutTransactionList()" type="button" name="filter" value="Filter">
                                    </div>
                                    <div class="col-md-1">
                                    <span class="date-select-btn">
                                        <button onclick="refresh()" class="btn btn-default" type="button">
                                                <i class="fa fa-repeat"></i>
                                       </button>
                                     </span>
                                   </div>
                                   </div>
                                </form>
                                
                            </div>
                      
                        </div>
                      
                        <div class="table-responsive"id="payout-list" >
                        <table class="table jambo_table table-striped table-bordered data-table">
                        <thead>
                                <tr class="headings">
                                    <th>S. No.</th>
                                    <th>User Name</th>
                                    <th>Amount</th>
                                    <th>TransactionId</th>
                                    <th>Payment Type</th>
                                    <th>Payment Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<script>

function getPayoutTransactionList(){
     
     var table = $('.data-table').DataTable({
         processing: true,
         serverSide: true,
         destroy: true,
         oLanguage: {
            sZeroRecords: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sEmptyTable: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sProcessing:'<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
         },
         ajax: {
             url:"{{ url('admin/payout-transaction-list') }}",
             data: function (d) {
                 d.userId = $('#userSearch').val(),
                 d.payment_method = $('#payment_method').val(),
                 d.date_between = $('#filter-date-range').val()
             }},
         columns: [
             {data: 'DT_RowIndex', name: 'DT_RowIndex'},
             {data: 'usename', name: 'usename', searchable: true},
             {data: 'amount', name: 'amount', searchable: true},
             {data: 'transaction_id', name: 'transaction_id', searchable: true},
             {data: 'payment_type', name: 'payment_type', searchable: false,orderable:false},
             {data: 'created_at', name: 'created_at', searchable: false},
             {data: 'action', name: 'action', searchable: false,orderable:false},
         ],
         "order": [[ 1, 'asc' ]]
     });
     table.on( 'order.dt search.dt', function () {
         table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
             cell.innerHTML = i+1;
         } );
     } ).draw();
      
 }

function refresh(){
    $('#payout-filter')[0].reset();
    $("#userSearch").select2("destroy");
    $('#userSearch').select2();
    getPayoutTransactionList();
}
$(document).ready(function() {
    getPayoutTransactionList(); 
});

</script>
@endsection