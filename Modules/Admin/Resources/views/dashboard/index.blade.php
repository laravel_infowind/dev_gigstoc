@extends('admin::layouts.inner-master')
@section('content')
<div class="right_col" role="main">

          <!-- top tiles -->

          <div class="dashboard-page">

            <div class="dashboard-page-top">

                <div class="row tile_count">

                  <div class="col-md-3 col-sm-4 col-xs-12">
                  
                    <div class="tile_stats_count tile_stats_count1">
                     
                      <span class="count_top">
                        
                        <span class="count_top_icon">

                          <i class="fa fa-users"></i>

                        </span>
                        <a href="{{url('admin/users')}}">
                        <span class="count_top_txt"> Total Users</span>
                        </a>
                      </span>                       
                      <a href="{{url('admin/users')}}">
                      <div class="count">{{getTotalUser()}} </div>
                      </a>
                    </div>

                  </div> 

                  <div class="col-md-3 col-sm-4 col-xs-12">

                    <div class="tile_stats_count tile_stats_count4">

                      <span class="count_top">
                        
                        <span class="count_top_icon">

                          <i class="fa fa-usd"></i>

                        </span>
                        <a href="javascript:void(0)">
                        <span class="count_top_txt">Admin Total Earning</span>
                        </a>
                      </span>
                       <a href="javascript:void(0)">
                      <div class="count">${{number_format($adminTotalEarning,2)}}</div>
                      </a>
                    </div>

                  </div>
 

                  <div class="col-md-3 col-sm-4 col-xs-12">

                    <div class="tile_stats_count tile_stats_count4">

                      <span class="count_top">
                        
                        <span class="count_top_icon">

                          <i class="fa fa-cc-paypal"></i>

                        </span>
                      
                        <span class="count_top_txt">Total Paypal  Available Amount</span>
                    
                      </span>
                       <a href="javascript:void(0)">
                      <div class="count">${{$paypalTotal}}</div>
                      </a>
                    </div>

                  </div>
			
                  <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="tile_stats_count tile_stats_count4">
                      <span class="count_top">
                        <span class="count_top_icon">
                          <i class="fa fa-exchange"></i>
                        </span>
                        <a href="javascript:void(0)">
                        <span class="count_top_txt">Pending Paypal Payouts</span>
                        </a>
                      </span>
                       <a href="javascript:void(0)">
                      <div class="count">${{getPendingPaypalAmount()}}</div>
                      </a>
                    </div>
              

                  </div>
                  <div class="col-md-3 col-sm-4 col-xs-12">
                      <div class="tile_stats_count tile_stats_count4">
                        <span class="count_top">
                          <span class="count_top_icon">
                            <i class="fa fa-usd"></i>
                          </span>
                          <a href="javascript:void(0)">
                          <span class="count_top_txt">Current Year Earning</span>
                          </a>
                        </span>
                        <a href="javascript:void(0)">
                        <div class="count">${{number_format($adminCurrentYearEarning,2)}}</div>
                        </a>
                      </div>
                      </div>

                </div>

              </div>


            <!-- /top tiles -->

            <div class="row">

            </div>
            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                        <div id="chartContainer" style="height: 300px; width: 1000px">
                        <div >
                              <center> <span><img src="" id="loader"></span></center>
                        </div>
             </div>
          </div>

        </div>
<script type="text/javascript">
  
   $( document ).ready(function() {
    getGraphData();
   });

  function getGraphData(){
   
     $.ajax({
        url: "{{url('admin/get-earning')}}",
        cache: false,
        type:'get',
        dataType:'json',
        success: function(response){
            $('#loader').attr('src',"{{url('public/images/loading.gif')}}");
            var values=[];
                values.push(['Months', 'Earning']);
                var maxDate = response.data[response.data.length-1].date;
                var dates = [];
                for(var i=0;i<response.data.length;i++){
                    var row = [response.data[i].month_name,response.data[i].sum];
                    values.push(row);
                }
                console.log(values);
             google.charts.load('current', {'packages':['bar']});
            google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable(values);
        var formatNumer = new google.visualization.NumberFormat({pattern: '$#.00'});
            formatNumer.format(data, 1);
              var options = {
                title: 'Current Year Earning Graph',
                width: 1100,
                legend: { position: 'none' },
                chart: { title: 'Current Year Earning Graph',
                        },
                bars: 'vertical', 
                axes: {
                  y: {
                    0: { side: 'top', label: 'Earning'} 
                  }
                },
                
                bar: { groupWidth: "100%" },
                displayExactValues: true
                
              };
              var chart = new google.charts.Bar(document.getElementById('chartContainer'));
              $('#loader').attr('src',"");
              chart.draw(data, options);
      };
    }
    });
  
  }
  </script>
        @endsection