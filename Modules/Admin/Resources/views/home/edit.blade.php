@extends('admin::layouts.inner-master')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>{{ $data->section }}</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form action="{{ route('content/update') }}" method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$data->id}}">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"> Sub Heading <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="sub_heading" value="{{ $data->sub_heading }}" placeholder="Enter Sub Heading"  class="form-control col-md-7 col-xs-12">
                                    @if($errors->has('sub_heading'))
                                    <p class="input-error"> {{ $errors->first('sub_heading') }} </p>
                                    @endif
                                </div>
                            </div>

                            @if ($data->id != 5 || $data->id != 7 || $data->id != 8)
                            @if($data->id == '6')
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Main Heading <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="heading" value="{{ $data->heading }}" placeholder="Enter Heading"  class="form-control col-md-7 col-xs-12">
                                        @if($errors->has('heading'))
                                        <p class="input-error"> {{ $errors->first('heading') }} </p>
                                        @endif
                                    </div>
                                </div>
                            @endif
                            
                                @if($data->id == '6')
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Description <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <textarea cols="80" id="editor1" name="description" rows="10"  class="form-control col-md-7 col-xs-12" data-sample-short >{{ $data->description }}</textarea>
                                        @if($errors->has('description'))
                                        <p class="input-error"> {{ $errors->first('description') }} </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Image <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        @if($data->image)
                                        <img src="{{ url('public/images').'/'.$data->image}}" height="80px" width="100px">
                                        @endif
                                        <input type="hidden" name="old_img" value="{{ $data->image}}">
                                        <input type="file" name="image" class="form-control col-md-7 col-xs-12" accept="image/*">
                                        @if($errors->has('image'))
                                        <p class="input-error"> {{ $errors->first('image') }} </p>
                                        @endif
                                    </div>
                                </div>
                                @endif
                            @endif
                            
                            <!-- Key feature & finding costs -->
                            @if($data->id == 9 || $data->id == 10)
                            <?php $res = ($data->id == 9) ? $data->keyFeatures : $data->findingCosts; ?>
                            @foreach($res as $key => $val)
                                <input type="hidden" name="keyf_id[]" value="{{ $val->id }}"/>
                                <div class="x_title">
                                    <h3><?php echo ($data->id == 9) ? 'Key feature' : 'Finding Cost'; ?> {{ $key+1 }}</h3>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Main Heading <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="heading[]" value="{{ $val->heading }}" placeholder="Enter Heading"  class="form-control col-md-7 col-xs-12">
                                        @if($errors->has('heading'))
                                        <p class="input-error"> {{ $errors->first('heading') }} </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> Description <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <textarea cols="80" id="editor1" name="description[]" rows="10"  class="form-control col-md-7 col-xs-12" data-sample-short >{{ $val->description }}</textarea>
                                            @if($errors->has('description'))
                                            <p class="input-error"> {{ $errors->first('description') }} </p>
                                            @endif
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Image <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        @if($val->image)
                                        <img src="{{ url('public/images').'/'.$val->image}}" height="80px" width="100px">
                                        @endif
                                        <input type="file" name="image[]" class="form-control col-md-7 col-xs-12" accept="image/*">
                                        
                                        @if($errors->has('image.'.$key)) 
                                        <p class="input-error">{{ $errors->first('image.'.$key)  }}</p>
                                        @endif
                                        <input type="hidden" name="old_img[]" value="{{ $val->image}}">
                                    </div>
                                </div>
                            @endforeach
                            @endif
                            <!-- Key feature & finding costs -->

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"> <span class="required"></span>
                                </label>
                                <!-- <div class="col-md-6 col-sm-6 col-xs-12">
                                    <p class="input-error">Recommended size 1263 width * 700 height</p>
                                </div> -->
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                                    <span><button type="submit" class="btn btn-primary">Save</button></span>
                                    <span><a href="{{ url('admin/content') }}" class="btn btn-warning">Cancel</a></span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection