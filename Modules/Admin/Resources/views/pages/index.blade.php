@extends('admin::layouts.inner-master')
@section('content')
    <div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                    <h1>Pages</h1>
                    <h2 style="float: right;">
                        <a class="btn btn-primary" href="{{url('admin/add-page')}}">Add Page</a>
                    </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                       
                        <div class="filter-list">
                            <div class="row">
                                <form method="get" id="filter_form" action="">
                                    <div class="col-md-3">
                                        <select class="form-control" id="status" >
                                            <option value="">Select Status</option>
                                            <option  value="active">Active</option>
                                            <option  value="inactive">Inactive</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <input class="form-control btn btn-primary" type="button" onclick="getPageList()" name="filter" value="Filter">
                                    </div>
                                    <div class="col-md-1">
                                    <span class="date-select-btn">
                                        <button onclick="refreshFilter()" class="btn btn-default" type="button">
                                                <i class="fa fa-repeat"></i>
                                       </button>
                                     </span>
                                   </div>
                                </form>
                            </div>
                        </div>

                        <div class="table-responsive">
                        <table class="table jambo_table table-bordered data-table" id="page_table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Page Name</th>
                                    <th>Created Date</th>
                                    <th>Active</th>
                                    <th width="200px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        </div>
                    </div>
                   
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

   
<script type="text/javascript">

  $(function () {
    getPageList();
  });

  function getPageList(){

    var table = $('#page_table').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        oLanguage: {
            sZeroRecords: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sEmptyTable: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sProcessing:'<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        ajax: {
            url:"{{ url('admin/page-list') }}",
            data: function (d) {
                d.status = $('#status').val()
            }},
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name', searchable: true},
            {data: 'created_at', created_at: 'created_at', searchable: true},
            {data: 'active', name: 'active', orderable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
  }

  function refreshFilter(){
    $('#filter_form')[0].reset();
    getPageList();
    }  
    
  </script>

  @endsection