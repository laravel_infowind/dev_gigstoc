@extends('admin::layouts.inner-master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Edit About Us</h2>
                            <div class="clearfix"></div>
                        </div>
                    
            <div class="x_content">
                <form action="{{ url('admin/about-us') }}" method="post" id="about_form" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                            {{ csrf_field() }}
                <div class="x_title">
                        <h3>Header Content </h3>
                        <div class="clearfix"></div>
                    </div>
                <input type="hidden" name="id" value="{{$page->id}}">
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Page Name <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="plan_title" name="name" class="form-control col-md-7 col-xs-12" placeholder="Enter Name here" autocomplete="off" value="{{$about->name}}" required="">
                    </div>
                </div>
                <!-- <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Heading <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="main_header" value="{{$about->main_header}}" placeholder="Enter Heading"  class="form-control col-md-7 col-xs-12">
                    
                    </div>
                </div> -->
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Header Content <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <!-- <textarea placeholder="Enter Description here" id="editor1" cols="80" name="header_content" rows="10"  class="form-control col-md-7 col-xs-12" data-sample-short>{{$about->header_content}}</textarea> -->
                        <textarea placeholder="Enter Description here" id="editor1" cols="80" name="header_content" rows="10"  class="form-control col-md-7 col-xs-12" data-sample-short>{{$about->header_content}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Image <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    <img src= "{{url('public/images/'.$about->image)}}"/>
                        <input type="file" name="image" class="form-control col-md-7 col-xs-12" accept="image/*">
                    </div>
                </div>
                <div class="x_title">
                        <h3>Middle Content </h3>
                        <div class="clearfix"></div>
                    </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Middle Content<span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <textarea cols="80" id="editor2" name="middle_content" rows="5"  class="form-control col-md-7 col-xs-12" data-sample-short >{{$about->middle_content}}</textarea>
                    </div>
                 </div>
               
                <div class="x_title">
                                    <h3>Footer Content </h3>
                                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Heading <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="footer_header" value="{{$about->footer_header}}" placeholder="Enter Heading"  class="form-control col-md-7 col-xs-12">
                    
                    </div>
                </div>
                <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> Footer Content <span class="required">*</span>
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <textarea cols="80" id="editor3" name="footer_content" rows="5"  class="form-control col-md-7 col-xs-12" data-sample-short >{{$about->footer_content}}</textarea>
                        </div>
                </div>
                <div class="x_title">
                        <h3>Meta Content </h3>
                        <div class="clearfix"></div>
                    </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Meta Title <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="meta_title" class="form-control col-md-7 col-xs-12" placeholder="Enter meta title here" autocomplete="off" required="" value="{{$page->meta_title}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Meta Description <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea cols="30" name="meta_desc" rows="3" class="form-control col-md-7 col-xs-12" data-sample-short="" required="">{{$page->meta_desc}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Meta Keywords </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea cols="30" name="meta_keywords" rows="3" class="form-control col-md-7 col-xs-12" data-sample-short="">{{$page->meta_keywords}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">is Visibile 
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="checkbox" name="is_visible" <?php if($page->is_visible == 1){ echo "checked"; }else{}?> value="1">
                    </div>
                </div>
                <div class="ln_solid"></div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> <span class="required"></span>
                    </label>
                </div>
                
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                        <span><button type="button" class="btn btn-primary">Save</button></span>
                        <span><a href="{{ url('admin/pages') }}" class="btn btn-warning">Cancel</a></span>
                    </div>
                </div>
            </form>
            {!! JsValidator::formRequest('App\Http\Requests\Admin\AboutUsRequest','#about_form') !!}
          
              </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>

textarea#editor3 {
    width: 66%;
}
textarea#editor2 {
    width: 66%;
}

</style>
<script>

$(".btn-primary").on('click',function(e) {
    e.preventDefault();
    var formData = new FormData($('#about_form')[0]);
    var editor_data = CKEDITOR.instances.editor1.getData();

    formData.append('header_content',editor_data);
    if (jQuery("#about_form").valid()) {
        jQuery('.btn-primary').prop('disabled',true);
        jQuery.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            contentType: false,
            processData: false,
            data: formData, 
            url: "{{url('admin/about-us')}}",
            dataType:'json',
            success: function(data) {
                if (data.success) {
                    toastr.success(data.message); 
                    setTimeout(function() {
                        window.location.href = "{{url('admin/pages')}}";
                    }, 1000);
                 
                } else {
                    toastr.error(data.message); 
                }
                jQuery('.btn-primary').prop('disabled',false);
            }
        });
    }
});

catIDS = jQuery('#catIDS').val();

$(document).ready(function(){
$('#ms-list-1').addClass('ms-options-wrap ms-active ms-has-selections');
});
</script>
@endsection