@extends('admin::layouts.inner-master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add Page</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
            <form method="post" action="{{url('admin/add-page')}}" id="page_form" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Page Name <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="plan_title" name="name" class="form-control col-md-7 col-xs-12" placeholder="Enter Name here" autocomplete="off" value="" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Content <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea placeholder="Enter Description here" id="editor1" cols="80" name="content" rows="10"  class="form-control col-md-7 col-xs-12" data-sample-short required=""></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Meta Title <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="meta_title" class="form-control col-md-7 col-xs-12" placeholder="Enter meta title here" autocomplete="off" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Meta Description <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea cols="30" name="meta_desc" rows="3" class="form-control col-md-7 col-xs-12" data-sample-short="" required=""></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Meta Keywords </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea cols="30" name="meta_keywords" rows="3" class="form-control col-md-7 col-xs-12" data-sample-short=""></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">is Visibile 
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="checkbox" name="is_visible" checked="checked" value="1">
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                    <span><button type="submit" id="page_form_button" class="btn btn-success">
                     <i id="page_form_loader" class="fa fa-spinner fa-spin" style="display:none"></i>
                    Submit</button></span>
                    <span><a href="{{ url('admin/pages') }}" class="btn btn-warning">Cancel</a></span>
                    </div>
                </div>
              </form>
              {!! JsValidator::formRequest('App\Http\Requests\Admin\AddPageRequest','#page_form') !!}
              </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  @endsection