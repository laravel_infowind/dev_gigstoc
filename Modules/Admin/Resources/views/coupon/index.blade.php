@extends('admin::layouts.inner-master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h1>Coupons</h1>
                        <h2 style="float: right;">
                            <a class="btn btn-primary" href="{{url('admin/add-coupon')}}">Add Coupon</a>
                        </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        

                        <div class="table-responsive">
                            <table class="table jambo_table table-bordered data-table">
                                <thead>
                                    <tr>
                                        <th >S.No.</th>
                                        <th >Coupon Code</th>
										<th >Amount</th>
										<th >Type</th>
										<th >Total Usage</th>
										<th >Current Usage</th>
										<th >Start Date</th>
										<th >End Date</th>
                                        <th width="100px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>



<script type="text/javascript">
    $(function () {
        getCouponList();
    });

    function getCouponList() {

        var table = $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            oLanguage: {
                sZeroRecords: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
                sEmptyTable: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
                sProcessing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            ajax: {
                url: "{{ url('admin/coupon-list') }}",
            },
            columns: [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'coupon',
                    name: 'coupon',
                    searchable: true
                },
                {
                    data: 'amount',
                    name: 'amount',
                    searchable: true
                },
				{
                    data: 'type',
                    name: 'type',
                    searchable: true
                },
				{
                    data: 'total_usage',
                    name: 'total_usage',
                    searchable: true
                },
				{
                    data: 'current_usage',
                    name: 'current_usage',
                    searchable: true
                },
				{
                    data: 'start_date',
                    name: 'start_date',
                    searchable: true
                },
				{
                    data: 'end_date',
                    name: 'end_date',
                    searchable: true
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
            "order": [
                [1, 'asc']
            ]
        });
    }

  

</script>

@endsection
