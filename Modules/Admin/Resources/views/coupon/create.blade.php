@extends('admin::layouts.inner-master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add Coupon</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                        <form method="post" action="{{ url('admin/save-coupon') }}" id="add_coupon" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                                {{ csrf_field() }}
                               
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Coupon<span class="required" autocomplete="off">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="plan_title" name="coupon" class="form-control col-md-7 col-xs-12" placeholder="Coupan" autocomplete="off">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Type <span class="required" autocomplete="off">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="type" class="form-control col-md-7 col-xs-12">
											<option value=""> select type</option>
											<option value="flat">flat</option>
											<option value="percenatge">percenatge</option>
										</select>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Amount/Percenatge<span class="required" autocomplete="off">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="amount" class="form-control col-md-7 col-xs-12" placeholder="Amount/Percenatge" autocomplete="off">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Start Date<span class="required" autocomplete="off">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="start_date" id='start_date' class="form-control col-md-7 col-xs-12" placeholder="Start Date" autocomplete="off">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">End Date<span class="required" autocomplete="off">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="end_date" id='end_date' class="form-control col-md-7 col-xs-12" placeholder="End date" autocomplete="off">
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Total usage<span class="required" autocomplete="off">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="total_usage" class="form-control col-md-7 col-xs-12" placeholder="Total usage" autocomplete="off">
                                    </div>
                                </div>
								
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                                    <span><button type="button" class="btn btn-success">
                                    <i id="form_loader" class="fa fa-spinner fa-spin" style="display:none"></i>
                                    Submit</button></span>
                                    <span><a href="{{ url('admin/coupons') }}" class="btn btn-warning">Cancel</a></span>
                                    </div>
                                </div>
                            </form>
                         {!! JsValidator::formRequest('App\Http\Requests\Admin\AddCouponRequest','#add_coupon') !!}
        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

$("#add_coupon .btn-success").on('click   ',function(e) {
    e.preventDefault(); 
    var formData = new FormData($('#add_coupon')[0]);
      if ($("#add_coupon").valid()) {
        $('.btn-success').prop('disabled',true);
        $('#form_loader').show();
        $.ajax({
            type: "POST",
            url: "{{url('admin/save-coupon')}}",
            contentType: false,
            processData: false,
            data: formData, 
            dataType:'json',
            success: function(data) {
                if (data.success) {
                    toastr.success(data.message); 
                    setTimeout(function() {
                        window.location.href = "{{url('admin/coupons')}}";
                    }, 1000);
                 
                } else {
                    toastr.error(data.message); 
                }
                $('#form_loader').hide();
                $('.btn-success').prop('disabled',false);
            }
        });
    }
});
</script>

<script type="text/javascript">
            $(function () {
			   var start = new Date()
			   var nowDate = new Date();
               var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);;
			   
                $('#start_date,#end_date').datepicker({
					format: 'yyyy-mm-dd',
					 autoclose: true,
					 startDate: "+0d"
					
				});
			
			 
				// set end date to max one year period:
				var end = new Date(new Date().setYear(start.getFullYear()+1));

				$('#start_date').datepicker({
					startDate : today,
					endDate   : end
				// update "toDate" defaults whenever "fromDate" changes
				}).on('changeDate', function(){
					// set the "toDate" start to not be later than "fromDate" ends:
					$('#end_date').datepicker('setStartDate', new Date($(this).val()));
				}); 

				$('#end_date').datepicker({
					startDate : today,
					endDate   : end
				// update "fromDate" defaults whenever "toDate" changes
				}).on('changeDate', function(){
					// set the "fromDate" end to not be later than "toDate" starts:
					$('#start_date').datepicker('setEndDate', new Date($(this).val()));
				});	
            });
	
			

</script>
 
@endsection