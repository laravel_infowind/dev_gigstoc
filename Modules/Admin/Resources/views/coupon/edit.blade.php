@extends('admin::layouts.inner-master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Edit Coupon</h2>
                            <div class="clearfix"></div>
                        </div>
                        
                        <div class="x_content">
                        <form method="post" action="{{ url('admin/save-coupon') }}" id="edit_tag" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                                {{ csrf_field() }}
								<input type="hidden" value="{{$coupon->id}}" name="id">
                               <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Coupon<span class="required" autocomplete="off">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="plan_title" value="{{$coupon->coupon}}" name="coupon" class="form-control col-md-7 col-xs-12" placeholder="Coupan" autocomplete="off">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Type <span class="required" autocomplete="off">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="type" class="form-control col-md-7 col-xs-12">
											<option value=""  > select type</option>
											<option value="flat" @if($coupon->type == 'flat') selected = "selected" @endif>flat</option>
											<option value="percenatge" @if($coupon->type == 'percenatge') selected = "selected" @endif>percenatge</option>
										</select>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Amount/Percenatge<span class="required" autocomplete="off">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="amount" value="{{$coupon->amount}}" class="form-control col-md-7 col-xs-12" placeholder="Amount/Percenatge" autocomplete="off">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Start Date<span class="required" autocomplete="off">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="start_date" id='start_date' value="{{$coupon->start_date}}" class="form-control col-md-7 col-xs-12" placeholder="Start Date" autocomplete="off">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">End Date<span class="required" autocomplete="off">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="end_date" id='end_date' value="{{$coupon->end_date}}" class="form-control col-md-7 col-xs-12" placeholder="End date" autocomplete="off">
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Total usage<span class="required" autocomplete="off">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="total_usage" value="{{$coupon->total_usage}}" class="form-control col-md-7 col-xs-12" placeholder="Total usage" autocomplete="off">
                                    </div>
                                </div>
								
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                                    <span><button type="button" class="btn btn-success">
                                    <i id="form_loader" class="fa fa-spinner fa-spin" style="display:none"></i>
                                    Submit</button></span>
                                    <span><a href="{{ url('admin/coupons') }}" class="btn btn-warning">Cancel</a></span>
                                    </div>
                                </div>
                            </form>
                         {!! JsValidator::formRequest('App\Http\Requests\Admin\AddCouponRequest','#edit_tag') !!}
        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

$("#edit_tag .btn-success").on('click   ',function(e) {
    e.preventDefault(); 
    var formData = new FormData($('#edit_tag')[0]);
   
    if ($("#edit_tag").valid()) {
        $('.btn-success').prop('disabled',true);
        $('#form_loader').show();
        $.ajax({
            type: "POST",
            url: "{{url('admin/save-coupon')}}",
            contentType: false,
            processData: false,
            data: formData, 
            dataType:'json',
            success: function(data) {
                if (data.success) {
                    toastr.success(data.message); 
                    setTimeout(function() {
                        window.location.href = "{{url('admin/coupons')}}";
                    }, 1000);
                 
                } else {
                    toastr.error(data.message); 
                }
                $('#form_loader').hide();
                $('.btn-success').prop('disabled',false);
            }
        });
    }
});
</script>

<script type="text/javascript">
		$(function () {
			$('#start_date,#end_date').datepicker({
				format: 'yyyy-mm-dd',
				autoclose: true,
				startDate: "+0d"
				
			});
			 
			   var start = new Date();
				// set end date to max one year period:
				var end = new Date(new Date().setYear(start.getFullYear()+1));

				$('#start_date').datepicker({
					startDate : start,
					endDate   : end
				// update "toDate" defaults whenever "fromDate" changes
				}).on('changeDate', function(){
					// set the "toDate" start to not be later than "fromDate" ends:
					$('#end_date').datepicker('setStartDate', new Date($(this).val()));
				}); 

				$('#end_date').datepicker({
					startDate : start,
					endDate   : end
				// update "fromDate" defaults whenever "toDate" changes
				}).on('changeDate', function(){
					// set the "fromDate" end to not be later than "toDate" starts:
					$('#start_date').datepicker('setEndDate', new Date($(this).val()));
				});	
			
			
		});
</script>
@endsection