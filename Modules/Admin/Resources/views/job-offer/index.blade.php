@extends('admin::layouts.inner-master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h1>Job Offers</h1>
                        <div class="clearfix"></div>
						<h2 style="float: right;">
                            <a class="btn btn-primary" href="{{url('admin/jobs')}}">Back</a>
                        </h2>
                    </div>
                    <div class="x_content">

                        <div class="filter-list">
                            <div class="">
                                <form method="get" action="" id="service_filter">
                                    <div class="row">
                                     
                                        <div class="col-md-2">
                                            <select class="form-control" onchange="getJobOfferList()" id="status">
                                                <option value="">Select Status</option>
                                                <option value="pending">Pending</option>
                                                <option value="1">Accepted</option>
                                                <option value="2">Rejected</option>
                                            </select>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="control-group">
                                                <div class="controls">
                                                    <div class="input-prepend input-group">
                                                        <span class="add-on input-group-addon"><i
                                                                class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                        <input type="text" name="date_range" id="filter-date-range"
                                                            placeholder="Date range" class="form-control"
                                                            autocomplete="off" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <input class="form-control btn btn-primary" type="button"
                                                onclick="getJobOfferList()" name="filter" value="Filter">
                                        </div>
                                        
                                        <div class="col-md-1">
                                            <span class="date-select-btn">
                                                <button onclick="refreshFilter()" class="btn btn-default" type="button">
                                                    <i class="fa fa-repeat"></i>
                                                </button>
                                            </span>
                                        </div>


                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table jambo_table table-bordered data-table">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Job Title</th>
                                        <th>Service Title</th>
                                        <th>Offer Created By</th>
                                       
                                        <th>Created Date</th>
                                       <!-- <th width="100px">Status</th> -->
                                    
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>




<script type="text/javascript">
    $(function () {
        getJobOfferList();
    });

    function getJobOfferList() {
        var table = $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            oLanguage: {
                sZeroRecords: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
                sEmptyTable: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
                sProcessing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            ajax: {
                url: "{{ url('admin/offer/list') }}",
                data: function (d) {
                    d.status = $('#status').val(),
                    d.date_between = $('#filter-date-range').val(),
                    d.category_id = $('#categories').val(),
					d.request_id = "{{$id}}"
                }
            },
            columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'job_title', created_at: 'job_title', searchable: true},
                    {data: 'service_tittle', created_at: 'service_tittle', searchable: true},
                    {data: 'created_by', created_at: 'created_by', searchable: true},
                    {data: 'created_at', created_at: 'created_at', searchable: true},
                    //{data: 'status', name: 'status', orderable: false},
            ]
        }).draw();
      
    }

    function refreshFilter()
    {
        $('#service_filter')[0].reset();
        getJobOfferList();
    }


</script>

@endsection