@extends('admin::layouts.inner-master')
@section('content')
    <div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                    <h1>Categories</h1>
                    <h2 style="float: right;">
                        <a class="btn btn-primary" href="{{url('admin/add-category')}}">Add Category</a>
                    </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                       
                        <div class="filter-list">
                            <div class="row">
                                <form method="get" id="filter_form" action="">
                                    <div class="col-md-3">
                                    <select class="form-control" onchange="getCategoryList()" id="parent_category_list">
                                            
                                    </select>
                                    </div>

                                    <div class="col-md-3">
                                        <select class="form-control" id="status" >
                                            <option value="">Select Status</option>
                                            <option  value="active">Active</option>
                                            <option  value="inactive">Inactive</option>
                                        </select>
                                    </div>

                                    <div class="col-md-2">
                                        <input class="form-control btn btn-primary" type="button" name="filter"  onclick="getCategoryList()" value="Filter">
                                    </div>
                                    <div class="col-md-1">
                                    <span class="date-select-btn">
                                        <button onclick="refreshFilter()" class="btn btn-default" type="button">
                                                <i class="fa fa-repeat"></i>
                                       </button>
                                     </span>
                                   </div>
                                     
                                </form>
                            </div>
                        </div>

                        <div class="table-responsive">
                        <table class="table jambo_table table-bordered data-table" id="category_table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Category Name</th>
                                    <th>Parent Category</th>
                                    <th>Description</th>
                                    <th width="200px">Status</th>
                                    <th width="200px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        </div>
                    </div>
                   
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

   
<script type="text/javascript">

  $(function () {
    getCategoryList();
    getParentCategory();
  });

  function getCategoryList(){

    var table = $('#category_table').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        oLanguage: {
            sZeroRecords: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sEmptyTable: "<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>",
            sProcessing:'<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        ajax: {
            url:"{{ url('admin/category-list') }}",
            data: function (d) {
                d.parent_category = $('#parent_category_list').val(),
                d.status = $('#status').val()
            }},
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name', searchable: true},
            {data: 'parent_category', name: 'parent_category', searchable: true},
            {data: 'description', name: 'description', searchable: true},
            {data: 'status', name: 'status',  orderable: false,},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
  }

  function getParentCategory(){
    $.ajax({
        type: "get",
        url: "{{url('admin/get-parent-category')}}",
        dataType:'json',
        success: function(data)
        {
            if (data.success){
                $('#parent_category_list').html(data.list);
            }
        }
        });
  }

  function refreshFilter(){
    $('#filter_form')[0].reset();
    $("#parent_category_list").select2("destroy");
    $('#parent_category_list').select2();
    getCategoryList();
    }
    
  </script>

  @endsection