@extends('admin::layouts.inner-master')
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add Category</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
            <form method="post" action="{{url('admin/add-category')}}" id="category_form" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Category/Subcategory Name <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="plan_title" name="name" class="form-control col-md-7 col-xs-12" placeholder="Enter Name here" autocomplete="off" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Category Description <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea placeholder="Enter Description here" cols="50" name="description" rows="3"  class="form-control col-md-7 col-xs-12" data-sample-short ></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Parent Category <span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    <select class="form-control parent_list" name="category_id" id="parent_category_list">
                        
                        
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Category Image</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="file" name="cat_image">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Visible in Menu </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <!-- <div class="checkbox"> -->
                            <input type="checkbox" name="visible_in_menu" value="1" >
                        <!-- </div> -->
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Is featured </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <!-- <div class="checkbox"> -->
                            <input type="checkbox" name="is_featured" value="1"  >
                        <!-- </div> -->
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 btn-sub">
                    <span><button type="submit" id="category_form_button" class="btn btn-success">
                     <i id="category_form_loader" class="fa fa-spinner fa-spin" style="display:none"></i>
                    Submit</button></span>
                    <span><a href="{{ url('admin/category') }}" class="btn btn-warning">Cancel</a></span>
                    </div>
                </div>
              </form>
              {!! JsValidator::formRequest('App\Http\Requests\Admin\AddCategoryRequest','#category_form') !!}
              </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>


$("#category_form").submit(function(e) {
     e.preventDefault(); 
        var form = $(this);
        var formData = new FormData(this);
        var url = form.attr('action');
        if ($("#category_form").valid()) {
            $('#category_form_button').prop('disabled',true);
            $('#category_form_loader').show();
            $.ajax({
            headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: url,
            contentType: false,
            processData: false,
            data: formData, 
            dataType:'json',
            success: function(data)
            {
                if (data.success){
                    toastr.success(data.message); 
                    setTimeout(function(){
                        window.location.href = "{{url('admin/category')}}";
                    },1000);
                 
                } else {
                    toastr.error(data.message); 
                }
                $('#category_form_loader').hide();
                $('#category_form_button').prop('disabled',false);
            }
            });
        }
});

function getParentCategory(){
    $.ajax({
        type: "get",
        url: "{{url('admin/get-parent-category')}}",
        dataType:'json',
        success: function(data)
        {
            if (data.success){
                $('.parent_list').html(data.list);
            }
        }
        });
}

$(document).ready(function(){
    getParentCategory();
});

</script>

  @endsection