@extends('admin::layouts.master')

@section('content')


<!------ Include the above in your HEAD tag ---------->
<div class="container login-page">
    <div class="row justify-content-center">
        <div class="login-sec">
            <div class="login-head-logo">
                <a href="{{ url('/') }}">
                <img src="<?php echo url('/public/images/setting/').'/'.getSettings('site_logo'); ?>"></a>
            </div>
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form id="login-form" action="{{url('admin/login')}}" method="post" autocomplete="off">
                        @csrf
                        <?php 
                            if (old('email')!="") {
                                $emaill = old('email');
                            } else {
                                if (isset($_COOKIE['email'])) {
                                    $emaill = $_COOKIE['email'];
                                }
                            }
                        ?>
                        <div class="form-group row"> 
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="<?php if(isset($_COOKIE["email"])) {  echo $_COOKIE["email"];} ?>" autocomplete="off" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" value="<?php if(isset($_COOKIE["password"])) {  echo $_COOKIE["password"];} ?>" name="password" autocomplete="off">

                                @error($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @enderror
                                <span class="password-error error-validate" style="color: red;"></span>
                            </div>
                        </div>
                        <?php 
                             if (old('email')!="") {
                                $emaill = old('email');
                             } else {
                                if (isset($_COOKIE['email'])) {
                                    $emaill = $_COOKIE['email'];
                                }
                             }
                            ?>
                        <div class="form-group row" style="margin-bottom:0.3rem">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input type="checkbox" id="login_checkbox" class="form-check-input" name="remember" {{ old('remember') || isset($emaill)? 'checked' : '' }}>
                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 offset-md-4 forgotpass">
                                <a href="{{ url('admin/forget-password-form') }}">Forget password</a>
                            </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary loginbtn">
                                    {{ __('Login') }}
                                    <i id="login_loader" class="fa fa-spinner fa-spin" style="display:none"></i>
                                </button>
                            </div>
                          
                           
                        </div>
                    </form>
                    {!! JsValidator::formRequest('App\Http\Requests\Admin\AdminLoginPostRequest','#login-form') !!}
                </div>
            </div>
        </div>
    </div>
</div>

<script>


$("#login-form .loginbtn").on('click',function(e) {
        e.preventDefault(); 
        if ($('#login_checkbox').is(':checked')) {
            $('#login_checkbox').val(1);
        } else {
            $('#login_checkbox').val(0);
        }
        if ($("#login-form").valid()) {
            $('#login_button').prop('disabled',true);
            $('#login_loader').show();
            $.ajax({
            headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: "{{url('admin/login')}}",
            data: $('#login-form').serialize(), 
            dataType:'json',
            success: function(data)
            {
                if (data.success){
                    toastr.success(data.message); 
                    window.location.href = "{{url('admin/dashboard')}}";
                } else {
                    toastr.error(data.message); 
                }
                $('#login_loader').hide();
                $('#login_button').prop('disabled',false);
            }
            });
        }
});

</script>

@endsection
