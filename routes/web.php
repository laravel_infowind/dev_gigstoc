<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'guest'], function () {
	
	Route::get('login', 'Auth\LoginController@index');
	Route::post('login', 'Auth\LoginController@login');
	Route::get('signup', 'Auth\RegisterController@index');
	Route::post('signup', 'Auth\RegisterController@signup');
	Route::get('forgot', 'Auth\ForgotPasswordController@forgotPassword');
	Route::post('forgot', 'Auth\ForgotPasswordController@sendPasswordMail');
	Route::get('reset/password/{token}', 'Auth\ForgotPasswordController@resetForm');
	Route::post('reset/password', 'Auth\ForgotPasswordController@resetPassword');
	Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
	Route::post('resend/verification-mail', 'Auth\LoginController@resendVerificationMail');
	Route::post('check-verification', 'Auth\LoginController@checkVerification');
	Route::get('login/{provider}', 'SocialController@redirectToProvider');
	Route::get('login/{provider}/callback', 'SocialController@handleProviderCallback');
	
});
Route::get('/', 'WelcomeController@index');
Route::post('newsletter-store', 'WelcomeController@newsletter_store')->name('newsletter-store');

//common link
Route::get('common/side-link', 'WelcomeController@getSideLink');

Route::group(['middleware' => 'user'], function () {
	
Route::get('logout', 'Auth\LoginController@logout');
Route::get('post-recruit', 'JobController@getHome');
Route::post('post-recruit', 'JobController@savePost');
Route::get('get-category', 'JobController@getCategory');

// Services
Route::get('post-service', 'ServiceController@addService');
Route::post('post-service', 'ServiceController@saveService');
Route::post('service/image-upload', 'ServiceController@imageUpload');
Route::get('service/delete-image/{image_name}/{image_type}', 'ServiceController@deleteImage');

//custom order
Route::post('custom/file-upload', 'CustomController@fileUpload');
Route::get('custom/delete-file/{image_name}/{image_type}', 'CustomController@deleteFile');
Route::post('send-custom-order', 'CustomController@sendCustomOrder');
Route::post('send-custom-offer', 'CustomController@sendCustomOffer');
Route::post('custom-order-detail', 'CustomController@customOrderDetail');
Route::post('order-declined', 'CustomController@orderDeclined');
Route::post('reject-offer', 'CustomController@rejectOffer');

// chat module
Route::post('send-contact-me', 'ChatController@sendContactMe');
Route::get('chat/{room_id}', 'ChatController@index');
Route::get('chat/chat-body/{room_id}', 'ChatController@chatBody');
Route::post('send-message', 'ChatController@sendMessage');
Route::post('chat/file-upload', 'ChatController@fileUpload');
Route::get('chat/delete-file/{image_name}/{image_type}/{chat_room_id}', 'ChatController@deleteFile');
Route::post('chat/check-unread-count', 'ChatController@checkUnreadCount');
Route::post('chat/chat-notification', 'ChatController@ChatNotification');
Route::get('my-message-list', 'ChatController@MyMessageList');
Route::get('mark-as-read', 'ChatController@markAllAsRead');

Route::get('dashboard', 'DashBoardController@dashboard');
Route::get('get-my-order-statistics', 'DashBoardController@getOrderStatics');

Route::get('change-password', 'UserController@getChangePassword');
Route::get('set-user-timezone', 'UserController@setUserTimeZone');
Route::post('change-password', 'UserController@changePassword');

Route::get('bookmark', 'BookMarkController@index');
Route::get('bookmark-list', 'BookMarkController@getBookMarkList');
Route::get('remove-bookmark', 'BookMarkController@removeFromBookMark');

Route::get('my-invoice', 'PaymentController@index');
Route::get('invoice-list', 'PaymentController@getInvoiceList');
Route::get('invoice-detail/{id}', 'PaymentController@invoiceDetail');

Route::get('payment-method/{type?}', 'UserController@paymentMethod');
Route::post('save-payment-method', 'UserController@savePaymentMethod');
Route::post('select-payment-method', 'UserController@selectMethod');
Route::post('add-bank-account', 'UserController@addBankAccount');


Route::post('save-user-rating', 'UserController@saveUserRating');
Route::get('my-profile/{id?}', 'UserController@myProfile');
Route::get('edit-profile', 'UserController@editProfile');
Route::post('update-profile', 'UserController@updateProfile');
Route::get('get-country', 'UserController@getCountry');
Route::post('update-picture', 'UserController@updatePicture');

Route::get('my-gig', 'ServiceController@myGig');
Route::get('gig-list', 'ServiceController@myGigList');

Route::get('my-job', 'JobController@myJob');
Route::get('job-list', 'JobController@myJobList');
Route::get('delete-job/{id}', 'JobController@deleteJob');
Route::get('subscription-setting', 'UserController@subscriptionSetting');
Route::get('set-noitification-subscription', 'UserController@setSubscribtion');
Route::get('get-uneadNotification', 'NotificationController@getUnreadCount');
Route::get('notifications', 'NotificationController@getNotification');
Route::get('notification-list', 'NotificationController@getNotificationList');
Route::get('add-remove-bookmark', 'ServiceController@addRemoveBookmark');

Route::get('revenues', 'RevenueController@index');
Route::get('revenues-list', 'RevenueController@revenueList');
Route::post('submit-offer', 'JobController@submitOffer');

Route::get('withdrawal-balance', 'WithdrawalController@index');
Route::get('get-withdrawal-account', 'WithdrawalController@getWithDrawalAccount');
Route::post('account-type', 'WithdrawalController@checkAccount');
Route::post('save-withdrawal-request', 'WithdrawalController@saveWithdrawalRequest');
Route::get('my-withdrawal/{type?}', 'WithdrawalController@myWithdrawal');
Route::get('withdrawal-request-list', 'WithdrawalController@getWithDrawalRequestList');
Route::get('withdrawal-hstory-list', 'WithdrawalController@getWithDrawalHistoryList');




});
Route::get('view-profile/{id?}', 'UserController@viewProfile');
Route::get('all-job/request', 'JobController@getJobs');
Route::get('job/list', 'JobController@getJobList');
Route::get('job-detail/{id}', 'JobController@getJobDetail');
Route::get('get-ip', 'WelcomeController@getip');
Route::get('view-service', 'ServiceController@viewService');
Route::get('get-ip-address', 'ServiceController@getIpAddress');
Route::get('category/{id?}', 'ServiceController@getService');
Route::get('service/list', 'ServiceController@getServiceList');
Route::get('user/service/list', 'ServiceController@getUserServiceList');
Route::post('save-tag', 'ServiceController@saveTag');
Route::get('service/{service_id}', 'ServiceController@serviceDetail');
Route::get('checkout-detail', 'ServiceController@checkoutDetail');
Route::post('coupon-apply', 'ServiceController@couponApply');
Route::post('paypal-checkout', 'ServiceController@paypalCheckout');
Route::get('thank-you/{order_id}', 'ServiceController@thank_you');
Route::get('paypal-cancel', 'ServiceController@cancel')->name('paypal.cancel');
Route::get('paypal/success', 'ServiceController@success')->name('paypal.success');
Route::post('paypal/notify', 'ServiceController@notify')->name('paypal.notify');
Route::post('save-order', 'ServiceController@saveOrder');
Route::get('stripe-checkout', 'ServiceController@stripeCheckout');
Route::post('make-stripe-payment', 'ServiceController@makeStripePayment');
Route::get('get-country-names', 'ServiceController@getCountryNames');
Route::post('save-service-rating', 'ServiceController@saveServiceRating');
Route::post('direct-checkout', 'ServiceController@directCheckout');
Route::post('mark-as-delivered', 'ServiceController@markAsDelivered');

Route::get('contact-us', 'WelcomeController@contactUs');
Route::post('contact-us', 'WelcomeController@saveContactUs');
Route::get('site-map', 'WelcomeController@sitemap');
Route::get('check-email-template', 'WelcomeController@checkTemplate');
Route::get('blog', 'BlogController@index');
Route::get('blog/list', 'BlogController@getBlogList');
Route::get('blog/{slug}', 'BlogController@blogDetail');
Route::get('blog-detail/{id}', 'BlogController@singleBlogDetail');

Route::get('faq', 'WelcomeController@faqList');
Route::get('{page}', 'WelcomeController@staticPage');

