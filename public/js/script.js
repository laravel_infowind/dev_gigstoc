jQuery(document).ready(function($) {

    $("#accordion").on("hide.bs.collapse show.bs.collapse", e => {
      $(e.target)
        .prev()
        .find("i:last-child")
        .toggleClass("fa-chevron-up fa-chevron-down");
    })

    jQuery(".category-side-menu li a").each(function() {
        if (jQuery(this).next().length > 0) {
            jQuery(this).addClass("parent");
        };
    })
    jQuery('.category-side-menu li a.parent').after('<span class="new"></span>');
    jQuery(".category-side-menu li .new").unbind('click').bind('click', function(e) {
      jQuery(this).parent("li").toggleClass("hover");
  });

})


jQuery(document).ready(function() {
  jQuery(".shop-filter-ul li a").each(function() {
      if (jQuery(this).next().length > 0) {
          jQuery(this).addClass("parent");
      };
  })

  adjustMenu();

  jQuery('.shop-filter-ul li a.parent').after('<span class="drop"></span>');

  jQuery(".shop-filter-ul li a.parent").unbind('click').bind('click', function(e) {
    e.preventDefault();
      jQuery(this).parent("li").toggleClass("hover");
  });
})

jQuery(window).bind('resize orientationchange', function() {
  ww = document.body.clientWidth;
  adjustMenu();
});


