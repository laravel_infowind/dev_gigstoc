/*
 *  Admin custom js
 */
var csrf_token = $('meta[name="csrf-token"]').attr('content');
// Editor js
bkLib.onDomLoaded(function() {
    $("textarea#buy_sell").prevAll('div').remove();
    $("textarea#buy_sell").show();
    var editor = CKEDITOR.replace('editor1', {
        enterMode: CKEDITOR.ENTER_BR,
        filebrowserUploadUrl: APP_URL + "/ckeditor/upload?type=images&_token=" + csrf_token,
        filebrowserUploadMethod: 'form',

    });
});

CKEDITOR.config.allowedContent = true;

$(document).ready(function() {
    $('#myDatepicker').datetimepicker({
        //minDate: new Date(),
        format: 'DD-MM-YYYY hh:mm',
    });

    $('#myDatepicker2').datetimepicker({
        minDate: moment(),
        format: 'DD-MM-YYYY',
    });

    $('#filter-date-range').daterangepicker({
        autoUpdateInput: false

    });

    $('#filter-date-range').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    });

    $('#filter-date-range').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('.deleted').on("click", function() {
        var ab = confirm('Are you sure want to delete ?');

        if (ab == true) {
            return true;
        } else {
            return false;
        }
    });

});


// Change status event	
$(document).on('change', '.change_status', function() {
    var id = $(this).attr('data-id');
    var status_val = $(this).attr('data-status');
    var dbtable = $(this).attr('data-dbtable');
    var url = APP_URL + "/user/toggle_active";

    if ($(this).is(':checked') === true) {
        change_status(url, dbtable, status_val, id);
    } else {
        var confirm_alert = confirm('Are you sure want to change status ?');
        $(this).prop("checked", true);
        if (confirm_alert == true) {
            $(this).attr("checked", false);
            change_status(url, dbtable, status_val, id);
        }
    }
});

// Change status event for tutor 
$(document).on('change', '.status', function() {
    var id = $(this).attr('data-id');
    var status_val = $(this).attr('data-status');
    var dbtable = $(this).attr('data-dbtable');
    var url = APP_URL + "/tutor-rating/toggle_active";

    if ($(this).is(':checked') === true) {
        change_status(url, dbtable, status_val, id);
    } else {
        var confirm_alert = confirm('Are you sure want to change status ?');
        $(this).prop("checked", true);
        if (confirm_alert == true) {
            $(this).attr("checked", false);
            change_status(url, dbtable, status_val, id);
        }
    }
});

// publish unpublish document  
$(document).on('change', '.publish_document', function() {
    var id = $(this).attr('data-id');
    var status_val = $(this).attr('data-status');
    var dbtable = $(this).attr('data-dbtable');
    var url = APP_URL + "/tutor-rating/toggle_active";

    if ($(this).is(':checked') === true) {
        change_status(url, dbtable, status_val, id);
    } else {
        var confirm_alert = confirm('Are you sure want to unpublish ?');
        $(this).prop("checked", true);
        if (confirm_alert == true) {
            $(this).attr("checked", false);
            change_status(url, dbtable, status_val, id);
        }
    }
});

// publish unpublish page  
$(document).on('change', '.page_status', function() {
    var id = $(this).attr('data-id');
    var status_val = $(this).attr('data-status');
    var dbtable = $(this).attr('data-dbtable');
    var url = APP_URL + "/update-page-status";

    if ($(this).is(':checked') === true) {
        //change_status(url, dbtable, status_val, id);
        //confirm('Are you sure want to unpublish ?');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "GET",
            url: url,
            dataType: 'json',
            data: "id=" + id + "&status=" + status_val, //POST variable name value  
            success: function(msg) {
                //$(".page_status").attr("data-status",msg.page_status_val);

                if (msg.success) {
                    toastr.success(msg.message);
                }

                $(".page_status").attr("data-status", msg.page_status_val);

            },
            error: function(jqXHR, exception) {
                alert('Something went wrong. Please try again !!');
                // Your error handling logic here..
            }
        });
    } else {
        var confirm_alert = confirm('Are you sure want to unpublish ?');
        $(this).prop("checked", true);
        if (confirm_alert == true) {
            $(this).attr("checked", false);
            //change_status(url, dbtable, status_val, id);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "GET",
                url: url,
                dataType: 'json',
                data: "id=" + id + "&status=" + status_val, //POST variable name value  
                success: function(msg) {

                    //$(".page_status").attr("data-status",msg.page_status_val);
                    if (msg.success) {
                        toastr.success(msg.message);
                    }

                    $(".page_status").attr("data-status", msg.page_status_val);

                },
                error: function(jqXHR, exception) {
                    alert('Something went wrong. Please try again !!');
                    // Your error handling logic here..
                }
            });
        }
    }
});

// change status perform
function change_status(url, dbtable, status_val, id) {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: url,
        dataType: 'html',
        data: "id=" + id + "&status_val=" + status_val + "&dbtable=" + dbtable, //POST variable name value	
        success: function(msg) {
            $('#msg').show().html(msg);
        },
        error: function(jqXHR, exception) {
            alert('Something went wrong. Please try again !!');
            // Your error handling logic here..
        }
    });
}


$(function() {
    $('select[multiple]#tags').multiselect({
        placeholder: 'Select Tags',
        selectAll: true,
        search: true
    });
    $('select[multiple]#blogCategory').multiselect({
        placeholder: 'Select Category',
        selectAll: true,
        search: true
    });
});

//single select with search
$(document).ready(function() {

    // Initialize select2
    $("#categories").select2();
    $("#filterBlogCategory").select2();
    $("#userSearch").select2();
    $("#service").select2();
    $("#parent_category_list").select2();
    $("#service_category_id").select2();






    // Read selected option
    // $('#but_read').click(function(){
    //     var username = $('#questionSearch option:selected').text();
    //     var userid = $('#questionSearch').val();

    //     $('#result').html("id : " + userid + ", name : " + username);
    // });
});

function document_confirmation(id) {
    msg = 'Do you really want to publish the document with the related school , subject and course also ? This process cannot be undone';
    open_model(msg, id);
}

function book_confirmation(id) {
    msg = 'Do you really want to publish the book with the related subject and type also ? This process cannot be undone';
    open_model(msg, id);
}

//open pop-up model
function open_model(body, id) {
    //$("#myModal .modal-title").html(title);
    $("#publishDocument .modal-body").html(body);
    $("#publishDocument").modal("show");
    $("#publishDocument .doc-id").val(id);
}

//publish-document-via ajax
function publish_document(thiss) {
    var id = $('input[name="id"]').val();
    var val = "document/publish-document" + '/' + id;
    var url = APP_URL + '/' + val;

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "GET",
        url: url,
        data: { document_id: id },
        beforeSend: function() {
            $(".ajax-loader-parent-div .ajax-loader").css("display", "block");
        },
        success: function(res) {
            $(".ajax-loader-parent-div .ajax-loader").css("display", "none");
            if (res.success) {
                $("#check_" + id).prop('checked', true);
                $("#publish-para-" + id).html('<button type="button" class="btn btn-success">Approved</button>')
                $('#msg').show().html(res.message + '<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>');
            } else {
                alert(res.message)
            }
        },
        error: function(jqXHR, exception) {
            alert('Something went wrong. Please try again !!');
            // Your error handling logic here..
        }
    });
}
$(document).ready(function() {
    $("#acceptRequest").click(function() {
        $(this).attr("disabled", "disabled");;
    });
});

$("#page_form").submit(function(e) {
    e.preventDefault();

    var editor_data = CKEDITOR.instances.editor1.getData();
    var form = $(this);
    var formData = new FormData(this);
    formData.append("content", editor_data);
    var url = form.attr('action');
    if ($("#page_form").valid()) {
        $('#page_form_button').prop('disabled', true);
        $('#page_form_loader').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': APP_TOKEN
            },
            type: "POST",
            url: url,
            contentType: false,
            processData: false,
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.success) {
                    toastr.success(data.message);
                    setTimeout(function() {
                        window.location.href = APP_URL + "/pages";
                    }, 1000);

                } else {
                    toastr.error(data.message);
                }
                $('#page_form_loader').hide();
                $('#page_form_button').prop('disabled', false);
            }
        });
    } else {
        $('<span id="content-error" class="invalid-feedback alert-danger"></span>').insertAfter("#cke_editor1");

        setTimeout(function() {
            $("#content-error").show();
            $("#content-error").text('The content field is required.');
        }, 1000);
    }
});


$("#edit_page_form #page_form_button").on("click", function(e) {
    e.preventDefault();

    var editor_data = CKEDITOR.instances.editor1.getData();
    var formData = new FormData(document.getElementById("edit_page_form"));
    formData.append("content", editor_data);

    if ($("#edit_page_form").valid()) {
        $('#page_form_button').prop('disabled', true);
        $('#page_form_loader').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': APP_TOKEN
            },
            type: "POST",
            url: APP_URL + "/edit-page",
            contentType: false,
            processData: false,
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.success) {
                    toastr.success(data.message);
                    setTimeout(function() {
                        window.location.href = APP_URL + "/pages";
                    }, 1000);

                } else {
                    toastr.error(data.message);
                }
                $('#page_form_loader').hide();
                $('#page_form_button').prop('disabled', false);
            }
        });
        return false;


    } else {
        $('<span id="content-error" class="invalid-feedback alert-danger"></span>').insertAfter("#cke_editor1");

        setTimeout(function() {
            $("#content-error").show();
            $("#content-error").text('The content field is required.');
        }, 1000);
    }
});


$(document).ready(function() {
    //var editor_key = CKEDITOR.instances.editor1.getData();

    CKEDITOR.on('instanceCreated', function(e) {
        e.editor.on('contentDom', function() {
            e.editor.document.on('keyup', function(event) {
                // keyup event in ckeditor
                $("#content-error").remove();
            });
        });
    });

});