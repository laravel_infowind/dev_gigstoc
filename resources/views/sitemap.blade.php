@extends('layouts.default')
@section('content')

<section class="section-middle">

      <div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">Sitemap</div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                          <li class="breadcrumb-item active" aria-current="page">Sitemap</li>
                        </ol>
                    </div>
                  </div>
              </div>
             </div>
     </div>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3 class="top-head"  style="font-size: 30px; font-weight: bolder;">Header Links</h3>
                <ul>
                    <li class="sitemap-link"><a href="{{ url('category') }}">Services</a></li>

                    <li class="sitemap-link"><a href="{{ url('all-job/request') }}">Job Requests</a></li>
                </ul>
                <br>
                <h3 style="font-size: 30px; font-weight: bolder;">Help Links</h3>
                <ul>
                    <li class="sitemap-link"><a href="{{ url('faq') }}">FAQ</a></li>
					<li class="sitemap-link"><a href="{{url('buying-on-gigstoc')}}">Buying on Gigstoc</a></li>
					<li class="sitemap-link"><a href="{{url('selling-on-gigstoc')}}">Selling on Gigstoc</a></li>
                    <li class="sitemap-link bottom-link"><a href="{{ url('contact-us') }}">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-md-6">
                <h3 class="top-head" style="font-size: 30px; font-weight: bolder;">Footer Links</h3>
                <ul>
                    <li class="sitemap-link" ><a href="{{ url('/') }}">Home</a></li>
					<li class="sitemap-link"><a href="{{ url('about-us') }}">About Us</a></li>
					<li class="sitemap-link"><a href="{{url('terms-of-service')}}">Terms of Service</a></li>
					<li class="sitemap-link"><a href="{{url('privacy-policy')}}">Privacy Policy</a></li>
                    <li class="sitemap-link"><a href="{{url('site-map')}}">Sitemap</a></li>
					<li class="sitemap-link"><a href="{{url('how-it-works')}}">How it works</a></li>
					<li class="sitemap-link"><a href="{{url('trust-and-quality')}}">Trust and Quality</a></li>
					<li class="sitemap-link bottom-link"><a href="{{ url('blog') }}">Blog</a></li>
                   
                </ul>
            </div>
        </div>
    </div>

</section>

<style>

.top-head {
    padding-top: 33px;
}
li.sitemap-link.bottom-link {
    padding-bottom: 30px;
}
a {
    font-size: 15px;
	font-weight: 500;
}
.sitemap-link {
    position: relative;
    display: block;
    padding: .5rem .75rem;
    margin-left: -1px;
    line-height: 1.25;
    color: #007bff;
    background-color: #fff;
   
}
</style>
<!-- middle-section -->
@endsection
