@extends('layouts.default')
@section('content')

<section class="middle-section">
         <div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">Payment Method</div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                          <li class="breadcrumb-item active" aria-current="page">Payment Method</li>
                        </ol>
                    </div>
                  </div>
              </div>
             </div>
         </div>
         <div class="main-page">
             <div class="createAgig-area">
                 <div class="container">
                     
                        <div class="row">
                            <div class="col-md-3">
                                <div class="createAgig-left-menu" id="side_link">
                                    @include('common.side-menu')
                                </div>
								<center style="display:none" id="side_loader"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></center>
                            </div>
                            <div class="col-md-9">
						<div class="dashboard-right-main payment-method-main">
                                <div class="dashboard-pro-top">
                                    <div class="check-head-div"><h2>Payment Settings</h2></div>
                                        <div class="user-dtl-div">
                                            <div class="withdraw-top-btns">
                                                <ul>
                                                    <li>
                                                        <a class="btn" id="paypal_btn" href="javascript:void(0)">
                                                            <span>Paypal</span></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="btn" id="stripe_btn" href="javascript:void(0)">
                                                            <span>Bank Account</span></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
							<div class="usr_form_inn" style="display:none;" id="stripe_form">
                                <div class="createAgig-right-main">
                                <h1><strong>Bank Account</strong></h1>
								  <div class="recruit-now-form">
								    <form id="bank-form" method="post" action="{{url('add-bank-account')}}"> 
									  @csrf
                                        <div class="form-group">
										<input type="hidden" name="id" value="{{(!empty($bankAccount))?$bankAccount->id:''}}">
                                            <div class="row">
											<?php
											if(!empty($bankAccount)){
												$accountInfo = json_decode($bankAccount['account_info']);
											}
											?>
                                                <div class="col-md-4">
                                                    <div class="form-input">
                                                        <label for="">First Name</label>
                                                        <input type="text" name="first_name"  value="{{(!empty($accountInfo))?$accountInfo->first_name:''}}" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-input">
                                                        <label for="">Middle Name</label>
                                                        <input type="text" name="middle_name" value="{{(!empty($accountInfo))?$accountInfo->middle_name:''}}" class="form-control" />
                                                    </div>
                                                </div>
												<div class="col-md-4">
                                                    <div class="form-input">
                                                        <label for="">Last Name</label>
                                                        <input type="text" name="last_name" value="{{(!empty($accountInfo))?$accountInfo->last_name:''}}" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										<div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-input">
                                                        <label for="">Bank Name</label>
                                                        <input type="text" name="bank_name" value="{{(!empty($accountInfo))?$accountInfo->bank_name:''}}" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										<div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-input">
                                                        <label for="">Swift Code</label>
                                                        <input type="text" name="swift_code" value="{{(!empty($accountInfo))?$accountInfo->swift_code:''}}" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										<div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-input">
                                                        <label for="">Account Number</label>
                                                        <input type="text" name="account_number" value="{{(!empty($accountInfo))?$accountInfo->account_number:''}}" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                   </form>
								     {!! JsValidator::formRequest('App\Http\Requests\FrontUser\AddBankAccountRequest','#bank-form') !!}
                                 </div>
                                  <div class="recruit-now-form-btn">
                                      <ul>
                                          <li>
										    <button type="button" onclick="saveBankAccount()" id="bank_form_button" class="btn save-btn">
											<i id="formLoader" class="fa fa-spinner fa-spin" style="display:none"></i>
											Save
											</button>
										 </li>
								      </ul>
                                  </div>
                             </div>
                            </div>
							
							<div class="usr_form_inn" style="display:none;" id="paypal_form">
							<div class="createAgig-right-main">
                            <h1><strong>Paypal</strong></h1>
							  <div class="recruit-now-form">
							 
								  <form id="payp_form" method="post" action="{{url('post-service')}}"> 
								  @csrf
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-12">
												<div class="form-input">
												<input type="hidden" value="paypal" name="type">
												<input type="hidden" @if(!empty($paypal_method) && $paypal_method->payment_type==2) value="{{$paypal_method->id}}" @endif name="id">
													<label for="">Email</label>
													<input type="text" @if(!empty($paypal_method) && $paypal_method->payment_type==2) value="{{$paypal_method->account_info}}" @endif name="email" placeholder="Enter email" class="form-control required" />
													<span></span>
												</div>
											</div>
										</div>
									</div>
									
							  </form>
							  </div>
							  <div class="recruit-now-form-btn">
								  <ul>
									  <li>
										<button type="button" onclick="savePaypal()" id="paypal_form_button" class="btn save-btn">
										<i id="paypalformLoader" class="fa fa-spinner fa-spin" style="display:none"></i>
										Save 
										</button>
									  </li>
								  </ul>
							  </div>
							  </div>
                             </div>
                             </div>
                            </div>
                           </div>
						   <!--------
						   <div class="dashboard-right-main">
							<div class="dashboard-pro-top">
                                <div class="check-head-div"><h2>Preferred Payment Method</h2></div>
									<div class="user-dtl-div">
									<form id="select_method_form" > 
										<div class="user-cont-btn withdraw-user-btns">
										<ul>
                                            <li>
                                                <label>
                                                    <input type="radio" class="paypal-preferred" <?php if(!empty($paypal_method) && $paypal_method->payment_type==2 && $paypal_method->preferred_method==1){ echo "checked"; } ?>  name="preferred_method" value="2" >
                                                    <span>Paypal</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" class="paypal-preferred" <?php if(!empty($bankAccount) && $bankAccount->payment_type==1 && $bankAccount->preferred_method==1){ echo "checked"; } ?> name="preferred_method" value="1" >
                                                    <span>Bank Account</span>
                                                </label>
                                            </li>
                                        </ul>
										 
										</div>
										<div class="recruit-now-form-btn">
										  <ul>
											  <li>
												<button type="button" id="select_method_btn" class="btn save-btn">
												<i id="prefferLoader" class="fa fa-spinner fa-spin" style="display:none"></i>
												Save
												</button>
											  </li>
										  </ul>
									  </div>
									</div>
									<form>
									
								</div>
							</div>
							-->
						</div>
					   </div>
                        
                 </div>
             </div>
         </div>
    </section>
	
<script>

$('#paypal_btn').on('click',function(){
    $('#stripe_btn.active').removeClass('active');
    $(this).addClass('active');
	$('#paypal_form').show();
	$('#stripe_form').hide();
});

$('#stripe_btn').on('click',function(){
    $('#paypal_btn.active').removeClass('active');
    $(this).addClass('active');
	$('#paypal_form').hide();
	$('#stripe_form').show();
});

//$('#bank_form_button').on('click',function(){
function saveBankAccount(){
	if ($("#bank-form").valid()) {
        //$('#bank_form_button').prop('disabled',true);
        $('#formLoader').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: "{{url('add-bank-account')}}",
            data: $('#bank-form').serialize(),
            dataType:'json',
            success: function(data) {
                if (data.success) {
                    toastr.success(data.message);
                    setTimeout(function(){
                        window.location.reload();
                    },1000);
                } else {
                    toastr.error(data.message);
                }
                $('#formLoader').hide();
                $('.btn-success').prop('disabled',false);
            }
        });
    }
}

function savePaypal(){
		    var flag = 0;
		$(".required").each(function () {
			if ($.trim($(this).val()) == "") {
			$(this).next('span').addClass('alert-danger').css({"color": "#a94442", "display": "inline-block", "margin-top": "5px"}).html('The field is required.');
				flag++;
			}
		});
		var mail_validation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		$(".required").each(function () {
		if ($(this).val()!='' && !mail_validation.test($(this).val())) {
         $(this).next('span').addClass('alert-danger').css({"color": "#a94442", "display": "inline-block", "margin-top": "5px"}).html('Please fill valid email address.');
         flag++;
		}
		});
		$('.required').keyup(function () {
			$(this).next('span').html("");
			flag--;
		});
		
		if (!flag) {
        $('#paypal_form_buttonv').prop('disabled',true);
        $('#paypalformLoader').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: "{{url('save-payment-method')}}",
            data: $('#payp_form').serialize(),
            dataType:'json',
            success: function(data) {
                if (data.success) {
                    toastr.success(data.message);
                } else {
                    toastr.error(data.message);
                }
                $('#paypalformLoader').hide();
                $('#paypal_form_button').prop('disabled',false);
            }
        });
    }
}
$('#select_method_btn').on('click',function(){
	$('#select_method_btn').prop('disabled',true);
	$('#prefferLoader').show();
	    $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: "{{url('select-payment-method')}}",
            data: $('#select_method_form').serialize(),
            dataType:'json',
            success: function(data) {
                if (data.success) {
                    toastr.success(data.message);
                } else {
                    toastr.error(data.message);
                }
                $('#prefferLoader').hide();
                $('#select_method_btn').prop('disabled',false);
            }
        });
});
$( document ).ready(function() {
   $('#paypal_btn').trigger('click');
   if("{{request()->get('type')}}" == 'bank_account'){
	   $('#stripe_btn').trigger('click');
   }
});
</script>
  @endsection