@extends('layouts.default')
@section('content')
<?php
$counter=1;
?>
 <script type = "text/javascript" src = "https://www.gstatic.com/charts/loader.js"></script>
 	  
 <section class="middle-section">
         <div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">Dashboard</div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                          <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                        </ol>
                    </div>
                  </div>
              </div>
             </div>
         </div>
         <div class="main-page">
             <div class="createAgig-area">
                 <div class="container">
                     
                        <div class="row">
                            <div class="col-md-3">
                               <div class="createAgig-left-menu" id="side_link">
                                    @include('common.side-menu')
                                </div>
								<center style="display:none" id="side_loader"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></center>
                            </div>
                            <div class="col-md-9">
                                <div class="dashboard-right-main">
                                    <div class="dashboard-pro-top">
                                        <div class="user-profile">
                                            <div class="user-img">
                                                <span><img src="{{getUserProfileImage(Auth::user()->id)}}" alt=""></span>
                                            </div>
                                            <div class="user-name">
                                                <h3>{{Auth::user()->fullname}}</h3>
                                                <p><small><i class="fas fa-envelope"></i> {{Auth::user()->email}}</small></p>
                                            </div>
                                        </div>
                                        <div class="user-dtl-div">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="user-dtl">
                                                        <h2><i class="fas fa-map-marker-alt"></i> <span>From</span></h2>
														<p>{{(Auth::user()->country)?Auth::user()->country->name:''}}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="user-dtl">
                                                        <h2><i class="fas fa-globe-africa"></i> <span>Languages</span></h2>
														@if(count(Auth::user()->languages)>0)
														@foreach(Auth::user()->languages as $val)
														<p>{{$val->language->name}}</p>
														@endforeach
														@endif
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="user-dtl">
                                                        <h2><i class="fas fa-info-circle"></i> <span>Bio</span></h2>
														 <p>{{Auth::user()->bio}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="user-cont-btn">
                                                <a class="btn" href="{{url('edit-profile')}}">
                                                <i class="fas fa-edit"></i> <span>Edit your profile</span></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dashboard-bal">
                                        <div class="check-head-div"><h2>Balance</h2></div>
                                        <div class="dashboard-box-div">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="dashboard-div-item">
                                                        <h4>WORKING</h4>
                                                        <h2>$ 0.00</h2>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="dashboard-div-item">
                                                        <h4>AVAILABLE</h4>
                                                        <h2>$ {{getAvailableAmount(Auth::user()->id)}}</h2>
                                                        <!--<p>Earning + Top-Up</p> -->
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="dashboard-div-item">
                                                        <h4>PENDING</h4>
                                                        <h2>$ {{getPendingAmount(Auth::user()->id)}}</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dashboard-box-bottom">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="dashb-btm-txt text-left">Total Balance : <span>${{getExistingBlanace(Auth::user()->id)}}</span></div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="dashb-btm-txt text-right"><a href="{{url('withdrawal-balance')}}"> Withdrawing Balance </a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="dashboard-bal" id="order_statics">
                                        <div class="check-head-div"><h2>Order Statistics</h2></div>
                                        <div class="dashboard-box-div">
                                            <div class="gra-div" id = "container" style = " height: 400px;">
                                                
                                            </div>
                                        </div>
                                    </div>
									
									

                                    <div class="dashboard-prod">
                                        <div class="check-head-div">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h2>MY JOBS</h2>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="dash-prod-head-btn">
                                                        <ul>
                                                            <li><a href="{{url('post-service')}}">Post mjob</a></li>
                                                            <li><a href="{{url('category')}}">View all</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dashboard-box-div">
                                            <div class="dashboard-pro-all">
                                                <ul>
												@if(count($services)>0)
												
												@foreach($services as $service)
												@if($counter<=4)
                                                    <li>
                                                        <div class="sellers-item">
                                                            <div class="sellers-img">
                                                             @foreach($service->images as $key => $value)
															 @if($value->is_featured == 1)
															  <a href="{{url('service/'.$service->id)}}"><img src="{{ url('public/images/services/thumbnail/'.$value->thumbnail) }}" alt=""></a>
															 @endif
															 @endforeach
                                                            </div>
                                                            <div class="sellers-content">
                                                              <div class="sellers-title">
                                                                <a href="{{url('service/'.$service->id)}}">{{$service->title}}</a>
                                                              </div>
                                                              <div class="sellers-cont">
                                                                <table class="table">
                                                                  <tbody><tr>
                                                                    <td class="admin-color"><b>{{$service->user->username}}</b></td>
                                                                    <td></td>
                                                                  </tr>
                                                                  <tr>
                                                                    <td>Price</td>
                                                                    <td><b>${{$service->price}}</b></td>
                                                                  </tr>
                                                                  <tr>
                                                                    <td>Views</td>
                                                                    <td class="view-icn"><b>{{getViewCount($service->id)}} <i class="far fa-eye"></i></b></td>
                                                                  </tr>
                                                                  <tr>
                                                                    <td>Rating</td>
                                                                    <td>
                                                                      <?php echo displayRating(getServicerating($service->id))?>
                                                                    </td>
                                                                  </tr>
                                                                </tbody>
															  </table>
                                                              </div>
                                                              <div class="sellers-btn">
                                                                <a href="{{url('service/'.$service->id)}}">View Detail <i class="fas fa-chevron-right"></i></a>
                                                              </div>
                                                            </div>
                                                          </div>
                                                    </li>
													@endif
													@php
													$counter++;
													@endphp
												   @endforeach
												   @else
												    <li>
												   <div style="width:785px" class='alert alert-danger'><center>No record found</center></div>
												   </li>
                                                   @endif
                                                </ul>
                                            </div>
											@if(count($services)>4)
											<!--	
                                            <div class="dashboard-view-all text-center">
                                                <a href="{{url('category')}}">View all</a>
                                            </div>
											-->
											@endif
                                        </div>
                                    </div>

                                    <div class="dashboard-tab-area">
                                        <div class="check-head-div">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h2>MY ORDERS</h2>
                                                    <!-- <h2>TASKS</h2> -->
                                                </div>
												
                                                <div class="col-md-6">
                                                    <div class="dash-prod-head-btn">
                                                        <ul>
                                                            <li><a href="{{url('my-invoice')}}">View all</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dash-prod-main-div">
                                        <div class="dash-prod-main-table">
										@if(count($orders)>0)
										<table class="table table-hover table-bordered table-striped">
										<tr>
										<th>Order Id</th>
										<th>Service</th>
										<th>Price</th>
										<th>Date</th>
										</tr>
										@foreach($orders as $order)
										<tr>
										<td><a href="{{url('invoice-detail').'/'.base64_encode($order->id)}}">#{{$order->order_id}}</a></td>
										<td>{{($order->detail->service)?$order->detail->service->title:'-'}}</td>
										<td>${{($order->detail->service)?$order->detail->service->price:'-'}}</td>
										<td>{{alphaDateFormat($order->created_at)}}</td>
										</tr>
										@endforeach
										</table>
										@else
                                            <p><div style="width:785px" class='alert alert-danger'><center>No record found</center></div></p>
										@endif
                                        </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        
                 </div>
             </div>
         </div>
    </section>
	 <script type = "text/javascript">
           
      </script>
	<script language = "JavaScript">
	
         
		
		 function getOrderStatics(){
			 $.ajax({
				url: "{{url('get-my-order-statistics')}}",
				cache: false,
				type:'get',
				dataType:'json',
				success: function(response){
					    var result = [];
						for(var i=0;i<response.res.length;i++){
							var row = [response.res[i].date,response.res[i].success_count
							,response.res[i].cancelled_count,response.res[i].pending_count];
							result.push(row);
						}
						google.charts.load('current', {packages: ['line']});
						google.charts.setOnLoadCallback(drawChart);
						function drawChart() {
						  var data = new google.visualization.DataTable();
						  data.addColumn('string', 'Days');
						  data.addColumn('number', 'cancelled');
						  data.addColumn('number', 'pending');
						  data.addColumn('number', 'success');
							console.log(result);
						  data.addRows(result);
						  var options = {
							chart: {
							  title: 'Previous week order statistics',
							  subtitle: ''
							},
							width: 900,
							height: 300,
							vAxis:{viewWindow: {min: 0}},
							axes: {
							  x: {
								0: {side: 'bottom'}
							  }
							}
						  };
						  var chart = new google.charts.Line(document.getElementById('container'));
						  chart.draw(data, google.charts.Line.convertOptions(options));
						}
					
				}
			});
		 }
		 
		 $(document).ready(function(){
			 getOrderStatics();
		 });
		 
		 $("#order_map").click(function() {
			$('html, body').animate({
				scrollTop: $("#order_statics").offset().top
			}, 2000);
		 });

      </script>
	 
@endsection