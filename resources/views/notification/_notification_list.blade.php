<table class="table table-hover table-bordered table-striped">
  <tr>
	<th>Notification</th>
	<th>From</th>
	<th>Notification Date</th>
  </tr> 
@if (count($notifications)>0)
@foreach ($notifications as $value)
  <tr>
	  
		  <td><a href="{{url($value->url)}}">{!!$value->title!!}</a> </td>
		  <td>{{$value->fromUser->fullname}}</td>
		  <td>{{converDateTimeToTz($value->created_at,Auth::user()->timezone)}}</td>
	   
  </tr>  

@endforeach
@else
	<tr>
	  <td colspan="3"><a href="#"><div style="width:100%" class='alert alert-danger'><center>No record found</center></div></a></td>
	</tr>
@endif
</table>
 {{$notifications->links()}}

<style>
a {
    color: #000;
}
</style>
<script>
$('.pagination li a').on('click', function(event) {
        event.preventDefault();
            var url = $(this).attr('href');
            getNotificationList(url);
});
</script>