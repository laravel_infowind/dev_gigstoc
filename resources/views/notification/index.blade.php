@extends('layouts.default')
@section('content')

<section class="middle-section">
         <div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">All Notifications </div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                          <li class="breadcrumb-item active" aria-current="page">Notifications</li>
                        </ol>
                    </div>
                  </div>
              </div>
             </div>
         </div>
         <div class="main-page">
             <div class="alljob-requests-area">
                 <div class="container">
                    <div class="alljob-requests-table-section">
                        <div class="alljob-requests-table" id="notification_list">
                            
                        </div>
                    </div>
                 </div>
             </div>
         </div>
    </section>
	<script>


$(document).ready(function(){
    getNotificationList();
});

function getNotificationList(url){
    var path = (url)?url:"{{url('notification-list')}}";
	$('#notification_list').html('<center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></center>') 
        $.ajax({
            url:path ,
            data: {sorting:$('#sorting').val()},
            method: 'get',
            dataType: 'JSON',
            success:function(result)
            {
                $('#notification_list').html(result.html);
            }
            
        });
    }



</script>
  @endsection