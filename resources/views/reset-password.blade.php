@extends('layouts.default')
@section('content')
<section class="middle-section">
         <div class="page-title">
             <div class="container">
                 <div class="row">
                     <div class="col-md-6">
                        <div class="page-title-head">Reset Password</div>
                     </div>
                     <div class="col-md-6">
                        <div class="page-breadcrumb-head">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Reset Password</li>
                            </ol>
                        </div>
                     </div>
                 </div>
             </div>
         </div>
         <div class="main-page">
             <div class="sign-area">
                 <div class="container">
                    <div class="sign-area-inn">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="sign-form-area">
                                    <div class="sign-head">
                                        <h2>Reset password</h2>
                                        <p>Please enter your password</p>
                                    </div>
                                    <div class="sign-form">
                                        <form id="forgot_form" action="{{url('forgot')}}" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label for="">Password<span>*</span></label>
                                                        <div class="input-group">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="fas fa-lock"></i>
                                                                </span>
                                                            </div>
                                                            <input type="hidden" name="token" value="{{($user)?$user->token : ''}}">
                                                            <input type="password" name="password" class="form-control input_user" value="" placeholder="Password*">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label for="">Confirm Password<span>*</span></label>
                                                        <div class="input-group">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="fas fa-lock"></i>
                                                                </span>
                                                            </div>
                                                            <input type="password" name="confirm_password" class="form-control input_user" value="" placeholder="Confirm Password*">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button type="submit" name="button" class="btn forgot_btn login_btn">Reset
                                                        <i id="form_loader" style="display:none" class="fas fa-spinner fa-spin"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        {!! JsValidator::formRequest('App\Http\Requests\FrontUser\ResetPasswordRequest','#forgot_form') !!}
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="sign-image">
                                    <img src="{{ asset('public/images/sign-img2.png')}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
             </div>
         </div>
    </section> 

<script>

$('#forgot_form .forgot_btn').on('click',function(e){
    e.preventDefault();
    if($('#forgot_form').valid()){
        $('#form_loader').show();
        $('.forgot_btn').prop('disabled',true);
        $.ajax({
            url: "{{url('reset/password')}}",
            data: $('#forgot_form').serialize(),
            method: 'post',
            dataType: 'JSON',
            success:function(result)
            {
                if(result.success){
                    toastr.success(result.message);
                    setTimeout(() => {
                        window.location.href= "{{url('login')}}";   
                    }, 1000);
                }else{
                    toastr.error(result.message);
                }
                $('#form_loader').hide();
                $('.forgot_btn').prop('disabled',false);
            }
            
        });
    }
});


</script>
@endsection