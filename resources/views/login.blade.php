@extends('layouts.default')
@section('content')
<section class="middle-section">
    <div class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="page-title-head">Login</div>
                </div>
                <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Login</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-page">
        <div class="sign-area">
            <div class="container">
                <div class="sign-area-inn">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="sign-form-area">
                                <div class="sign-head">
                                    <h2>Login into account</h2>
                                    <p>Use your credentials to access your account</p>
                                </div>
                                <div class="sign-form">
                                    <form  autocomplete="off" action="{{url('login')}}" id="login-form" method="post">
                                    <?php 
                                        if (old('email')!="") {
                                            $emaill = old('email');
                                        } else {
                                            if (isset($_COOKIE['email'])) {
                                                $emaill = $_COOKIE['email'];
                                            }
                                        }
                                    ?>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div style="display:none" id="resend" class="sign-have-link">
                                                        <label for="">
                                                        If you didn't receive verification email click here <a id="resend_link"  class="resend_link" onclick="resendVerificationMail()" href="javascript:void(0)">Resend mail
                                                        <i id="resend_loader" style="display:none" class="fas fa-spinner fa-spin"></i>
                                                        </a>
                                                        </label>
                                                    </div>
                                                    <label for="">Username or Email<span>*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                            <i class="far fa-envelope"></i>
                                                            </span>
                                                        </div>
                                                        <input type="text" onkeyup="checkError()" name="email" class="form-control input_user" value="" placeholder="UserName or Email address*" value="<?php if(isset($_COOKIE["email"])) {  echo $_COOKIE["email"];} ?>" autocomplete="off">
                                                    </div>
                                                </div>
                                            </div>
                                          
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="">Password<span>*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                            <i class="fas fa-lock"></i>
                                                            </span>
                                                        </div>
                                                        <input type="password" name="password" class="form-control input_pass" value="" placeholder="Password*" value="<?php if(isset($_COOKIE["password"])) {  echo $_COOKIE["password"];} ?>" autocomplete="off">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										       <?php 
												 if (old('email')!="") {
													$emaill = old('email');
												 } else {
													if (isset($_COOKIE['email'])) {
														$emaill = $_COOKIE['email'];
													}
												 }
												?>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="custom-checkbox">
                                                        <label class="form-check-label">
                                                        <input type="checkbox" id="customControlInline" class="custom-control-input" name="remember" {{ old('remember') || isset($emaill)? 'checked' : '' }}>
                                                        <span class="custom-control-label">Keep me logged in</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="custom-forgot text-right">
                                                        <a href="{{url('forgot')}}">Forgot Password?</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="submit" name="button" class="btn login_btn">Login
                                                    <i id="form_loader" style="display:none" class="fas fa-spinner fa-spin"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    {!! JsValidator::formRequest('App\Http\Requests\FrontUser\LoginRequest','#login-form') !!}
                                </div>
                               
                                <div class="sign-in-or">
                                    <div class="sign-or">OR</div>
                                    <div class="sign-with">
                                        <p>Login With</p>
                                        <ul>
                                            <li><a href="{{url('login/facebook')}}"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a href="{{url('login/google')}}"><i class="fab fa-google"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="sign-have-link">
                                    <label for="">
                                    Don't have an account? <a href="{{url('signup')}}">Register here</a>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="sign-image">
                                <img src="{{ asset('public/images/sign-img.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>



</script>



@endsection