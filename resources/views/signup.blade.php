@extends('layouts.default')
@section('content')
<section class="middle-section">
    <div class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="page-title-head">Sign up</div>
                </div>
                <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Sign up</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-page">
        <div class="sign-area">
            <div class="container">
                <div class="sign-area-inn">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="sign-form-area">
                                <div class="sign-head">
                                    <h2>Sign up</h2>
                                    <p>Create your account</p>
                                </div>
                                <div class="sign-form">
                                    <form method="post" action="{{url('signup')}}" autocomplete="off" data-parsley-validate enctype="multipart/form-data" id="signup_form">
                                    {{ csrf_field() }}
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="">Full name<span>*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                            <i class="far fa-user"></i>
                                                            </span>
                                                        </div>
                                                        <input type="text" name="fullname" class="form-control input_user" value="" placeholder="Full name*">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="">Username<span>*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                            <i class="far fa-user"></i>
                                                            </span>
                                                        </div>
                                                        <input type="text" name="username" class="form-control input_user" value="" autocomplete="off" placeholder="Username*">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="">Email address<span>*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                            <i class="far fa-envelope"></i>
                                                            </span>
                                                        </div>
                                                        <input type="text" name="email" class="form-control input_user" value="" placeholder="Email address*">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="">Password<span>*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                            <i class="fas fa-lock"></i>
                                                            </span>
                                                        </div>
                                                        <input type="password" name="password" class="form-control input_pass" value="" autocomplete="off" placeholder="Password*">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="">Confirm password<span>*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                            <i class="fas fa-lock"></i>
                                                            </span>
                                                        </div>
                                                        <input type="password" name="confirmed" class="form-control input_pass" value="" placeholder="Confirm password*">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="custom-checkbox">
                                                        <label class="form-check-label">
                                                        <input type="checkbox" class="custom-control-input" id="customControlInline" name="terms">
                                                        <span class="custom-control-label">I have read and accept the <a target="_blank" href="{{ url('terms-of-service') }}">Terms & Conditions</a></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="submit" name="button" class="btn signup_btn">Sign up
													 <i id="form_loader" style="display:none" class="fas fa-spinner fa-spin"></i>
													 </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    {!! JsValidator::formRequest('App\Http\Requests\FrontUser\SignupRequest','#signup_form') !!}
                                </div>
                                <div class="sign-have-link">
                                    <label for="">
                                    Already have an account? <a href="{{ url('login') }}">Login here</a>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="sign-image">
                                <img src="{{ asset('public/images/sign-img1.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$('.signup_btn').on('click',function(){
if($('#signup_form').valid()){
	$('#form_loader').show();
}	
});

</script>
<!-- middle-section -->
@endsection