@extends('layouts.default')
@section('content')
<section class="middle-section">
    <div class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="page-title-head">Thank You</div>
                </div>
                <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Thank You</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main-page">
        <div class="container">
            <div class="thank-you-page wht-bcack">
                <div class="thank-you">
                    @if(isset($transaction_id) && !empty($transaction_id))
                    <div class="order-success">
                        <div class="success-right"><i class="far fa-check-circle"></i></div>
                        <h2><span class="thank-mess">Thank you!</span> Your order <span class="th-orderid">#{{ 'GIG' . sprintf('%09d', $order_id) }}</span> has been placed successfully.</h2>
                        <h6>Transaction id : <span class="th-transid">{{ $transaction_id }}</span></h6>
                    </div>
                    @else
                    <div class="order-failed">
                        <h2>Your Payment has been Failed</h2>
                    </div>
                    @endif
                </div>
                <div class="continue-shop">
                    <a href="{{ url('category') }}"><i class="fas fa-arrow-left"></i> Back To Services</a>
                </div>
                <!--continue-shop-->
            </div>
        </div><!--container-->
    </div><!--container-->
</section><!--section-middle-->
@endsection