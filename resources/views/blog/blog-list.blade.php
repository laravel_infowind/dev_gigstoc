<div class="blog-list">
@if (!empty($blogs))
@foreach ($blogs as $key => $value)
    <div class="blog-item">
    <div class="blog-img">
    <img src="{{ url('public/images/blog-post').'/'.$value->image }}" alt="">
    </div>
    <div class="blog-cont">
    <div class="blog-date-name">
        <div class="blog-date">
            <b>{{ date('F d', strtotime($value->posted_at)) }}</b>
            <span>{{ date('Y', strtotime($value->posted_at)) }}</span>
            <small>{{ date('h:i A', strtotime($value->posted_at)) }}</small>
        </div>
        <div class="blog-name-user">
            <img src="{{ $value->user->user_image }}"/>
            <span>{{ $value->user->name }}</span>
        </div>
    </div>
    <div class="blog-txt">
        <h2><a href="{{ url('blog').'/'.$value->slug }}">{{ $value->title }}</a>
        </h2>
        <p>{!! $value->short_description !!}</p>
        <div class="blog-read-btn">
            <a href="{{ url('blog').'/'.$value->slug }}">Read More <i class="fas fa-long-arrow-alt-right"></i></a>
        </div>
    </div>
    </div>
    </div>
@endforeach
@endif
</div>

{{$blogs->links()}}

<script>
$('.pagination li a').on('click', function(event) {
        event.preventDefault();
            var url = $(this).attr('href');
            getBlogList(url);
});
</script>