<div class="blog-list">
                        <div class="blog-item">
                            <div class="blog-img">
                                <img src="{{ url('public/images/blog-post').'/'.$blog->image }}" alt="">
                            </div>
                            <div class="blog-cont">
                                <div class="blog-date-name">
                                    <div class="blog-date">
                                        <b>{{ date('F d', strtotime($blog->posted_at)) }}</b>
                                        <span>{{ date('Y', strtotime($blog->posted_at)) }}</span>
                                        <small>{{ date('h:i A', strtotime($blog->posted_at)) }}</small>
                                    </div>
                                    <div class="blog-name-user">
                                        <img src="{{ $blog->user->user_image }}"/>
                                        <span>{{ $blog->user->name }}</span>
                                    </div>
                                </div>
                                <div class="blog-txt">
                                    <h2>{{ $blog->title }}
                                    </h2>
                                    <p>{!! $blog->post_body !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                    <div class="pagination-div d-flex justify-content-center">

                        <ul class="pagination-page">
                            <li class="page-item"><a class="page-link @if(!$previous) disabled @endif" @if($previous) onclick="getBlogDetail('{{$previous}}')"  @endif href="javascript:void(0)">Previous Post</a></li>
                            <li class="page-item"><a class="page-link @if(!$next) disabled @endif" @if($next) onclick="getBlogDetail('{{$next}}')" @endif href="javascript:void(0)">Next Post</a></li>
                        </ul>
                    </div>



                    </script>

                    <style>
                    a.disabled {
                    pointer-events: none;
                    cursor: default;
                    }
                    </style>