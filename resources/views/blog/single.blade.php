@extends('layouts.default')
@section('content')
<section class="middle-section">
    <div class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="page-title-head">Blog</div>
                </div>
                <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Blog</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-page">
        <div class="single-blog-page-area">
            <div class="container">
                <div class="blog-page-inn blog-detail">
                    
                </div>
            </div>
        </div>
    </div>
</section>
<?php 
// echo $blog->id;die;
?>
<script>

$(document).ready(function(){
    getBlogDetail("{{$blog->id}}");
});



function getBlogDetail(id){
	$('.blog-detail').html('<center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></center>') 
        $.ajax({
            url:"{{url('blog-detail/')}}"+"/"+id ,
            method: 'get',
            dataType: 'JSON',
            success:function(result)
            {
                $('.blog-detail').html(result.html);
            }
        });
    }


</script>
<!-- middle-section -->
@endsection