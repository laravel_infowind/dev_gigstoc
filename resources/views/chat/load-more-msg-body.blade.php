@if(!empty($all_messages))
    @foreach($all_messages as $key => $value)
    @if($value->from_id != $user_id)
    <div class="d-flex justify-content-start mb-4" chat-id="{{ $value->id }}">
        <div class="img_cont_msg">
            <a target="_blank" href="{{ url('my-profile/'. $value->userFrom->id) }}" title="{{ $value->userFrom->name }}">
            <img src="{{ $value->userFrom->user_image }}" class="rounded-circle user_img_msg">
            </a>
        </div>
            <?php if($value->msg_type == 1) { ?>
                <!-- Custom Offer Message -->
                <div class="msg_cotainer custom-offer">
                    <p class="offer-label"><a href="javascript:void(0);" data-id="<?php echo $value->custom_order_id; ?>" class="color-custom-label name-customer-order"><?php echo 'Custom Offer'; ?></a></p>
                    {!! $value->message !!}
                    <ul>
                        @if(($value->offer->images) && !empty($value->offer->images))
                        @foreach($value->offer->images as $single)
                        <li class="image-item" id="{{ $single->id }}"><a class="ellipsis" target="_blank" title="{{ $single->image }}" href="{{ url('public/images/job_request/'.$single->image) }}"><i class="fa fa-paperclip"></i> {{ $single->image }}</a></li>
                        @endforeach
                        @endif
                    </ul>
                    <div class="budget">
                        <p><?php echo 'Budget'; ?>
                            <span class="mje-price-text">
                            <span title="$<?php if ($value->offer->price) echo number_format($value->offer->price, 2); ?>"><span class="mje-price"><sup>$</sup><?php if ($value->offer->price) echo number_format($value->offer->price, 2); ?></span></span></span>
                        </p>
                    </div>
                    <div class="deadline"><p><?php echo 'Time of delivery'; ?><span><?php if($value->offer->deadline) echo $value->offer->deadline. ' days'; ?></span></p></div>
                </div>
                <!-- End Custom Offer Message -->
            <?php } else if($value->msg_type == 2) { ?>
                <!-- Custom Order Message -->
                <div class="msg_cotainer custom-order">
                    <p class="offer-label"><?php echo $value->custom_order->title; ?></p>
                    <p class="view-custom-order"><a href="javascript:void(0);" class="link-view-custom-order name-customer-order" data-id="<?php echo $value->custom_order_id; ?>"><?php echo 'View'; ?></a> <?php echo 'this custom order'; ?></p>
                    <div class="budget">
                        <p><?php echo 'Budget'; ?>
                            <span class="mje-price-text">
                            <span title="$<?php if ($value->custom_order->budget) echo number_format($value->custom_order->budget, 2); ?>"><span class="mje-price"><sup>$</sup><?php if ($value->custom_order->budget) echo number_format($value->custom_order->budget, 2); ?></span></span></span>
                        </p>
                    </div>
                    <div class="deadline"><p><?php echo 'Time of delivery'; ?>
                    <span><?php if ($value->custom_order->deadline) echo $value->custom_order->deadline.' days'; ?></span></p></div>
                </div>
            <?php } else if($value->msg_type == 3) { ?>
                <div class="msg_cotainer">
                    <p class="offer-label name-customer-order" data-id="<?php echo $value->custom_order_id; ?>"><?php echo 'CUSTOM ORDER DECLINED'; ?></p>
                    {!! $value->message !!}
                </div>
            <?php } else if($value->msg_type == 4) { ?>
                <div class="msg_cotainer">
                    <p class="offer-label name-customer-order" data-id="<?php echo $value->custom_order_id; ?>"><?php echo 'OFFER REJECTED'; ?></p>
                    {!! $value->message !!}
                </div>
            <?php } else { ?>
                <div class='msg_cotainer'>
                    {!! $value->message !!}
                    <ul>
                        @if(($value->files) && !empty($value->files))
                        @foreach($value->files as $single)
                        <li class="image-item" id="{{ $single->id }}"><a class="ellipsis" target="_blank" title="{{ $single->file_name }}" href="{{ url('public/images/chat/'.$single->chat_room_id.'/'.$single->file_name) }}"><i class="fa fa-paperclip"></i> {{ $single->file_name }}</a></li>
                        @endforeach
                        @endif
                    </ul>
                </div>
            <?php }?>
        <span class="msg_time msg_receiver_time">{{  $value->chat_time }}</span>
    </div>
    @endif
    @if($value->from_id == $user_id)
    <div class="d-flex justify-content-end mb-4" chat-id="{{ $value->id }}">
        <?php if($value->msg_type == 1) { ?>
            <!-- Custom Offer Message -->
            <div class="msg_cotainer_send custom-offer">
                <p class="offer-label"><a href="javascript:void(0);" data-id="<?php echo $value->custom_order_id; ?>" class="color-custom-label name-customer-order"><?php echo 'Custom Offer'; ?></a></p>
                {!! $value->message !!}
                <ul>
                    @if(($value->offer->images) && !empty($value->offer->images))
                    @foreach($value->offer->images as $single)
                    <li class="image-item" id="{{ $single->id }}"><a class="ellipsis" target="_blank" title="{{ $single->image }}" href="{{ url('public/images/job_request/'.$single->image) }}"><i class="fa fa-paperclip"></i> {{ $single->image }}</a></li>
                    @endforeach
                    @endif
                </ul>
                <div class="budget">
                    <p><?php echo 'Budget'; ?>
                        <span class="mje-price-text">
                        <span title="$<?php if ($value->offer->price) echo number_format($value->offer->price, 2); ?>"><span class="mje-price"><sup>$</sup><?php if ($value->offer->price) echo number_format($value->offer->price, 2); ?></span></span></span>
                    </p>
                </div>
                <div class="deadline"><p><?php echo 'Time of delivery'; ?><span><?php if($value->offer->deadline) echo $value->offer->deadline. ' days'; ?></span></p></div>
            </div>
            <!-- End Custom Offer Message -->
        <?php } else if($value->msg_type == 2) { ?>
            <!-- Custom Order Message -->
            <div class="msg_cotainer_send custom-order">
                <p class="offer-label"><?php echo $value->custom_order->title; ?></p>
                <p class="view-custom-order"><a href="javascript:void(0);" class="link-view-custom-order name-customer-order" data-id="<?php echo $value->custom_order_id; ?>"><?php echo 'View'; ?></a> <?php echo 'this custom order'; ?></p>
                <div class="budget">
                    <p><?php echo 'Budget'; ?>
                        <span class="mje-price-text">
                        <span title="$<?php if ($value->custom_order->budget) echo number_format($value->custom_order->budget, 2); ?>"><span class="mje-price"><sup>$</sup><?php if ($value->custom_order->budget) echo number_format($value->custom_order->budget, 2); ?></span></span></span>
                    </p>
                </div>
                <div class="deadline"><p><?php echo 'Time of delivery'; ?>
                <span><?php if ($value->custom_order->deadline) echo $value->custom_order->deadline.' days'; ?></span></p></div>
            </div>
        <?php } else if($value->msg_type == 3) { ?>
                <div class="msg_cotainer_send">
                    <p class="offer-label name-customer-order" data-id="<?php echo $value->custom_order_id; ?>"><?php echo 'CUSTOM ORDER DECLINED'; ?></p>
                    {!! $value->message !!}
                </div>
        <?php } else if($value->msg_type == 4) { ?>
                <div class="msg_cotainer_send">
                    <p class="offer-label name-customer-order" data-id="<?php echo $value->custom_order_id; ?>"><?php echo 'OFFER REJECTED'; ?></p>
                    {!! $value->message !!}
                </div>
        <?php } else { ?>
            <div class='msg_cotainer_send'>
                {!! $value->message !!}
                <ul>
                    @if(($value->files) && !empty($value->files))
                    @foreach($value->files as $single)
                    <li class="image-item" id="{{ $single->id }}"><a class="ellipsis" target="_blank" title="{{ $single->file_name }}" href="{{ url('public/images/chat/'.$single->chat_room_id.'/'.$single->file_name) }}"><i class="fa fa-paperclip"></i> {{ $single->file_name }}</a></li>
                    @endforeach
                    @endif
                </ul>
            </div>
        <?php }?>
        <span class="msg_time msg_sender_time">{{ $value->chat_time }}</span>
        <div class="img_cont_msg">
            <a target="_blank" href="{{ url('my-profile/'. $value->userFrom->id) }}" title="{{ $value->userFrom->name }}">
            <img src="{{ $value->userFrom->user_image }}" class="rounded-circle user_img_msg">
            </a>
        </div>
    </div>
    @endif
    @endforeach
@endif