@extends('layouts.default')
@section('content')
<link rel="stylesheet" href="{{ asset('public/css/chat.css') }}" />
<section class="middle-section">
        <div class="page-title">
            <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">Chat <small>with</small> <a target="_blank" href="{{ url('my-profile/'.$to_id) }}"><span>{{ getUsername($to_id) }}</span></a></div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                          <li class="breadcrumb-item active" aria-current="page">Chat</li>
                        </ol>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-page block-page">
            <!--custom offer partial view-->
            <div class="outer-detail-custom-order">
                <div class="detail-custom-order custom-order-box">
                    <div class="close-detail"><i class="fa fa-times"></i></div>
                    <div class="action-form content-custom-order" style="display: block;">
                        <!--content custom order-->
                    </div>
                    
                    
                </div>
            </div>
            <!--custom offer partial view-->
            <div class="overlay-custom-detail"></div>
            
            <div class="withdraw-balance-area">
                <div class="container">
                    <div class="row justify-content-center h-100">
                        <div class="col-md-8">
                            <div class="msg_head_main">
                                <div class="card-header msg_head">
                                    <div class="d-flex bd-highlight">

                                    </div>
                                </div>

                                <!-- chat_wrapper -->
                                <div id="msg_body">
                                    <div class="chat-paginations"><button id="ae_message-inview" chat-id="" class="load-more-chat btn btn-primary">Load older messages <i class="load_older_loader fa fa-spinner fa-spin" style="display:none"></i></button></div>
                                    <div class="conversation-text card-body msg_card_body">
                                        <center class="chat-loader"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></center>
                                    </div>
                                </div>
                                <!-- chat_wrapper -->

                                <form id="chat-form">
                                <input type="hidden" id="chat_room_id" value="{{ $room_id }}"/>
                                <input type="hidden" id="from_id" value="{{ $from_id }}"/>
                                <input type="hidden" id="to_id" value="{{ $to_id }}"/>
                                <div class="card-footer msg_card_footer">
                                    <ul class="gallery-image carousel-list carousel_single_conversation-image-list" id="image-list">
                                    </ul>
                                    <div class="input-group">
                                        <textarea name="text_msg" id="chat_msg" class="form-control type_msg" placeholder="Type your message..."></textarea>
                                        <div class="input-group-append">
                                            <span class="input-group-text send_btn"><i class="fas fa-location-arrow"></i></span>
                                        </div>
                                        <div class="input-group-append">
                                            <span class="input-group-text attach_btn"><i class="fas fa-paperclip"></i>
                                                <input id="images" type="file" name="chat_attachment" value="" multiple/>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="custom-order-box custom-order-chat wht-bcack">
                                <div class="extra-div-head">
                                    <h2>Custom orders</h2>
                                </div>
                                <div class="extra-div-main">
                                <div class="list-order list-custom-order-"wrapper">
                                    <ul class="list-custom-order">
                                    @if(!empty($custom_orders) && $custom_orders->count() > 0)
                                        @foreach($custom_orders as $key => $value)
                                        <?php
                                        $offet_btn = true;
                                        $offer_color = '';
                                        $get_offer_text = 'Order sent';
                                        if ($value->custom_offer_status == 1) {
                                            if (($user_id == $value->sent_to)) {
                                                $offer_color = "get-offer-color";
                                                $get_offer_text = '';
                                                $offet_btn = false;
                                            }
                                        } else if ($value->custom_offer_status == 2) {
                                            if (($user_id == $value->sent_to)) {
                                                $offer_color = "get-offer-color";
                                                $get_offer_text = 'Offer Sent';
                                            } else {
                                                $offer_color = "get-offer-color";
                                                $get_offer_text = 'Got Offer';
                                            }
                                        }
                                        ?>
                                        <li>
                                            <div id="custom-order-{{ $value->id }}">
                                                <h2>
                                                    <a data-id="{{ $value->id }}" title="{{ $value->title }}" class="name-customer-order">{{ $value->title }}</a>
                                                </h2>
                                                @if($offet_btn)
                                                <div class="label-status order-color {{ $offer_color }} "><span>{{ $get_offer_text }}</span></div>
                                                @endif
                                                <p class="post-content">{!! $value->description !!}</p>
                                                <div class="outer-etd">
                                                    <div class="deadline">
                                                        <p>
                                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                                            <span>{{ $value->deadline }} {{ ($value->deadline > 1) ? 'days' : 'day' }}</span>
                                                        </p>
                                                    </div>
                                                    <div class="budget">
                                                        <p><span class="mje-price-text"><span title="${{ number_format($value->budget, 2) }}"><span class="mje-price"><sup>$</sup>{{ number_format($value->budget, 2) }}</span></span></span></p>
                                                    </div>
                                                </div>
                                                <?php
                                                if (($value->custom_offer_status == 1) && ($user_id == $value->sent_to)) {?>
                                                <div class="custom-order-btn">
                                                    <button class="btn-decline" data-custom-order="{{ $value->id }}" data-sent-by="{{ $value->created_by }}">Decline</button>
                                                    <button class="btn-send-offer" data-custom-order="{{ $value->id }}"  data-title="{{ $value->title }}" data-parent-service-id="{{ $value->service_id }}" data-created-by="{{ $value->created_by }}">Send offer</button>
                                                </div>
                                                <?php }?>
                                            </div>
                                        </li>
                                        @endforeach
                                    @else
                                        <p class="text-center">No Custom Orders</p>
                                    @endif
                                    </ul>
                                    <div class="more"></div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</section>

<!-- Offer send model -->
<div class="modal fade in" id="customOfferModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Send Offer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body form-custom-order">
                <form class="et-form form-delivery-order customOfferModal ">
                    <h3 class="mjob-name">Title</h3>
                    <div class="form-group text-field">
                        <label for="form_post_content">Description</label>
                        <textarea class="form-control description_val" name="post_content" id="description" ></textarea>
                        <p id="from_description" class="error">This field is required.</p>
                    </div>

                    <div class="form-group clearfix">
                        <div class="form-group">
                            <label for="form-budget">Budget (USD)</label>
                            <input type="number" min="1" name="budget" id="budget" class="form-control budget_val">
                            <p id="from_budget" class="error">This field is required.</p>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <div class="form-group time">
                            <label for="from_deadline">Time of delivery (Day)</label>
                            <input type="number" min="1" name="deadline" id="deadline" class="form-control deadline_val">
                            <p id="from_deadline" class="error">This field is required.</p>
                        </div>
                    </div>

                    <div class="form-group clearfix">
                        <div class="attachment-file gallery_container" id="gallery_container">
                            <div class="attachment-image">
                                <ul class="offer-gallery-image carousel-list custom-order-image-list" id="offer-image-list">
                                </ul>
							    <div>
									<span class="plupload_buttons" id="custom-order_container" style="position: relative;">
										<span class="img-gallery" id="custom-order_browse_button" style="position: relative; z-index: 1;">
											<a href="#" class="add-img">Attach file <i class="fa fa-plus"></i></a>
										</span>
                                        <div>
                                            <input id="offer_images" type="file" multiple="" accept="">
                                        </div>
                                    </span>
								</div>
                                </div>
                        </div>
                    </div>
                    <input type="hidden" id="job_request_id" value=""/>
                    <input type="hidden" id="created_by" value="{{ $user_id }}"/>
                    <input type="hidden" id="service_title" value=""/>
                    <input type="hidden" id="parent_service_id" value=""/>
                    <input type="hidden" id="offer_sent_to" value=""/>
                    <button type="button" class="send-custom-offer waves-effect waves-light btn-diplomat btn btn-primary">Send Offer<i style="display:none" class="loader fas fa-spinner fa-spin"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Offer send model -->

<!-- Order declined model -->
<div class="modal fade in" id="declinedModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">DECLINE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body form-decline-order">
                <form class="decline-form  et-form">
                    <p class="cata-title"></p>
                    <div class="form-group clearfix">
                        <textarea required="required" class="form-control decline-reason" name="why_decline" placeholder="Why do you decline this custom order? "></textarea>
                        <p id="decline_reason" class="error">This field is required.</p>
                    </div>
                    <input type="hidden" class="input-item" id="decline_job_request_id" name="custom_order_id" value="">
                    <input type="hidden" id="decline_sent_to" name="sent_to" value=""/>
                    <button type="submit" class="send-decline-order waves-effect waves-light btn-diplomat btn btn-primary">Send<i style="display:none" class="loader fas fa-spinner fa-spin"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Order declined model -->

<!-- Rejected offer model -->
<div class="modal fade in" id="rejectModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">REJECT OFFER</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body form-reject-order">
                <form class="reject-form  et-form">
                    <p class="cata-title"></p>
                    <div class="form-group clearfix">
                        <textarea required="required" class="form-control reject-reason" name="why_reject" placeholder="Why do you reject this offer? "></textarea>
                        <p id="reject_reason" class="error">This field is required.</p>
                    </div>
                    <input type="hidden" class="input-item" id="reject_job_request_id" name="custom_order_id" value="">
                    <input type="hidden" id="reject_sent_to" name="sent_to" value=""/>
                    <input type="hidden" id="reject_offer_id" name="offer_id" value=""/>
                    <button type="submit" class="send-reject-offer waves-effect waves-light btn-diplomat btn btn-primary">Send<i style="display:none" class="loader fas fa-spinner fa-spin"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Rejected offer model -->
<script>
var gallery_images = [];
var page = 1;

// Refresh chat afetr 15 sec
$(document).ready(function(){
    var chat_room_id = $('#chat_room_id').val();
    var from_id = $('#from_id').val();
    var to_id = $('#to_id').val();
    setInterval(function() {
        checkUnreadCount(chat_room_id, from_id, to_id);
        }, 10000);
});

function chatBody(page)
{
	$('#msg_body .msg_card_body').show();
    $.ajax({
        url:"{{ url('chat/chat-body/'.$room_id.'/?page=') }}" + page,
        method: 'get',
        dataType: 'JSON',
        success:function(res)
        {
            if (res.success) {
                $('#msg_body .chat-loader').hide();
                if (res.msg_count < 10) {
                    $('#msg_body .load-more-chat').hide();
                }
                if (res.page_number == '1') {
                    $('#msg_body .load-more-chat').attr('chat-id', res.last_chat_id);
                    $('#msg_body .msg_card_body').html(res.html);
                    $("#msg_body").animate({ scrollTop: $("#msg_body")[0].scrollHeight}, 1000);
                } else {
                    $('.load_older_loader').css("display", "none");
                    $('#msg_body .load-more-chat').attr('chat-id', res.last_chat_id);
                    $('#msg_body .msg_card_body').prepend(res.html);
                    $("#msg_body").animate({ scrollTop: 0}, 1000);
                }

            }
        },
        error: function(xhr, status, errorThrown) {
            toastr.error("Sorry, there was a problem!");
            $('#msg_body .chat-loader').hide();
        }
    });
}



$(document).ready(function(){
    // on enter keypress trigger send msg
    $('#chat_msg').keypress(function (e) {
        if (e.which == 13 && !e.shiftKey) {
            e.preventDefault();
            $('.send_btn').trigger('click');
            return true;
        }
    });

    // send msg button
    $(document).on('click', '.send_btn', function(){
        var msg = $('#chat_msg').val().trim();
        var chat_room_id = $('#chat_room_id').val();
        var from_id = $('#from_id').val();
        var to_id = $('#to_id').val();

        if (msg == '') {
            setCaretToPos($('#chat_msg')[0], 0);
            return false;
        } else {
            // AJAX request
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                url: '{{ url("send-message") }}',
                type: 'post',
                data: 'chat_room_id=' + chat_room_id + '&from_id=' + from_id + '&to_id=' + to_id + '&message=' + msg + '&images=' + JSON.stringify(gallery_images),
                dataType: 'json',
                success: function (res) {
                    console.log(res);
                    if(res.success) {
                        setCaretToPos($("#chat_msg")[0], 0);
                        $('#image-list').empty();
                        if (res.html != '') {
                            $('#msg_body .load-more-chat').attr('chat-id', res.last_chat_id);
                            $('#msg_body .msg_card_body').append(res.html);
                            $("#msg_body").animate({ scrollTop: $("#msg_body")[0].scrollHeight}, 1000);
                        }
                        gallery_images = [];
                    } else {
                        toastr.error(data.message);
                    }

                },
                error: function(xhr, status, errorThrown) {
                    toastr.error("Sorry, there was a problem!");
                    setCaretToPos($("#chat_msg")[0], 0);
                }
            });
            return true;
        }
    });

    // reset cursor on textarea
    function setSelectionRange(input, selectionStart, selectionEnd) {
        if (input.setSelectionRange) {
            input.focus();
            input.setSelectionRange(selectionStart, selectionEnd);
        } else if (input.createTextRange) {
            var range = input.createTextRange();
            range.collapse(true);
            range.moveEnd('character', selectionEnd);
            range.moveStart('character', selectionStart);
            range.select();
        }
    }

    function setCaretToPos(input, pos) {
        document.getElementById('chat_msg').value = "";
        setSelectionRange(input, pos, pos);
    }

    // load chat when page load
    chatBody(page);

    // load more chat
    $(document).on('click', '.load-more-chat', function() {
        page++;
        $('.load_older_loader').css("display", "inline-block");
        chatBody(page);
    });

    // upload files
    $(document).on('change', '#images', function(){
        var chat_room_id = $('#chat_room_id').val();
        var form_data = new FormData();
        var err = 0;
        form_data.append('chat_room_id', chat_room_id);
        // Read selected files
        var totalfiles = document.getElementById('images').files.length;
        for (var index = 0; index < totalfiles; index++) {
            var name = document.getElementById("images").files[index].name;
            form_data.append("images[]", document.getElementById('images').files[index]);

            var ext = name.split('.').pop().toLowerCase();
            if(jQuery.inArray(ext, ['gif','png','jpg','jpeg','docx','doc','pdf']) == -1)
            {
                toastr.error("Invalid File Extension");
                err++;
            }

            var size = document.getElementById("images").files[index].size;
            if(size > 20000000)
            {
                toastr.error("File Size should be less than 20 MB");
                err++;
            }

        }

        if (err > 0) {
            return false;
        } else {
            // AJAX request
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                url: '{{ url("chat/file-upload") }}',
                type: 'post',
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (res) {
                    if(res.success) {
                        for(var i=0;i<res.data.length ; i++){
                            $('#image-list').append("<li id="+ res.data[i].name +" class='image-item' id="+ res.data[i].name +"><a href='javascript:void(0)'><i class='fa fa-paperclip'></i> "+ res.data[i].name + "." + res.data[i].type +"</a> <a title='Delete' class='del_img remove_photo_" + res.data[i].name + "' href='javascript:void(0)' onclick='deleteFile("+res.data[i].name+","+"`"+res.data[i].type+"`"+")'><i class='fa fa-times'></i></a><i id='login_loader_"+res.data[i].name +"' class='fa fa-spinner fa-spin' style='display:none'></i></li>");
                            gallery_images.push(res.data[i]);
                        }
                            console.log(gallery_images);
                    } else {
                        $.each(res.message, function(i, val) {
                            toastr.error(val)
                        });
                    }
                },
                error: function(xhr, status, errorThrown) {
                    toastr.error("Sorry, there was a problem!");
                }
            });
        }
    });
});

// check unread messages and append in chat
function checkUnreadCount(chat_room_id, from_id, to_id)
{
    // check unread message
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        url: '{{ url("chat/check-unread-count") }}',
        type: 'post',
        data: 'chat_room_id=' + chat_room_id + '&from_id=' + from_id + '&to_id=' + to_id,
        dataType: 'json',
        success: function (res) {
            //console.log(res);
            if(res.success) {
                if ((res.html != '') && (res.chat_room_id == chat_room_id)) {
                    $('#msg_body .msg_card_body').append(res.html);
                    $("#msg_body").animate({ scrollTop: $("#msg_body")[0].scrollHeight}, 1000);
                }
            } else {
                //toastr.error(data.message);
            }

        },
        error: function(xhr, status, errorThrown) {
            //toastr.error("Sorry, there was a problem!");
        }
    });
}

// delete file
function deleteFile(name, type) {
    var chat_room_id = $('#chat_room_id').val();
    var result = confirm("Are you sure to delete?");
    if(result){
        $('#login_loader_'+name).show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "GET",
            url: "{{ url('chat/delete-file') }}/" + name + "/"+ type + "/"+ chat_room_id,
            dataType:'json',
            success: function(data) {
                $('#login_loader_'+name).hide();
                if (data.success) {
                    $('li#'+name).remove();
                    $('#images').val('');
                    console.log(gallery_images);
                    const index = gallery_images.indexOf('"'+name+'"');
                    if (index < 1) {
                    gallery_images.splice(index, 1);
                    }

                    console.log(gallery_images);
                    toastr.success(data.message);
                } else {
                    toastr.error(data.message);
                }
            }

        });
    }
}

// send custom offer
$(document).on("click", ".btn-send-offer", function() {
    var custom_order_id = $(this).data('custom-order');
    var service_title = $(this).data('title');
    var parent_service_id = $(this).data('parent-service-id');
    var offer_sent_to = $(this).data('created-by');
    $('#customOfferModal .modal-body #job_request_id').val(custom_order_id);
    $('#customOfferModal .modal-body .mjob-name').text(service_title);
    $('#customOfferModal .modal-body #service_title').val(service_title);
    $('#customOfferModal .modal-body #parent_service_id').val(parent_service_id);
    $('#customOfferModal .modal-body #offer_sent_to').val(offer_sent_to);
    $('#customOfferModal').modal('show');
});

$(document).ready(function() {
    $(document).on('click', '.send-custom-offer', function(){
        var error =0;
        var description = $('.description_val').val().trim();
        var budget = $('.budget_val').val();
        var deadline = $('.deadline_val').val();
        var job_request_id = $('#job_request_id').val();
        var parent_service_id = $('#parent_service_id').val();
        var created_by = $('#created_by').val();
        var title = $('#service_title').val();
        var offer_sent_to = $('#offer_sent_to').val();


        if(description == '') {
            $('#from_description').text('This field is required');
            $('#from_description').show();
            error++;
        } else {
            $('#from_description').text('');
            $('#from_description').hide();
        }

        if(budget == '') {
            $('#from_budget').text('This field is required');
            $('#from_budget').show();
            error++;
        } else if(parseInt(budget) < parseInt(1)) {
            $('#from_budget').text('Value greater than or equal to 1');
            $('#from_budget').show();
            error++;
        } else {
            $('#from_budget').text('');
            $('#from_budget').hide();
        }

        if(deadline == '') {
            $('#from_deadline').text('This field is required');
            $('#from_deadline').show();
            error++;
        } else if(parseInt(deadline) < parseInt(1)) {
            $('#from_deadline').text('Value greater than or equal to 1');
            $('#from_deadline').show();
            error++;
        } else {
            $('#from_deadline').text('');
            $('#from_deadline').hide();
        }

        if(error > 0) {
            return false;
        } else {
            $('.send-custom-order').prop('disabled',true);
            $('.send-custom-order .loader').show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                type: "POST",
                url: "{{url('send-custom-offer')}}",
                data: 'title=' + title + '&description=' + description + '&budget=' + budget + '&deadline=' + deadline + '&job_request_id=' + job_request_id + '&parent_service_id=' + parent_service_id + '&offer_sent_to=' + offer_sent_to + '&images=' + JSON.stringify(offer_gallery_images),
                dataType:'json',
                success: function(data) {
                    if (data.success) {
                        toastr.success(data.message);
                        $('#customOfferModal').modal('hide');
                        location.reload();
                    } else {
                        toastr.error(data.message);
                        $('#customOfferModal').modal('hide');
                    }
                    $('.send-custom-offer .loader').hide();
                    $('.send-custom-offer').prop('disabled',false);
                },
                error: function(xhr, status, errorThrown) {
                    toastr.error("Sorry, there was a problem!");
                    $('.send-custom-offer .loader').hide();
                    $('.send-custom-offer').prop('disabled',false);
                }
            });
            return true;
        }
    });
});

// upload files
var offer_gallery_images = [];
$(document).on('change', '#offer_images', function(){
    var job_request_id = $('#job_request_id').val();
    var form_data = new FormData();
    var err = 0;
    form_data.append('job_request_id', job_request_id);
    // Read selected files
    var totalfiles = document.getElementById('offer_images').files.length;
    for (var index = 0; index < totalfiles; index++) {
        var name = document.getElementById("offer_images").files[index].name;
        form_data.append("images[]", document.getElementById('offer_images').files[index]);

        var ext = name.split('.').pop().toLowerCase();
        if(jQuery.inArray(ext, ['gif','png','jpg','jpeg','docx','doc','pdf']) == -1)
        {
            toastr.error("Invalid File Extension");
            err++;
        }

        var size = document.getElementById("offer_images").files[index].size;
        if(size > 20000000)
        {
            toastr.error("File Size should be less than 20 MB");
            err++;
        }

    }

    if (err > 0) {
        return false;
    } else {
        // AJAX request
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            url: '{{ url("custom/file-upload") }}',
            type: 'post',
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (res) {
                if(res.success) {
					for(var i=0;i<res.data.length ; i++){
                        $('#offer-image-list').append("<li id="+ res.data[i].name +"><a href='#'><i class='fa fa-paperclip'></i> "+ res.data[i].name + "." + res.data[i].type +"<i class='del_img fa fa-trash remove_photo_" + res.data[i].name + "' onclick='deleteOfferImage("+res.data[i].name+","+"`"+res.data[i].type+"`"+")'></i><i id='login_loader_"+res.data[i].name +"' class='fa fa-spinner fa-spin' style='display:none'></i></li>");
                        offer_gallery_images.push(res.data[i]);
                    }
						console.log(offer_gallery_images);
                } else {
                    $.each(res.message, function(i, val) {
                        toastr.error(val)
                    });
                }
            },
            error: function(xhr, status, errorThrown) {
                toastr.error("Sorry, there was a problem!");
            }
        });
    }
});

// delete file
function deleteOfferImage(name, type) {
    var result = confirm("Are you sure to delete?");
    if(result){
        $('#login_loader_'+name).show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "GET",
            url: "{{ url('custom/delete-file') }}/" + name+"/"+type,
            dataType:'json',
            success: function(data) {
                $('#login_loader_'+name).hide();
                if (data.success) {
                    $('li#'+name).remove();
                    $('#offer_images').val('');
					console.log(offer_gallery_images);
					const index = offer_gallery_images.indexOf('"'+name+'"');
					if (index < 1) {
					  offer_gallery_images.splice(index, 1);
					}

                    console.log(offer_gallery_images);
                    toastr.success(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function(xhr, status, errorThrown) {
                toastr.error("Sorry, there was a problem!");
            }

        });
    }
}

// custom order and offer detail overview
$(document).ready(function(){
    $(document).on('click', '.close-detail', function(){
        $('.outer-detail-custom-order').hide('slide');
        $('.overlay-custom-detail').fadeOut();
    });

    // click on custom order detail
    $(document).on('click', '.name-customer-order', function()
    {
        var custom_order_id = $(this).data('id');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: "{{ url('custom-order-detail') }}",
            data: 'custom_order_id=' + custom_order_id,
            dataType:'json',
            success: function(res) {
				console.log(res);
                if (res.success) {
                    $('.content-custom-order').html(res.html);
                    showOuterDetail();
                } else {
                    toastr.error(res.message);
                }
            },
            error: function(xhr, status, errorThrown) {
                toastr.error("Sorry, there was a problem!");
            }
        });
    });
});

// show custom order content
function showOuterDetail()
{
    $(".outer-detail-custom-order" ).show({direction: "left" }, 1000);
    $('.overlay-custom-detail').fadeIn();
    var body = $("html, body");
    body.stop().animate({scrollTop: 0}, '500', 'swing');
}

$(document).ready(function(){
    // decline order
    $(document).on('click', '.btn-decline', function(){
        var custom_order_id = $(this).data('custom-order');
        var sent_by = $(this).data('sent-by');
        $('#declinedModal .modal-body #decline_job_request_id').val(custom_order_id);
        $('#declinedModal .modal-body #decline_sent_to').val(sent_by);
        $('#declinedModal').modal('show');
    });
    
    $(document).on('click', '.send-decline-order', function(e){
        e.preventDefault();
        var decline_val = $('.decline-reason').val().trim();
        if(decline_val == '') {
            $('#decline_reason').show();
            return false;
        } else {
            $('#decline_reason').hide();
            $('.send-decline-order').prop('disabled',true);
            $('.send-decline-order .loader').show();
            $.ajax({  
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                type: "POST",
                url: "{{ url('order-declined') }}",
                data: $('form.decline-form').serialize(),
                success: function(res) {
                    $('#declinedModal').modal('hide');
                    $('.send-decline-order').prop('disabled',false);
                    $('.send-decline-order .loader').hide(); 
                    if (res.success) {
                        toastr.success(res.message);
                        location.reload();
                    } else {
                        toastr.error(res.message);
                    }
                },
                error: function(xhr, status, errorThrown) {
                    $('.send-decline-order').prop('disabled',false);
                    $('.send-decline-order .loader').hide();
                    toastr.error("Sorry, there was a problem!");
                }
            });
            return true;
        }
    });

    // Reject offer
    $(document).on('click', '.btn-reject', function(){
        var custom_order_id = $(this).data('custom-order');
        var sent_by = $(this).data('sent-by');
        var offer_id = $(this).data('offer-id');
        $('#rejectModal .modal-body #reject_job_request_id').val(custom_order_id);
        $('#rejectModal .modal-body #reject_sent_to').val(sent_by);
        $('#rejectModal .modal-body #reject_offer_id').val(offer_id);
        $('#rejectModal').modal('show');
    });
    
    $(document).on('click', '.send-reject-offer', function(e){
        e.preventDefault();
        var reject_val = $('.reject-reason').val().trim();
        if(reject_val == '') {
            $('#reject_reason').show();
            return false;
        } else {
            $('#reject_reason').hide();
            $('.send-reject-offer').prop('disabled',true);
            $('.send-reject-offer .loader').show();
            $.ajax({  
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                type: "POST",
                url: "{{ url('reject-offer') }}",
                data: $('form.reject-form').serialize(),
                success: function(res) {
                    $('#rejectModal').modal('hide');
                    $('.send-reject-offer').prop('disabled',false);
                    $('.send-reject-offer .loader').hide(); 
                    if (res.success) {
                        toastr.success(res.message);
                        location.reload();
                    } else {
                        toastr.error(res.message);
                    }
                },
                error: function(xhr, status, errorThrown) {
                    $('.send-reject-offer').prop('disabled',false);
                    $('.send-reject-offer .loader').hide();
                    toastr.error("Sorry, there was a problem!");
                }
            });
            return true;
        }
    });
});
</script>
<!-- middle-section -->
@endsection