@if($sent_message->from_id != $user_id)
    <div class="d-flex justify-content-start mb-4" chat-id="{{ $sent_message->id }}">
        <div class="img_cont_msg">
            <a target="_blank" href="{{ url('my-profile/'. $sent_message->userFrom->id) }}" title="{{ $sent_message->userFrom->name }}">
            <img src="{{ $sent_message->userFrom->user_image }}" class="rounded-circle user_img_msg">
            </a>
        </div>
        <div class="msg_cotainer">
            {!! $sent_message->message !!}
            <ul>
                @if(($sent_message->files) && !empty($sent_message->files))
                @foreach($sent_message->files as $single)
                <li class="image-item" id="{{ $single->id }}"><a class="ellipsis" target="_blank" title="{{ $single->file_name }}" href="{{ url('public/images/chat/'.$single->chat_room_id.'/'.$single->file_name) }}"><i class="fa fa-paperclip"></i> {{ $single->file_name }}</a></li>
                @endforeach
                @endif
            </ul>
            <span class="msg_time">{{  $sent_message->chat_time }}</span>
        </div>
    </div>
    @endif
    @if($sent_message->from_id == $user_id)
    <div class="d-flex justify-content-end mb-4" chat-id="{{ $sent_message->id }}">
        <div class="msg_cotainer_send">
            {!! nl2br($sent_message->message) !!}
            <ul>
                @if(($sent_message->files) && !empty($sent_message->files))
                @foreach($sent_message->files as $single)
                <li class="image-item" id="{{ $single->id }}"><a class="ellipsis" target="_blank" title="{{ $single->file_name }}" href="{{ url('public/images/chat/'.$single->chat_room_id.'/'.$single->file_name) }}"><i class="fa fa-paperclip"></i> {{ $single->file_name }}</a></li>
                @endforeach
                @endif
            </ul>
            <span class="msg_time">{{ getTimeago(date('Y-m-d H:i:s', $sent_message->time)) }}</span>
        </div>
        <div class="img_cont_msg">
            <a target="_blank" href="{{ url('my-profile/'. $sent_message->userFrom->id) }}" title="{{ $sent_message->userFrom->name }}">
            <img src="{{ $sent_message->userFrom->user_image }}" class="rounded-circle user_img_msg">
            </a>
        </div>
    </div>
    @endif