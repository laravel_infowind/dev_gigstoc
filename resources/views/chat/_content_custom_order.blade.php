<div class="outer-detail-custom">
    <h2>
        <a href="{{ url('service/'.$custom_order->service_id) }}" target="_blank">
        {{ $custom_order->title }}
        </a>
    </h2>
    <p class="date">Modify date: <span>{{ date('F d, Y' ,strtotime($custom_order->updated_at)) }}</span></p>
    <div class="block-text">
        <p class="title">brief</p>
        
        <p>{!! $custom_order->description !!}</p>
        
        <div class="budget">
            <p>Budget<span class="mje-price-text"><span title="${{ number_format($custom_order->budget, 2) }}"><span class="mje-price"><sup>$</sup>{{ number_format($custom_order->budget, 2) }}</span></span></span></p>
        </div>
        <div class="deadline">
            <p>Time of Delivery<span>{{ $custom_order->deadline }} day(s)</span></p>
        </div>
        <ul class="list-attach">
            @if(($custom_order->files) && !empty($custom_order->files))
                @foreach($custom_order->files as $single)
                <li><a target="_blank" title="{{ $single->file }}" href="{{ url('public/images/job_request/'.$single->file) }}"><i class="fa fa-paperclip"></i> {{ $single->file }}</a></li>
                @endforeach
            @endif
        </ul>
    </div>
    <div class="block-text">
        <?php if($custom_order->is_offer == true) { ?>
            <?php if($custom_order->created_by == $user_id) { ?>
                <p class="title">seller's offer</p>
            <?php } else { ?>
                <p class="title">your offer</p>
            <?php } ?>
            <div class="padding-top10">
                <p>{!! $custom_order->offer->description !!}</p>
            </div>
            <div class="budget">
                <p>Price<span class="mje-price-text"><span title="${{ number_format($custom_order->offer->price, 2) }}"><span class="mje-price"><sup>$</sup>{{ number_format($custom_order->offer->price, 2) }}</span></span></span></p>
            </div>
            <div class="deadline">
                <p>Time of Delivery<span>{{ $custom_order->offer->deadline }} day(s)</span></p>
            </div>
            <ul class="list-attach">
            @if(($custom_order->offer->images) && !empty($custom_order->offer->images))
                @foreach($custom_order->offer->images as $single)
                <li><a target="_blank" title="{{ $single->image }}" href="{{ url('public/images/job_request/'.$single->image) }}"><i class="fa fa-paperclip"></i> {{ $single->image }}</a></li>
                @endforeach
            @endif
            </ul>

            <?php if($custom_order->created_by == $user_id) { ?>
                    <?php if($custom_order->custom_offer_status == 2) { ?>
                        <div class="action">
                            <button class="btn-reject" data-custom-order="{{ $custom_order->id }}" data-offer-id="{{ $custom_order->offer->id }}" data-sent-by="{{ $custom_order->offer->created_by }}">Reject offer</button>
                            <a href="{{ url('checkout-detail?sid='.$custom_order->offer->id) }}" class="btn-accept-offer">Accept &amp; Checkout</a>
                        </div>
                <?php } ?>
            <?php } ?>
        <?php } else { ?>
                <?php if($custom_order->created_by != $user_id) { ?>
                    <?php if($custom_order->custom_offer_status == 1) { ?>
                    <div class="custom-order-btn">
                        <button class="btn-decline" data-custom-order="{{ $custom_order->id }}" data-sent-by="{{ $custom_order->created_by }}"><?php echo 'Decline'; ?></button>
                        <button class="btn-send-offer" data-custom-order="{{ $custom_order->id }}" data-title="{{ $custom_order->title }}" data-parent-service-id="{{ $custom_order->service_id }}" data-created-by="{{ $custom_order->created_by }}"><?php echo 'Send offer'; ?></button>
                    </div>
                    <?php } ?>
                <?php } ?>
        <?php } ?>
         
        <?php if($custom_order->custom_offer_status == 5) { ?>
            <div class="custom-order-status checkout">
                <i class="fa fa-check-circle" aria-hidden="true"></i>This offer has been accepted. View order detail <a href="{{ url('invoice-detail/'.base64_encode($custom_order->order_id)) }}" target="_blank">here.</a>
            </div>
        <?php } else if($custom_order->custom_offer_status == 3) { ?>
            <div class="custom-order-status decline">
                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>This custom order has been declined.
            </div>
        <?php } else if($custom_order->custom_offer_status == 4) { ?>
            <div class="custom-order-status reject">
                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>This offer has been rejected
            </div>
        <?php } ?>
</div>