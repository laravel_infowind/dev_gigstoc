@extends('layouts.default')
@section('content')
<section class="middle-section">
    <div class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="page-title-head">Message List</div>
                </div>
                <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Message List</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-page">
        <div class="container">
            <div class="my-message-page">
                <div class="my-message-list">
                    <ul class="list-message">
                    @if(!empty($messages) && count($messages) > 0)
                        @foreach($messages as $key => $value)
                        <li class="message-item {{ ( (($value->is_read  == 1) && ($value->to_id == $user_id)) || (($value->is_shown  == 1) && ($value->from_id == $user_id)) ) ? '' : 'unread-list' }}">
                            <div class="list-inner">
                                <div class="img-avatar">
                                    <a href="{{ url('my-profile/'.$value->profile_id) }}" target="_blank">
                                        <img src="{{ $value->userimage }}" title="{{ $value->name }}">
                                    </a>
                                </div>
                                <div class="message-item-cont">
                                    <a href="{{ url('chat/'.$value->chat_room_id) }}">
                                        <div class="message-item-user">{{ $value->name }}</div>
                                        <div class="txt-item-user">{{ $value->message }}</div>
                                        <div class="user-item-date">{{ $value->chat_time }}</div>
                                    </a>
                                </div>
                                
                            </div>
                        </li>
                        @endforeach
                    @else
                        <p>Chat not found</p>
                    @endif
                    </ul>
                </div>
            </div>
        </div><!--container-->
    </div>
</section><!--section-middle-->
@endsection