							<div>
							<div class="category-main-pro">
									<form id="filter">
                                      <div class="payment-type1 filter-right text-right">
                                        <select name="account_type" id="account_type" onchange="getRequestList()"> 
                                            <option value="">All Account</option>
                                            <option value="bank">Bank</option>
                                            <option value="paypal">Paypal</option>
                                        </select>
									 <select name="status"  id="request_status" onchange="getRequestList()">
                                            <option value="">All Status</option>
                                            <option value="processing">Processing</option>
                                            <option value="pending">Pending</option>
                                        </select>
                                      </div>
									  
									</form>
                                    </div>
									 <div class="createAgig-right-main page-wt-bk my-invoice"  style="margin-top: 10px;">
							        <div class="invoice-list-table">
							        <table class="table table-hover table-bordered table-striped" >
                                      <tr>
                                        <th>Withdrawal Account</th>
                                        <th>Requested date</th>
										<th>Admin Event Logs</th>
									    <th>Total Amount</th>
										<th>Status</th>
								      </tr> 
										@if (count($requestList)>0)
                                        @foreach ($requestList as $value)
                                      <tr>
                                          <td>{{ucfirst($value->selected_method)}}</td>
                                          <td>{{changeDateformat($value->created_at)}}</td>
										  <td>{{changeDateformat($value->updated_at)}}</td>
										  <td>${{$value->amount}}</td>
										  <td>
										  <span class="btn btn-warning">{{ucfirst($value->status)}}</span>
										  </td>
                                      </tr> 
									@endforeach
                                    @else
										<tr>
                                          <td colspan="5"><a href="#"><div style="width:785px" class='alert alert-danger'><center>No record found</center></div></a></td>
                                        </tr>
									@endif
									</table>
								</div>
                                   {{$requestList->links()}}

             </div>
<script>
$('.pagination li a').on('click', function(event) {
        event.preventDefault();
            var url = $(this).attr('href');
            getRequestList(url);
});
</script>