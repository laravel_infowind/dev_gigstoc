@extends('layouts.default')
@section('content')


<section class="middle-section">
         <div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">Payment Method</div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                          <li class="breadcrumb-item active" aria-current="page">Payment Method</li>
                        </ol>
                    </div>
                  </div>
              </div>
             </div>
         </div>
         <div class="main-page">
             <div class="createAgig-area">
                 <div class="container">
                     
                        <div class="row">
                            <div class="col-md-3">
                                <div class="createAgig-left-menu" id="side_link">
								@include('common.side-menu')
                                </div>
								<center style="display:none" id="side_loader"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></center>
                            </div>
							 
                            <div class="col-md-9">
							 <div class="withdraw-top-btns">
								<ul>
									<li>
										<a class="btn" id="request_tab" onclick="getRequestList()" href="javascript:void(0)">
											<span>Withdrawal Request</span></i>
										</a>
									</li>
									<li>
										<a class="btn" id="history_tab" onclick="getRequestHistoryList()" href="javascript:void(0)">
											<span>Withdrawal History</span></i>
										</a>
									</li>
								</ul>
                                </div>
                                <div class="createAgig-right-main page-wt-bk my-invoice" id="request_list">
                                    
                                </div>

							 
                            </div>
                        </div>
                        
                 </div>
             </div>
         </div>
    </section>
<?php 
$type = request()->get('type');
?>
	<script>

$(document).ready(function(){
    getRequestList();
	var type = "{{$type}}";
	if(type == 'history'){
		$('#history_tab').trigger('click');
	}
});



function getRequestList(url){
	
	  var account = $('#account_type').val();
	  var status = $('#request_status').val();
	  var path = (url)?url:"{{url('withdrawal-request-list')}}";
	  $('#request_list').html('<center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></center>') 
        $.ajax({
            url:path ,
			data:{account_type:account,status:status},
            method: 'get',
            dataType: 'JSON',
            success:function(result)
            {
				$('#request_list').html(result.html);
				$('#request_tab').css({'background-color':'#000'});
				$('#history_tab').css({'background-color':''});
				
            }
            
        });
}

function getRequestHistoryList(url){
	   var account = $('#account_type').val();
	   var status = $('#status').val();
	  var path = (url)?url:"{{url('withdrawal-hstory-list')}}";
	  $('#request_list').html('<center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></center>') 
        $.ajax({
            url:path ,
			data:{account_type:account,status:status},
            method: 'get',
            dataType: 'JSON',
            success:function(result)
            {
                $('#request_list').html(result.html);
				$('#history_tab').css({'background-color':'#000'});
				$('#request_tab').css({'background-color':''});
            }
            
        });
}

</script>
@endsection