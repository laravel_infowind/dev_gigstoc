							<div>
							<div class="category-main-pro">
									<form id="filter">
                                      <div class="payment-type1 filter-right text-right">
                                        <select name="account_type" id="acnt_type" onchange="getRequestHistoryList()"> 
                                            <option value="">All Account</option>
                                            <option value="bank">Bank</option>
                                            <option value="paypal">Paypal</option>
                                        </select>
                                        <select name="status"  id="status" onchange="getRequestHistoryList()">
                                            <option value="">All Status</option>
                                            <option value="decline">Declined</option>
                                            <option value="complete">Approved</option>
                                        </select>
                                      </div>
									</form>
                                    </div>
									 <div class="createAgig-right-main page-wt-bk my-invoice"  style="margin-top: 10px;">
							      <div class="invoice-list-table">
							      <table class="table table-hover table-bordered table-striped" >
                                      <tr>
                                        <th>Withdrawal Account</th>
                                        <th>Requested date</th>
										<th>Admin Event Logs</th>
									    <th>Total Amount</th>
										<th>Status</th>
								      </tr> 
										@if (count($historyList)>0)
                                        @foreach ($historyList as $value)
                                      <tr>
                                          <td>{{ucfirst($value->selected_method)}}</td>
                                          <td>{{changeDateformat($value->created_at)}}</td>
										  <td>{{changeDateformat($value->updated_at)}}</td>
										  <td>${{number_format($value->amount,2)}}</td>
										  <td>
										  @if($value->status=='decline')
										  <span class="btn btn-danger">Declined</span>
										  @endif
										  @if($value->status=='complete')
										  <span class="btn btn-success">Success</span>
										  @endif
										  @if($value->status=='processing')
										  <span class="btn btn-warning">Processing</span>
										  @endif
										  </td>
                                      </tr> 
									@endforeach
                                    @else
										<tr>
                                          <td colspan="5"><a href="#"><div style="width:785px" class='alert alert-danger'><center>No record found</center></div></a></td>
                                        </tr>
									@endif
									</table>
								</div>
                                   {{$historyList->links()}}

					  </div>
<script>
$('.pagination li a').on('click', function(event) {
        event.preventDefault();
            var url = $(this).attr('href');
            getRequestList(url);
});
</script>