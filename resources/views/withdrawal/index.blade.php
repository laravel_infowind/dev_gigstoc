@extends('layouts.default')
@section('content')
<section class="middle-section">
         <div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">WITHDRAWING BALANCE</div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                          <li class="breadcrumb-item active" aria-current="page">WITHDRAWING BALANCE</li>
                        </ol>
                    </div>
                  </div>
              </div>
             </div>
         </div>
         <div class="main-page">
             <div class="withdraw-balance-area">
                 <div class="container">
                     
                        <div class="row">
                            <div class="col-md-3">
                                <div class="createAgig-left-menu">
                                    	@include('common.side-menu')
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="withdraw-right-main">
                                    <div class="dashboard-bal">
                                        <div class="check-head-div"><h2>Balance</h2></div>
                                        <div class="dashboard-box-div">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="dashboard-div-item">
                                                        <h4>WORKING</h4>
                                                        <h2>$ 0.00</h2>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="dashboard-div-item">
                                                        <h4>AVAILABLE</h4>
                                                        <h2>$ {{getAvailableAmount(Auth::user()->id)}}</h2>
                                                        <!-- <p>Earning + Top-Up</p> -->
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="dashboard-div-item">
                                                        <h4>PENDING</h4>
                                                        <h2>$ {{getPendingAmount(Auth::user()->id)}}</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dashboard-box-bottom">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="dashb-btm-txt text-left">Total Balance : <span>${{getExistingBlanace(Auth::user()->id)}}</span></div>
                                                </div>
                                                <div class="col-md-6">
                                                     
                                                </div>
                                            </div>
                                        </div>
                                    </div>
									
									@if(empty($paymentmethod))
                                    <div class="dashboard-bal">
                                        <div class="check-head-div"><h2>Balance</h2></div>
                                        <div class="dashboard-box-div">
                                             <div class="dashboard-withdraw-txt">
                                                 <p>Withdrawal request requires at least one withdrawal account so that you can withdraw from your available balance.</p>
                                                 <div class="dashboard-withdraw-btn post-aJob">
                                                     <a href="{{url('payment-method')}}" class="btn">Create the withdrawal account</a>
                                                 </div>
                                             </div>
                                        </div>
                                    </div>
                                    @else
								 <div class="withdraw-right-in wht-bcack">
                                    <div class="withdraw-top-btns">
                                        <ul>
                                            <h1>Withdrawal Request</h1>
                                        </ul>
                                    </div>
                                    <div class="recruit-now-form">
                                        <form id="accoun_form" method="post" action="{{url('account-type')}}">
										@csrf
                                          <div class="form-group">
                                              <div class="row">
                                                  <div class="col-md-12">
                                                      <div class="form-input">
                                                          <label for="">Withdrawal Request</label>
                                                          <select id="account_type" name="account_type" class="form-control" onchange="getWithdrawalAccount()">
														    <option value="">Select Account</option>
														  <option value="bank">Bank</option>
														  <option value="paypal">Paypal</option>
														  </select>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
										  <div id ="bank_info" style="display:none">
											<div id="account_info" style="dispaly:none">
											</div>
											<div id ="bank_add_update_link">
											
											</div>
										  </br>
										  </div>
                                          <div id ="paypal_info" style="display:none">
											<div id="paypal_email_info" style="dispaly:none">
											</div>
											<div id ="paypal_add_update_link">
											
											</div>
											</br>
										  </div>
										  <p><strong>Minimum withdraw : ${{getSettings('minimum_withdrawal_amount')}}</strong></p>
										  <p><strong>Available Amount : ${{getExistingBlanace(Auth::user()->id)- getPendingAmount(Auth::user()->id)}}</strong></p>
										  <input type="hidden" name="method_id" id="method_id" value=""> 
                                          <div class="form-group">
										      <div class="row">
                                                  <div class="col-md-12">
                                                      <div class="form-input">
                                                          <label for="">Amount</label>
                                                          <input type="text" name="amount" class="form-control" />
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
										  <div class="form-group">
                                              <div class="row">
                                                  <div class="col-md-12">
                                                      <div class="form-input">
                                                          <input type="button" onclick="sendRequestPreviw()" value="Send" class="btn save-btn" />
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </form>
									  {!! JsValidator::formRequest('App\Http\Requests\FrontUser\AddPreviewRequest','#accoun_form') !!}
                                     </div>
                                     </div>
									@endif
                                </div>
                            </div>
                        </div>
                        
                 </div>
             </div>
         </div>
    </section> 
	
<div class="modal fade" id="withdra_confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Withdrawal Request Confirmation</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  <div id="content"></div>
	  <form id="confirm_form" >
	  @csrf
	    <div id="form_content"></div>
		<div class="modal-footer d-flex justify-content-center">
			<button type="button" onclick="sendRequest()" class="btn save-btn">Send
			<i id="confrom_form_loader" style="display:none" class="fas fa-spinner fa-spin"></i>
			</button>
		</div>
	  </form>
	 
    </div>
  </div>
</div>

<script>

function getWithdrawalAccount(){
	   $.ajax({
            url:"{{url('get-withdrawal-account')}}" ,
	        data:{account_type:$('#account_type').val()},
            method: 'get',
            dataType: 'JSON',
            success:function(result)
            {
				$('#account_info').html('');
				$('#bank_add_update_link').html('');
				$('#paypal_email_info').html('');
				$('#paypal_add_update_link').html('');
				
				if(result.data.length!=0 && result.data.payment_type == 1){
					bankdata = JSON.parse(result.data.account_info);
				}
				if(result.data.length != 0 && result.data.payment_type == 1 && result.account_type == 'bank'){
		
					var str = "<div class='row'><div class='col-lg-4'><div class='account_info_div'><label><strong>First Name</strong></label><p>"+bankdata.first_name+"</p></div></div><div class='col-lg-4'><div class='account_info_div'><label><strong>Middle Name</strong></label><p>"+bankdata.middle_name+"</p> </div></div>"+
                    "<div class='col-lg-4'><div class='account_info_div'><label><strong>Last Name</strong></label><p>"+bankdata.last_name+"</p></div></div><div class='col-lg-4'><div class='account_info_div'><label><strong>Bank Name</strong></label><p>"+bankdata.bank_name+"</p></div></div>"+
							"<div class='col-lg-4'><div class='account_info_div'><label><strong>Swift Code</strong></label><p>"+bankdata.swift_code+"</p></div></div><div class='col-lg-4'><div class='account_info_div'><label><strong>Account Number</strong></label><p>"+bankdata.account_number+"</p></div></div></div>";
					$('#account_info').html(str);
					var url = "{{url('payment-method?type=bank_account')}}";
					var str = '<a  href="'+url+'"><span>Update Account</span></i></a>';
                    $('#bank_add_update_link').html(str);					
					$('#bank_info').show();
					$('#paypal_info').hide();
					$('#method_id').val(result.data.id);
				}else if(result.account_type == 'bank'){
					var url = "{{url('payment-method?type=bank_account')}}";
					var str = '<a  href="'+url+'"><span>Add Account</span></i></a>';
                    $('#bank_add_update_link').html(str);
					$('#bank_info').show();
					$('#paypal_info').hide();
				}
				if(result.data.length != 0 && result.data.payment_type == 2 && result.account_type == 'paypal'){
					$('#paypal_email_info').html('');
					var str = "<div class='row'><div class='col-lg-12'><div class='account_info_div'><label><strong>Email</strong></label><p>"+result.data.account_info+"</p></div></div></div>";
					$('#paypal_email_info').html(str);
					var url = "{{url('payment-method')}}";
					 $('#paypal_add_update_link').html('');
					var str = '<a  href="'+url+'"><span>Update Account</span></i></a>';
                    $('#paypal_add_update_link').html(str);					
					$('#bank_info').hide();
					$('#paypal_info').show();
					$('#method_id').val(result.data.id);
				}else if(result.account_type == 'paypal'){
					var url = "{{url('payment-method')}}";
					var str = '<a  href="'+url+'"><span>Add Account</span></i></a>';
					$('#paypal_add_update_link').html('');
                    $('#paypal_add_update_link').html(str);					
					$('#bank_info').hide();
					$('#paypal_info').show();
				}
               
            }
            
        });
}

function sendRequestPreviw(){
	if($('#accoun_form').valid()){
	  $.ajax({
            url:"{{url('account-type')}}" ,
	        data:$('#accoun_form').serialize(),
            method: 'post',
            dataType: 'JSON',
            success:function(result)
            {
				if(result.success){
					var form_content = "<input type='hidden' name='account' value='"+result.account_type+"'><input type='hidden' name='amount' value='"+result.amount+"'>"+
						"<input type='hidden' name='payment_method_id' value='"+result.payment_method_id+"'><input type='hidden' name='selected_method' value='"+result.account_type+"'>";
					if(result.account_type == 'bank'){
						var bankdata = JSON.parse(result.data.account_info);
					var str = "<div class='row'><div class='col-lg-12'><div class='account_info_div'><label><strong>First Name</strong></label><p>"+bankdata.first_name+"</p></div></div><div class='col-lg-12'><div class='account_info_div'><label><strong>Middle Name</strong></label><p>"+bankdata.middle_name+"</p></div></div>"+
								"<div class='col-lg-12'><div class='account_info_div'><label><strong>Last Name</strong></label> <p>"+bankdata.last_name+"</p></div></div><div class='col-lg-12'><div class='account_info_div'><label><strong>Bank Name</strong></label><p>"+bankdata.bank_name+"</p></div></div>"+
								"<div class='col-lg-12'><div class='account_info_div'><label><strong>Swift Code</strong></label><p>"+bankdata.swift_code+"</p></div></div><div class='col-lg-12'><div class='account_info_div'><label><strong>Account Number</strong></label><p>"+bankdata.account_number+"</p></div></div>"+
								"<div class='col-lg-12'><div class='account_info_div'><label><strong>Account Type</strong></label> <p>"+result.account_type+"</p></div></div><div class='col-lg-12'><div class='account_info_div'><label><strong>Amount</strong></label><p>$"+result.amount+"</p></div></div></div>";
							 $('#content').html(str);
					
					}else{
							var str = "<div class='row'><div class='col-lg-12'><div class='account_info_div'><label><strong>Email</strong></label><p>"+result.data.account_info+"</p></div></div>"+
									  "<div class='col-lg-12'><div class='account_info_div'><label><strong>Account Type</strong></label> <p>"+result.account_type+"</p></div></div><div class='col-lg-12'><div class='account_info_div'><label><strong>Amount</strong></label><p>$"+result.amount+"</p></div></div></div>";
							$('#content').html(str);
					}
					$('#form_content').html(form_content);
					$('#withdra_confirm').modal('show');
				}else{
					toastr.error(result.message);
				}
			}
            
          });
	}
}

function sendRequest(){
	$('#confrom_form_loader').show();
	$.ajax({
            url:"{{url('save-withdrawal-request')}}" ,
	        data:$('#confirm_form').serialize(),
            method: 'post',
            dataType: 'JSON',
            success:function(result)
            {
				if (result.success) {
                    toastr.success(result.message);
					$('#confrom_form_loader').hide();
					$('#withdra_confirm').modal('hide');
					setTimeout(function(){
					window.location.href="{{url('my-withdrawal')}}";	
					},1000);
				} else {
                    toastr.error(result.message);
                }
			}
            
          });
}

</script>
<!-- middle-section -->
@endsection