<!DOCTYPE html>
<html lang="en">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {!! MetaTag::setDefault(['title' => 'Gigstoc','description'=>'GradStock description','keywords'=>'GradStock keywords'])->render() !!}
        <title>{{ env('APP_NAME') }}</title>
        <link rel="shortcut icon" href="{{ asset('public/images/favicon.png') }}" />
        <!---------Css------------>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;700;800;900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('public/css/bootstrap-select.min.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('public/fontawesome-free/css/all.min.css') }}" type="text/css" />
        <!---------Slider Css------------>
        <link rel="stylesheet" href="{{ asset('public/css/owl.carousel.min.css') }}" type="text/css" />
        <!---------Animate Css------------>
        <link rel="stylesheet" href="{{ asset('public/css/aos.css') }}" />
        <!---------Css------------>
        <link rel="stylesheet" href="{{ asset('public/css/menu.css') }}" />
        <link rel="stylesheet" href="{{ asset('public/css/style.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('public/css/custom-style.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('public/css/responsive.css') }}" type="text/css" />
        

        <!---------Js------------>
        <script src="{{ asset('public/js/jquery.min.js') }}"></script>
        <script src="{{ asset('public/js/popper.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/vendor/jsvalidation/js/jsvalidation.js')}}"></script>
        <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/js/jstz.min.js') }}"></script>
		<script type="text/javascript" src="{{asset('public/js/bootbox.min.js')}}"></script>
		
       
        
        @if(!empty(Auth::user()))
        <script type="text/javascript">
            $(document).ready(function(){
                var tz = jstz.determine(); 
                var timezone = tz.name(); 
                $.get("{{ url('set-user-timezone') }}", {timezone: timezone},function(response){
                    if(response.success){
                    }
                });

    
                //Notification Script Section.
                setInterval(function(){
                    //getUnreadCount();
                }, 5000);
            });
        </script>
        @endif
        
        <script type="text/javascript">
            var APP_URL = "{{ url('/') }}";
            var APP_TOKEN = "{{ csrf_token() }}";
            var STRIPE_KEY = "{{ env('STRIPE_KEY') }}";
        </script>
  </head>

  <body class="{{ request()->is('/') ? 'home' : '' }}">
    <div class="main-wrapper">
        <header class="header">
            <div class="top-header">
                <div class="container">
                    <div class="row">
                        <div class="col-4">
                            <button class="nav-toggle">
                                <div class="icon-menu">
                                    <span class="line line-1"></span>
                                    <span class="line line-2"></span>
                                    <span class="line line-3"></span>
                                </div>
                            </button>
                        </div>
                        <div class="col-8">
                            <div class="top-header-icn navbar justify-content-end">
                                <ul class="nav">
                                    <li><a href="{{getSettings('facebook_url')}}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="{{getSettings('twitter_url')}}" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="{{getSettings('linkedin_url')}}" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                                    <li><a href="{{getSettings('instagram_url')}}" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="main-header">
                <div class="container">
                    <div class="main-head">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="main-logo">
                                    <a href="{{url('/')}}" class="navbar-brand"><img src="{{ asset('public/images/logo.png') }}" alt=""></a>
                                </div>
                            </div>
                            @if(Auth::check())
                            <div class="col-md-8">
                                <div class="header-main-right d-flex justify-content-end">
                                    <div class="header-main-af">
                                        <div class="postAjob"><a href="{{url('post-service')}}">Post a Job <i class="fas fa-plus"></i></a></div>
                                        <div class="top-notification">
                                        <ul>
                                            <li>
                                            <div class="dropdown">
                                                <?php $get_unread_chat = getUnreadChat(Auth::user()->id); ?>
                                                <button class="dropdown-toggle" type="button" data-toggle="dropdown">
                                                <i class="fas fa-comment"></i> 
                                                
                                                <span style="{{ ($get_unread_chat['unread_count'] <= 0) ? 'display:none' : '' }}" class="unread_thread_count chat_notify_count">{{ $get_unread_chat['unread_count'] }}</span>
                                                 
                                                </button>
                                                    <div class="dropdown-menu">
                                                        <div class="dropdown-inner">
                                                            <div class="dropdown-header-bar">
                                                            <div class="row">
                                                                <div class="col-6">
                                                                <div class="text-left"><span class="unread_thread_count">{{ $get_unread_chat['unread_count'] }}</span> New</div>
                                                                </div>
                                                                <div class="col-6">
                                                                    <div class="text-right">
                                                                        <a class="mark-as-read" href="javascript:void(0)">Mark all as read</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            </div>
                                                            <div class="dropdown-middle-bar">
                                                            <ul class="notification-list chat-list">
                                                            @if(!empty($get_unread_chat['messages']))
                                                                @foreach($get_unread_chat['messages'] as $key => $value)
                                                                <li class="chat_id {{ ( (($value->is_read  == 1) && ($value->to_id == Auth::user()->id)) || (($value->is_shown  == 1) && ($value->from_id == Auth::user()->id)) ) ? 'read-chat-notification' : '' }}" id="{{ $value->id }}">
                                                                    <a href="{{ url('chat/'.$value->chat_room_id) }}">
                                                                        <div class="not-list-img">
                                                                            <span>
                                                                                <img title="{{ $value->name }}" src="{{ $value->userimage }}" class="rounded-circle">
                                                                            </span>
                                                                        </div>
                                                                        <div class="not-list-cont">
                                                                            <h2>{{ $value->message }}</h2>
                                                                            <p>{{ $value->chat_time }}</p>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                @endforeach
                                                            @endif
                                                            </ul>
                                                            </div>
                                                            <div class="dropdown-footer-bar">
                                                            <div class="view-all-btn"><a href="{{ url('my-message-list') }}">View All</a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            </li>
                                            <li>
                                            <div class="dropdown">
                                                <button class="dropdown-toggle" type="button" data-toggle="dropdown">
                                                <i class="fas fa-bell"></i> <span style="display:none" id="notification_count"></span>
                                                </button>
                                                <div class="dropdown-menu">
                                                <div class="dropdown-inner">
                                                    <div class="dropdown-header-bar">
                                                    <div class="text-center not-head">Notifications</div>
                                                    </div>
                                                    <div class="dropdown-middle-bar">
                                                    <ul class="notification-list">
													<?php
													$userId = Auth::user()->id;
													$notifictions = App\Models\Notification::whereRaw("FIND_IN_SET($userId,to_id)");
													$notifictions = $notifictions->limit(5)->latest()->get();
													?>
													@if(count($notifictions)>0)
													@foreach($notifictions as $noti)
                                                        <li >
                                                        <a @if(checkNotificationRead(Auth::user()->id,$noti->id)) style="background-color:#dddddd" @endif href="{{url($noti->url)}}">
                                                        <div class="not-list-img">
                                                            <span><i class="fas fa-info-circle"></i></span>
                                                        </div>
                                                        <div class="not-list-cont">
                                                            <h2  >{!!$noti->title!!}</h2>
                                                            <p>{{converToTz($noti->created_at,Auth::user()->timezone)}}</p>
                                                        </div>
                                                        </a>
                                                        </li>
                                                     @endforeach
													 @endif
                                                    </ul>
                                                    </div>
                                                    <div class="dropdown-footer-bar">
                                                    <div class="view-all-btn"><a href="{{url('notifications')}}">View All</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            </li>
                                        </ul>
                                        </div>
                                        <div class="head-user-drop">
                                        <div class="dropdown">
                                            <button class="dropdown-toggle" type="button" data-toggle="dropdown">
                                            <span class="user-img">
                                                <img src="{{ Auth::user()->user_image }}" alt=""/>
                                            </span>
                                            <span class="user-name">{{ Auth::user()->name }}</span>
                                            <span class="caret"></span>
                                            </button>
                                            <div class="dropdown-menu">
                                            <ul class="dropdown-link">
												<?php 
												
													if(getUserRole(Auth::user()->id) == 'admin'){
														$dashaboardUrl = 'admin/dashboard';
														$logOuturl = 'admin/logout';
													}else{
														$dashaboardUrl = 'dashboard';
														$logOuturl = 'logout';
													}
												?>
												<li><a href="{{url($dashaboardUrl)}}">Dashboard</a></li>
												@if(getUserRole(Auth::user()->id) != 'admin')
												<li><a href="{{url('my-profile')}}">Profile</a></li>
												@endif
                                                <li><a href="{{ url($logOuturl) }}">Logout</a></li>
                                            </ul>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            @else
                            <div class="col-md-8">
                                <div class="header-main-right d-flex justify-content-end">
                                    <div class="hd-label"><a href="{{ url('signup') }}">Become A Seller</a></div>
                                    <div class="header-sign-btn">
                                        <ul class="nav">
                                            <li><a href="{{ url('login') }}" class="btn"><i class="far fa-user"></i> Login</a></li>
                                            <li><a href="{{ url('signup') }}" class="btn"><i class="fas fa-sign-out-alt"></i> Sign up</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    
                    @if (isset($category) && !empty($category))
                    <div class="header-main-menu">
                        <nav class="navbar nav-container navbar-expand justify-content-center">
                            <ul class="nav links navbar-nav">
                                @foreach ($category as $value)
                                @if ($value->visible_in_menu == 1)
                                <li class="nav-item {{ (request()->segments(0) == $value->slug) ? 'active' : '' }}">
                                    <a class="nav-link parent" href="{{ url('category/') }}/{{ $value->id }}">{{ ucwords($value->name) }}</a>
                                    @if(count($value->categories) > 0)
                                    <ul class="submenu">
                                        @foreach ($value->categories as $child)
                                        <li>
                                            <a class="nav-link" href="{{ url('category/') }}/{{ $child->id }}">{{ ucwords($child->name) }}</a>
                                        </li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </li>
                                @endif
                                @endforeach
                            </ul>
                        </nav>
                    </div>
                    @endif
                </div>
            </div>
        </header>
        <!-- header -->


  