<?php
use Illuminate\Support\Facades\Route;

$route = url()->current();
$arr = explode("/",$route);
$currentURl = $arr[count($arr)-1];

?>

<ul>
	<li @if($currentURl=='dashboard') class="active" @endif ><a href="{{url('dashboard')}}">Dashboard
	<li @if($currentURl=='notifications') class="active" @endif><a href="{{url('notifications')}}">Notification</a></li>
	<li @if($currentURl=='my-message-list') class="active" @endif><a href="{{url('my-message-list')}}">Chat</a></li>
	<li @if($currentURl=='my-gig') class="active" @endif><a href="{{url('my-gig')}}">My Gigs</a></li>
	<li @if($currentURl=='revenues') class="active" @endif><a href="{{url('revenues')}}">Revenues</a></li>
	<li id="order_map"><a  href="@if($currentURl!='dashboard'){{url('dashboard#order_statics')}} @else javascript:void(0) @endif"   >Order Statistics</a></li>
	<li @if($currentURl=='withdrawal-balance') class="active" @endif><a href="{{url('withdrawal-balance')}}">Withdraw Balance</a></li>
	<li @if($currentURl=='my-withdrawal') class="active" @endif><a href="{{url('my-withdrawal')}}">My Withdrawal</a></li>
	 <!--<li><a href="#">My Purchases & Tasks</a></li>-->
	<li @if($currentURl=='my-invoice') class="active" @endif><a href="{{url('my-invoice')}}">My Invoices</a></li>
	<li @if($currentURl=='my-job') class="active" @endif><a href="{{url('my-job')}}">My Job Requests</a></li>
	<li @if($currentURl=='bookmark') class="active" @endif><a href="{{url('bookmark')}}">My Bookmarks</a></li>
	<!--<li><a href="#">Job Verification</a></li>-->
	<li @if($currentURl=='subscription-setting') class="active" @endif><a href="{{url('subscription-setting')}}">Subscriber Settings</a></li>
	<li @if($currentURl=='payment-method') class="active" @endif><a href="{{url('payment-method')}}">Payment Method</a></li>
	<li ><a href="{{url('change-password')}}">Change Password</a></li>
	<li><a href="{{url('logout')}}">Logout</a></li>
</ul>