@if(!empty($all_messages))
    @foreach($all_messages as $key => $value)
    <li class="chat_id {{ ( (($value->is_read  == 1) && ($value->to_id == $user_id)) || (($value->is_shown  == 1) && ($value->from_id == $user_id)) ) ? 'read-chat-notification' : '' }}" id="{{ $value->id }}">
        <a href="{{ url('chat/'.$value->chat_room_id) }}">
            <div class="not-list-img">
                <span>
                    <img title="{{ $value->name }}" src="{{ $value->userimage }}" class="rounded-circle">
                </span>
            </div>
            <div class="not-list-cont">
                <h2>{{ $value->message }}</h2>
                <p>{{ $value->chat_time }}</p>
            </div>
        </a>
    </li>
    @endforeach
@endif