<footer class="footer">
    <div class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div class="footer-menu">
                                <p>COMPANY</p>
                                <ul>
                                    <li><a href="{{ url('/') }}">Home</a></li>
                                    <li><a href="{{ url('about-us') }}">About Us</a></li>
                                    <li><a href="{{url('terms-of-service')}}">Terms of Service</a></li>
                                    <li><a href="{{url('privacy-policy')}}">Privacy Policy</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="footer-menu">
                                <p>DISCOVER</p>
                                <ul>
								    <li><a href="{{url('site-map')}}">Sitemap</a></li>
                                    <li><a href="{{url('how-it-works')}}">How it works</a></li>
                                    <li><a href="{{url('trust-and-quality')}}">Trust and Quality</a></li>
                                    <li><a href="{{ url('blog') }}">Blog</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="footer-menu">
                                <p>HELP & SUPPORT</p>
                                <ul>
                                    <li><a href="{{url('faq')}}">FAQ</a></li>
                                    <li><a href="{{url('buying-on-gigstoc')}}">Buying on Gigstoc</a></li>
                                    <li><a href="{{url('selling-on-gigstoc')}}">Selling on Gigstoc</a></li>
                                    <li><a href="{{ url('contact-us') }}">Contact Us</a></li>
                                </ul>
                            </div>
                            <div class="footer-menu">
                                <p>CONTACT US</p>
                                <ul>
                                    <li><a href="#">support@gigstoc.com</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="footer-email-area">
                        <div class="envl-icon"><img src="{{ asset('public/images/envo-img.png') }}" alt=""></div>
                        <div class="footer-email-form">
                            <h2>Email updates</h2>
                            <p>Be the first to hear about our offers and announcements.</p>
                            <form action="{{ route('newsletter-store') }}" method="post"> 
                                {{ csrf_field() }}
                                <div class="input-group">
                                    <input class="form-control" type="email" placeholder="Enter your email address..." name="user_email" id="subscriber_email" />
                                    <div class="input-group-prepend">
                                        <span class="input-group-text pink" id="basic-text1">
                                            <input class="btn btn-primary" type="image" src="{{ asset('public/images/send-icon.png') }}" id="subscribe" value="Subscribe" />
                                        </span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                    <div class="newsletter-response-msg" style="display: none;">
                        
                                    </div>
                            </form>

                            {{-- <form action="{{ route('newsletter-store') }}" method="post">                
                                <div class="frm-int">
                                    <input type="email" placeholder="Email" required="required" name="user_email" id="subscriber_email" class="form-control">
                                </div>
                                {{ csrf_field() }}
                               <div class="frm-int-btn">
                                  <input class="btn btn-primary" type="button" id="subscribe" value="Subscribe" />
                              </div>
                              <div class="clearfix"></div>
                              <div class="response-msg" style="display: none;">
                
                                </div>
                            </form> --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copy-right">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="copy-text">
                    <?php
                        $copyright_text = App\Models\ConfigSetting::where(['meta_key' => 'copyright_text'])->first();
                            ?>
                        {{($copyright_text) ? $copyright_text->meta_value : '-'}}
                    </div>
                    
                </div>
                <div class="col-md-6">
                    <div class="footer-scl">
                        <ul class="nav justify-content-end">
                        <li><a href="{{getSettings('facebook_url')}}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="{{getSettings('twitter_url')}}" target="_blank"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="{{getSettings('linkedin_url')}}" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="{{getSettings('instagram_url')}}" target="_blank"><i class="fab fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!--login Modal -->

<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="sign-head">
				<h2>Login into account</h2>
				<p>Use your credentials to access your account</p>
			</div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="sign-form-area">
			<div class="sign-form">
				<form  autocomplete="off" action="{{url('login')}}" id="login-form" method="post">
				<?php
					if (old('email')!="") {
						$emaill = old('email');
					} else {
						if (isset($_COOKIE['email'])) {
							$emaill = $_COOKIE['email'];
						}
					}
				?>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div style="display:none" id="resend" class="sign-have-link">
									<label for="">
									If you didn't receive verification email click here <a id="resend_link"  class="" onclick="resendVerificationMail()" href="javascript:void(0)">Resend mail
									<i id="resend_loader" style="display:none" class="fas fa-spinner fa-spin"></i>
									</a>
									</label>
								</div>
								<label for="">Username or Email<span>*</span></label>
								<div class="input-group">
									<div class="input-group-append">
										<span class="input-group-text">
										<i class="far fa-envelope"></i>
										</span>
									</div>
									<input type="text" onkeyup="checkError()" name="email" class="form-control input_user" value="" placeholder="UserName or Email address*" value="<?php if(isset($_COOKIE["email"])) {  echo $_COOKIE["email"];} ?>" autocomplete="off">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<label for="">Password<span>*</span></label>
								<div class="input-group">
									<div class="input-group-append">
										<span class="input-group-text">
										<i class="fas fa-lock"></i>
										</span>
									</div>
									<input type="hidden" name="type" value="modal">
									<input type="password" name="password" class="form-control input_pass" value="<?php if(isset($_COOKIE["password"])) {  echo $_COOKIE["password"];} ?>" placeholder="Password*" autocomplete="off">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">
								<div class="custom-checkbox">
									<label class="form-check-label">
									<input type="checkbox" id="customControlInline" class="custom-control-input" name="remember" {{ old('remember') || isset($emaill)? 'checked' : '' }}>
									<span class="custom-control-label">Keep me logged in</span>
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<button type="button" name="button" onclick="userLogin()" class="btn login_btn">Login
								<i id="modal_form_loader" style="display:none" class="fas fa-spinner fa-spin"></i>
								</button>
							</div>
						</div>
					</div>
				</form>
				{!! JsValidator::formRequest('App\Http\Requests\FrontUser\LoginRequest','#login-form') !!}
			</div>
		   
			<div class="sign-in-or">
				<div class="sign-or">OR</div>
				<div class="sign-with">
					<p>Login With</p>
					<ul>
						<li><a href="{{url('login/facebook')}}"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="{{url('login/google')}}"><i class="fab fa-google"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="sign-have-link">
				<label for="">
				Don't have an account? <a href="{{url('signup')}}">Register here</a>
				</label>
			</div>
		</div>
      </div>
     
    </div>
  </div>
</div>
<!-- middle-section -->
</footer>
<!-- footer -->
</div>
<!---------Js------------>
<script src="{{ asset('public/js/menu.js') }}" type="text/jscript"></script>
<script src="{{ asset('public/js/script.js') }}" type="text/jscript"></script>
<!---------slider Js------------>
<script src="{{ asset('public/js/owl.carousel.js') }}" type="text/jscript"></script>
<script src="{{ asset('public/js/aos.js') }}"></script>
<script src="{{ asset('public/js/bootstrap-select.min.js') }}" type="text/jscript"></script>
<script>

   /** $(document).ready(function(){
    CKEDITOR.replace( 'editor1' );
    });
    AOS.init({
        easing: 'ease-in-out-sine'
    });
	*/
function loginNow(){
	$('#loginModal').modal('show');
}

function userLogin(){
	if($('#login-form').valid()){
	 $('.login_btn').prop('disabled',true);
	 $('#modal_form_loader').show();
	 $.ajax({
            url: "{{url('login')}}",
            data: $('#login-form').serialize(),
            method: 'post',
            dataType: 'JSON',
            success:function(result)
            {
				$('.login_btn').prop('disabled',false);
				$('#modal_form_loader').hide();
                if(result.success){
                    toastr.success(result.message);
					window.location.reload();
                }else{
                    toastr.error(result.message);
                }
            }
        });
	}
}
function checkError(){
    $.ajax({
            url: "{{url('check-verification')}}",
            data: {username:$('.input_user').val(),"_token": "{{ csrf_token() }}"},
            method: 'post',
            dataType: 'JSON',
            success:function(result)
            {
                if(result.success){
                    $('#resend').show();
                }else{
                    $('#resend').hide();
                }
            }
        });
    }

function resendVerificationMail(){
        $('#resend_loader').show();
        $('#resend_link').prop('disabled',true);
        $('#resend_link').addClass('disabled');
        $.ajax({
            url: "{{url('resend/verification-mail')}}",
            data: {username:$('.input_user').val(),"_token": "{{ csrf_token() }}"},
            method: 'post',
            dataType: 'JSON',
            success:function(result)
            {
                if(result.success){
                    toastr.success(result.message);
                }else{
                    toastr.error(result.message);
                }
                $('#resend_loader').hide();
                $('#resend_link').removeClass('disabled');
                $('#resend_link').prop('disabled',false);
            }
        });
 }
 
</script>

<script>
    var owl = jQuery('.banner-slider');
    owl.owlCarousel({
      margin: 0,
      autoplay:true,
      singleItem: true,
      autoplayTimeout: 5000,
      autoHeight: true,
      URLhashListener: true,
      startPosition: 'URLHash',
      //loop: true,
      lazyLoad:true,
      nav: true,
      video: true,
      animateIn: 'fadeIn',
      animateOut: 'fadeOut',
      // onTranslate: function() {
      //       $('.owl-item.active').find('video').each(function() {
      //           this.pause();
      //       });
      // },
           afterAction: function(current) {
            current.find('video').get(0).play();
        },
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 1
        },
        1000: {
          items: 1
        }
      }
    });
    
    var owl = jQuery('.sellers-slider');
    owl.owlCarousel({
        margin: 0,
        autoplay: false,
        autoplayTimeout: 5000,
        //loop: true,
        navigation: true,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        responsive: {
            0: {
                items: 1
            },
            375: {
                items: 2
            },
            600: {
                items: 3
            },
            992: {
                items: 4
            },
            1000: {
                items: 5
            }
        }
    })
    
    var owl = jQuery('.testimonials-slider');
    owl.owlCarousel({
        margin: 0,
        autoplay: true,
        autoplayTimeout: 5000,
        loop: true,
        navigation: true,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            992: {
                items: 2
            },
            1000: {
                items: 2
            }
        }
    })

    var owl = jQuery('.service-images-slider');
    owl.owlCarousel({
        margin: 0,
        autoplay: true,
        autoplayTimeout: 5000,
        loop: true,
        navigation: true,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            992: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })
	
function getSideLink(){
    var path = "{{url('common/side-link')}}";
	$('#side_loader').show(); 
        $.ajax({
            url:path ,
            method: 'get',
            dataType: 'JSON',
            success:function(result)
            {
				$('#side_loader').hide();
                $('#side_link').html(result.html);
            }
           });
    }
	
$(document).ready(function(){
	
    getNotificationUnreadCount();
});

function getNotificationUnreadCount(){
	$.ajax({
	url: "{{url('get-uneadNotification')}}",
	data: {},
    type: 'get',
	dataType: 'JSON',
	success: function (response) {
		if(response.count>0){
		 $('#notification_count').show();
		 $('#notification_count').text(response.count);
		}
	},
	error:function(err){
	  
	}
	});
}
// subscribe newsletter
$('#subscribe').click(function(e){
    e.preventDefault();
    var subscriber_email = document.getElementById('subscriber_email').value;
    //$('#subscriber_email').val('');
    //alert(subscriber_email);
    var error = 0;
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(subscriber_email == ''){
        //$('.newsletter-response-msg').html('<p class="newsletter-danger">Please enter email.</p>').show();
        toastr.error('Please enter email');
        error++;
    }else if(!regex.test(subscriber_email)) {
        //$('.newsletter-response-msg').html('<p class="newsletter-danger">Please enter valid email.</p>').show();
        toastr.error('Please enter valid email');
        error++;
    }
    
    if (error > 0) {
        return false;
    } else{
        $('#loading').show();
        $.ajax({
            type: "POST",
            url: "{{url('newsletter-store')}}",
            data: 'user_email='+ subscriber_email,
            dataType: 'html',
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function(response)
            {
                $('#loading').hide();
                var jsonData = JSON.parse(response);

                if (jsonData.success == true)
                {
                    //$('.newsletter-response-msg').html(jsonData.msg).show();
                    toastr.success(jsonData.msg);
                    $('#subscriber_email').val('');
                }
                else
                {
                    //$('.newsletter-response-msg').html(jsonData.msg).show();
                    toastr.error(jsonData.msg);
                }
            }
        });
    }

});

$(document).ready(function(){
    if ('{{ Auth::check() }}' == true) {
        var user_id = '{{ Auth::check() ? Auth::user()->id : '' }}';
        setInterval(function() {
            chatNotification(user_id);
        }, 10000);
    }

    // mark all as read chat
    $(document).on('click', '.mark-as-read', function(){
        $.ajax({
        url: "{{ url('mark-as-read') }}",
        type: 'get',
        dataType: 'JSON',
        success: function (res) {
            if(res.success) {
                var user_id = '{{ Auth::check() ? Auth::user()->id : '' }}';
                chatNotification(user_id);
                toastr.success(res.message);
            } else {
                toastr.error(res.message);
            }
        },
        error:function(err){
            toastr.error('Sorry, there was a problem!');
        }
        });
    });
});

function chatNotification(user_id)
{
    // check unread message
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        url: '{{ url("chat/chat-notification") }}', 
        type: 'post',
        data: 'user_id=' + user_id,
        dataType: 'json',
        success: function (res) {
            //console.log(res);
            if(res.success) {
                $('.chat_notify_count').show();
                $('.chat-list').html(res.html);
                $('.unread_thread_count').text(res.unread_count);
                if (res.unread_count == 0) {
                    $('.chat_notify_count').hide();
                }
            } else {
                //toastr.error(data.message);
            }
            
        },
        error: function(xhr, status, errorThrown) {
            //toastr.error("Sorry, there was a problem!");
        }
    });  
}
</script>
<!---------Js------------>
</body>
</html>