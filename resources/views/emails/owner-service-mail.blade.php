<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gigstoc Email</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
<style>
body {
    margin: 0;
    font-family: 'Montserrat', sans-serif;
}
.main-page-email {
    max-width: 750px;
    margin: 0 auto;
    border:1px solid #ccc;
}
.header {
    padding: 35px;
    text-align: center;
    border-bottom: 2px solid #0076b8;
}
.middle-section {
    padding: 55px 45px;
}
.tam-name {
    color: #1274c6;
    font-size: 20px;
    font-weight: 700;
    margin: 0 0 50px;
}
.tam-cont-main {
    color: #000000;
    font-size: 14px;
    font-weight: 500;
    line-height: 25px;
    text-align: justify;
}
.tam-cont-main p {
    margin: 0;
}
.tam-cont-main .tam-cont-head {
    margin: 0 0 25px;
}
.tam-cont-main .tam-cont-head h2 {
    font-size: 25px;
    color: #0d8f25;
    margin: 0 0 10px;
}
.tam-cont-main .tam-cont-head p {
    font-size: 16px;
}
.tam-cont-btn {
    margin: 25px 0 0;
}
.tam-cont-btn a {
    background: #1381c8;
    color: #fff;
    font-size: 12px;
    box-shadow: 0 0 18px rgba(14,121,190,0.25);
    text-transform: uppercase;
    font-weight: 800;
    padding: 8px 20px;
    text-decoration: none;
    border: none;
    transition: all 0.3s ease-in;
    display: inline-block;
    border-radius: 5px;
    background-image: linear-gradient(90deg, #0e79be , #30aeff);
}
.footer {
    background: url(em-back2.png) no-repeat bottom left, url(gigstoc-ftr.png) no-repeat center center / cover;
}
.footer-copy {
    background: #2c2c2c;
    padding: 15px;
    text-align: center;
    color: #fff;
    text-transform: uppercase;
    font-weight: 700;
    font-size: 14px;
}
.main-footer {
    padding: 10px 45px 55px;
    text-align: center;
}
.footer-logo span {
    background: #fff;
    border-radius: 10px;
    display: inline-block;
    min-width: 200px;
    padding: 15px 10px;
}
.footer-logo {
    margin: 0 0 40px;
}
.footer-menu {
    margin: 0 0 25px;
}
.footer-menu ul, .footer-icns ul {
    margin: 0;
    padding: 0;
}
.footer-menu ul li, .footer-icns ul li {
    display: inline-block;
    position: relative;
    padding: 0 5px;
}
.footer-menu ul {
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    justify-content: center;
}
.footer-menu ul li {
    padding: 0 8px;
}
.footer-menu ul li a {
    font-size: 12px;
    text-transform: uppercase;
    font-weight: 600;
    color: #fff;
    text-decoration: none;
}
.footer-menu ul li:not(:last-child)::after {
    content: "/";
    font-size: 12px;
    color: #fff;
    font-weight: 600;
    position: absolute;
    right: -2px;
    top: 3px;
}
.footer-icns ul li a {
    background: #fff;
    color: #087ccf;
    display: inline-block;
    text-decoration: none;
    width: 28px;
    height: 28px;
    line-height:25px;
    border-radius: 50%;
    font-size: 14px;
    text-align: center;
    padding: 2px;
    box-sizing: border-box;
}
.footer-icns ul li a img {
    max-width: 100%;
    max-height: 55%;
}
</style>
</head>
<body>


    <div class="main-page-email" style="background:url(<?php echo url('/public/images/em-back1.png'); ?>) no-repeat top right; box-shadow: 0 0 13px #ccc;">
        <div class="header">
            <div class="main-logo"><a href="{{ url('/') }}"><img style="display:inline-block" src="<?php echo url('/public/images/setting/').'/'.getSettings('site_logo'); ?>" alt=""></a></div>
        </div>
        <div class="middle-section">
            <div class="tam-name">Hi, {{$data['username']}}</div>
            <div class="tam-cont-main">
                <div class="tam-cont-head">
                    <h2>Service order</h2>
                </div>
                <div class="tam-cont">
                    <p>Your Job <strong>{{$data['service']}}</strong> has been ordered.</p>
					</br>
					<p>You can view the order details <a href="{{url($data['order_link'])}}">here</a></p></br>
					<p>Sincerely,</p></br>
					<p>Gigstoc Customer Support</p>
                </div>
            </div>
        </div>
        <div class="footer" style="background:url(<?php echo url('/public/images/gigstoc-ftr.png'); ?>) no-repeat center center / cover">
            <div class="main-footer" style="background:url(<?php echo url('/public/images/em-back2.png'); ?>) no-repeat bottom left">
                <div class="footer-logo"><span><a href="{{ url('/') }}"><img style="display:inline-block" src="<?php echo url('/public/images/setting/').'/'.getSettings('site_logo'); ?>" alt=""></a></span></div>
                <div class="footer-icns">
                    <ul>
                         <li><a href="{{getSettings('facebook_url')}}" target="_blank"><img style="display:inline-block; align-items: center; justi" src="<?php echo url('/public/images/setting/facebook1.png'); ?>"></a></li>
                         <li><a href="{{getSettings('twitter_url')}}" target="_blank"><img style="display:inline-block; align-items: center; justi" src="<?php echo url('/public/images/setting/twitter1.png'); ?>"></a></li>
                         <li><a href="{{getSettings('linkedin_url')}}" target="_blank"><img style="display:inline-block; align-items: center; justi" src="<?php echo url('/public/images/setting/linkedin1.png'); ?>"></a></li>
                         <li><a href="{{getSettings('instagram_url')}}" target="_blank"><img style="display:inline-block; align-items: center; justi" src="<?php echo url('/public/images/setting/insta1.png'); ?>"></a></li>
                    </ul>
                </div>
            </div>
            <div class="footer-copy">
            {{ getSettings('copyright_text') }}
			</div>
        </div>
    </div>
</body>
</html>