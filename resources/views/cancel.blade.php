@extends('layouts.default')
@section('content')
<section class="section-middle">
    <div class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="page-title-head">Cancel</div>
                </div>
                <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Cancel</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-page"><div class="container">
        <div class="container">
            <div class="row">
                <div class="col-md-12 order-cancel">
                    <div class="order-failed">
                        <h2>Your PayPal Transaction has been Cancelled</h2>
                    </div>
                </div>
                <div class="continue-shop">
                    <a href="{{ url('category') }}">Back To Services</a>
                </div>
                <!--continue-shop-->
            </div>
        </div><!--container-->
    </div><!--container-->
</section><!--section-middle-->
@endsection