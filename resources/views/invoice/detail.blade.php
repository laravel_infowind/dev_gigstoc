@extends('layouts.default')
@section('content')
<section class="middle-section">
    <div class="main-page">
        <div class="alljob-requests-area">
            <div class="container">
            <div class="recently-add-inn wht-bcack">
                <div class="recently-left-txt">
                    <div class="invoice-detail-items">
                        <div class="invoice-detail-item">
                            @if($order->detail->service->is_custom == 0)
                                <h1>Order for {{ $order->detail->service->title }}</h1>
                            @else
                                <h1>Custom Order for {{ $order->detail->service->title }}</h1>
                            @endif
                            <p>Modyfied Date: <span>{{ changeDateformat($order->detail->service->updated_at) }}</span></p>
                        </div>
                        <div class="invoice-detail-item">
                            <h5><small>Service:</small></h5>
                            @if($order->detail->service->is_custom == 0)
                                <p><a href="{{url('service/'.$order->detail->service->id)}}">{!! $order->detail->service->title !!}</a></p>
                            @else
                                <p><a href="{{url('service/'.$order->detail->service->parent_service_id)}}">{!! $order->detail->service->title !!}</a></p>
                            @endif
                        </div>
                        <div class="invoice-detail-item">
                            <h5><small>Description:</small></h5>
                            <p>{!! $order->detail->service->description !!}</p>
                        </div>
                    </div>
                </div>
            </div>
                <div class="alljob-requests-inn">
                    <?php 
                        //echo "<pre>";
                        //print_r($order);die;
                        ?>
                    <div class="category-list-page">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="invoice-add-left">
                                    <div class="recently-add-inn wht-bcack">
                                        <div class="recently-left-txt">
                                            <div class="invoice-detail-items">
                                                <div class="invoice-detail-item">
                                                    <div class="recently-right-txt">
                                                    @if($order->status==1)
                                                    @if($order->delivered == 1)
                                                        <button order-id="{{ $order->id }}" class="btn btn-success finished">Finished</button>
                                                    @else
                                                        @if($order->detail->service->created_by != Auth::user()->id)
                                                        <button order-id="{{ $order->id }}" class="btn btn-primary mark-as-delivered">Mark As Delivered <i id="formLoader" style="display:none" class="fas fa-spinner fa-spin"></i></button>
                                                        @endif
                                                    @endif
                                                    @endif
                                                    </div>
                                                    <h5><small>Invoice Id:</small></h5>
                                                    <h1>#{{$order->order_id}}</h1>
                                                    <p>Date: <span>{{changeDateformat($order->created_at)}}</span></p>
                                                </div>
                                                <div class="invoice-detail-item">
                                                    <h5><small>Transaction_id:</small></h5>
                                                    <h3>{{$order->transaction_id}}</h3>
                                                </div>
                                                <div class="invoice-detail-item">
                                                    <h5><small>Payment Method:</small></h5>
                                                    <h3>{{ ucwords($order->payment_type) }}</h3>
                                                </div>
                                                <div class="invoice-detail-item">
                                                    <h5><small>Status:</small></h5>
                                                    <h3>
                                                        @if($order->status==2)
                                                        <span class="btn btn-danger">Cancelled</span>
                                                        @endif
                                                        @if($order->status==1)
                                                        <span class="btn btn-success">Success</span>
                                                        @endif
                                                        @if($order->status==0)
                                                        <span class="btn btn-warning">Pending</span>
                                                        @endif
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="my-invoice">
                                    @if($order->delivered == 1)
                                        @if(Auth::user() && Auth::user()->id != $order->detail->service->user->id)
                                        <div class="rec-left-btn">
                                            <div class="rec-btn">
                                            @if($order->detail->service->is_custom==1)
                                                @if(!checkServiceRating($order->detail->service->parent_service_id, true))
                                                    
                                                    <button id="service_rating_{{ $order->detail->service->parent_service_id }}" data-id="{{ $order->detail->service->parent_service_id }}" onclick="showRatingModel('{{ $order->detail->service->parent_service_id }}', 1)" class="btn"> <span  id="btn_service_rating">Give Service Rating<span></span></span></button>
                                                @endif
                                            @else
                                                @if(!checkServiceRating($order->detail->service->id))
                                                    <button id="service_rating_{{ $order->detail->service->id }}" data-id="{{ $order->detail->service->id }}" onclick="showRatingModel('{{ $order->detail->service->id }}', 0)" class="btn"> <span  id="btn_service_rating">Give Service Rating<span></span></span></button>
                                                @endif
                                            @endif
                                                
                                            @if($order->detail->service->is_custom == 1)
                                                @if(!checkuserrating($order->detail->service->user->id,$order->detail->service->parent_service_id, true))
                                                <button id="user_rating_{{ $order->detail->service->parent_service_id }}"  data-id="{{ $order->detail->service->parent_service_id }}" onclick="showUserRatingModel('{{ $order->detail->service->parent_service_id }}','{{$order->detail->service->user->id}}', 1)" class="btn"> <span  id="btn_user_rating">Give User Rating<span></span></span></button>
                                                @endif
                                            @else
                                                @if(!checkuserrating($order->detail->service->user->id,$order->detail->service->id, false))
                                                <button id="user_rating_{{ $order->detail->service->id }}"  data-id="{{ $order->detail->service->id }}" onclick="showUserRatingModel('{{ $order->detail->service->id }}','{{$order->detail->service->user->id}}', 0)" class="btn"> <span  id="btn_user_rating">Give User Rating<span></span></span></button>
                                                @endif
                                            @endif
                                            </div>
                                        </div>
                                        @endif
                                    @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="invoice-detail-right wht-bcack">
                                    <div class="invoice-detail-table">
                                        <table class="table">
                                            <tr>
                                                <th>Total:</th>
                                                <td>${{number_format($order->subtotal,2)}}</td>
                                            </tr>
                                            <tr>
                                                <th>Fee:</th>
                                                <td>${{number_format($order->tax,2)}}</td>
                                            </tr>
                                            <tr>
                                                <th>Discount:</th>
                                                <td>${{number_format($order->discount,2)}}</td>
                                            </tr>
                                            <tr>
                                                <th>Paid Amount:</th>
                                                <td>${{number_format($order->total,2)}}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                                <div class="category-side-bar recently-add-right">
                                    <div class="user-profile">
                                        <div class="user-img">
                                            <span><img src="{{ $order->detail->service->user->user_image }}" alt=""></span>
                                        </div>
                                        <div class="user-name">
                                            <h3>{{ $order->detail->service->user->name }}</h3>
                                            <p>
                                                <?php $user = App\User::with('rating')->find($order->detail->service->created_by);
                                                $rating = $user->rating()->avg('rating');
                                                $rating = round($rating);
                                                displayRating($rating);
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="user-dtl-div">
                                        <div class="user-dtl">
                                            <h2><i class="fas fa-map-marker-alt"></i> <span>From</span></h2>
                                            <p>{{ ($user->country) ? $user->country->name : '' }}</p>
                                        </div>
                                        <div class="user-dtl">
                                            <h2><i class="fas fa-globe-africa"></i> <span>Languages</span></h2>
                                            @if(count($user->languages)>0)
                                            @foreach($user->languages as $val)
                                            <p>{{$val->language->name}}</p>
                                            @endforeach
                                            @endif
                                        </div>
                                        <div class="user-dtl">
                                            <h2><i class="fas fa-info-circle"></i> <span>Bio</span></h2>
                                            <p>{!! $user->bio !!}</p>
                                        </div>
                                        <div class="user-cont-btn text-center">
                                            <ul>
                                                <li>
                                                    @if(Auth::check())
                                                    <?php
                                                    $logged_in_id = Auth::user()->id;
                                                    $chat_room = checkChatRoom($logged_in_id, $order->detail->service->created_by); ?>
                                                    <?php
                                                    if ($logged_in_id != $order->detail->service->created_by) {
                                                    if($chat_room) { ?>
                                                        <a class="btn" href="{{ url('chat/'.$chat_room) }}">
                                                        <span>Contact me</span> <i class="far fa-paper-plane"></i>
                                                        </a>
                                                    <?php } else { ?>
                                                        <a class="btn btn-contact-me" href="javascript:void(0)">
                                                        <span>Contact me</span> <i class="far fa-paper-plane"></i>
                                                        </a>
                                                    <?php } } ?>
                                                    @else
                                                    <a onclick="loginNow()" class="btn" href="javascript:void(0)">
                                                    <span>Contact me</span> <i class="far fa-paper-plane"></i>
                                                    </a>
                                                    @endif
                                                </li>
                                                <li>
                                                    <a class="btn" href="{{url('view-profile').'/'.base64_encode($order->detail->service->created_by)}}">
                                                    <span>View my profile</span> <i class="fas fa-eye"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
</section>
<!-- middle-section -->
<!-- Modal -->
<div class="modal fade" id="serviceRatingModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Service Rating</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
       <div class="modal-body">
        <form id="service_rating_form" method="post" action="{{url('save-service-rating')}}">
		@csrf
          <div class="form-group">

            <div class="stars">
			<label for="message-text" class="col-form-label">Rating:</label>
			</br>
			 	<input class="star star-5" id="star-5" type="radio" name="star"/>
				<label class="star star-5" onclick = "addStartValue(5)" for="star-5"></label>
				<input class="star star-4" id="star-4" type="radio" name="star"/>
				<label class="star star-4" onclick = "addStartValue(4)" for="star-4"></label>
				<input class="star star-3" id="star-3" type="radio" name="star"/>
				<label class="star star-3" onclick = "addStartValue(3)" for="star-3"></label>
				<input class="star star-2" id="star-2" type="radio" name="star"/>
				<label class="star star-2" onclick = "addStartValue(2)" for="star-2"></label>
				<input class="star star-1" id="star-1" type="radio" name="star"/>
				<label class="star star-1" onclick = "addStartValue(1)" for="star-1"></label>
			</div>
          </div>
		  <input type="hidden" name="rating" value="" id="rating_count">
		  <input type="hidden" name="service_id" id="service_id" value="">
		  <input type="hidden" name="custom_offer" id="custom_offer" value="">
          <div class="form-group">
            <label for="message-text" class="col-form-label">Review:</label>
            <textarea class="form-control" name="review" id="message-text"></textarea>
          </div>
        </form>
		{!! JsValidator::formRequest('App\Http\Requests\FrontUser\SaveRatingRequest','#service_rating_form') !!}
      </div>
      <div class="modal-footer">
	    <button type="button" id="sav-btn" onclick="saveServiceRating()" class="btn btn-primary">
		<i id="form_loader" style="display:none" class="fas fa-spinner fa-spin"></i>
		Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- middle-section -->



<!-- Modal -->
<div class="modal fade" id="ratingModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">User Rating</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
       <div class="modal-body">
        <form id="user_rating_form" method="post" action="{{url('save-service-rating')}}">
		@csrf
          <div class="form-group">
		    <div class="stars-ur">
			<label for="message-text" class="col-form-label">Rating:</label>
			</br>
			 	<input class="star-ur star-ur-5" id="star-ur-5" type="radio" name="star-ur"/>
				<label class="star-ur star-ur-5" onclick = "addUserStartValue(5)" for="star-ur-5"></label>
				<input class="star-ur star-ur-4" id="star-ur-4" type="radio" name="star-ur"/>
				<label class="star-ur star-ur-4" onclick = "addUserStartValue(4)" for="star-ur-4"></label>
				<input class="star-ur star-ur-3" id="star-ur-3" type="radio" name="star-ur"/>
				<label class="star-ur star-ur-3" onclick = "addUserStartValue(3)" for="star-ur-3"></label>
				<input class="star-ur star-ur-2" id="star-ur-2" type="radio" name="star-ur"/>
				<label class="star-ur star-ur-2" onclick = "addUserStartValue(2)" for="star-ur-2"></label>
				<input class="star-ur star-ur-1" id="star-ur-1" type="radio" name="star-ur"/>
				<label class="star-ur star-ur-1" onclick = "addUserStartValue(1)" for="star-ur-1"></label>
			</div>
          </div>
		  <input type="hidden" name="rating" value="" id="user_rating_count">
		  <input type="hidden" name="to_id" id="to_id" value="">
		  <input type="hidden" name="service_id" id="user_service_id" value="">
		  <input type="hidden" name="user_custom_offer" id="user_custom_offer" value="">
          <div class="form-group">
            <label for="message-text" class="col-form-label">Review:</label>
            <textarea class="form-control" name="review" id="message-text"></textarea>
          </div>
        </form>
		{!! JsValidator::formRequest('App\Http\Requests\FrontUser\SaveRatingRequest','#user_rating_form') !!}
      </div>
      <div class="modal-footer">
	    <button type="button" id="user-sav-btn" onclick="saveUserRating()" class="btn btn-primary">
		<i id="user_form_loader" style="display:none" class="fas fa-spinner fa-spin"></i>
		Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- middle-section -->

<!-- Contact Me model -->
<div class="modal fade in" id="contactMeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Contact Me</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body form-contact-me">
                <form class="et-form form-delivery-order contactMeModal ">
                    <div class="form-group text-field">
                        <label for="form_post_content">Your Message</label>
                        <textarea class="form-control" name="message" id="message"></textarea>
                        <p id="from_message" class="error">This field is required.</p>
                    </div>

                    <input type="hidden" id="chat_to_id" value="{{ $order->detail->service->created_by }}"/>
                    <button type="button" class="send-contact-me waves-effect waves-light btn-diplomat btn btn-primary">Send <i style="display:none" class="loader fas fa-spinner fa-spin"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Contact Me model -->

<script>
//Mark as delivered
$(document).ready(function() {
    $(document).on('click', '.mark-as-delivered', function() {
        var order_id = $(this).attr('order-id');
        $('.mark-as-delivered').prop('disabled',true);
        $('#formLoader').show();
        $.ajax({
            url:"{{url('mark-as-delivered')}}",
            headers: {  
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')    
            }, 
            data: 'order_id=' + order_id,
            method: 'post',
            dataType: 'JSON',
            success:function(result)
            {
                $('#formLoader').hide();
                $('.mark-as-delivered').prop('disabled',false);
                if(result.success){
                    toastr.success(result.message)
                    window.location.reload();
                }else{
                    toastr.error(result.message)	
                }
            },
            error: function(xhr, status, errorThrown) {
                toastr.error("Sorry, there was a problem!");
                $('#formLoader').hide();
                $('.mark-as-delivered').prop('disabled',false);
            }
        });
    });
});

function addStartValue(value){
	$('#rating_count').val(value);
}

function addUserStartValue(value){
	$('#user_rating_count').val(value);
}

function showRatingModel(serviceId, customOffer){
	$('#service_id').val(serviceId);
	$('#custom_offer').val(customOffer);
	$('#serviceRatingModel').modal('show');
}

function saveServiceRating(){
	if($('#service_rating_form').valid()){
		$('#sav-btn').prop('disabled',true);
		$('#form_loader').show();
		$.ajax({
			url: "{{url('save-service-rating')}}",
			data: $('#service_rating_form').serialize(),
			type: 'post',
			dataType: 'JSON',
			success: function (response) {
				$('#sav-btn').prop('disabled',false);
		        $('#form_loader').hide();
				toastr.success(response.message);
				$('.close').trigger('click');
				$('#service_rating_form')[0].reset();
				setTimeout(function(){
					$('#service_rating_'+$('#service_id').val()).hide();
					$('#service_rating_success_'+$('#service_id').val()).show();
				},1000);
			}
	   })
	}
}

function showUserRatingModel(serviceId, toUserId, userCustomOffer){
	$('#user_service_id').val(serviceId);
	$('#user_custom_offer').val(userCustomOffer);
	$('#to_id').val(toUserId);
	$('#ratingModel').modal('show');
}

function saveUserRating(){
	if($('#user_rating_form').valid()){
		$('#user-sav-btn').prop('disabled',true);
		$('#user_form_loader').show();
		$.ajax({
			url: "{{url('save-user-rating')}}",
			data: $('#user_rating_form').serialize(),
			type: 'post',
			dataType: 'JSON',
			success: function (response) {
				$('#user-sav-btn').prop('disabled',false);
		        $('#user_form_loader').hide();
				toastr.success(response.message);
				$('.close').trigger('click');
				$('#user_rating_form')[0].reset();
				setTimeout(function(){
					$('#user_rating_'+$('#user_service_id').val()).hide();
					$('#user_rating_success_'+$('#user_service_id').val()).show();
					
				},1000);
			}
	   })
	}
}

//send contact me message
$(document).ready(function() {
    $(document).on('click', '.btn-contact-me', function() {
        $('#contactMeModal').modal('show');
    });

    $(document).on('click', '.send-contact-me', function(e) {
        e.preventDefault();
        var error = 0;
        var message = $('#message').val().trim();
        var chat_to_id = $('#chat_to_id').val();

        if(message == '') {
            $('#from_message').show();
            error++;
        } else {
            $('#from_message').hide();
        }

        if(error > 0) {
            return false;
        } else {
            $('.send-contact-me').prop('disabled',true);
            $('.send-contact-me .loader').show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                type: "POST",
                url: "{{url('send-contact-me')}}",
                data: 'message=' + message + '&chat_to_id=' + chat_to_id,
                dataType:'json',
                success: function(data) {
                    if (data.success) {
                        toastr.success(data.message);
                        $('#contactMeModal').modal('hide');
                        location.href = APP_URL + '/chat/' + data.chat_room_id;
                    } else {
                        toastr.error(data.message);
                        $('#contactMeModal').modal('hide');
                    }
                    $('.send-contact-me .loader').hide();
                    $('.send-contact-me').prop('disabled',false);
                },
                error: function(xhr, status, errorThrown) {
                    toastr.error("Sorry, there was a problem!");
                    $('.send-contact-me .loader').hide();
                    $('.send-contact-me').prop('disabled',false);
                }
            });
            return true;
        }
    });
});
</script>
<style>
div.stars {
  width: 270px;
  display: inline-block;
}

input.star { display: none; }

label.star {
  float: right;
  padding: 10px;
  font-size: 36px;
  color: #444;
  transition: all .2s;
}

input.star:checked ~ label.star:before {
  content: '\2605';
  color: #FD4;
  transition: all .25s;
}

input.star-5:checked ~ label.star:before {
  color: #FE7;
  text-shadow: 0 0 20px #952;
}

input.star-1:checked ~ label.star:before { color: #F62; }

label.star:hover { transform: rotate(-15deg) scale(1.3); }

label.star:before {
  content: '\2605';
}


div.stars-ur {
  width: 270px;
  display: inline-block;
}

input.star-ur { display: none; }

label.star-ur {
  float: right;
  padding: 10px;
  font-size: 36px;
  color: #444;
  transition: all .2s;
}

input.star-ur:checked ~ label.star-ur:before {
  content: '\2605';
  color: #FD4;
  transition: all .25s;
}

input.star-ur-5:checked ~ label.star-ur:before {
  color: #FE7;
  text-shadow: 0 0 20px #952;
}

input.star-ur-1:checked ~ label.star-ur:before { color: #F62; }

label.star-ur:hover { transform: rotate(-15deg) scale(1.3); }

label.star-ur:before {
  content: '\2605';
}
</style>
<!-- middle-section -->
@endsection