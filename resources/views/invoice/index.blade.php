@extends('layouts.default')
@section('content')
<section class="middle-section">
         <div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">MY INVOICES</div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                          <li class="breadcrumb-item active" aria-current="page">MY INVOICES</li>
                        </ol> 
                    </div>
                  </div>
              </div>
             </div>
         </div>
         <div class="main-page">
             <div class="createAgig-area">
                 <div class="container">
                     
                        <div class="row">
                            <div class="col-md-3">
                                <div class="createAgig-left-menu" id="side_link">
								@include('common.side-menu')
                                </div>
								<center style="display:none" id="side_loader"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></center>
                            </div>
                            <div class="col-md-9">
                                <div class="createAgig-right-main page-wt-bk my-invoice">
                                    <div class="category-main-pro">
									<form id="filter">
                                      <div class="payment-type1 filter-right text-right">
                                        <select name="payment_type" id="payment_type" onchange="getInvoiceList()"> 
                                            <option value="">All Payment</option>
                                            <option value="stripe">Stripe</option>
                                            <option value="paypal">Paypal</option>
                                        </select>
                                        <select name="status" id="status" onchange="getInvoiceList()">
                                            <option value="">All Status</option>
                                            <option value="2">Cancelled</option>
                                            <option value="pending">Pending</option>
                                            <option value="1">Success</option>
                                        </select>
                                      </div>
									</form>
                                    </div>
                                </div>

                                 <div class="createAgig-right-main page-wt-bk my-invoice" id="invoice_list" style="margin-top: 10px;">
                                   
                                    
                                 </div>
                            </div>
                        </div>
                        
                 </div>
             </div>
         </div>
    </section>
<script>

$(document).ready(function(){
    getInvoiceList();
});



function getInvoiceList(url,type,sort){
	
    var path = (url)?url:"{{url('invoice-list')}}";
	$('#invoice_list').html('<center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></center>') 
        $.ajax({
            url:path ,
			data:{payment_type:$('#payment_type').val(),status:$('#status').val(),type:type,sort:sort},
            method: 'get',
            dataType: 'JSON',
            success:function(result)
            {
                $('#invoice_list').html(result.html);
            }
            
        });
}

</script>
@endsection