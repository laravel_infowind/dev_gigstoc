<div class="invoice-list-table">
    <table class="table table-hover table-bordered table-striped" >
        <tr>
            <th>Invoice</th>
            <th>
                Date
                <!--
                    <a href="javascript:void(0)" onclick="getInvoiceList('','date','desc')"><i style= "color: #fff;"  class="fas fa-sort-desc" ></i>desc</a>
                    <a href="javascript:void(0)" onclick="getInvoiceList('','date','asc')"><i style= "color: #fff;" class="fas fa-sort-asc" ></i>asc</a>
                    -->
            </th>
            <!-- <th>Total
                <a href="javascript:void(0)" onclick="getInvoiceList('','total','desc')"><i style= "color: #fff;"  class="fas fa-sort-desc" ></i>desc</a>
                <a href="javascript:void(0)" onclick="getInvoiceList('','total','asc')"><i style= "color: #fff;" class="fas fa-sort-asc" ></i>asc</a>
                </th>-->
            <th>Total Paid</th>
            <!--<th>TransactionId</th>-->
            <th>Payment type</th>
            <th>Status</th>
        </tr>
        @if (count($myOrders)>0)
        @foreach ($myOrders as $value)
        <tr>
            <td><a href="{{url('invoice-detail').'/'.base64_encode($value->id)}}">#{{$value->order_id}}</a></td>
            <td>{{changeDateformat($value->created_at)}}</td>
            <!--<td>${{$value->discount}}</td>-->
            <!--<td>${{$value->subtotal}}</td>-->
            <!--<td>${{$value->tax}}</td>-->
            <td>${{$value->total}}</td>
            <!--<td>{{($value->transaction_id)?$value->transaction_id:'-'}}</td>-->
            <td>
                @if($value->payment_type == 'paypal')
                <img src="{{url('public/images/payment_03.png')}}" alt="">
                @else
                <img src="{{url('public/images/payment_07.png')}}" alt="">
                @endif
            </td>
            <td>
                @if($value->status==2)
                <span class="btn btn-danger">Cancelled</span>
                @endif
                @if($value->status==1)
                <span class="btn btn-success">Success</span>
                @endif
                @if($value->status==0)
                <span class="btn btn-warning">Pending</span>
                @endif
            </td>
        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="6">
                <a href="#">
                    <div style="width:785px" class='alert alert-danger'>
                        <center>No record found</center>
                    </div>
                </a>
            </td>
        </tr>
        @endif
    </table>
</div>
{{$myOrders->links()}}
<script>
    $('.pagination li a').on('click', function(event) {
            event.preventDefault();
                var url = $(this).attr('href');
                getInvoiceList(url);
    });
</script>