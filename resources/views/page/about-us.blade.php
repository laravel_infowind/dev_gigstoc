@extends('layouts.default')
@section('content')
<?php
// echo "<pre>";
// print_r($about);die;
?>
<section class="middle-section">
         <div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">About us</div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                          <li class="breadcrumb-item active" aria-current="page">About us</li>
                        </ol>
                    </div>
                  </div>
              </div>
             </div>
         </div>
         <div class="main-page">
             <div class="about-us-page">
                 <div class="container">
                    <div class="about-page page-wt-bk">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="about-page-cont">
                                <?php echo $about->header_content ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="about-cont-img">
                                    <img src="{{url('public/images/'.$about->image)}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about-page-hire">
                        <div class="about-in-hire text-center">
                            <h1>Hire expert freelancers <br>
                                for any job, online</h1>
                            <p>{{$about->middle_content}}</p>
                            <div class="view-jobs-btn text-center">
                                <a class="btn" href="{{url('contact-us')}}"><span>Contact us</span>
                                    <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="about-page-cout">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="about-item-cout">
                                    <div class="about-item-cout-item">
                                        <span><i class="fas fa-user-tie"></i></span>
                                    </div>
                                    <div class="about-item-cout-cont">
                                        <h3>274</h3>
                                        <p>Clients</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="about-item-cout">
                                    <div class="about-item-cout-item">
                                        <span><i class="fas fa-briefcase"></i></span>
                                    </div>
                                    <div class="about-item-cout-cont">
                                        <h3>421</h3>
                                        <p>Projects</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="about-item-cout">
                                    <div class="about-item-cout-item">
                                        <span><i class="fas fa-headset"></i></span>
                                    </div>
                                    <div class="about-item-cout-cont">
                                        <h3>1,364</h3>
                                        <p>Hours Of Support</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="about-item-cout">
                                    <div class="about-item-cout-item">
                                        <span><i class="fas fa-user-tie"></i></span>
                                    </div>
                                    <div class="about-item-cout-cont">
                                        <h3>274</h3>
                                        <p>Hard Workers</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about-page-txt page-wt-bk">
                        <h2>{{$about->footer_header}}</h2>
                        <p>{{$about->footer_content}}</p>
                        
                    </div>
                 </div>
             </div>
         </div>
    </section>
@endsection