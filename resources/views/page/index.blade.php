@extends('layouts.default')
@section('content')
<section class="middle-section">
         <div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">{{$page->name}}</div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                          <li class="breadcrumb-item active" aria-current="page">{{$page->name}}</li>
                        </ol>
                    </div>
                  </div>
              </div>
             </div>
         </div>
         <div class="main-page">
             <div class="trust-and-quality">
                 <div class="container">
                    <div class="trust-qut-page page-wt-back">
                        {!!$page->content!!}
                    </div>
                 </div>
             </div>
         </div>
    </section
@endsection