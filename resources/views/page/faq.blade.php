@extends('layouts.default')
@section('content')
<section class="middle-section">
         <div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">FAq’s</div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                          <li class="breadcrumb-item active" aria-current="page">FAq’s</li>
                        </ol>
                    </div>
                  </div>
              </div>
             </div>
         </div>
         <div class="main-page">
             <div class="alljob-requests-area">
                 <div class="container">
                    <div class="faq-page page-wt-back">
                        <div class="faq-item">
						
                            <div id="accordion">
							@if (count($faqs)>0)
                         @foreach ($faqs as $faq)
                                <div class="card">
                                    <div class="card-header" id="heading{{$faq->id}}">
                                      <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse{{$faq->id}}" aria-expanded="false" aria-controls="collapse{{$faq->id}}">
										{{$faq->question}}
                                        </button>
                                      </h5>
                                    </div>
                                    <div id="collapse{{$faq->id}}" class="collapse" aria-labelledby="heading{{$faq->id}}" data-parent="#accordion">
                                      <div class="card-body">
                                        {{$faq->answer}}
                                      </div>
                                    </div>
                                </div>
								@endforeach
                             @else
								 <div class="card">
									<div class='alert alert-danger'><center>No record found</center></div>
								</div>
								@endif
                              </div>
							 
                        </div>
                    </div>
                 </div>
             </div>
         </div>
    </section> <!-- middle-section -->@endsection