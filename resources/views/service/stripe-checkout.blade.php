@extends('layouts.default')
@section('content')
<style>
    /**
 * The CSS shown here will not be introduced in the Quickstart guide, but shows
 * how you can use CSS to style your Element's container.
 */
.StripeElement {
    box-sizing: border-box;

    height: 40px;

    padding: 10px 12px;
    margin-bottom: 10px;
    border: 1px solid rgba(32, 34, 134, 0.7);
    border-radius: 4px;
    background-color: white;

    box-shadow: 0 1px 3px 0 #e6ebf1;
    -webkit-transition: box-shadow 150ms ease;
    transition: box-shadow 150ms ease;
}

.StripeElement--focus {
    box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid {
    border-color: #fa755a;
}

.StripeElement--webkit-autofill {
    background-color: #fefde5 !important;
}
</style>
<section class="middle-section">
    <div class="main-page">
        <div class="alljob-requests-area">
            <div class="container">
            <div class="checkout-page-inn">
                <div class="checkout-now-stripe">
                    <div class="checkout-stripe-form">
                        <div id="msg">
                            @if(Session::has('card-success'))
                            <p class="alert alert-success">{{ Session::get('card-success') }}</p>
                            @endif
                        </div>
                        <div id="refresh_token">

                        </div>

                        <!-- placeholder for Elements -->
                        <form id="payment-form" data-secret="<?= $intent->client_secret ?>" class="payment-form">
                        <section>
                        <h2>Billing Information</h2>
                        <fieldset>
                        <input id="cardholder-email" type="hidden" value="{{ $user->email }}">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" required id="cardholder-name" placeholder="Cardholder Name" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" required id="cardholder-address" placeholder="Address" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" required id="cardholder-city" placeholder="City" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" required id="cardholder-state" placeholder="State" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <select required id="cardholder-country" placeholder="Country" class="form-control">
                                        
                                    </select>
                                </div>
                            </div>
                            </fieldset>
                            </section>

                            <section>
                            <h2>Payment Information</h2>
                            <div class="form-group">
                                <div id="card-element" class="card-number-date">
                
                                </div>
                            </div>
                            </section>
                            <div class="form-group">
                                <div class="input-group pay-button">
                                    <input type="submit" class="btn form-control" id="pay-by-new-card" value="Pay ${{ $cart_total }}">

                                    <i id="co_loader" style="display:none" class="fas fa-spinner fa-spin"></i>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</section> <!-- middle-section -->
<script type="text/javascript" src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">
    var stripe = Stripe(STRIPE_KEY);
    var style = {
        base: {
            color: '#303238',
            fontSize: '16px',
            fontFamily: '"Open Sans", sans-serif',
            fontSmoothing: 'antialiased',
            '::placeholder': {
            color: '#CFD7DF',
            },
        },
        invalid: {
            color: '#e5424d',
            ':focus': {
            color: '#303238',
            },
        },
        };
    var elements = stripe.elements();
	var cardElement = elements.create('card', {style: style, hidePostalCode: true});
	cardElement.mount('#card-element');
    
    var cardholderName = document.getElementById('cardholder-name');
	var cardholderEmail = document.getElementById('cardholder-email');
	var cardholderAddress = document.getElementById('cardholder-address');
	var cardholderCity = document.getElementById('cardholder-city');
	var cardholderState = document.getElementById('cardholder-state');
	var cardholderCountry = document.getElementById('cardholder-country');
    var form = document.getElementById('payment-form');
	var clientSecret = form.dataset.secret;

	var form = document.getElementById('payment-form');

    form.addEventListener('submit', function(event) {
    // We don't want to let default form submission happen here,
    // which would refresh the page.
        event.preventDefault();
        if (cardholderName.value == '') {
            $('#msg').html('<p class="alert alert-danger">Please enter card holder name.</p>');
        } else {
            $('#msg').html('');
            $('#refresh_token').html('');
            $("#co_loader").css("display", "inline-block");
            $('#pay-by-new-card').attr('disabled', true);
            stripe.createToken(cardElement, {name: cardholderName.value}).then(function(res) {
                // Handle result.error or result.token
                //console.log(data);
                if (res.token) {
                    stripe.confirmCardPayment(clientSecret, {
                        payment_method: {
                            card: cardElement,
                            billing_details: {
                                name: cardholderName.value,
                                email: cardholderEmail.value,
                                address:{
                                    line1: cardholderAddress.value,
                                    city: cardholderCity.value,
                                    state: cardholderState.value,
                                    country: cardholderCountry.value,
                                }
                            },
                        }
                    }).then(function(result) {
                        console.log(result);
                        if (result.error) {
                            // Show error to your customer
                            //console.log(result.error.message);
                            $("#co_loader").css("display", "none");
                            // Display error.message in your UI.
                            $('#msg').html('<p class="alert alert-danger">' + result.error.message + '</p>');
                            refresh_token();
                        } else {
                            if (result.paymentIntent.status === 'succeeded') {
                                // Show a success message to your customer
                                // There's a risk of the customer closing the window before callback execution
                                // Set up a webhook or plugin to listen for the payment_intent.succeeded event
                                // to save the card to a Customer
                                
                                // The PaymentMethod ID can be found on result.paymentIntent.payment_method
                                $('#pay-by-new-card').attr('disabled', true);
                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    url: APP_URL + "/make-stripe-payment",
                                    method: "POST",
                                    data: 'token=' + res.token.id + '&payment_intent_id=' + result.paymentIntent.id + '&payment_mehtod_id=' + result.paymentIntent.payment_method + '&email=' + cardholderEmail.value + '&name=' + cardholderName.value,
                                    dataType: 'json',
                                    success: function(data) {
                                        $("#co_loader").css("display", "none");
                                        if (data.success) {
                                            location.href = APP_URL + '/thank-you/' + data.order_id;
                                        } else {
                                            $('#pay-by-new-card').attr('disabled', false);
                                            $('#msg').html('<p class="alert alert-danger">' + data.msg + '</p>');
                                        }

                                    },
                                    error: function(xhr, status, errorThrown) {
                                        $('#pay-by-new-card').attr('disabled', false);
                                        $("#co_loader").css("display", "none");
                                        alert("Sorry, there was a problem!");
                                    }
                                });
                            } else {
                                $('#pay-by-new-card').attr('disabled', false);
                                $("#co_loader").css("display", "none");
                                $('#msg').html('<p class="alert alert-danger">'+ result.paymentIntent.cancellation_reason +'</p>');
                                refresh_token();
                            }
                        }
                    });
                } else {
                    //console.log(res);
                    $('#pay-by-new-card').attr('disabled', false);
                    $("#co_loader").css("display", "none");
                    $('#msg').html('<p class="alert alert-danger">' + res.error.message + '</p>');
                    refresh_token();
                }
            });
                
        }
    });

function refresh_token()
{
    setTimeout(function() {
        //location.reload()
    },
    3000);
    $('#refresh_token').html('<p class="alert alert-danger">Payment token will refresh automatically. If not refresh then <a href="' + window.location.href + '">click here</a></p>');
}
function getCountryList(){
    $.ajax({
        type: "get",
        url: "{{url('get-country-names')}}",
        dataType:'json',
        success: function(data)
        {
            if (data.success){
                $('#cardholder-country').html(data.list);
            }
        }
    });
}

$(document).ready(function(){
    getCountryList();	
});
</script>
@endsection
