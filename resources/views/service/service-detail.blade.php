@extends('layouts.default')
@section('content')
<section class="middle-section">
    <!-- <div class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="page-title-head">Get a poster design</div>
                </div>
                <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Get a poster design</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <div class="main-page">
        <div class="alljob-requests-area">
            <div class="container">
                <div class="alljob-requests-inn">
                    <div class="top-section-pajob">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="top-section-head">
                                    <h2>Get freelance services for <br>
                                        as little as $5
                                    </h2>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="post-aJob-right">
                                    <div class="post-aJob text-right">
                                        <a class="btn" href="{{ url('post-recruit') }}">
                                        <span>Post a Job Request</span> <i class="fas fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="category-list-page">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="recently-add-left">
                                    <div class="recently-add-inn wht-bcack">
                                        <div class="recently-head">{{ $service->title }}</div>
                                        <div class="recently-image">
                                            <div class="recently-img-head">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="rec-txt text-left"><a  href="{{ url('category/'.$service->category->id) }}">{{ $service->category->name }}</a></div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="rec-txt text-right">Last modified: {{ date('F d, Y', strtotime($service->updated_at)) }}</div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="service-gallery">
                                            @if ($service->is_video)
                                                {!! getVideo($service->video_link) !!}
                                            @else
                                            @if (!$service->images)
                                            <div class="owl-carousel service-images-slider">
                                                @foreach($service->images as $value)
                                                <div class="item">
                                                    <div class="recently-img">
                                                        <div class="banner-img">
                                                        <img src="{{ url('public/images/services/' . $value->image) }}" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                                @else
                                                    <div class="recently-img">
                                                        <img src="{{ url('public/images/recently.png') }}" alt="">
                                                    </div>
                                                @endif
                                            @endif
                                            </div>


                                            <div class="recently-left-txt">
                                                <h2>DESCRIPTION</h2>
                                                {!! $service->description !!}
                                                <div class="rec-left-tag">
                                                    @foreach($service->tags as $tag)
                                                    <a href="{{url('category?tag='.$tag->name)}}"><span>{{ $tag->name }}</span></a>
                                                    @endforeach
                                                </div>
                                                <div class="rec-btn">
												    <button @if(!Auth::user())  onclick="loginNow()"  @endif data-id="{{ $service->id }}" class="btn <?php if (Auth::user() && Auth::user()->id != $service->user->id) {?> order_now_btn <?php } elseif (Auth::user()) {?> disable <?php }?>">Order now <span price-data="{{ number_format($service->price, 2) }}" class="order-price"><span class="price-text">($ {{ number_format($service->price, 2) }})</span></span></button>
                                                </div>
												<?php if (Auth::user() && Auth::user()->id != $service->user->id) {?>
												<div class="rec-btn">
                                                    <button data-id="{{ $service->id }}" @if(Auth::user()) onclick="addRemoveBookmark('{{$service->id}}')" @else onclick="loginNow()" @endif   class="btn"> <span  id="bookmark">Add to Bookmark<span></span></span></button>
                                                </div>
												<?php }?>
                                            </div>

											



                                        </div>
                                    </div>
                                    <div class="review-totle wht-bcack">
                                        <div class="review-head">
                                            <div class="check-head-div">
                                                <h2>REVIEW <small>({{ count($service->rating) }})</small></h2>
                                            </div>
                                        </div>
                                        <div class="review-items">
                                            @if(!empty($service->rating) && count($service->rating)>0)
                                            @foreach($service->rating as $value)
                                            <div class="review-item">
											    <div class="review-itm-top">
                                                    <div class="review-user-img"><span><img src="{{getUserProfileImage($value->user->id)}}" alt=""></span></div>
                                                    <div class="review-user-name">
                                                        <h3>{{ $value->user->name }}</h3>
                                                        <p>{{ date('d-m-Y', strtotime($value->created_at)) }}</p>
                                                    </div>
                                                </div>
                                                <div class="review-itm-txt">
                                                    <p>{!! $value->review !!}</p>
                                                    <div class="review-star">
                                                        <span class="pro-rating">
                                                        <?php
														$rating = $value->rating;
														$rating = round($rating);
														displayRating($rating);
														?>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                           @else
												<div class="alert alert-danger mt-md-5 mt-3 w-90"><center>No record found</center></div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="recently-add-right">
                                    <div class="recently-top-right wht-bcack">
                                        <div class="recently-right-top">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="recently-rating">
                                                        <span class="pro-rating">
                                                        <?php
															$rating = $service->rating->avg('rating');
															$rating = round($rating);
															displayRating($rating);
															?>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="recently-price text-right">
                                                        <small>$</small> {{ number_format($service->price, 2) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="recently-table">
                                            <table class="table">
                                                <tr>
                                                    <td>Overall rate</td>
                                                    <td>{{getServicerating($service->id)}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Reviews</td>
                                                    <td>{{getReviewCount($service->id)}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Sales</td>
                                                    <td>{{getServicesalesCount($service->id)}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Time of delivery</td>
                                                    <td>{{ $service->deadline }} day(s)</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="recently-table-btmbtn">
                                            <div class="rec-btn">
                                                <button @if(!Auth::user())  onclick="loginNow()" @endif data-id="{{ $service->id }}" class="btn <?php if (Auth::user() && Auth::user()->id != $service->user->id) {?> order_now_btn <?php } elseif (Auth::user()) {?> disable <?php }?>">Order now <span price-data="{{ number_format($service->price, 2) }}" class="order-price"><span class="price-text">($ {{ number_format($service->price, 2) }})</span></span></button>
                                                <input type="hidden" value="{{ number_format($service->price, 2) }}" id="service-price"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="extra-div wht-bcack">
                                        <div class="extra-div-head">
                                            <h2>EXTRA</h2>
                                        </div>
                                        <div class="extra-div-main">
                                            <table class="table">
                                                @if (!empty($service->extras) && count($service->extras) >0 )
                                                @foreach($service->extras as $value)
                                                <tr>
                                                    <td>
                                                        <label>
                                                        <input type="checkbox" class="mjob_extra" name="mjob_extra[]" id="{{ $value->id }}" value="{{ $value->price }}"> <span>{{ $value->service_name }}</span>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <div class="extra-price text-right">$ {{ number_format($value->price, 2) }}</div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="2">
                                                            There are no extra services
                                                        </td>
                                                    </tr>
                                                @endif
                                            </table>
                                            <div class="extra-div-btn">
                                                <div class="user-cont-btn text-center">
                                                    @if(Auth::check())
														<?php if (Auth::user() && Auth::user()->id != $service->user->id) { ?>
														<a onclick="customOrderModal()" class="btn" href="javascript:void(0)">
														<span>Send custom order</span> <i class="far fa-paper-plane"></i>
														</a>
														<?php } ?>
                                                    @else
                                                    <a onclick="loginNow()" class="btn" href="javascript:void(0)">
                                                    <span>Send custom order</span> <i class="far fa-paper-plane"></i>
                                                    </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="category-side-bar">
                                        <div class="user-profile">
                                            <div class="user-img">
                                                <span><img src="{{ $service->user->user_image }}" alt=""></span>
                                            </div>
                                            <div class="user-name">
                                                <h3>{{ $service->user->name }}</h3>
                                                <p>
                                                    <?php $user = App\User::with('rating')->find($service->created_by);
													$rating = $user->rating()->avg('rating');
													$rating = round($rating);
													displayRating($rating);
													?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="user-dtl-div">
                                            <div class="user-dtl">
                                                <h2><i class="fas fa-map-marker-alt"></i> <span>From</span></h2>
                                                <p>{{ ($user->country) ? $user->country->name : '' }}</p>
                                            </div>
                                            <div class="user-dtl">
                                                <h2><i class="fas fa-globe-africa"></i> <span>Languages</span></h2>
                                                @if(count($user->languages)>0)
												@foreach($user->languages as $val)
												<p>{{$val->language->name}}</p>
												@endforeach
												@endif
                                            </div>
                                            <div class="user-dtl">
                                                <h2><i class="fas fa-info-circle"></i> <span>Bio</span></h2>
                                                <p>{{ $service->user->bio }}</p>
                                            </div>
                                            <div class="user-cont-btn text-center">
                                                <ul>
                                                    <li>
                                                        @if(Auth::check())
                                                        <?php
                                                        $logged_in_id = Auth::user()->id;
                                                        $chat_room = checkChatRoom($logged_in_id, $service->user->id); ?>
                                                        <?php
                                                        if ($logged_in_id != $service->user->id) {
                                                        if($chat_room) { ?>
                                                            <a class="btn" href="{{ url('chat/'.$chat_room) }}">
                                                            <span>Contact me</span> <i class="far fa-paper-plane"></i>
                                                            </a>
                                                        <?php } else { ?>
                                                            <a class="btn btn-contact-me" href="javascript:void(0)">
                                                            <span>Contact me</span> <i class="far fa-paper-plane"></i>
                                                            </a>
                                                        <?php } } ?>
                                                        @else
                                                        <a onclick="loginNow()" class="btn" href="javascript:void(0)">
                                                        <span>Contact me</span> <i class="far fa-paper-plane"></i>
                                                        </a>
                                                        @endif
                                                    </li>
                                                    <li>
                                                        <a class="btn" href="{{url('view-profile').'/'.base64_encode($service->user->id)}}">
                                                        <span>View my profile</span> <i class="fas fa-eye"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Custom order model -->
<div class="modal fade in" id="customOrderModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Custom Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body form-custom-order">
                <form class="et-form form-delivery-order customOrderModal ">
                    <h3 class="mjob-name">{{ $service->title }}</h3>
                    <div class="form-group text-field">
                        <label for="form_post_content">Description</label>
                        <textarea class="form-control" name="post_content" id="description"></textarea>
                        <p id="from_description" class="error">This field is required.</p>
                    </div>

                    <div class="form-group clearfix">
                        <div class="form-group">
                            <label for="form-budget">Budget (USD)</label>
                            <input type="number" min="1" name="budget" id="budget" class="form-control">
                            <p id="from_budget" class="error">This field is required.</p>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <div class="form-group time">
                            <label for="from_deadline">Time of delivery (Day)</label>
                            <input type="number" min="1" name="deadline" id="deadline" class="form-control">
                            <p id="from_deadline" class="error">This field is required.</p>
                        </div>
                    </div>

                    <div class="form-group clearfix">
                        <div class="attachment-file gallery_container" id="gallery_container">
                            <div class="attachment-image">
                                <ul class="gallery-image carousel-list custom-order-image-list" id="image-list">
                                </ul>
							    <div>
									<span class="plupload_buttons" id="custom-order_container" style="position: relative;">
										<span class="img-gallery" id="custom-order_browse_button" style="position: relative; z-index: 1;">
											<a href="#" class="add-img">Attach file <i class="fa fa-plus"></i></a>
										</span>
                                        <div>
                                            <input service-id="{{ $service->id }}" id="images" type="file" multiple="" accept="">
                                        </div>
                                    </span>
								</div>
                                </div>
                        </div>
                    </div>
                    <input type="hidden" id="service_id" value="{{ $service->id }}"/>
                    <input type="hidden" id="sent_to" value="{{ $service->created_by }}"/>
                    <input type="hidden" id="job_request_title" value="{{ $service->title }}"/>
                    <button type="button" class="send-custom-order waves-effect waves-light btn-diplomat btn btn-primary">Send <i style="display:none" class="loader fas fa-spinner fa-spin"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Custom order model -->

<!-- Contact Me model -->
<div class="modal fade in" id="contactMeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Contact Me</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body form-contact-me">
                <form class="et-form form-delivery-order contactMeModal ">
                    <div class="form-group text-field">
                        <label for="form_post_content">Your Message</label>
                        <textarea class="form-control" name="message" id="message"></textarea>
                        <p id="from_message" class="error">This field is required.</p>
                    </div>

                    <input type="hidden" id="chat_to_id" value="{{ $service->user->id }}"/>
                    <button type="button" class="send-contact-me waves-effect waves-light btn-diplomat btn btn-primary">Send <i style="display:none" class="loader fas fa-spinner fa-spin"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Contact Me model -->

<script>


var order_amount = 0;
var price = parseFloat($('#service-price').val());
order_amount = price;
var extra_ids = [];
$(document).ready(function(){
  
    $(document).on('click', '.mjob_extra', function () {
        var checked_extras = (this.checked ? true : false);
        var extra_price = parseFloat($(this).val());
        var extra_id = $(this).attr('id');

        if( checked_extras === true ){
            order_amount = parseFloat(order_amount) + parseFloat(extra_price);
            extra_ids.push(extra_id);
        }
        else{
            order_amount = parseFloat(order_amount) - parseFloat(extra_price);
            index = extra_ids.indexOf(extra_id);
            if( index != -1 ){
                extra_ids.splice(index, 1);
            }
        }
        //console.log(order_amount);
        //console.log(extra_ids);
        update_order_amount(order_amount);
    });

    function update_order_amount(amount)
    {
        $('.order-price').html('<span class="price-text">($ '+ order_amount.toFixed(2) +')</span>');
    }

    // click on order now button
    $(document).on('click', '.order_now_btn', function(){
		
        var redirect_url = APP_URL + '/checkout-detail?sid=' +  $(this).attr('data-id');

        $('.extra-div-main').find('input[type="checkbox"]').each(function(){
            if($(this).prop('checked') ){
                redirect_url += '&extra_ids[]='+ $(this).attr('id');
            }
        });
		var serviceStatus = "{{$service->is_active}}";
		if(serviceStatus == 1){
           window.location.href = redirect_url;
		}else{
			toastr.error('Sorry,Service is inactive');
		}
        //alert(redirect_url);
    });
	viewService();
	$('.disable').prop('disabled',true);
});

function addRemoveBookmark(serviceId){
	$.ajax({
		url: "{{url('add-remove-bookmark')}}",
		data: {service_id:serviceId},
	    type: 'get',
		dataType: 'JSON',
		success: function (response) {
			if(response.type == 'remove'){
				$('#bookmark').text('Remove from bookmark');
			}else{
				$('#bookmark').text('Add to bookmark');
			}
			toastr.success(response.message);
		}
	});
}

function viewService(){
	$.getJSON("https://api.ipify.org?format=json",function(input) {
	$.ajax({
	url: "{{url('view-service')}}",
	data: {service_id:"{{$service->id}}",ip_address:input.ip},
    type: 'get',
	dataType: 'JSON',
	success: function (response) {

	}
	});
	})
}

// send custom order
function customOrderModal(){
	$('#customOrderModal').modal('show');
}

$(document).ready(function() {
    $(document).on('click', '.send-custom-order', function(){
        var error =0;
        var description = $('#description').val().trim();
        var budget = $('#budget').val();
        var deadline = $('#deadline').val();
        var service_id = $('#service_id').val();
        var sent_to = $('#sent_to').val();
        var title = $('#job_request_title').val();
        
        
        if(description == '') {
            $('#from_description').text('This field is required');
            $('#from_description').show();
            error++;
        } else {
            $('#from_description').text('');
            $('#from_description').hide();
        }

        if(budget == '') {
            $('#from_budget').text('This field is required');
            $('#from_budget').show();
            error++;
        } else if(parseInt(budget) < parseInt(1)) {
            $('#from_budget').text('Value greater than or equal to 1');
            $('#from_budget').show();
            error++;
        } else {
            $('#from_budget').text('');
            $('#from_budget').hide();
        }

        if(deadline == '') {
            $('#from_deadline').text('This field is required');
            $('#from_deadline').show();
            error++;
        } else if(parseInt(deadline) < parseInt(1)) {
            $('#from_deadline').text('Value greater than or equal to 1');
            $('#from_deadline').show();
            error++;
        } else {
            $('#from_deadline').text('');
            $('#from_deadline').hide();
        }

        if(error > 0) {
            return false;
        } else {
            $('.send-custom-order').prop('disabled',true);
            $('.send-custom-order .loader').show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                type: "POST",
                url: "{{url('send-custom-order')}}",
                data: 'title=' + title + '&description=' + description + '&budget=' + budget + '&deadline=' + deadline + '&service_id=' + service_id + '&sent_to=' + sent_to + '&images=' + JSON.stringify(gallery_images),
                dataType:'json',
                success: function(data) {
                    if (data.success) {
                        toastr.success(data.message);
                        $('#customOrderModal').modal('hide');
                        location.href = APP_URL + '/chat/' + data.chat_room_id;
                    } else {
                        toastr.error(data.message);
                        $('#customOrderModal').modal('hide');
                        //location.reload();
                    }
                    $('.send-custom-order .loader').hide();
                    $('.send-custom-order').prop('disabled',false);
                },
                error: function(xhr, status, errorThrown) {
                    toastr.error("Sorry, there was a problem!");
                    $('.send-custom-order .loader').hide();
                    $('.send-custom-order').prop('disabled',false);
                    //location.reload();
                }
            });
            return true;
        }
    });
});

// upload files
var gallery_images = [];
$(document).on('change', '#images', function(){
    var service_id = $('#images').attr('service-id');
    var form_data = new FormData();
    var err = 0;
    form_data.append('service_id', service_id);
    // Read selected files
    var totalfiles = document.getElementById('images').files.length;
    for (var index = 0; index < totalfiles; index++) {
        var name = document.getElementById("images").files[index].name;
        form_data.append("images[]", document.getElementById('images').files[index]);

        var ext = name.split('.').pop().toLowerCase();
        if(jQuery.inArray(ext, ['gif','png','jpg','jpeg','docx','doc','pdf']) == -1) 
        {
            toastr.error("Invalid File Extension");
            err++;
        }

        var size = document.getElementById("images").files[index].size;
        if(size > 20000000)
        {
            toastr.error("File Size should be less than 20 MB");
            err++;
        }
	
    }

    if (err > 0) {
        return false; 
    } else {
        // AJAX request
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            url: '{{ url("custom/file-upload") }}', 
            type: 'post',
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (res) {
                if(res.success) {
					for(var i=0;i<res.data.length ; i++){
                        $('#image-list').append("<li id="+ res.data[i].name +"><a href='#'><i class='fa fa-paperclip'></i> "+ res.data[i].name + "." + res.data[i].type +"<i class='del_img fa fa-trash remove_photo_" + res.data[i].name + "' onclick='deleteImage("+res.data[i].name+","+"`"+res.data[i].type+"`"+")'></i><i id='login_loader_"+res.data[i].name +"' class='fa fa-spinner fa-spin' style='display:none'></i></li>");
                        gallery_images.push(res.data[i]);
                    }
						console.log(gallery_images);
                } else {
                    $.each(res.message, function(i, val) {
                        toastr.error(val)
                    });
                }
            },
            error: function(xhr, status, errorThrown) {
                toastr.error("Sorry, there was a problem!");
            }
        });
    }
});

// delete file
function deleteImage(name, type) {
    var result = confirm("Are you sure to delete?");
    if(result){
        $('#login_loader_'+name).show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "GET",
            url: "{{ url('custom/delete-file') }}/" + name+"/"+type,
            dataType:'json',
            success: function(data) {
                $('#login_loader_'+name).hide();
                if (data.success) {
                    $('li#'+name).remove();
                    $('#images').val('');
					console.log(gallery_images);
					const index = gallery_images.indexOf('"'+name+'"');
					if (index < 1) {
					  gallery_images.splice(index, 1);
					}

                    console.log(gallery_images);
                    toastr.success(data.message);
                } else {
                    toastr.error(data.message);
                }
            }
            
        });
    }
}

//send contact me message
$(document).ready(function() {
    $(document).on('click', '.btn-contact-me', function() {
        $('#contactMeModal').modal('show');
    });

    $(document).on('click', '.send-contact-me', function(e) {
        e.preventDefault();
        var error = 0;
        var message = $('#message').val().trim();
        var chat_to_id = $('#chat_to_id').val();

        if(message == '') {
            $('#from_message').show();
            error++;
        } else {
            $('#from_message').hide();
        }

        if(error > 0) {
            return false;
        } else {
            $('.send-contact-me').prop('disabled',true);
            $('.send-contact-me .loader').show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                type: "POST",
                url: "{{url('send-contact-me')}}",
                data: 'message=' + message + '&chat_to_id=' + chat_to_id,
                dataType:'json',
                success: function(data) {
                    if (data.success) {
                        toastr.success(data.message);
                        $('#contactMeModal').modal('hide');
                        location.href = APP_URL + '/chat/' + data.chat_room_id;
                    } else {
                        toastr.error(data.message);
                        $('#contactMeModal').modal('hide');
                    }
                    $('.send-contact-me .loader').hide();
                    $('.send-contact-me').prop('disabled',false);
                },
                error: function(xhr, status, errorThrown) {
                    toastr.error("Sorry, there was a problem!");
                    $('.send-contact-me .loader').hide();
                    $('.send-contact-me').prop('disabled',false);
                }
            });
            return true;
        }
    });
});
</script>
@endsection