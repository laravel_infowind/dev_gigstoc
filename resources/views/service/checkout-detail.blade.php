@extends('layouts.default')
@section('content')
<?php 
$subtotal = number_format($service->price, 2);
$service_charge = $subtotal * number_format($sc, 2);
$total = $service_charge + $subtotal;
$total = number_format($total, 2);
$is_custom_order = ($service->is_custom == 1) ? true : false;
$parent_service = ($service->is_custom == 1) ? getParentService($service->parent_service_id) : false;
?>
<section class="middle-section">
    <div class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="page-title-head">Checkout details</div>
                </div>
                <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Checkout details</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-page">
        <div class="alljob-requests-area">
            <div class="container">
                <div class="alljob-requests-inn">
                    <div class="category-list-page">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="category-side-bar">
                                    <div class="check-head-div">
                                        <h2><?php echo (!$is_custom_order) ? 'MICROJOB NAME' : 'CUSTOM FOR MJOB';?></h2>
                                    </div>
                                    <?php if (!$is_custom_order) { ?>
                                    <div class="user-dtl-div">
                                        <div class="sellers-item">
                                            <div class="sellers-img">
                                                @if(!empty($service->images))
                                                @foreach($service->images as $key => $value)
												@if($value->is_featured == 1)
                                                <a href="{{ url('service/'.$service->id) }}"><img src="{{ url('public/images/services/thumbnail/'.$value->thumbnail) }}" alt=""></a>
                                                @endif
                                                @endforeach
                                                @else
                                                <a href="{{ url('service/'.$service->id) }}"><img src="{{ url('public/images/recently.png') }}" alt=""></a>
                                                @endif
                                            </div>
                                            <div class="sellers-content">
                                                <div class="sellers-title">
                                                    <a href="{{ url('service/'.$service->id) }}">{{ $service->title }}</a>
                                                </div>
                                                <div class="sellers-cont">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td class="admin-color"><b>{{ $service->user->name }}</b></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Price</td>
                                                                <td><b>${{ number_format($service->price, 2) }}</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Views</td>
                                                                <td class="view-icn"><b>{{getViewCount($service->id)}}<i class="far fa-eye"></i></b></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Rating</td>
                                                                <td>
                                                                    <span class="pro-rating">
                                                                    <?php
                                                                    $rating = $service->rating->avg('rating');
                                                                    $rating = round($rating);
                                                                    displayRating($rating);
                                                                    ?>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="sellers-btn">
                                                    <a href="{{ url('service/'.$service->id) }}">View Detail <i class="fas fa-chevron-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } else { ?>
                                        <div class="user-dtl-div">
                                            <div class="sellers-item">
                                                <div class="sellers-img">
                                                    @if(!empty($parent_service->images))
                                                    @foreach($parent_service->images as $key => $value)
                                                    @if($value->is_featured == 1)
                                                    <a href="{{ url('service/'.$parent_service->id) }}"><img src="{{ url('public/images/services/thumbnail/'.$value->thumbnail) }}" alt=""></a>
                                                    @endif
                                                    @endforeach
                                                    @else
                                                    <a href="{{ url('service/'.$parent_service->id) }}"><img src="{{ url('public/images/recently.png') }}" alt=""></a>
                                                    @endif
                                                </div>
                                                <div class="sellers-content">
                                                    <div class="sellers-title">
                                                        <a href="{{ url('service/'.$parent_service->id) }}">{{ $parent_service->title }}</a>
                                                    </div>
                                                    <div class="sellers-cont">
                                                        <table class="table">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="admin-color"><b>{{ $parent_service->user->name }}</b></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Price</td>
                                                                    <td><b>${{ number_format($parent_service->price, 2) }}</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Views</td>
                                                                    <td class="view-icn"><b>{{getViewCount($parent_service->id)}}<i class="far fa-eye"></i></b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rating</td>
                                                                    <td>
                                                                        <span class="pro-rating">
                                                                        <?php
                                                                        $rating = $parent_service->rating->avg('rating');
                                                                        $rating = round($rating);
                                                                        displayRating($rating);
                                                                        ?>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="sellers-btn">
                                                        <a href="{{ url('service/'.$parent_service->id) }}">View Detail <i class="fas fa-chevron-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="check-out-main">
                                    <div class="check-out-div wht-bcack">
                                        <div class="check-head-div">
                                            <h2><?php echo (!$is_custom_order) ? 'ORDER SUMMARY' : 'Custom Offer'; ?></h2>
                                        </div>
                                        <div class="check-out-main-div">
                                        <?php if ($is_custom_order) { ?>
                                            <div class="custom_offer_summary">
                                                <p>{!! $service->description !!}</p>
                                                @if (!empty($service->images))
                                                <div class="attachment-file-name">
                                                    <ul class="list-attach">
                                                    @foreach($service->images as $value)    
                                                        <li><a href="{{ url('public/images/job_request/' . $value->image) }}" target="_blank"><i class="fa fa-paperclip"></i> {{ $value->image }}</a>
                                                        </li>
                                                    @endforeach
                                                    </ul>
                                                </div>
                                                @endif
                                                <div class="time-delivery-custom">
                                                    <span class="title-sub">Time of delivery</span>
                                                    <span>{{ $service->deadline }} day(s)</span>
                                                </div>
                                            </div>
                                        <?php } ?>

                                            <div class="check-out-table check-out-in">
                                                <table class="table">
                                                    <tr>
                                                        <th>PRICE</th>
                                                        <td>${{ number_format($service->price, 2) }}</td>
                                                    </tr>
                                                    @if (!empty($service->extras) && count($service->extras) >0 )
                                                    @foreach($service->extras as $value)
                                                    <tr>
                                                        <?php
                                                        $checked = '';
                                                        if(in_array($value->id, $extra_ids)) {
                                                            $checked = 'checked';
                                                            
                                                            $subtotal = $subtotal + number_format($value->price, 2);
                                                            $service_charge = $subtotal * number_format($sc, 2);
                                                            $total = $service_charge + $subtotal;
                                                            $total = number_format($total, 2);
                                                        } ?>
                                                        <th><input type="checkbox" class="checkout_mjob_extra" name="checkout_mjob_extra[]" id="{{ $value->id }}" value="{{ $value->price }}" {{ $checked }}> EXTRA <span class="float-right">
                                                        {{ $value->service_name }}</span></th>
                                                        <td>${{ number_format($value->price, 2) }}</td>
                                                    </tr>
                                                    @endforeach
                                                    @endif
                                                    
                                                    <tr>
                                                        <th>SUBTOTAL</th>
                                                        <td>
                                                            $<span class="checkout-subtotal">{{ number_format($subtotal, 2) }}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>OTHER FEES <span class="float-right">Service Charge</span></th>
                                                        <td>
                                                            $<span class="checkout-sc">{{ $service_charge }}</span>
                                                        </td>
                                                    </tr>
                                                    <tr id="discount_row" style="display:none">
                                                        <th>Discount</th>
                                                        <td>
                                                            -$<span class="discount_amt">0.00</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>TOTAL</th>
                                                        <td>
                                                            $<span class="checkout-total fourth-td">{{ $total }}</span>
                                                        </td>
                                                    </tr>
                                                    @if(Auth::check())
                                                    <tr class="wallet-discount">
                                                        <th>Wallet Discount</th>
                                                        <td>-$<span class="wallet-discount-amt-cart">0</span></td>
                                                    </tr>
                                                    <tr class="wallet-discount">
                                                        <th>Now Total Pay</th>
                                                        <td>$<span class="after-discount-amt-cart">0</span></td>
                                                    </tr>
                                                    @endif
                                                </table>
                                            </div>
                                            
                                            
                                            <div class="wallet-wrapper">
                                            @if(Auth::check())
                                                <?php $wallet_balance = getExistingBlanace(Auth::user()->id); ?>
                                                @if($wallet_balance > 0)
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <span class="checkbox-accept">
                                                                <label>
                                                                    <input type="checkbox" name="use_wallet" id="use_wallet" value="">
                                                                    <span></span>
                                                                </label> Use Wallet Balance : $<span id="wallet_amount">{{ number_format($wallet_balance, 2) }}</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                            @endif
                                            </div>

                                            <br>
                                            <!--choose-your-payment-->
                                            <div class="choose-your-payment aside-card card card-body">
                                                <div class="cart-head">
                                                    Choose your payment method
                                                </div>
                                                <!--cart-head-->

                                                <div class="currently-div payment">
                                                    <ul>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="selected_method" value="stripe">
                                                                <span><img src="{{ url('public/images/pay-img-02.png') }}" alt="Stripe"></span>
                                                            </label>
                                                        </li>

                                                        <li>
                                                            <label>
                                                                <input type="radio" name="selected_method" value="paypal">
                                                                <span><img src="{{ url('public/images/pay-img-01.png') }}" alt="Paypal"></span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!--choose-your-payment-->

                                            <div class="check-out-coupon-code">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="check-out-coupon-left">
                                                            <div class="form-group">
                                                                <input id="coupon_code" class="form-control" type="text" placeholder="Coupon code" />
                                                                <button id="coupon_btn" class="btn">apply <i id="coupon_loader" style="display:none" class="fas fa-spinner fa-spin"></i></button>
                                                            </div>
                                                            <div class="coupon-error"></div>
                                                        </div>
                                                        <div class="after-coupon-applied" style="display:none">
                                                            <h4>Discount</h4>
                                                            <p class="coupon-txt">COUPON : <span id="c_code"></span> - $<span class="discount_amt">0.00</span> OFF</p>
                                                        </div>
                                                    </div>
                                                    

                                                    <form>
                                                        <input name="service_charge" type="hidden" id="sc_val" value="{{ $service_charge }}"/>
                                                        <input name="subtotal" type="hidden" id="subtotal_val" value="{{ $subtotal }}"/>
                                                        <input name="total" type="hidden" id="total_val" value="{{ $total }}"/>
                                                        <input name="sc" type="hidden" id="sc" value="{{ $sc }}"/>
                                                        <input name="sid" type="hidden" id="sid" value="{{ $service->id }}"/>
                                                        <input name="coupon_id" type="hidden" id="coupon_id" value=""/>
                                                        <input name="is_custom_offer" type="hidden" id="is_custom_offer" value="{{ ($is_custom_order) ? 1 : 0 }}"/>
                                                        <input name="custom_job_id" type="hidden" id="custom_job_id" value="{{ $service->job_request_id }}"/>
                                                    </form>
                                                    <div class="col-md-6">
                                                        <div class="check-out-coupon-right text-right">
                                                        @if(Auth::check())
                                                            <button id="checkout-form" class="btn checkout-btn">Checkout now <i id="co_loader" style="display:none" class="fas fa-spinner fa-spin"></i></button>
                                                        @else
                                                            <button class="btn">Checkout now</button>
                                                        @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="check-out-div wht-bcack">
                                        <div class="check-head-div">
                                            <h2>SUMMARY DESCRIPTION</h2>
                                        </div>
                                        <div class="recently-left-txt">
                                        <?php echo (!$is_custom_order) ? $service->description : $parent_service->description;?>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- middle-section -->
<script>
var checkout_amount;
var checkout_subtotal;
var checkout_sc;
var sid;
var co_extra_ids;
var discount = false;
var discount_amt = 0;
$(document).ready(function(){
// checkout calculation
sid = $('#sid').val();
checkout_subtotal = parseFloat($('#subtotal_val').val());
checkout_sc = parseFloat($('#sc_val').val()).toFixed(2);
var sc = parseFloat($('#sc').val());
var checkout_total = parseFloat($('#total_val').val());
checkout_amount = parseFloat(checkout_total).toFixed(2);
co_extra_ids = [];


    $('.checkout_mjob_extra').each(function () {
        var checked_extras = (this.checked ? true : false);
        var co_extra_id = $(this).attr('id');

        if( checked_extras === true ){
            co_extra_ids.push(co_extra_id);
        }
        else{
            index = co_extra_ids.indexOf(co_extra_id);
            if( index != -1 ) {
                co_extra_ids.splice(index, 1);
            }
        }
    });

    $(document).on('click', '.checkout_mjob_extra', function () {
        checkout_subtotal = parseFloat($('#subtotal_val').val());
        checkout_sc = parseFloat($('#sc_val').val()).toFixed(2);
        var checked_extras = (this.checked ? true : false);
        var co_extra_price = parseFloat($(this).val());
        var co_extra_id = $(this).attr('id');
        
        if( checked_extras === true ){
            checkout_subtotal = parseFloat(checkout_subtotal) + parseFloat(co_extra_price);
            checkout_sc = parseFloat(checkout_subtotal) * parseFloat(sc);
            checkout_amount = parseFloat(checkout_sc) + parseFloat(checkout_subtotal);
            if (discount === true) {
                checkout_amount = parseFloat(checkout_amount) - parseFloat(discount_amt);
            }
            co_extra_ids.push(co_extra_id);
        }
        else{
            checkout_subtotal = parseFloat(checkout_subtotal) - parseFloat(co_extra_price);
            checkout_sc = parseFloat(checkout_subtotal) * parseFloat(sc);
            checkout_amount = parseFloat(checkout_sc) + parseFloat(checkout_subtotal);
            if (discount === true) {
                checkout_amount = parseFloat(checkout_amount) - parseFloat(discount_amt);
            }
            index = co_extra_ids.indexOf(co_extra_id);
            if( index != -1 ){
                co_extra_ids.splice(index, 1);
            }
        }
        //console.log(co_extra_ids);
        if ($('input[name="use_wallet"]').is(':checked')) {
            $('#use_wallet').click();
        }
        update_checkout_amount(checkout_sc, checkout_subtotal, checkout_amount);
    });
    
    function update_checkout_amount(checkout_sc, checkout_subtotal, checkout_amount)
    {
        $('.checkout-sc').html(checkout_sc.toFixed(2));
        $('.checkout-subtotal').html(checkout_subtotal.toFixed(2));
        $('.checkout-total').html(checkout_amount.toFixed(2));

        $('#sc_val').val(checkout_sc.toFixed(2));
        $('#subtotal_val').val(checkout_subtotal.toFixed(2));
        $('#total_val').val(checkout_amount.toFixed(2));
    }

    
    // click on checkout now button
    var wallet_amt = 0;
    var total_pay;
    var use_wallet = false;
    wallet_amt = parseFloat($('#wallet_amount').text()).toFixed(2);
    var wallet_discount = 0;
    total_pay = parseFloat($('.fourth-td').text()).toFixed(2);
    
    // use wallet
    $(document).on('change', '#use_wallet', function() {
        if ($('input[name="use_wallet"]').is(':checked')) {
            use_wallet = true;
            total_pay = checkout_amount;
            if (parseFloat(wallet_amt) >= parseFloat(total_pay)) {
                wallet_discount = parseFloat(total_pay);
                wallet_amt = parseFloat(wallet_amt) - parseFloat(total_pay);
                total_pay = 0;
            } else {
                wallet_discount = parseFloat(wallet_amt);
                wallet_amt = parseFloat(wallet_amt) - parseFloat(wallet_discount);
                total_pay = parseFloat(total_pay) - parseFloat(wallet_discount);
            }
            wallet_discount = parseFloat(wallet_discount).toFixed(2);
            total_pay = total_pay.toFixed(2);
            $('.wallet-discount-amt-cart').text(wallet_discount);
            $('.after-discount-amt-cart').text(total_pay);
            $('.wallet-discount').show();
        } else {
            use_wallet = false;
            wallet_amt = parseFloat($('#wallet_amount').text()).toFixed(2);
            wallet_discount = 0;
            //total_pay = parseFloat('<?php echo $total; ?>').toFixed(2);
            total_pay = parseFloat($('.fourth-td').text()).toFixed(2);
            $('.wallet-discount-amt-cart').text(wallet_discount);
            $('.after-discount-amt-cart').text(total_pay);
            $('.wallet-discount').hide();
        }
    });

    // checkout click
    $(document).on('click', '#checkout-form', function(e) {
        checkout_sc = parseFloat(checkout_sc).toFixed(2);
        checkout_subtotal = parseFloat(checkout_subtotal).toFixed(2);
        checkout_amount = parseFloat(checkout_amount).toFixed(2);
        // if (discount === true) {
        //     checkout_amount = parseFloat(checkout_amount) - parseFloat(discount_amt);
        // }
        var selected_method = $('input[name="selected_method"]:checked');
        wallet_amt = parseFloat(wallet_amt).toFixed(2);
        total_pay = parseFloat(total_pay).toFixed(2);
        //alert('wallet=' + wallet_amt);
        //alert('pay=' + total_pay);
        use_wallet = (use_wallet) ? 1 : 0;
        var is_custom_offer = $('#is_custom_offer').val();
        var custom_job_id = $('#custom_job_id').val();
        if (use_wallet && parseFloat(total_pay) <= 0) {
            $('.checkout-btn').attr('disabled', true);
            $.ajax({   
                headers: {  
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')    
                },  
                type: "POST",   
                url: APP_URL + '/direct-checkout',
                dataType: 'json',   
                data: 'subtotal='+ checkout_subtotal + '&tax=' + checkout_sc + '&use_wallet=' + use_wallet + '&total_amount=' + checkout_amount + '&wallet_amt=' + wallet_amt + '&total_pay=' + total_pay + '&sid=' + sid + '&extra_ids=' + co_extra_ids + '&is_discount=' + discount + '&discount_amt=' + discount_amt + '&coupon_id=' + $('#coupon_id').val() + '&is_custom_offer=' + is_custom_offer + '&custom_job_id=' + custom_job_id,
                beforeSend: function() {
                    $("#co_loader").css("display", "inline-block");
                },
                success: function(res){ 
                    //console.log(res);
                    if(res.success) {
                        location.href = APP_URL + '/thank-you/' + res.order_id;
                    } else {
                        $('.checkout-btn').attr('disabled', false);
                        $("#co_loader").css("display", "none");
                        alert(res.msg);
                    }
                },
                error: function (jqXHR, exception) {
                    $('.checkout-btn').attr('disabled', false);
                    $("#co_loader").css("display", "none");
                    alert('Something went wrong. Please try again !!');
                    // Your error handling logic here..
                }
            });
            return true;
        } else {
            if ( selected_method.length == 0 ) {
                alert('Please select payment method.');
                e.preventDefault();
                return false;
            } else {
                var payment_method = selected_method.val();
                if(payment_method == 'stripe') {
                    $('.checkout-btn').attr('disabled', true);
                    $.ajax({   
                        headers: {  
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')    
                        },  
                        type: "POST",   
                        url: APP_URL + '/save-order',
                        dataType: 'json',   
                        data: 'subtotal='+ checkout_subtotal + '&tax=' + checkout_sc + '&use_wallet=' + use_wallet + '&total_amount=' + checkout_amount + '&wallet_amt=' + wallet_amt + '&total_pay=' + total_pay + '&sid=' + sid + '&extra_ids=' + co_extra_ids + '&is_discount=' + discount + '&discount_amt=' + discount_amt + '&coupon_id=' + $('#coupon_id').val()  + '&is_custom_offer=' + is_custom_offer + '&custom_job_id=' + custom_job_id,
                        beforeSend: function() {
                            $("#co_loader").css("display", "inline-block");
                        },
                        success: function(res){ 
                            //console.log(res);
                            if(res.success) {
                                location.href = APP_URL + '/stripe-checkout';
                            } else {
                                $('.checkout-btn').attr('disabled', false);
                                $("#co_loader").css("display", "none");
                                alert(res.msg);
                            }
                        },
                        error: function (jqXHR, exception) {
                            $('.checkout-btn').attr('disabled', false);
                            $("#co_loader").css("display", "none");
                            alert('Something went wrong. Please try again !!');
                            // Your error handling logic here..
                        }
                    });
                } else {
                    $('.checkout-btn').attr('disabled', true);
                    $.ajax({   
                        headers: {  
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')    
                        },  
                        type: "POST",   
                        url: APP_URL + '/paypal-checkout',
                        dataType: 'json',   
                        data: 'subtotal='+ checkout_subtotal + '&tax=' + checkout_sc + '&use_wallet=' + use_wallet + '&total_amount=' + checkout_amount + '&wallet_amt=' + wallet_amt + '&total_pay=' + total_pay + '&sid=' + sid + '&extra_ids=' + co_extra_ids + '&is_discount=' + discount + '&discount_amt=' + discount_amt  + '&coupon_id=' + $('#coupon_id').val()  + '&is_custom_offer=' + is_custom_offer + '&custom_job_id=' + custom_job_id,
                        beforeSend: function() {
                            $("#co_loader").css("display", "inline-block");
                        },
                        success: function(res){ 
                            if(res.success) {
                                location.href = res.paypal_link;
                            } else {
                                $('.checkout-btn').attr('disabled', false);
                                $("#co_loader").css("display", "none");
                                alert(res.msg);
                                location.reload();
                            }
                        },
                        error: function (jqXHR, exception) {
                            $('.checkout-btn').attr('disabled', false);
                            $("#co_loader").css("display", "none");
                            alert('Something went wrong. Please try again !!');
                            location.reload();
                            // Your error handling logic here..
                        }
                    });
                }
                return true;
            }
        } //wallet
        
    });

    // coupon code apply
    $(document).on('click', '#coupon_btn', function(){
        checkout_sc = parseFloat(checkout_sc).toFixed(2);
        checkout_subtotal = parseFloat(checkout_subtotal).toFixed(2);
        checkout_amount = parseFloat(checkout_amount).toFixed(2);
        var coupon_code = $('#coupon_code').val();
        if (coupon_code == '') {
            $('.coupon-error').removeClass('alert alert-success');
            $('.coupon-error').addClass('alert alert-danger');
            $('.coupon-error').text('Please apply code');
            $('.coupon-error').show();
        } else {
            $('#coupon_btn').attr('disabled', true);
            $.ajax({   
                headers: {  
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')    
                },  
                type: "POST",   
                url: APP_URL + '/coupon-apply',
                dataType: 'json',   
                data: 'subtotal='+ checkout_subtotal + '&tax=' + checkout_sc + '&use_wallet=' + use_wallet + '&total_amount=' + checkout_amount + '&wallet_amt=' + wallet_amt + '&sid=' + sid + '&coupon_code=' + coupon_code,
                beforeSend: function() {
                    $("#coupon_loader").css("display", "inline-block");
                },
                success: function(res){ 
                    $('.coupon-error').removeClass('alert alert-success');
                    $('.coupon-error').removeClass('alert alert-danger');
                    $('.coupon-error').text('');
                    $('.coupon-error').hide();
                    if(res.success) {
                        $('#coupon_btn').attr('disabled', false);
                        $("#coupon_loader").css("display", "none");
                        $('.coupon-error').removeClass('alert alert-danger');
                        $('.coupon-error').addClass('alert alert-success');
                        $('.coupon-error').text(res.msg);
                        $('.coupon-error').show();
                        $('.checkout-total').html(res.total);
                        //$('#total_val').val(res.total);
                        $('#c_code').val(res.code);
                        $('#coupon_id').val(res.coupon_id);
                        $('.discount_amt').text(parseFloat(res.discount).toFixed(2));
                        $('.check-out-coupon-left').hide();
                        $('.after-coupon-applied, #discount_row').show();
                        discount = true;
                        discount_amt = parseFloat(res.discount).toFixed(2);
                        checkout_amount = res.total;
                        
                        if ($('input[name="use_wallet"]').is(':checked')) {
                            $('#use_wallet').click();
                        }
                    } else {
                        $('#coupon_btn').attr('disabled', false);
                        $("#coupon_loader").css("display", "none");
                        $('.coupon-error').removeClass('alert alert-success');
                        $('.coupon-error').addClass('alert alert-danger');
                        $('.coupon-error').text(res.msg);
                        $('.coupon-error').show();
                        discount = false;
                    }
                },
                error: function (jqXHR, exception) {
                    $('#coupon_btn').attr('disabled', false);
                    $("#coupon_loader").css("display", "none");
                    $('.coupon-error').removeClass('alert alert-success');
                    $('.coupon-error').removeClass('alert alert-danger');
                    $('.coupon-error').text('');
                    $('.coupon-error').hide();
                    discount = false;
                    alert('Something went wrong. Please try again !!');
                    // Your error handling logic here..
                }
            });
        }
        
    });
});

</script>
<script type="text/javascript"> 
//disable back button
    function preventBack() { 
        window.history.forward();  
    } 
        
    setTimeout("preventBack()", 0); 
        
    window.onunload = function () { null }; 
</script> 
@endsection