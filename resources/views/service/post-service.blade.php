@extends('layouts.default')
@section('content')

<section class="middle-section">
         <div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">Create a gig</div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                          <li class="breadcrumb-item active" aria-current="page">Create a gig</li>
                        </ol>
                    </div>
                  </div>
              </div>
             </div>
         </div>
         <div class="main-page">
             <div class="createAgig-area">
                 <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="createAgig-right-main page-wt-bk">
                                  <div class="recruit-now-form">
                                      <form id="service_form" method="post" action="{{url('post-service')}}"> 
									  @csrf
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-input">
                                                        <label for="">Job name</label>
                                                        <input type="text" name="title" placeholder="Please fill job name" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-input">
                                                        <label for="">Price (USD)</label>
                                                        <input type="text" name="price" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-input">
                                                        <label for="">Time of delivery (Day)</label>
                                                        <input type="text" name="deadline" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-input">
                                                        <label for="">Category</label>
                                                        <select class="form-control" name="category_id" id="categorys" tabindex="-1" aria-hidden="true">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-input">
                                                        <label for="">Description</label>
														 <textarea placeholder="Enter Description here" name="description"  cols="140"  rows="10"  class="form-control" data-sample-short></textarea>
                                                       
                                                    </div>
										        </div>
												
                                            </div>
                                        </div>
                                       <div class="form-group gallery_images_group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-input">
                                                        <label for="">Gallery</label>
                                                        <div class="create-a-gig-img">
														   <ul id="image_preview" class="row">
														   </ul>
														   <i id="preview_loader" class="fa fa-spinner fa-spin" style="display:none"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                          <div class="row">
                                              <div class="col-md-6">
												  <div class="form-input">
                                                      <input type="hidden" name="gallery_images" id="gallery_image" value="">
													  <input service-id="1" type="file" class="form-control" id="images" name="images[]" multiple/>
												  </div>
                                              </div>
                                              <div class="col-md-6">
                                                  <div class="form-input">
                                                      <label for="">Video</label>
                                                      <input type="text" name = "video" class="form-control" />
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-input">
                                                    <label for="">Extra services</label>
                                                    <div class="create-extra-services">
                                                      <div class="add-btn-extra"><span>Add extra</span> <i onclick="addMoreService()" class="fas fa-plus"></i></div>
                                                      <div class="create-extra-input">
                                                        <div class="create-extra-input-inn">
                                                          <div class="row" id='row1'>
														  
                                                            <!--<div class="col-md-7">
                                                              <input class="form-control required" placeholder='service' name = "extra[0][service]"  type="text" />
															  <span></span>
                                                            </div>
                                                            <div class="col-md-4">
                                                              <input class="form-control required extra_price" placeholder='price' name = "extra[0][extra_price]" type="text" />
															  <span></span>
                                                            </div>
                                                            <div class="col-md-1">
                                                              <div class="delete-icn"><i class="fas fa-times remove_button"></i></div>
                                                            </div>-->
															
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <div class="row">
                                          <div class="col-md-10">
                                              <div class="form-input">
                                                  <label for="">Tags</label>
                                                 <select name="tag_id[]"  id="tags" class="selectpicker form-control"
                                                  multiple="multiple">
                                                            @foreach($tags as $key => $val)
                                                                <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                            @endforeach
                                                </select>
                                              </div>
                                          </div>
										  <div class="col-md-2">
                                              <div class="form-input">
                                             <button onClick="addTagModel()" type="button" id="add_tag" class="btn tag-btn">
											    <i  class="fa fa-plus"></i>
											    Add Tag
											</button>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-input">
                                                <label for="">Opening message</label>
                                                <textarea class="form-control" name="opening_message" id="" cols="30" rows="10"></textarea>
                                                <div class="note-tg"><small>Opening message is automatically displayed as your first messag</small></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    </form>
									{!! JsValidator::formRequest('App\Http\Requests\FrontUser\AddServiceRequest','#service_form') !!}
                                  </div>
                                  <div class="recruit-now-form-btn">
                                      <ul>
                                          <li>
										    <button type="button" id="page_form_button" class="btn save-btn">
											<i id="formLoader" class="fa fa-spinner fa-spin" style="display:none"></i>
											Save
											</button>
								
                                          <li><button onclick="discardService()" class="btn discard-btn">discard</button></li>
                                      </ul>
                                  </div>
                                </div>
                            </div>
                        </div>
                        
                 </div>
             </div>
         </div>
    </section>

<div class="modal fade" id="addTag" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Add tag</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  <form id="add_tag" method="post" action="{{url('save-tag')}}">
	  @csrf
		  <div class="modal-body mx-3">
			<div class="md-form mb-5">
			  <label data-error="wrong" data-success="right" for="">Tag</label>
			  <input type="text" name="name" id="tag_name" class="form-control require">
			  <span></span>
			</div>
		  </div>
		  <div class="modal-footer d-flex justify-content-center">
			<button type="button" id="save_tag" class="btn tag-btn">Add</button>
		  </div>
	  </form>
	 
    </div>
  </div>
</div>


<script>

function addTagModel(){
	$('#addTag').modal('show');
}

$('#save_tag').on('click', function(){
	    var flag = 0;
		$(".require").each(function () {
			if ($.trim($(this).val()) == "") {
			$(this).next('span').addClass('alert-danger').css({"color": "#a94442", "display": "inline-block", "margin-top": "5px"}).html('The field is required.');
				flag++;
			}
		});
	if($("#add_tag").valid() && !flag) {
	$.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: "{{url('save-tag')}}",
            data: {name:$('#tag_name').val()},
            dataType:'json',
            success: function(response) {
               if (response.success) {
                    toastr.success(response.message);
					$('#addTag').modal('hide');
					$('#tags').append("<option value='"+response.data.id+"'>"+response.data.name+"</option>");
					$('#tags').selectpicker('refresh');
					$('#tag_name').val('');
					
                } else {
                    toastr.error(data.message);
                }
            }
        });
	}
});

var gallery_images = [];
var count=1;
function addMoreService(){
	
	$('.create-extra-input-inn').append("<div id='row"+count+"' class='row'><div class='col-md-7'><input class='form-control required' name='extra["+count+"][service]' placeholder='service'  type='text' /><span></span></div><div class='col-md-4'><input class='form-control required extra_price' placeholder='price' name='extra["+count+"][extra_price]' type='text' /><span></span></div><div class='col-md-1'><div class='delete-icn'><i class='fas fa-times remove_button'></i></div></div></div>");
	++count;				
}

 $(document).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').parent('div').parent('div').remove(); //Remove field html
  });

$(".save-btn").on('click',function(e) {
    e.preventDefault();
	
	    var flag = 0;
		$(".required").each(function () {
			if ($.trim($(this).val()) == "") {
			$(this).next('span').addClass('alert-danger').css({"color": "#a94442", "display": "inline-block", "margin-top": "5px"}).html('The field is required.');
				flag++;
			}
		});
		var feature = 0;
		$(".featured").each(function () {
			if ($(this).is(':checked')){
				feature=1;
			}
		});
		if($('#images-error').text()== '' && feature==0){
				toastr.error('Please check one image as featured image.');
		}
		
		$(".extra_price").each(function () {
			if(isNaN($(this).val())) {
			$(this).next('span').addClass('alert-danger').css({"color": "#a94442", "display": "inline-block", "margin-top": "5px"}).html('The price should be numeric.');
				flag++;
			}
			if(!isNaN($(this).val()) && $(this).val() < 0) {
			$(this).next('span').addClass('alert-danger').css({"color": "#a94442", "display": "inline-block", "margin-top": "5px"}).html('The price should be minimum 0.');
				flag++;
			}
			/*
			if(!isNaN($(this).val()) && !Number.isInteger($(this).val())) {
			$(this).next('span').addClass('alert-danger').css({"color": "#a94442", "display": "inline-block", "margin-top": "5px"}).html('The price should be only integer.');
				flag++;
			}*/
		});
		
		$('.extra_price').keyup(function () {
			$(this).next('span').html("");
			flag--;
		});
		
		$('.required').keyup(function () {
			$(this).next('span').html("");
			flag--;
		});


var formData = new FormData($('#service_form')[0]);
 /* var editor_data = CKEDITOR.instances.editor1.getData();
    formData.append('description',editor_data);
	 if(!editor_data){
        $('#description_error').show();
     }else{
         console.log('out');
         $('#description_error').hide();
     }
	 */
for(var i=0;i<gallery_images.length ; i++)
{
	formData.append('gallery_image[]',JSON.stringify(gallery_images[i]))
   
}
  if ($("#service_form").valid() && !flag && feature) {
        $('.save-btn').prop('disabled',true);
        $('#formLoader').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: "{{url('post-service')}}",
            contentType: false,
            processData: false,
            data: formData,
            dataType:'json',
            success: function(data) {
                if (data.success) {
                    toastr.success(data.message);
                    setTimeout(function(){
                        window.location="{{url('category')}}";
                    },1000);
                } else {
                    toastr.error(data.message);
                }
                $('#formLoader').hide();
                $('.btn-success').prop('disabled',false);
            }
        });
    }
});

$(document).on('change', '#images', function(){
    var service_id = $('#images').attr('service-id');
    var form_data = new FormData();
    var err = 0;
    form_data.append('service_id', service_id);
    // Read selected files
    var totalfiles = document.getElementById('images').files.length;
    for (var index = 0; index < totalfiles; index++) {
        var name = document.getElementById("images").files[index].name;
        form_data.append("images[]", document.getElementById('images').files[index]);

        var ext = name.split('.').pop().toLowerCase();
        if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
        {
            toastr.error("Invalid Image File");
            err++;
        }

        var size = document.getElementById("images").files[index].size;
        if(size > 20000000)
        {
            toastr.error("Image Size should be less than 20 MB");
            err++;
        }
	
    }

    if (err > 0) {
        return false; 
    } else {
		$('#preview_loader').show();
        // AJAX request
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            url: '{{ url("service/image-upload") }}', 
            type: 'post',
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (res) {
                if(res.success) {
						for(var i=0;i<res.data.length ; i++){
						$('#image_preview').append("<li id='remove_"+res.data[i].name+"'><label><img class='img-responsive' src='"+ res.data[i].url +"'><div class='featured_image'><input type='radio' class = 'featured' name='is_featured' value='"+ res.data[i].name +"'><span></span></div><i class='del_img fa fa-remove remove_photo_" + res.data[i].name + "' onclick='deleteImage("+res.data[i].name+","+"`"+res.data[i].type+"`"+")'></i><i id='login_loader_"+res.data[i].name +"' class='fa fa-spinner fa-spin' style='display:none'></i></label></li>");
						 gallery_images.push(res.data[i]);
						}
						$('#preview_loader').hide();
						console.log(gallery_images);
                } else {
                    $.each(res.message, function(i, val) {
                        toastr.error(val)
                    });
                }
            },
            error: function(xhr, status, errorThrown) {
                toastr.error("Sorry, there was a problem!");
            }
        });
    }
});

function deleteImage(name,type){
    var result = confirm("Are you sure to delete?");
    if(result){
        $('#login_loader_'+name).show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "GET",
            url: "{{ url('service/delete-image') }}/" + name+"/"+type,
            dataType:'json',
            success: function(data) {
                $('#login_loader_'+name).hide();
                if (data.success) {
					$('#remove_'+name).remove();
                    $('.remove_photo_' + name).parent('div').remove();
					console.log(gallery_images);
					const index = gallery_images.indexOf('"'+name+'"');
					if (index < 1) {
					  gallery_images.splice(index, 1);
					}

                    console.log(gallery_images);
                    toastr.success(data.message);
                } else {
                    toastr.error(data.message);
                }
            }
            
        });
    }
}


 function getParentCategory(){

    $.ajax({
        type: "get",
        url: "{{url('get-category')}}",
        dataType:'json',
        success: function(data)
        {
            if (data.success){
                $('#categorys').html(data.list);
            }
        }
        });
  }
  
  function discardService(){
	  window.location = "{{url('/')}}"
  }
  
  $(document).ready(function(){
    getParentCategory();
  });
  /*
    CKEDITOR.on('instanceCreated', function(e) {
        e.editor.on('contentDom', function() {
            e.editor.document.on('keyup', function(event) {
                $("#description_error").hide();
            });
        });
    });
*/
</script>
@endsection