
                                            <div class="category-main-pro">
                                            <ul >
											@if (count($services)>0)
                                            @foreach ($services as $service)
											<li>
													<div class="sellers-item">
                                                        <div class="sellers-img">
														 @if ($service->is_video)
                                                         {!! getVideo($service->video_link) !!}
                                                         @else(!empty($service->images))
                                                         @foreach($service->images as $key => $value)
													     @if($value->is_featured == 1)
                                                          <a href="{{url('service/'.$service->id)}}"><img src="{{ url('public/images/services/thumbnail/'.$value->thumbnail) }}" alt=""></a>
														 @endif
														 @endforeach
                                                         @endif
														</div>
                                                        <div class="sellers-content">
                                                          <div class="sellers-title">
                                                            <a href="{{url('service/'.$service->id)}}">{{$service->title}}</a>
                                                          </div>
                                                          <div class="sellers-cont">
                                                            <table class="table">
                                                              <tbody><tr>
                                                                <td class="admin-color"><b>{{$service->user->name}}</b></td>
                                                                <td></td>
                                                              </tr>
                                                              <tr>
                                                                <td>Price</td>
                                                                <td><b>${{$service->price}}</b></td>
                                                              </tr>
                                                              <tr>
                                                                <td>Views</td>
                                                                <td class="view-icn"><b>{{getViewCount($service->id)}} <i class="far fa-eye"></i></b></td>
                                                              </tr>
                                                              <tr>
                                                                <td>Rating</td>
                                                                <td>
                                                                  <?php echo displayRating(getServicerating($service->id))?>
                                                                </td>
                                                              </tr>
                                                            </tbody></table>
                                                          </div>
                                                          <div class="sellers-btn">
                                                            <a href="{{url('service/'.$service->id)}}">View Detail <i class="fas fa-chevron-right"></i></a>
                                                          </div>
                                                        </div>
                                                      </div>
                                                </li>
												@endforeach
                                           </ul>
										   @else
												<div style="width:100%" class='alert alert-danger'><center>No record found</center></div>

											@endif
                                        </div>
										{{$services->links()}}
<script>
$('.pagination li a').on('click', function(event) {
        event.preventDefault();
            var url = $(this).attr('href');
            getServiceList(url);
});
</script>