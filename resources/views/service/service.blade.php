@extends('layouts.default')
@section('content')

<section class="middle-section">
         <div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">CATEGORIES</div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                          <li class="breadcrumb-item active" aria-current="page">CATEGORIES</li>
                        </ol>
                    </div>
                  </div>
              </div>
             </div>
         </div>
         <div class="main-page">
             <div class="alljob-requests-area">
                 <div class="container">
                    <div class="alljob-requests-inn">
                        <div class="top-section-pajob">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="top-section-head">
                                        <h2>Get freelance services for <br>
                                            as little as $5</h2>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="post-aJob-right">
                                        <div class="post-aJob text-right">
                                            <a class="btn" href="{{url('post-service')}}">
                                                <span>Post a Job Request</span> <i class="fas fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="top-filter">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="filter-left">
                                        <h3 id="total_rows"> </h3>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="filter-right text-right">
                                        <label>Sort by</label>
                                        <select name="" id="sorting" onchange="getServiceList('','','sort')">
										     <option value="">SELECT</option>
                                            <!-- <option value="featured">Featured</option> -->
                                             <option value="newest">Newest</option>
											 <option value="oldest">Oldest</option>
											 <option value="hightest_rating">Hightest Ratings</option>
											 <!--<option value="best_seller">Best Seller</option> -->
											 <option value="hightest_views">Hightest Views</option>
											 <option value="low_to_high">Price:Low to High</option>
											 <option value="high_to_low">Price:High to Low</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="category-list-page">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="category-side-bar">
                                        <div class="category-side-head">
                                            <h3>CATEGORIES</h3>
                                        </div>
                                        <div class="category-side-menu">
                                            <ul>
												<li ><a id="all_tab" onclick="getServiceList('','','all')" href="javascript:void(0)">All</a></li>
												@if (!empty($categories))
												@foreach ($categories as $val)
												 <input type="hidden" id="category_{{$val->id}}" value="{{$val->id}}">
                                                <li >
												<a id="cat_{{$val->id}}" onclick="getServiceList('','{{$val->id}}')" href="javascript:void(0)">{{ucwords($val->name)}}</a>
												@if(count($val->categories) > 0)
												<span class="new"></span>
												<ul>
												<?php  $c=0 ?>
													@foreach ($val->categories as $child)
													<?php 
													$c++;
													if($c==1){
														$parentId = $child->id-1;
													}
													?>
													<input type="hidden" id="category_{{$child->id}}" value="{{$child->id}}">
													<li >
														<a id="cat_{{$child->id}}" class="nav-link" onclick="getServiceList('','{{$child->id}}','','{{$parentId}}')" href="javascript:void(0)">{{ ucwords($child->name) }}</a>
													</li>
													@endforeach
												</ul>
												@endif
                                                </li>
												@endforeach
												@endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="category-main-items" id="service_list">

                                    </div>
                                </div>
                            </div>
                        </div>
                         
                    </div>
                 </div>
             </div>
         </div>
    </section> 
	
<script>

var CATEGORY_ID;
var CAT;
var temp = "{{$category_id}}";
var search = "{{$search}}";
search = (search.length!=0)?search:'';
var tag = "{{$tag}}";
tag = (tag.length!=0)?tag:'';

$(document).ready(function(){
	
	 getServiceList();
	if(temp.length==0){
	CATEGORY_ID='';
	}
});

CATEGORY_ID = (temp.length!=0)?temp:'';

function getServiceList(url,ID,type,parentId=''){
	console.log(parentId);
    var path = (url)?url:"{{url('service/list')}}";
	var category_id = (CATEGORY_ID)?CATEGORY_ID:$('#category_'+ID).val();
	CATEGORY_ID='';
	if(type!='sort'){
		CAT = category_id;
	}
	if(type=='sort'){
		category_id = CAT;
	}
	if (history.pushState && (category_id||(type=='all'))) {
		 $('a').css({'color':''});
		 search = '';
		 tag = '';
		   window.history.replaceState({}, document.title, "<?php echo url('category');?>");
		if(type=='all'){
	 	 $('#all_tab').css({'color':'#0e79be'});
		}else{
		  let searchParams = new URLSearchParams(window.location.search||window.location.tag);
		  let newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '/' + category_id;
          -window.history.pushState({path: newurl}, '', newurl);
		  $("li").removeClass("hover");
		  $('#cat_'+category_id).addClass("hover");
		  if(parentId.length != 0){
			 var parLi = $('#cat_'+parentId).parent('li').addClass('hover');
		  }
		  $('#cat_'+category_id).css({'color':'#0e79be'});
		}
	}
	
	$('#service_list').html('<center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></center>') 
	$.ajax({
		url:path ,
		data: {category_id:category_id,sorting:$('#sorting').val(),search:search,tag:tag},
		method: 'get',
		dataType: 'JSON',
		success:function(result)
		{
			$('#service_list').html(result.html);
			$('#total_rows').html(result.count+'  MJOBS AVAILABLE');
		}
		
	});
 }



</script>
  @endsection