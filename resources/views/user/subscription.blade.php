@extends('layouts.default')
@section('content')
 <section class="middle-section">
         <div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">Subscriber Settings</div>
                  </div>
                  <div class="col-md-6"> 
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="index.html">Home</a></li> 
                          <li class="breadcrumb-item active" aria-current="page">Subscriber Settings</li>
                        </ol>
                    </div>
                  </div>
              </div>
             </div>
         </div> 
         <div class="main-page">
             <div class="createAgig-area">
                 <div class="container">
                     
                        <div class="row">
                            <div class="col-md-3">
                                <div class="createAgig-left-menu">
                                    @include('common.side-menu')
                                </div>
                            </div>
                            <div class="col-md-9">        

                                <div class="dashboard-bal"> 
                                  <div class="check-head-div"><h2>Web Notification Setting</h2></div>

                                  <div class="createAgig-right-main page-wt-bk subsc">
                                  Receive Web Notification  
                                  <label class="switch">
                                        <input id="notification_check" onclick="setSubscribtion()" @if(Auth::user()->notification_subscription==1) checked="checked" @endif type="checkbox">
                                        <span class="slider round"></span>
                                  </label>
                                </div>

                                </div> 

                            </div>
                        </div>
                        
                 </div>
             </div>
         </div>
    </section>  <!-- middle-section -->
	
<script>

function setSubscribtion(){
	
	bootbox.confirm({                      
				message: "Are you sure you want to change status?",
				buttons: {
					confirm: {
						label: 'Yes',
						className: 'btn-success'
					},
					cancel: {
						label: 'No',
						className: 'btn-danger'
					}
				},
				callback: function (result) {
					if(result){
					   var path = "{{url('set-noitification-subscription')}}";
						$.ajax({
							url:path ,
							method: 'get',
							dataType: 'JSON',
							success:function(result)
							{
								toastr.success(result.message);
							}
							
						});
					}else{
						if(!$("#notification_check").is(':checked')){
							$('#notification_check').prop('checked',true);
						}else{
							$('#notification_check').prop('checked',false);
						}
					}
				
				}
			});
	
 }


</script>
@endsection