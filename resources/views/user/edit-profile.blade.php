@extends('layouts.default')
@section('content')
 <section class="middle-section">
         <div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">Edit profile</div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                          <li class="breadcrumb-item active" aria-current="page">Edit profile</li>
                        </ol>
                    </div>
                  </div>
              </div>
             </div>
         </div>
         <div class="main-page">
             <div class="edit-profile-area">
                 <div class="container">
                    <div class="edit-profile-inn">
                         
                        <div class="category-list-page">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="category-side-bar">
                                         <div class="user-profile">
                                             <div class="user-img">
													
                                                 <span>
												 <img id="profile_img" src="{{getUserProfileImage(Auth::user()->id)}}" alt="">
												 </span>
												 <i id="loader" class="fa fa-spinner fa-spin" style="display:none"></i>
                                             </div>
                                             <div class="user-name">
                                                 <h3>{{Auth::user()->name}}</h3>
                                                 <p>
                                                    <?php echo displayRating(getUserRating(Auth::user()->id));?>
                                                 </p>
                                             </div>
											 <input type="file"  id="profile_pic">
                                         </div>
                                         <div class="user-dtl-div">
										 <?php
										 $user = App\User::where(['id'=>\Auth::user()->id])->first();
										 $langArry = []
;											if(count($user->languages)>0){
													foreach($user->languages as $val){
													$langArry[] = $val->language_id;
													}
												}
											 ?>
                                             <div class="user-dtl">
											     <h2><i class="fas fa-map-marker-alt"></i> <span>From</span></h2>
                                                 <p>{{($user->country)?$user->country->name:''}}</p>
                                             </div>
                                             <div class="user-dtl">
                                                <h2><i class="fas fa-globe-africa"></i> <span>Languages</span></h2>
                                                @if(count($user->languages)>0)
												@foreach($user->languages as $val)
												<p>{{$val->language->name}}</p>
												@endforeach
												@endif
                                             </div>
                                             
                                             <div class="user-cont-btn">
                                                <a class="btn" href="#">
                                                    <span>Contact me</span> <i class="far fa-paper-plane"></i>
                                                </a>
                                             </div>
											 @if($user->id == Auth::user()->id)
											  <div class="user-cont-btn">
                                                <a class="btn" href="{{url('my-profile')}}">
                                                    <span>My profile</span> <i class="fas fa-eye"></i>
                                                </a>
                                             </div>
											 @endif
                                         </div>
                                    </div>
                                </div>
                                <div class="col-md-9"> 
                                        <div class="edt-pro-bal wht-bcack">
                                            
                                             <form action="{{url('update-profile')}}" method="post" id="edit_profile">
                                                @csrf
												<div class="edt-pro-form">
                                                    <h3>DESCRIPTION</h3>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-input">
                                                                    <p>
                                                                        <?php echo displayRating(getUserRating(Auth::user()->id));?>
                                                                     </p>
                                                                     <textarea placeholder="There is no content" name="bio" class="form-control" id="" cols="30" rows="10">{{(\Auth::user()->bio)?\Auth::user()->bio:''}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
													<div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-input">
                                                                    <label for="">Fullname</label>
                                                                    <input type="text" name="fullname"value="{{(\Auth::user()->fullname)?\Auth::user()->fullname:''}}" placeholder="Enter fullname" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
													<div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-input">
                                                                    <label for="">Country</label>
                                                                    <select name="country" class="form-control" id="country">
                                                                        
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
													<div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-input">
																
                                                                    <label for="">Languages</label>
                                                                    <select name="language[]" class="selectpicker form-control" id="language" multiple>
                                                                        <option value="">Select Languages</option>
																	
																		@foreach($languages as $language)
																		<option value="{{$language->id}}" <?php echo searchCommaValue($langArry,$language['id']) ?> >{{ucfirst($language->name)}}</option>
																		@endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
													
													<div class="user-cont-btn save_btn">
														<a class="btn" onclick="updateProfile()" href="javascript:void(0)">
														<i id="form_loader" style="display:none" class="fas fa-spinner fa-spin"></i>
															<span> Update</span>
														</a>
													 </div>
                                                </div>
                                                
                                                    
										     </form>
											 {!! JsValidator::formRequest('App\Http\Requests\FrontUser\EditProfileRequest','#edit_profile') !!}
                                        </div>
                               
                                </div>
                            </div>
                        </div>
                         
                    </div>
                 </div>
             </div>
         </div>
    </section> <!-- middle-section -->
	
	
<script>
var countryId = "{{\Auth::user()->country_id}}";
function updateProfile(){
    if($('#edit_profile').valid()){
        $('#form_loader').show();
        $('.save_btn').prop('disabled',true);
        $.ajax({
		    headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: "{{url('update-profile')}}",
            data: $('#edit_profile').serialize(),
            dataType: 'JSON',
            success:function(result)
            {
                if(result.success){
                    toastr.success(result.message);
                    setTimeout(function(){
						window.location.href = "{{url('my-profile')}}";
					},1000);
                }else{
                    toastr.error(result.message);
                }
                $('#form_loader').hide();
                $('.save_btn').prop('disabled',false);
            }
            
        });
    }
}

$("#profile_pic").on('change',function(e) {
	$('#loader').show();
	var img = e.target.files[0];
	var formData = new FormData();
	formData.append('profile',img)
   $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: "{{url('update-picture')}}",
            contentType: false,
            processData: false,
            data: formData,
            dataType:'json',
            success: function(data) {
				$('#loader').hide();
				$('#profile_img').attr('src',data.url);
            }
        });
});

function getCountryList(){
		$.ajax({
			type: "get",
			url: "{{url('get-country')}}",
			dataType:'json',
			data:{countryId:countryId},
			success: function(data)
			{
				if (data.success){
					$('#country').html(data.list);
				}
			}
        });
}

$(document).ready(function(){
getCountryList();	
});
</script>
@endsection