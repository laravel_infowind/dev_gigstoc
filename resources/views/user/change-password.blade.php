@extends('layouts.default')
@section('content')

<section class="middle-section">
         <div class="page-title">
             <div class="container">
                 <div class="row">
                     <div class="col-md-6">
                        <div class="page-title-head">Change Password</div>
                     </div>
                     <div class="col-md-6">
                        <div class="page-breadcrumb-head">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Change Password</li>
                            </ol>
                        </div>
                     </div>
                 </div>
             </div>
         </div>
         <div class="main-page">
             <div class="sign-area">
                 <div class="container">
                    <div class="sign-area-inn">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="sign-form-area">
                                    <div class="sign-head">
                                        <h2>Change password</h2>
                                        <p>Please change your password</p>
                                    </div>
                                    <div class="sign-form">
                                        <form id="chnage_password_form" method="post" action="{{url('change-password')}}">
                                           @csrf
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label for="">Current password<span>*</span></label>
                                                        <div class="input-group">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="fas fa-lock"></i>
                                                                </span>
                                                            </div>
                                                            <input type="password" name="current_password" class="form-control input_pass" value="" placeholder="Current password*">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label for="">Password<span>*</span></label>
                                                        <div class="input-group">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="fas fa-lock"></i>
                                                                </span>
                                                            </div>
                                                            <input type="password" name="new_password" class="form-control input_pass" value="" placeholder="Password*">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label for="">Confirm password<span>*</span></label>
                                                        <div class="input-group">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="fas fa-lock"></i>
                                                                </span>
                                                            </div>
                                                            <input type="password" name="confirm_password" class="form-control input_pass" value="" placeholder="Confirm password*">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button type="submit" name="button" class="btn login_btn">Save changes
                                                        <i id="form_loader" style="display:none" class="fas fa-spinner fa-spin"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        {!! JsValidator::formRequest('App\Http\Requests\FrontUser\ChangePasswordRequest','#chnage_password_form') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="sign-image">
                                    <img src="{{url('public/images/sign-img3.png')}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
             </div>
         </div>
    </section>
    <script>

$('#chnage_password_form .login_btn').on('click',function(e){
    e.preventDefault();
    if($('#chnage_password_form').valid()){
        $('#form_loader').show();
        $('.login_btn').prop('disabled',true);
        $.ajax({
            url: "{{url('change-password')}}",
            data: $('#chnage_password_form').serialize(),
            method: 'post',
            dataType: 'JSON',
            success:function(result)
            {
                if(result.success){
                    toastr.success(result.message);
                    $('#chnage_password_form')[0].reset();
                }else{
                    toastr.error(result.message);
                }
                $('#form_loader').hide();
                $('.login_btn').prop('disabled',false);
            }
            
        });
    }
});


</script>
@endsection