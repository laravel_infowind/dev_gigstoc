@extends('layouts.default')
@section('content')
 <section class="middle-section">
         <div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">My profile</div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                          <li class="breadcrumb-item active" aria-current="page">My profile</li>
                        </ol>
                    </div>
                  </div>
              </div>
             </div>
         </div>
         <div class="main-page">
             <div class="alljob-requests-area">
                 <div class="container">
                    <div class="alljob-requests-inn">

                        <div class="profile-page-head">
                            <h1>{{$user->fullname}}'s Profile</h1>
                        </div>

                        <div class="category-list-page">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="category-side-bar">
                                         <div class="user-profile">
                                             <div class="user-img">
                                                 <span><img src="{{getUserProfileImage($user->id)}}" alt=""></span>
                                             </div>
                                             <div class="user-name">
                                                 <h3>{{$user->name}}</h3>
												 <?php echo displayRating(getUserRating($user->id)) ?>
                                              </div>
                                         </div>
                                         <div class="user-dtl-div">
                                             <div class="user-dtl">
                                                 <h2><i class="fas fa-map-marker-alt"></i> <span>From</span></h2>
                                                 <p>{{($user->country)?$user->country->name:''}}</p>
                                             </div>
                                             <div class="user-dtl">
                                                <h2><i class="fas fa-globe-africa"></i> <span>Languages</span></h2>
                                                @if(count($user->languages)>0)
												@foreach($user->languages as $val)
												<p>{{$val->language->name}}</p>
												@endforeach
												@endif
                                             </div>
                                             <div class="user-dtl">
                                                <h2><i class="fas fa-info-circle"></i> <span>Bio</span></h2>
                                                <p>{{$user->bio}}</p>
                                             </div>
                                            <div class="user-cont-btn">
                                                @if(Auth::check())
                                                <?php
                                                $logged_in_id = Auth::user()->id;
                                                $chat_room = checkChatRoom($logged_in_id, $user->id);?>
                                                <?php
                                                if ($logged_in_id != $user->id) {
                                                    if ($chat_room) {?>
                                                    <a class="btn" href="{{ url('chat/'.$chat_room) }}">
                                                    <span>Contact me</span> <i class="far fa-paper-plane"></i>
                                                    </a>
                                                <?php } else {?>
                                                    <a class="btn btn-contact-me" href="javascript:void(0)">
                                                    <span>Contact me</span> <i class="far fa-paper-plane"></i>
                                                    </a>
                                                <?php }}?>
                                                @else
                                                <a onclick="loginNow()" class="btn" href="javascript:void(0)">
                                                <span>Contact me</span> <i class="far fa-paper-plane"></i>
                                                </a>

                                             @endif
                                                </div>
											 @if(!empty(Auth::user()) && $user->id == Auth::user()->id)
											 <div class="user-cont-btn">
                                                <a class="btn" href="{{url('edit-profile')}}">
                                                    <span>Edit profile</span> <i class="fas fa-edit"></i>
                                                </a>
                                             </div>
											 @endif
                                         </div>

                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="category-main-items" id="service_list">

                                    </div>
									<div class="review-totle wht-bcack">
                                        <div class="review-head">
                                            <div class="check-head-div">
                                                <h2>REVIEW <small>({{ count($user->rating) }})</small></h2>
                                            </div>
                                        </div>
                                        <div class="review-items">
                                            @if(count($user->rating)>0)
                                            @foreach($user->rating as $value)
                                            <div class="review-item">
											    <div class="review-itm-top">
                                                    <div class="review-user-img"><span><img src="{{getUserProfileImage($value->fromUser->id)}}" alt=""></span></div>
                                                    <div class="review-user-name">
                                                        <h3>{{ $value->fromUser->name }}</h3>
                                                        <p>{{ date('d-m-Y', strtotime($value->created_at)) }}</p>
                                                    </div>
                                                </div>
                                                <div class="review-itm-txt">
                                                    <p>{!! $value->review !!}</p>
                                                    <div class="review-star">
                                                        <span class="pro-rating">
                                                        <?php
														$rating = $value->rating;
														$rating = round($rating);
														displayRating($rating);
														?>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
											@else
												<div class="alert alert-danger mt-md-5 mt-3 w-90"><center>No record found</center></div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                 </div>
             </div>
         </div>
    </section> <!-- middle-section -->

<!-- Contact Me model -->
<div class="modal fade in" id="contactMeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Contact Me</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body form-contact-me">
                <form class="et-form form-delivery-order contactMeModal ">
                    <div class="form-group text-field">
                        <label for="form_post_content">Your Message</label>
                        <textarea class="form-control" name="message" id="message"></textarea>
                        <p id="from_message" class="error">This field is required.</p>
                    </div>

                    <input type="hidden" id="chat_to_id" value="{{ $user->id }}"/>
                    <button type="button" class="send-contact-me waves-effect waves-light btn-diplomat btn btn-primary">Send <i style="display:none" class="loader fas fa-spinner fa-spin"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Contact Me model -->
<script>
$(document).ready(function(){
	getServiceList();
});


function getServiceList(url){

	 var path = (url)?url:"{{url('user/service/list')}}";
	$('#service_list').html('<center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></center>')
	$.ajax({
		url:path ,
		data: {user_id:"{{$userId}}"},
		method: 'get',
		dataType: 'JSON',
		success:function(result)
		{
			$('#service_list').html(result.html);
		}

	});
 }

//send contact me message
$(document).ready(function() {
    $(document).on('click', '.btn-contact-me', function() {
        $('#contactMeModal').modal('show');
    });

    $(document).on('click', '.send-contact-me', function(e) {
        e.preventDefault();
        var error = 0;
        var message = $('#message').val().trim();
        var chat_to_id = $('#chat_to_id').val();

        if(message == '') {
            $('#from_message').show();
            error++;
        } else {
            $('#from_message').hide();
        }

        if(error > 0) {
            return false;
        } else {
            $('.send-contact-me').prop('disabled',true);
            $('.send-contact-me .loader').show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                type: "POST",
                url: "{{url('send-contact-me')}}",
                data: 'message=' + message + '&chat_to_id=' + chat_to_id,
                dataType:'json',
                success: function(data) {
                    if (data.success) {
                        toastr.success(data.message);
                        $('#contactMeModal').modal('hide');
                        location.href = APP_URL + '/chat/' + data.chat_room_id;
                    } else {
                        toastr.error(data.message);
                        $('#contactMeModal').modal('hide');
                    }
                    $('.send-contact-me .loader').hide();
                    $('.send-contact-me').prop('disabled',false);
                },
                error: function(xhr, status, errorThrown) {
                    toastr.error("Sorry, there was a problem!");
                    $('.send-contact-me .loader').hide();
                    $('.send-contact-me').prop('disabled',false);
                }
            });
            return true;
        }
    });
});
</script>
@endsection