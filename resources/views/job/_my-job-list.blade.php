				  <tr>
					<th>Title</th>
					<th>Date</th>
					<th>Budget</th>
					<th>Status</th>
					<!--<th>Action</th>-->
				  </tr> 
				@if(count($myJobs)>0)
				@foreach ($myJobs as $job)
				  <tr id="job_{{$job->id}}">
					<td><a href="{{url('job-detail').'/'.base64_encode($job->id)}}">{{$job->title}}</a></td>
					<td>{{changeDateformat($job->created_at)}}</td>
					<td>$ {{number_format($job->budget,2)}}</td>
					<td>
					<span class="btn btn-warning">Publish</span>
					</td>
					<!--
					<td><a href="javascript:void(0)" onclick="deleteJob('{{$job->id}}')" class="ac"><i class="far fa-trash-alt"></i></a></td>
					-->
				  </tr>
				@endforeach
				@else
					<tr class='alert alert-danger mt-md-5 mt-3 w-90'><td colspan="5"><center>No record found</center></td></tr>
				@endif
