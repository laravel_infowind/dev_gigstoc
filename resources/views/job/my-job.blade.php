@extends('layouts.default')
@section('content')
 <section class="middle-section">
         <div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">My Job Requests</div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li> 
                          <li class="breadcrumb-item active" aria-current="page">My Job Requests</li>
                        </ol>
                    </div>
                  </div>
              </div>
             </div>
         </div> 
         <div class="main-page">
             <div class="createAgig-area">
                 <div class="container">
                     
                        <div class="row">
                            <div class="col-md-3">
                                <div class="createAgig-left-menu">
                                    @include('common.side-menu')
                                </div>
                            </div>
                            <div class="col-md-9">   

                               <div class="dashboard-bal"> 
                              <div class="user-dtl-div user-dtl-div22">
                                    <div class="user-cont-btn">
                                        <a class="btn" href="{{url('post-recruit')}}">
                                        <i class="fas fa-plus"></i> <span>Post a Job Request</span>
                                        </a>
                                    </div>
                                </div>
                                 </div>        

                                <div class="dashboard-bal"> 
                                  <div class="check-head-div"><h2>My Job Requests</h2></div>

                                  <div class="createAgig-right-main page-wt-bk my-invoice">
                                    <div class="createAgig-right-table">
                                        <table class="table table-hover table-bordered table-striped" id="job_list">
                                            
                                        </table>
                                    </div>
                                  </div>

                                </div>

                            </div>
                        </div>
                        
                 </div>
             </div>
         </div>
    </section> <!-- middle-section -->
	<script>


$(document).ready(function(){
    getJobList();
});

function getJobList(url){
    var path = (url)?url:"{{url('job-list')}}";
	$('#job_list').html('<center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></center>') 
        $.ajax({
            url:path ,
            data: {sorting:$('#sorting').val()},
            method: 'get',
            dataType: 'JSON',
            success:function(result)
            {
                $('#job_list').html(result.html);
            }
            
        });
    }


function deleteJob(id){
	bootbox.confirm({
			message: "Are you sure you want to delete?",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'No',
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				if(result){
				  $.ajax({
						url:"{{url('delete-job')}}"+"/"+id ,
						method: 'get',
						dataType: 'JSON',
						success:function(result)
						{
						  if(result.success){
								toastr.success(result.message);
								$('#job_'+id).remove();
							}
						}
					});
				}
			
			}
			});
	}


</script>
  @endsection