@extends('layouts.default')
@section('content')
<section class="middle-section">
         <!--<div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">Get a poster design</div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                          <li class="breadcrumb-item active" aria-current="page">Get a poster design</li>
                        </ol>
                    </div>
                  </div>
              </div>
             </div>
         </div>  -->
         <div class="main-page">
             <div class="alljob-requests-area">
                 <div class="container">
                    <div class="alljob-requests-inn">
                        
                        <div class="category-list-page">
                            <div class="row">
                              <div class="col-md-8">
                                <div class="recently-add-left">
                                  <div class="recently-add-inn wht-bcack">
                                    <div class="recently-head">{{$jobDetail->title}}</div>
                                    <div class="recently-image">
                                      <div class="recently-img-head">
                                        <div class="row">
                                          <?php 
                                          //echo "<pre>";
                                          //print_r($jobDetail);die;
                                          ?>
                                          <div class="col-md-6">
                                            <div class="rec-txt text-left"><a href="{{url('category'.'/'.$jobDetail->category->id)}}">{{ $jobDetail->category->name }}</a></div>
                                          </div>
                                          <div class="col-md-6">
                                            <div class="rec-txt text-right">Last modified: {{ date('F d, Y', strtotime($jobDetail->updated_at)) }}</div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="recently-img">
                                        <img src="{{ url('public/images/recently.png') }}" alt="">
                                      </div>
                                      <div class="recently-left-txt">
                                        <h2>DESCRIPTION</h2>
                                        <p>{{$jobDetail->description}}</p>
                                       
                                        <div class="rec-left-tag">
                                           @foreach($jobDetail->tags as $tag)
                                              <a href="{{url('category?tag='.$tag->name)}}"><span>{{ $tag->name }}</span></a>
                                           @endforeach
                                        </div>

                                      </div>
                                    </div>
                                  </div>
								  @if((Auth::user() && (Auth::user()->id != $jobDetail->created_by))||!Auth::user())
								   <div class="review-totle wht-bcack">
                                    <div class="review-items">
									<form id="offer_form">
									@csrf
										<div class="full form-group">
											&nbsp;
										</div>
										 <div class="full form-group">
											<label>SUBMIT YOUR OFFER</label>
											<p>Select the job you want to offer the buyer to fulfill their request. You may also post a new job to offer a related service for purchase.</p>
										</div>
                        
										<input type="hidden" name="request_id" class="input-item" value="{{$jobDetail->id}}">
										@if(!empty(Auth::user()))
										<div class="full form-group">
                                        <select class="form-control required Offer-id-row" id="service_id" required="service_id" name="service_id">
                                            <option value="">Choose a your service</option>
											@if(count($myServices)>0)
												@foreach($myServices as $service)
											     <option value="{{$service->id}}">{{$service->title}}</option>
												@endforeach
											@endif
                                        </select>
										</div>
										@endif
										<div class="full form-group job-detail-submit-btns">
										    @if(!empty(Auth::user()))
											<button type="button" onclick="submitOffer()" id="form_button" class="btn save-btn">
											  <i id="formLoader" class="fa fa-spinner fa-spin" style="display:none"></i>
											  Submit
											  </button>
											  <div class="plus-circle">
												<a class="postAjob btn save-btn" target="blank" href="{{url('post-service')}}"><i class="fa fa-plus"></i>Post a mJob
												</a>
											  </div>
											@else
											<p>Please <a href="javascript:void(0)" onclick="loginNow()" class="">Signin</a> to submit an offer.</p>
											@endif
										</div>
                                    </form>
                                    </div>
                                  </div>
                                @endif
                                </br>
                                  <div class="review-totle wht-bcack">
                                    <div class="review-head">
                                      <div class="check-head-div"><h2>OFFERS <small>({{count($jobDetail->offers)}} Total)</small></h2></div>
                                    </div>
                                    <div class="review-items">
                                      @if(count($jobDetail->offers))
                                      @foreach($jobDetail->offers as $offer)
                                        <div class="review-item">
                                          <div class="review-itm-top">
                                              <div class="review-user-img">
                                                <span>
                                                  <img src="{{getUserProfileImage($offer->requestedUser->id)}}" alt="">
                                                </span>
                                              </div>
                                              <div class="review-user-name">
                                                <h3>{{$offer->requestedUser->fullname}}</h3>
                                                <p class="offer-title">{{$offer->requestedUser->fullname}} used the service 
                                                  <a target="_blank" href="{{url('service/'.$offer->serviceDetail->id)}}" >
                                                  {{$offer->serviceDetail->title}}</a> to submit an offer.
                                                </p>
                                              </div>
                                          </div>
                                          @if(Auth::check()) 
                                          @if($jobDetail->created_by == $user_id)
<!-- Custom order model -->
<div class="modal fade in" id="customOrderModal_{{ $offer->id }}" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Custom Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body form-custom-order">
                <form class="et-form form-delivery-order customOrderModal ">
                    <h3 class="mjob-name">{{ $offer->serviceDetail->title }}</h3>
                    <div class="form-group text-field">
                        <label for="form_post_content">Description</label>
                        <textarea class="form-control" name="post_content" id="description_{{ $offer->id }}"></textarea>
                        <p id="from_description_{{ $offer->id }}" class="error">This field is required.</p>
                    </div>

                    <div class="form-group clearfix">
                        <div class="form-group">
                            <label for="form-budget">Budget (USD)</label>
                            <input type="number" min="1" name="budget" id="budget_{{ $offer->id }}" class="form-control">
                            <p id="from_budget_{{ $offer->id }}" class="error">This field is required.</p>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <div class="form-group time">
                            <label for="from_deadline">Time of delivery (Day)</label>
                            <input type="number" min="1" name="deadline" id="deadline_{{ $offer->id }}" class="form-control">
                            <p id="from_deadline_{{ $offer->id }}" class="error">This field is required.</p>
                        </div>
                    </div>

                    <div class="form-group clearfix">
                        <div class="attachment-file gallery_container" id="gallery_container_{{ $offer->id }}">
                            <div class="attachment-image">
                                <ul class="gallery-image carousel-list custom-order-image-list" id="image-list_{{ $offer->id }}">
                                </ul>
							    <div>
									<span class="plupload_buttons" id="custom-order_container_{{ $offer->id }}" style="position: relative;">
										<span class="img-gallery" id="custom-order_browse_button_{{ $offer->id }}" style="position: relative; z-index: 1;">
											<a href="#" class="add-img">Attach file <i class="fa fa-plus"></i></a>
										</span>
                                        <div>
                                            <input service-id="{{ $offer->service_id }}" id="images_{{ $offer->id }}" type="file" multiple="" accept="" class="select_files">
                                        </div>
                                    </span>
								</div>
                                </div>
                        </div>
                    </div>
                    <input type="hidden" id="service_id_{{ $offer->id }}" value="{{ $offer->service_id }}"/>
                    <input type="hidden" id="sent_to_{{ $offer->id }}" value="{{ $offer->requested_by }}"/>
                    <input type="hidden" id="job_request_title_{{ $offer->id }}" value="{{ $offer->serviceDetail->title }}"/>
                    <button type="button" id="order_btn_{{ $offer->id }}" class="send-custom-order waves-effect waves-light btn-diplomat btn btn-primary">Send <i style="display:none" class="loader fas fa-spinner fa-spin"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Custom order model -->
                                          <div class="review-itm-btn text-right">
                                            <ul>
                                                <li><a id="{{ $offer->id }}" class="btn custom_order_btn" href="#">Send custom order <i class="far fa-paper-plane"></i></a></li>
                                                <li>
                                                    <?php
                                                    $logged_in_id = Auth::user()->id;
                                                    $chat_room = checkChatRoom($logged_in_id, $offer->requested_by); ?>
                                                    <?php
                                                    if ($logged_in_id != $offer->requested_by) {
                                                    if($chat_room) { ?>
                                                        <a class="btn" href="{{ url('chat/'.$chat_room) }}">
                                                        Contact me<i class="far fa-paper-plane"></i>
                                                        </a>
                                                    <?php } else { ?>
                                                        <a id="{{ $offer->id }}" class="btn btn-contact-me" href="javascript:void(0)">
                                                        Contact me<i class="far fa-paper-plane"></i>
                                                        </a>
                                                    <?php } } ?>
                                                </li>

                                                <!-- Contact Me model -->
                                                <div class="modal fade in contact" id="contactMeModal_{{ $offer->id }}" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLongTitle">Contact Me</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body form-contact-me">
                                                                <form class="et-form form-delivery-order contactMeModal ">
                                                                    <div class="form-group text-left">
                                                                        <label for="form_post_content">Your Message</label>
                                                                        <textarea class="form-control" name="message" id="message_{{ $offer->id }}"></textarea>
                                                                        <p class="from_message_{{ $offer->id }} error">This field is required.</p>
                                                                    </div>
                                                                    
                                                                    <input type="hidden" id="chat_to_id_{{ $offer->id }}" value="{{ $offer->requested_by }}"/>
                                                                    <button type="button" class="send-contact-me waves-effect waves-light btn-diplomat btn btn-primary" id="{{ $offer->id }}">Send <i style="display:none" class="loader fas fa-spinner fa-spin"></i></button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Contact Me model -->
                                            </ul>
                                          </div>
                                        @endif
                                        @endif
                                        </div>

                                        @endforeach
                                        @else
                                      <div style="width:100%" class='alert alert-danger'><center>No record found</center></div>
                                      @endif
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="recently-add-right">
                                  <div class="recently-top-right wht-bcack">
                                    <div class="recently-right-top">
                                      <div class="row">
                                      <div class="col-md-6"></div>
                                        <div class="col-md-6">
                                          <div class="recently-price text-right">
                                            <small>$</small> {{number_format($jobDetail->budget,2)}}
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  
                                  <div class="category-side-bar">
                                      <div class="user-profile">
                                          <div class="user-img">
                                              <span><img src="{{getUserProfileImage($jobDetail->user->id)}}" alt=""></span>
                                          </div>
                                          <div class="user-name">
                                              <h3>{{$jobDetail->user->name}}</h3>
                                              <?php echo displayRating(getUserRating($jobDetail->user->id))?>
                                          </div>
                                      </div>
                                      <div class="user-dtl-div">
                                          <div class="user-dtl">
                                              <h2><i class="fas fa-map-marker-alt"></i> <span>From</span></h2>
                                              <p>{{($jobDetail->user->country)?$jobDetail->user->country->name:''}}</p>
                                          </div>
                                          <div class="user-dtl">
                                            <h2><i class="fas fa-globe-africa"></i> <span>Languages</span></h2>
                                              @if(count($jobDetail->user->languages)>0)
                                            @foreach($jobDetail->user->languages as $val)
                                            <p>{{$val->language->name}}</p>
                                            @endforeach
                                            @endif
                                          </div>
                                          <div class="user-dtl">
                                            <h2><i class="fas fa-info-circle"></i> <span>Bio</span></h2>
                                            <p>{!!$jobDetail->user->bio !!}</p>
                                          </div>
                                          <div class="user-cont-btn text-center">
                                            <ul>
                                             <li>
                                                <a class="btn" href="{{url('view-profile').'/'.base64_encode($jobDetail->user->id)}}">
                                                    <span>View my profile</span> <i class="fas fa-eye"></i>
                                                </a>
                                              </li>
                                            </ul>
                                          </div>
                                      </div>
                                   </div>
                                </div>
                              </div>
                            </div>
                        </div>
                         
                    </div>
                 </div>
             </div>
         </div>
    </section>
<script>

function submitOffer(){
    if($('#service_id').val()==''){
        toastr.error('Please select service.')
    }else{
        $('#form_button').prop('disabled',true);
        $('#formLoader').show();
        $.ajax({
        url:"{{url('submit-offer')}}",
        data:$('#offer_form').serialize(),
        method: 'post',
        dataType: 'JSON',
        success:function(result)
        {
            $('#formLoader').hide();
            $('#form_button').prop('disabled',false);
            if(result.success){
            toastr.success(result.message)
            window.location.reload();
            }else{
            toastr.error(result.message)	
            }
        }
        });
    }
}

//send contact me message
$(document).ready(function() {
    $(document).on('click', '.btn-contact-me', function() {
        var contact_id = $(this).attr('id');
        $('#contactMeModal_'+ contact_id).modal('show');
    });

    $(document).on('click', '.send-contact-me', function(e) {
        e.preventDefault();
        var contact_id = $(this).attr('id');
        var error = 0;
        var message = $('#message_'+contact_id).val().trim();
        var chat_to_id = $('#chat_to_id_'+contact_id).val();

        if(message == '') {
            $('.from_message_'+contact_id).show();
            error++;
        } else {
            $('.from_message_'+contact_id).hide();
        }

        if(error > 0) {
            return false;
        } else {
            $('.send-contact-me').prop('disabled',true);
            $('.send-contact-me .loader').show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                type: "POST",
                url: "{{url('send-contact-me')}}",
                data: 'message=' + message + '&chat_to_id=' + chat_to_id,
                dataType:'json',
                success: function(data) {
                    if (data.success) {
                        toastr.success(data.message);
                        $('#contactMeModal_'+ contact_id).modal('hide');
                        location.href = APP_URL + '/chat/' + data.chat_room_id;
                    } else {
                        toastr.error(data.message);
                        $('#contactMeModal_'+ contact_id).modal('hide');
                    }
                    $('.send-contact-me .loader').hide();
                    $('.send-contact-me').prop('disabled',false);
                },
                error: function(xhr, status, errorThrown) {
                    toastr.error("Sorry, there was a problem!");
                    $('.send-contact-me .loader').hide();
                    $('.send-contact-me').prop('disabled',false);
                }
            });
            return true;
        }
    });
});

//send custom order
$(document).ready(function()
{
    $(document).on('click', '.custom_order_btn', function()
    {
        var offer_id = $(this).attr('id');
        $('#customOrderModal_'+ offer_id).modal('show');
    });

    $(document).on('click', '.send-custom-order', function()
    {
        var offer_id = $(this).attr('id').replace('order_btn_', '');
        var error =0;
        var description = $('#description_'+offer_id).val().trim();
        var budget = $('#budget_'+offer_id).val();
        var deadline = $('#deadline_'+offer_id).val();
        var service_id = $('#service_id_'+offer_id).val();
        var sent_to = $('#sent_to_'+offer_id).val();
        var title = $('#job_request_title_'+offer_id).val();
        
        
        if(description == '') {
            $('#from_description_'+offer_id).text('This field is required');
            $('#from_description_'+offer_id).show();
            error++;
        } else {
            $('#from_description_'+offer_id).text('');
            $('#from_description_'+offer_id).hide();
        }

        if(budget == '') {
            $('#from_budget_'+offer_id).text('This field is required');
            $('#from_budget_'+offer_id).show();
            error++;
        } else if(parseInt(budget) < parseInt(1)) {
            $('#from_budget_'+offer_id).text('Value greater than or equal to 1');
            $('#from_budget_'+offer_id).show();
            error++;
        } else {
            $('#from_budget_'+offer_id).text('');
            $('#from_budget_'+offer_id).hide();
        }

        if(deadline == '') {
            $('#from_deadline_'+offer_id).text('This field is required');
            $('#from_deadline_'+offer_id).show();
            error++;
        } else if(parseInt(deadline) < parseInt(1)) {
            $('#from_deadline_'+offer_id).text('Value greater than or equal to 1');
            $('#from_deadline_'+offer_id).show();
            error++;
        } else {
            $('#from_deadline_'+offer_id).text('');
            $('#from_deadline_'+offer_id).hide();
        }

        if(error > 0) {
            return false;
        } else {
            $('.send-custom-order').prop('disabled',true);
            $('.send-custom-order .loader').show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                type: "POST",
                url: "{{url('send-custom-order')}}",
                data: 'title=' + title + '&description=' + description + '&budget=' + budget + '&deadline=' + deadline + '&service_id=' + service_id + '&sent_to=' + sent_to + '&images=' + JSON.stringify(gallery_images),
                dataType:'json',
                success: function(data) {
                    if (data.success) {
                        toastr.success(data.message);
                        $('#customOrderModal_'+offer_id).modal('hide');
                        location.href = APP_URL + '/chat/' + data.chat_room_id;
                    } else {
                        toastr.error(data.message);
                        $('#customOrderModal_'+offer_id).modal('hide');
                        //location.reload();
                    }
                    $('.send-custom-order .loader').hide();
                    $('.send-custom-order').prop('disabled',false);
                },
                error: function(xhr, status, errorThrown) {
                    toastr.error("Sorry, there was a problem!");
                    $('.send-custom-order .loader').hide();
                    $('.send-custom-order').prop('disabled',false);
                    //location.reload();
                }
            });
            return true;
        }
    });
});

// upload files
var gallery_images = [];
$(document).on('change', '.select_files', function(){
    var offer_id = $(this).attr('id').replace('images_', '');
    var service_id = $('#images_'+offer_id).attr('service-id');
    var form_data = new FormData();
    var err = 0;
    form_data.append('service_id', service_id);
    // Read selected files
    var totalfiles = document.getElementById('images_'+offer_id).files.length;
    for (var index = 0; index < totalfiles; index++) {
        var name = document.getElementById('images_'+offer_id).files[index].name;
        form_data.append('images[]', document.getElementById('images_'+offer_id).files[index]);

        var ext = name.split('.').pop().toLowerCase();
        if(jQuery.inArray(ext, ['gif','png','jpg','jpeg','docx','doc','pdf']) == -1) 
        {
            toastr.error("Invalid File Extension");
            err++;
        }

        var size = document.getElementById('images_'+offer_id).files[index].size;
        if(size > 20000000)
        {
            toastr.error("File Size should be less than 20 MB");
            err++;
        }
	
    }

    if (err > 0) {
        return false; 
    } else {
        // AJAX request
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            url: '{{ url("custom/file-upload") }}', 
            type: 'post',
            data: form_data,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (res) {
                if(res.success) {
					for(var i=0;i<res.data.length ; i++){
                        $('#image-list_'+offer_id).append("<li id="+ res.data[i].name +"><a href='#'><i class='fa fa-paperclip'></i> "+ res.data[i].name + "." + res.data[i].type +" <i class='del_img fa fa-times remove_photo_" + res.data[i].name + "' onclick='deleteImage("+res.data[i].name+","+"`"+res.data[i].type+"`"+","+ offer_id +")'></i><i id='login_loader_"+res.data[i].name +"' class='fa fa-spinner fa-spin' style='display:none'></i></li>");
                        gallery_images.push(res.data[i]);
                    }
						console.log(gallery_images);
                } else {
                    $.each(res.message, function(i, val) {
                        toastr.error(val)
                    });
                }
            },
            error: function(xhr, status, errorThrown) {
                toastr.error("Sorry, there was a problem!");
            }
        });
    }
});

// delete file
function deleteImage(name, type, offer_id) {
    var result = confirm("Are you sure to delete?");
    if(result){
        $('#login_loader_'+name).show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "GET",
            url: "{{ url('custom/delete-file') }}/" + name+"/"+type,
            dataType:'json',
            success: function(data) {
                $('#login_loader_'+name).hide();
                if (data.success) {
                    $('li#'+name).remove();
                    $('#images_'+offer_id).val('');
					console.log(gallery_images);
					const index = gallery_images.indexOf('"'+name+'"');
					if (index < 1) {
					  gallery_images.splice(index, 1);
					}

                    console.log(gallery_images);
                    toastr.success(data.message);
                } else {
                    toastr.error(data.message);
                }
            }
            
        });
    }
}
</script>
  @endsection