
<div class="top-section-pajob">
		<div class="row">
			<div class="col-md-6">
				<div class="top-section-head">
					<h2>Get freelance services for <br>
						as little as $5</h2>
				</div>
			</div>
			<div class="col-md-6">
				<div class="post-aJob-right">
					<div class="post-aJob text-right">
						<a class="btn" href="{{url('post-recruit')}}">
							<span>Post a Job Request</span> <i class="fas fa-plus"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="top-filter">
		<div class="row">
			<div class="col-md-6">
				<div class="filter-left">
					<p>Job Requests {{ $jobs->currentPage() }}-{{ $jobs->perPage() }} of {{ $jobs->total() }}</p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="filter-right text-right">
					<label>Sort by</label>
					<select name="" id="sorting" onchange="getJobList()">
						<option value="">SELECT</option>
						<option value="asc">Oldest</option>
						<option value="desc">Newest</option>
					</select>
				</div>
			</div>
		</div>
	</div>
<div class="alljob-requests-items">
@if(count($jobs)>0)
  @foreach ($jobs as $job)
		<div class="alljob-requests-item">
			<div class="alljob-req-main">
				<h2><a href="{{url('job-detail/'.base64_encode($job->id))}}">{{$job->title}}</a></h2>
				<p>Posted {{changeDateformat($job->created_at)}} | {{getofferCount($job->id)}} offers | ${{$job->budget}}</p>
				<p><span>{!!$job->description!!}</span></p>
			</div>
			<div class="alljob-req-tag">
				<label>Tags :</label>
				<ul>
				@if(!empty($job->tags))
					@foreach($job->tags as $tag)
					<li><a href="{{url('category?tag='.$tag->name)}}">{{$tag->name}}</a></li>
					@endforeach
				@endif
				</ul>
			</div>
		</div>
	@endforeach
	@else
		<div class='alert alert-danger mt-md-5 mt-3 w-90'><center>No record found</center></div>
	@endif
	</div>
  {{$jobs->links()}}
						 
<script>
$('.pagination li a').on('click', function(event) {
        event.preventDefault();
            var url = $(this).attr('href');
            getJobList(url);
});
</script>
  