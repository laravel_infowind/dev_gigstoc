@extends('layouts.default')
@section('content')


<section class="middle-section">
         <div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">Recruit now</div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                          <li class="breadcrumb-item active" aria-current="page">Recruit now</li>
                        </ol>
                    </div>
                  </div>
              </div>
             </div>
         </div>
         <div class="main-page">
             <div class="alljob-requests-area">
                 <div class="container">
                    <div class="recruit-now-page page-wt-back">
                         <div class="recruit-now-form">
                         <form method="post" action="{{url('post-recruit')}}" id="post_form" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                          {{ csrf_field() }}
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-input">
                                                <label for="">Job Request name</label>
                                                <input type="text" name="title" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-input">
                                                <label for="">Your budget (USD)</label>
                                                <input type="text" name="budget" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-input">
                                                <label for="">Time of delivery (Day)</label>
                                                <input type="text" name="deadline" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-input">
                                                <label for="">Category</label>
                                                <select class="form-control" name="category_id" id="parent_category" tabindex="-1" aria-hidden="true">
                                               </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-input">
                                                <label for="">Description</label>
                                                <textarea placeholder="Enter Description here" name="description" cols="120"  rows="10"  class="form-control" data-sample-short required=""></textarea>

                                            </div>
											 
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-input">
                                                <label for="">Tags</label>
                                                <select name="tag_id[]"  id="tags" class="selectpicker form-control" multiple="multiple">
                                                            @foreach($tags as $key => $val)
                                                                <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                            @endforeach
                                                </select>
                                            </div>
                                        </div>
										<div class="col-md-2">
                                              <div class="form-input">
                                             <button onClick="addTagModel()" type="button" id="add_tag" class="btn tag-btn">
											    <i  class="fa fa-plus"></i>
											    Add Tag
											</button>
                                              </div>
                                          </div>
                                    </div>
                                </div>
                             </form>
                             {!! JsValidator::formRequest('App\Http\Requests\FrontUser\AddPostRequest','#post_form') !!}
                         </div>
                         <div class="recruit-now-form-btn">
                             <ul>
                                 <li>
                                 <button type="button" id="page_form_button" class="btn btn-success">
                                <i id="formLoader" class="fa fa-spinner fa-spin" style="display:none"></i>
                                Save</button>
                                 <li><button onclick="discardRequest()" class="btn discard-btn">discard</button></li>
                             </ul>
                         </div>
                    </div>
                 </div>
             </div>
         </div>
    </section> <!-- middle-section -->
	
<div class="modal fade" id="addTag" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Add tag</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  <form id="add_tag" method="post" action="{{url('save-tag')}}">
	  @csrf
		  <div class="modal-body mx-3">
			<div class="md-form mb-5">
			  <label data-error="wrong" data-success="right" for="">Tag</label>
			  <input type="text" name="name" id="tag_name" class="form-control require">
			  <span></span>
			</div>
		  </div>
		  <div class="modal-footer d-flex justify-content-center">
			<button type="button" id="save_tag" class="btn tag-btn">Add</button>
		  </div>
	  </form>
	 
    </div>
  </div>
</div>


<script>

function addTagModel(){
	$('#addTag').modal('show');
}

$('#save_tag').on('click', function(){
	    var flag = 0;
		$(".require").each(function () {
			if ($.trim($(this).val()) == "") {
			$(this).next('span').addClass('alert-danger').css({"color": "#a94442", "display": "inline-block", "margin-top": "5px"}).html('The field is required.');
				flag++;
			}
		});
	if($("#add_tag").valid() && !flag) {
	$.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: "{{url('save-tag')}}",
            data: {name:$('#tag_name').val()},
            dataType:'json',
            success: function(response) {
               if (response.success) {
                    toastr.success(response.message);
					$('#addTag').modal('hide');
					$('#tags').append("<option value='"+response.data.id+"'>"+response.data.name+"</option>");
					$('#tags').selectpicker('refresh');
					$('#tag_name').val('');
					
                } else {
                    toastr.error(response.message);
                }
            }
        });
	}
});

$(".btn-success").on('click',function(e) {
    e.preventDefault();
    var formData = new FormData($('#post_form')[0]);
    //var editor_data = CKEDITOR.instances.editor1.getData();
    //formData.append('description',editor_data);
    
 /*if(!editor_data){
        $('#description_error').show();
     }else{
         console.log('out');
         $('#description_error').hide();
     }*/
  if ($("#post_form").valid()) {
        $('.btn-success').prop('disabled',true);
        $('#formLoader').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: "{{url('post-recruit')}}",
            contentType: false,
            processData: false,
            data: formData,
            dataType:'json',
            success: function(data) {
                if (data.success) {
                    toastr.success(data.message);
                    setTimeout(function(){
                        window.location = "{{url('all-job/request')}}";
                    },1000);
                } else {
                    toastr.error(data.message);
                }
                $('#formLoader').hide();
                $('.btn-success').prop('disabled',false);
            }
        });
    }
});

 function getParentCategory(){

    $.ajax({
        type: "get",
        url: "{{url('get-category')}}",
        dataType:'json',
        success: function(data)
        {
            if (data.success){
                $('#parent_category').html(data.list);
            }
        }
        });
  }
  
  function discardRequest(){
	  window.location = "{{url('/')}}"
  }
  
  $(document).ready(function(){
    getParentCategory();
  });
/*
    CKEDITOR.on('instanceCreated', function(e) {
        e.editor.on('contentDom', function() {
            e.editor.document.on('keyup', function(event) {
                $("#description_error").hide();
            });
        });
    });
*/
</script>
  @endsection