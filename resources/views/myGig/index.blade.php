@extends('layouts.default')
@section('content')
 <section class="middle-section">
         <div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">My Gigs</div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                          <li class="breadcrumb-item active" aria-current="page">My Gigs</li>
                        </ol>
                    </div>
                  </div>
              </div>
             </div>
         </div>
         <div class="main-page">
             <div class="createAgig-area">
                 <div class="container">
                     
                        <div class="row">
                            <div class="col-md-3">
                                <div class="createAgig-left-menu">
                                     @include('common.side-menu')
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="createAgig-right-main page-wt-bk" id="gig_list">
                                    
                                    
                                </div>
                            </div>
                        </div>
                        
                 </div>
             </div>
         </div>
    </section> <!-- middle-section -->
	<script>
	$(document).ready(function(){
		getGigList();
	});
	
	function getGigList(url){
		var path = (url)?url:"{{url('gig-list')}}";
		$('#gig_list').html('<center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></center>') 
		$.ajax({
			url:path,
			data: {},
			method: 'get',
			dataType: 'JSON',
			success:function(result)
			{
				$('#gig_list').html(result.html);
			}
			
		});
	}
	
	</script>
  @endsection