@extends('layouts.default')

@section('content')
<section class="middle-section">
    <div class="banner-section">
        <div class="banner-slider owl-carousel">
            @if (isset($slider) && !empty($slider))
            @foreach ($slider as $key => $value)
            <div class="item">
                <div class="banner-content">
                    <div class="banner-img">
                        <?php if ($value->mime_type == 'image/jpg' || $value->mime_type == 'image/jpeg' || $value->mime_type == 'image/png') {?>
                                <img src="{{ asset('public/images/slider/').'/'.$value->slider_img }}" alt="" />
                            <?php } elseif ($value->mime_type == 'video/mp4') {?>
                                <video width="1349" loop autoplay muted id="vid" preload="none">
                                    <source src="{{ asset('public/images/slider/').'/'.$value->slider_img }}" type="video/mp4">
                                </video>
                            <?php } else { ?>
                                <img src="{{ asset('public/images/slider/').'/'.$value->slider_img }}" alt="" />
                            <?php } ?>
                        <!-- <img src="{{ asset('public/images/slider/'.$value->slider_img) }}" alt=""> -->
                    </div>
                    <div class="banner-cont">
                        <div class="container">
                            <div class="banner-cont-in">
                                <h2>{!! $value->title !!}
                                </h2>
                                <p>{!! $value->description !!}
                                </p>
                                <div class="banner-secrch">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text pink" id="basic-text1">
                                            <i class="fas fa-search"></i>
                                            </span>
                                        </div>
										<form  method="get" action="{{url('category')}}">
												<input class="form-control" name="search" type="text" placeholder="Find Professionals & Agencies" aria-label="Search" />
												<button type="submit" class="btn"><div class="search-title"><span class="text-search">Search now</span></div></button>
										</form>
										
								   </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
    <div class="best-sellers-section section-div">
        <div class="container">
            <div class="best-sellers-inn">
                <div class="section-head">
                    <h2>Best sellers</h2>
                    <p>{!! $data['best_sellers']->sub_heading !!}</p>
                </div>
                <div class="best-sellers-slider">
                    <div class="owl-carousel sellers-slider">
						@foreach($mostSalesServices as $value)
                        <div class="item">
                            <div class="sellers-item">
                                <div class="sellers-img">
								
                                    @if ($value->service->is_video)
									 {!! getVideo($value->service->video_link) !!}
									 @else(!empty($value->service->images))
									 @foreach($value->service->images as $key => $val)
									 @if($val->is_featured == 1)
									  <a href="{{url('service/'.$value->service->id)}}"><img src="{{ url('public/images/services/thumbnail/'.$val->thumbnail) }}" alt=""></a>
									 @endif
									 @endforeach
									 @endif
                                </div>
                                <div class="sellers-content">
                                    <div class="sellers-title">
                                       <a href="{{url('service/'.$value->service->id)}}">{{$value->service->title}}</a>
                                    </div>
                                    <div class="sellers-cont">
                                        <table class="table">
                                            <tr>
                                                <td class="admin-color"><b>{{$value->service->user->name}}</b></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Price</td>
                                                <td><b>${{$value->service->price}}</b></td>
                                            </tr>
                                            <tr>
                                                <td>Views</td>
                                                <td class="view-icn"><b>{{getViewCount($value->service->id)}} <i class="far fa-eye"></i></b></td>
                                            </tr>
                                            <tr>
                                                <td>Rating</td>
                                                <td>
                                                    <?php echo displayRating(getServicerating($value->service->id))?>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="sellers-btn">
                                        <a href="{{url('service/'.$value->service->id)}}">View Detail <i class="fas fa-chevron-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
						@endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="freelancer-section section-div">
        <div class="container">
            <div class="freelancer-section-inn">
                <div class="section-head">
                    <h2>Find the Perfect Freelancer for any Job</h2>
                    <p>{!! $data['find_the_perfect_freelancer_for_any_job']->sub_heading !!}</p>
                </div>
                <div class="freelancer-cont">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="freelancer-text">
                                <h2>{!! $data['find_the_perfect_freelancer_for_any_job']->heading !!}</h2>
                                <p>{!! $data['find_the_perfect_freelancer_for_any_job']->description !!}
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="freelancer-img"><img src="{{ asset('public/images/'.$data['find_the_perfect_freelancer_for_any_job']->image) }}" alt=""></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (isset($category) && !empty($category))
    <div class="categories-section section-div">
        <div class="container">
            <div class="categories-section-inn">
                <div class="section-head">
                    <h2>Categories</h2>
                    <p>{!! $data['categories']->sub_heading !!}</p>
                </div>
                <div class="categories-cont">
                    <ul>
                    @foreach ($category as $value)
                         @if ($value->is_featured == 1)
                        <li>
                            <a href="{{ url('category/') }}/{{ $value->id }}">
                            <div class="categories-item">
                                <div class="categories-img"><img src="{{ asset('public/images/category/'.$value->cat_image) }}" alt=""></div>
                                <div class="categories-title">{{ ucwords($value->name) }}</div>
                            </div>
                            </a>
                        </li>
                        @endif
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @endif

    <div class="recently-added-section section-div">
        <div class="recently-added-head">
            <div class="container">
                <div class="section-head">
                    <h2>Recently Added</h2>
                    <p>{!! $data['recently_added']->sub_heading !!}</p>
                </div>
            </div>
        </div>
        <div class="recently-added-cont">
            <div class="container">
                <div class="row">
				
				@if(count($recentAddedServices)>0)
				@foreach($recentAddedServices as $service)
                    <div class="col-md-3">
                        <div class="sellers-item">
                            <div class="sellers-img">
                               @if ($service->is_video)
							 {!! getVideo($service->video_link) !!}
							 @else(!empty($service->images))
							 @foreach($service->images as $key => $value)
							 @if($value->is_featured == 1)
							  <a href="{{url('service/'.$service->id)}}"><img src="{{ url('public/images/services/thumbnail/'.$value->thumbnail) }}" alt=""></a>
							 @endif
							 @endforeach
							 @endif
                            </div>
                            <div class="sellers-content">
                                <div class="sellers-title">
                                   <a href="{{url('service/'.$service->id)}}">{{$service->title}}</a>
                                </div>
                                <div class="sellers-cont">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td class="admin-color"><b>{{$service->user->name}}</b></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Price</td>
                                                <td><b>${{$service->price}}</b></td>
                                            </tr>
                                            <tr>
                                                <td>Views</td>
                                                <td class="view-icn"><b>{{getViewCount($service->id)}} <i class="far fa-eye"></i></b></td>
                                            </tr>
                                            <tr>
                                                <td>Rating</td>
                                                <td>
                                                    <?php echo displayRating(getServicerating($service->id))?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="sellers-btn">
                                    <a href="{{url('service/'.$service->id)}}">View Detail <i class="fas fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
					@endforeach
					@endif
                    
                   
                </div>
                <div class="view-jobs-btn text-center"><a class="btn" href="{{url('category')}}"><span>View all jobs</span> <i class="fas fa-chevron-right"></i></a></div>
            </div>
        </div>
    </div>
    <div class="features-gigstoc-section section-div">
        <div class="container">
            <div class="features-gigstoc-inn">
                <div class="section-head">
                    <h2>Key Features of Gigstoc</h2>
                    <p>{!! $data['key_features_of_gigstoc']->sub_heading !!}</p>
                </div>
                <div class="features-gigstoc-items">
                    <div class="row">
                        @if(!empty($data['key_features_of_gigstoc']->keyFeatures))
                        @foreach($data['key_features_of_gigstoc']->keyFeatures as $value)
                        <div class="col-md-4">
                            <div class="features-gigstoc-item">
                                <div class="features-gi-iocn">
                                    <div class="features-icn-in">
                                        <span><img src="{{ asset('public/images/'.$value->image) }}" alt=""></span>
                                    </div>
                                </div>
                                <div class="features-gi-cont">
                                    <h2>{{ $value->heading }}</h2>
                                    <p>{!! $value->description !!}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="effective-work-section section-div">
        <div class="container">
            <div class="effective-work-inn">
                <div class="section-head">
                    <h2>Finding Cost Effective Work Has Never Been Easier</h2>
                </div>
                <div class="effective-work-items">
                    <div class="row">
                        @if(!empty($data['finding_cost_effective_work_has_never_been_easier']))
                        @foreach($data['finding_cost_effective_work_has_never_been_easier']->findingCosts as $value)
					    <div class="col-md-4">
                            <div class="features-gigstoc-item">
                                <div class="features-gi-iocn">
                                    <div class="features-icn-in">
                                        <span><img src="{{ asset('public/images/'.$value->image) }}" alt=""></span>
                                    </div>
                                </div>
                                <div class="features-gi-cont">
                                    <h2>{{ $value->heading }}</h2>
                                    <p>{!! $value->description !!}</p>
                                    <div class="features-num">
                                        <span>01</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @if (isset($job_request) && !empty($job_request))
    <div class="projects-section section-div">
        <div class="container">
            <div class="projects-section-inn">
                <div class="section-head">
                    <h2>Projects</h2>
                    <p>{!! $data['projects']->sub_heading !!}</p>
                </div>
                <div class="projects-items-div">
                    <div class="projects-table">
                        <table class="table table-striped">
                            <tbody>
                                @foreach($job_request as $single => $value)
                                <tr>
                                    <td>{{ $value->title }}</td>
                                    <td>{{ date('F d, Y', strtotime($value->created_at)) }}</td>
                                    <td>${{ number_format($value->budget, 2) }}</td>
                                    <td>
                                        <div class="view-detail-btn text-right"><a class="btn" href="{{url('job-detail/'.base64_encode($value->id))}}">View Detail</a></div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="projects-table-btns">
                        <ul>
                            <li><a class="btn" href="{{url('all-job/request')}}"><span>Browse all Job Requests</span> <i class="fas fa-chevron-right"></i></a></li>
                            <li><a class="btn" href="{{url('post-recruit')}}"><span>Post a Job Request</span> <i class="fas fa-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    <div class="testimonials-section section-div">
        <div class="container">
            <div class="testimonials-section-inn">
                <div class="section-head">
                    <h2>Testimonials</h2>
                    <p>{!! $data['testimonials']->sub_heading !!}</p>
                </div>
            </div>
            <div class="testimonials-slider-section">
                <div class="owl-carousel testimonials-slider">
                @if(count($testimonials)>0)
                @foreach($testimonials as $testimonial)
                    <div class="item">
                        <div class="testimonials-item">
                            <div class="testimonials-icn"><span><img src="{{ asset('public/images/ar-icn.png') }}" alt=""></span></div>
                            <div class="testimonials-rat">
                            <?php 
                                    echo displayRating($testimonial->rating);
                            ?>
                            </div>
                            <div class="testimonials-cont">
                                <p>{{$testimonial->comment}}</p>
                            </div>
                            <div class="testimonials-user">
                                <h2>{{$testimonial->author}}</h2>
                                <div class="testimonials-user-img">
                                    <?php 
                                    $url = url('public/images/testimonial/'.$testimonial->image);
                                    ?>
                                    <span><img src="{{$url}}" alt=""></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
                </div>
            </div>
        </div>
    </div>

    <div class="get-join-section">
        <div class="container">
            <div class="get-join-area">
                @if (Auth::check())
                    <h2>Thank You for Choosing Us</h2>
                    <p>Lets Get Started</p>
                    <div class="join-btn">
                        <span><a class="go_to_dashboard" href="{{ url('dashboard') }}">Go To Dashboard</a></span>
                    </div>
                @else
                    <h2>Get it done Faster, Better, and Cheaper</h2>
                    <p>Find the perfect Microjob or Freelance Service for your Business</p>
                    <div class="join-btn">
                        <span><a href="{{ url('signup') }}">Join  now</a></span>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>
<script>

$(document).ready(function(){
    $('.owl-nav').css({'display':'none'});
});

</script>
<!-- middle-section -->
@endsection
