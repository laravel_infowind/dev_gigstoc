@extends('layouts.default')
@section('content')

<section class="middle-section">
         <div class="page-title">
             <div class="container">
                 <div class="row">
                     <div class="col-md-6">
                        <div class="page-title-head">Contact us</div>
                     </div>
                     <div class="col-md-6">
                        <div class="page-breadcrumb-head">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Contact us</li>
                            </ol>
                        </div>
                     </div>
                 </div>
             </div>
         </div>
         <div class="main-page">
             <div class="sign-area">
                 <div class="container">
                    <div class="sign-area-inn">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="sign-form-area">
                                    <div class="sign-head">
                                        <h2>Request A Quote</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing</p>
                                    </div>
                                    <div class="sign-form">
                                        <form method="post" action="{{url('contact-us')}}" id="contact_form">
                                           @csrf
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label for="">Full name<span>*</span></label>
                                                        <div class="input-group">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="far fa-user"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" name="name" class="form-control input_user" value="" placeholder="Full name*">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label for="">Email address<span>*</span></label>
                                                        <div class="input-group">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="far fa-envelope"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" name="email_id" class="form-control input_user" value="" placeholder="Email address*">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label for="">Phone number<span>*</span></label>
                                                        <div class="input-group">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="fas fa-mobile-alt"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" name="phone_no" class="form-control input_user" value="" placeholder="Phone number*">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label for="">Message<span></span></label>
                                                        <div class="input-group">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="far fa-comment-alt"></i>
                                                                </span>
                                                            </div>
                                                            <textarea name="message" class="form-control input_user" value="" placeholder="Please enter your message here"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button type="submit" name="button" class="btn login_btn contact_btn">Submit
                                                        <i id="form_loader" style="display:none" class="fas fa-spinner fa-spin"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        {!! JsValidator::formRequest('App\Http\Requests\FrontUser\ContactUsRequest','#contact_form') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="sign-image">
                                    <img src="{{url('public/images/cont-img.png')}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sign-area-inn">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="sign-form-area">
                                    <div class="sign-head">
                                        <h2>Let’s get in touch</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing</p>
                                    </div>
                                    <div class="sign-lets-form">
                                        <ul>
                                            <li>
                                                <div class="form-lets-icn"><span><i class="fas fa-map-marker-alt"></i></span></div>
                                                <div class="form-lets-text">
                                                    <h3>ADDRESS</h3>
                                                    <p>{{$data['contact_address']}}</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-lets-icn"><span><i class="fas fa-mobile-alt"></i></span></div>
                                                <div class="form-lets-text">
                                                    <h3>PHONE NUMBER</h3>
                                                    <p>{{$data['contact_no']}}</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-lets-icn"><span><i class="far fa-envelope"></i></span></div>
                                                <div class="form-lets-text">
                                                    <h3>EMAIL ADDRESS</h3>
                                                    <p>{{$data['contact_email']}}</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="sign-map">
                                <?php echo getSettings('contact_address_frame');?>
                                    <!-- <img src="{{url('public/images/map.png')}}" alt=""> -->
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
             </div>
         </div>
    </section>
    <script>

$('#contact_form .contact_btn').on('click',function(e){
    e.preventDefault();
    if($('#contact_form').valid()){
        $('#form_loader').show();
        $('.contact_btn').prop('disabled',true);
        $.ajax({
            url: "{{url('contact-us')}}",
            data: $('#contact_form').serialize(),
            method: 'post',
            dataType: 'JSON',
            success:function(result)
            {
                if(result.success){
                    toastr.success(result.message);
                    $('#contact_form')[0].reset();
                }else{
                    toastr.error(result.message);
                }
                $('#form_loader').hide();
                $('.contact_btn').prop('disabled',false);
            }
            
        });
    }
});


</script>
@endsection