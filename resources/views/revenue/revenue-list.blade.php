							<table class="table table-hover table-bordered table-striped">
                                     <tr>
                                        <th>Payment method</th>
                                        <th>Date</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                      </tr> 
									@if (count($revenueList)>0)
                                    @foreach ($revenueList as $value)
                                      <tr>
                                        <td>
										{{ucfirst($value->selected_method)}}
										</td>
                                        <td>{{changeDateformat($value->created_at)}}</td>
                                        <td>${{number_format($value->amount,2)}}</td>
                                        <td>
										<span class="btn btn-success">Success</span>
										</td>
                                      </tr>

									@endforeach
                                    @else
										<tr>
                                          <td colspan="6"><a href="#"><div style="width:785px" class='alert alert-danger'><center>No record found</center></div></a></td>
                                        </tr>
									@endif
									</table>
                                   {{$revenueList->links()}}
                             
<script>
$('.pagination li a').on('click', function(event) {
        event.preventDefault();
            var url = $(this).attr('href');
            getRevenueList(url);
});
</script>