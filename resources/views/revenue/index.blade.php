@extends('layouts.default')
@section('content')
<section class="middle-section">
         <div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">Revenues</div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="index.html">Home</a></li> 
                          <li class="breadcrumb-item active" aria-current="page">Revenues</li>
                        </ol>
                    </div>
                  </div>
              </div>
             </div>
         </div> 
         <div class="main-page">
             <div class="createAgig-area">
                 <div class="container">
                     
                        <div class="row">
                            <div class="col-md-3">
                                <div class="createAgig-left-menu">
                                    @include('common.side-menu')
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="dashboard-bal">
                                  <div class="check-head-div"><h2>Balance</h2></div>
                                  <div class="dashboard-box-div">
                                      <div class="row">
                                          <div class="col-md-4">
                                              <div class="dashboard-div-item">
                                                  <h4>WORKING</h4>
                                                  <h2>$ 0.00</h2>
                                              </div>
                                          </div>
                                          <div class="col-md-4">
                                              <div class="dashboard-div-item">
                                                  <h4>AVAILABLE</h4>
                                                  <h2>$ {{getAvailableAmount(Auth::user()->id)}}</h2>
                                                  <!-- <p>Earning + Top-Up</p> -->
                                              </div>
                                          </div>
                                          <div class="col-md-4">
                                              <div class="dashboard-div-item">
                                                  <h4>PENDING</h4>
                                                  <h2>$ {{getPendingAmount(Auth::user()->id)}}</h2>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="dashboard-box-bottom">
                                      <div class="row">
                                          <div class="col-md-6">
                                              <div class="dashb-btm-txt text-left">Total Balance : <span>${{getExistingBlanace(Auth::user()->id)}}</span></div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="dashb-btm-txt text-right"><a href="{{url('withdrawal-balance')}}">Withdrawing Balance</a></div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                                <div class="dashboard-bal"> 
                                  <div class="check-head-div"><h2>Balance</h2></div>
                                    <div class="createAgig-right-main page-wt-bk my-invoice">
                                        <div class="dash-prod-main-table" id="invoice-list">
                                        
                                        </div>
                                  </div>
                                </div>

                            </div>
                        </div>
                        
                 </div>
             </div>
         </div>
    </section>
<script>

$(document).ready(function(){
    getRevenueList();
});



function getRevenueList(url,type,sort){
	
    var path = (url)?url:"{{url('revenues-list')}}";
	$('#invoice-list').html('<center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></center>') 
        $.ajax({
            url:path ,
			data:{payment_type:$('#payment_type').val(),status:$('#status').val(),type:type,sort:sort},
            method: 'get',
            dataType: 'JSON',
            success:function(result)
            {
                $('#invoice-list').html(result.html);
            }
            
        });
    }


</script>
<!-- middle-section -->
@endsection