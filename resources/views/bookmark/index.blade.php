@extends('layouts.default')
@section('content')
 <section class="middle-section">
         <div class="page-title">
             <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <div class="page-title-head">my bookmarks</div>
                  </div>
                  <div class="col-md-6">
                    <div class="page-breadcrumb-head">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li> 
                          <li class="breadcrumb-item active" aria-current="page">my bookmarks</li>
                        </ol>
                    </div>
                  </div>
              </div>
             </div>
         </div> 
         <div class="main-page">
             <div class="createAgig-area">
                 <div class="container">
                     
                        <div class="row">
                            <div class="col-md-3">
                                <div class="createAgig-left-menu" id="side_link">
                                    @include('common.side-menu')
                                </div>
								<center style="display:none" id="side_loader"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></center>
                            </div>
                            <div class="col-md-9">
                                <div class="createAgig-right-main page-wt-bk" id="bookmark_list">
                                   
                                       
                                </div>
                            </div>
                        </div>
                        
                 </div>
             </div>
         </div>
    </section> <!-- middle-section -->

<script>

$(document).ready(function(){
    getBookMarkListList();
});



function getBookMarkListList(url){
    var path = (url)?url:"{{url('bookmark-list')}}";
	$('#bookmark_list').html('<center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></center>') 
        $.ajax({
            url:path ,
            data: $('#contact_form').serialize(),
            method: 'get',
            dataType: 'JSON',
            success:function(result)
            {
                $('#bookmark_list').html(result.html);
            }
            
        });
    }
	
	
function addRemoveBookmark(id){
	
	bootbox.confirm({                      
		message: "Are you sure you want to remove from bokmark?",
		buttons: {
			confirm: {
				label: 'Yes',
				className: 'btn-success'
			},
			cancel: {
				label: 'No',
				className: 'btn-danger'
			}
		},
		callback: function (result) {
			if(result){
				 $.ajax({
					url: "{{url('remove-bookmark')}}",
					data: {id:id},
					type: 'get',
					dataType: 'JSON',
					success: function (response) {
						$('#remove_'+id).hide();
						toastr.success('Service removed from bookmark.');
					}
				});
			}
		
		}
	});
}

</script>
<!-- middle-section -->
@endsection