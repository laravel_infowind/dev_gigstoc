 <div class="category-main-pro">
 <ul>
@if (count($bookmarks)>0)
@foreach ($bookmarks as $bookmark)
   
	    <li id="remove_{{$bookmark->id}}">
			<div class="sellers-item">
				<div class="sellers-img">
				  @if(!empty($bookmark->service->images))
				 @foreach($bookmark->service->images as $key => $value)
				 @if($value->is_featured == 1)
				  <a href="{{url('service/'.$value->service_id)}}"><img src="{{ url('public/images/services/thumbnail/'.$value->thumbnail) }}" alt=""></a>
				 @endif
				 @endforeach
				 @endif
				</div>
				<div class="sellers-content">
				  <div class="sellers-title">
					<a href="#">{{($bookmark->service)?$bookmark->service->title:'-'}}</a>
				  </div>
				  <div class="sellers-cont">
					<div class="m-jobs">
					 <a href="javascript:void(0)" onclick="addRemoveBookmark('{{$bookmark->id}}')" class="ac"><i class="far fa-trash-alt"></i></a>
                   </div>
				  </div> 
				</div>
			  </div>
		</li> 

@endforeach

</ul>
@else
	<div style="width:100%" class='alert alert-danger'><center>No record found</center></div>

@endif
</div>


{{$bookmarks->links()}}

<script>
$('.pagination li a').on('click', function(event) {
        event.preventDefault();
            var url = $(this).attr('href');
            getBookMarkListList(url);
});
</script>