<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;

class Common extends Model
{
    public function getAllData($table, $select = '*', $where = [])
    {
    	$result = DB::table($table)->select($select)->where($where)->get();
    	return $result;
    }

    public function getSingleRow($table, $select = '*', $where = [])
    {
    	$result = DB::table($table)->select($select)->where($where)->first();

        if (count((array)$result) == 0) {
            return false;
        }
        return $result;
    }

    public function storeData($table, $input)
    {
    	$last_insert_id = DB::table($table)->insertGetId($input);
    	return $last_insert_id;
    }

    public function deleteData($table, $where = [])
    {
    	$result = DB::table($table)->where($where)->delete();
        return $result;
    }

    public function updateData($table, $input, $where = [])
    {
    	$result = DB::table($table)->where($where)->update($input);
    	return $result;
    }

    public function checkExistData($table, $where = [])
    {
        $count = DB::table($table)->where($where)->count();
        return $count;
    }
    
    public function getRoles($user_id)
    {
    	$result = DB::table('user_in_roles as uir')
            ->select('r.name')
            ->join('roles as r', 'r.id', '=', 'uir.role_id')
            ->where('uir.user_id', '=', $user_id)
            ->where('uir.role_id', '!=', '1')
            ->get()
            ->toArray();

        $roles = array();
        foreach ($result as $key => $value) {
        	$roles[] = $value->name;
        }

    	return $roles;
    }

    //Get all data with pagination
    public function allPaginateData($table, $where, $per_page = 10)
    {
        $result = DB::table($table)->where($where)->orderBy('id', 'desc')->paginate($per_page);

        return $result;
    }

    //Get all active users
    public function getAllUser($where=[])
    {
        $result = DB::table('users')
            ->select('users.first_name', 'users.last_name', 'users.id')
            ->join('user_in_roles as role', 'role.user_id', '=', 'users.id')
            ->where('users.is_active', '=', '1')
            ->where('users.is_verified', '=', '1')
            ->where('role.role_id', '!=', '1')
            ->where('role.role_id', '!=', '2')
            ->distinct()
            ->get();

        return $result;
    }



    //get post blog paginated data
    public function allBlogPostData($table, $where = [], $column, $order,$perpage=10)
    {
        $result = DB::table($table)->where($where)->orderBy($column,$order)->paginate($perpage);
        return $result;
    }

    //get next blog for front end
    public function nextSingleBlogData($table, $blog_id)
    {
        $result = DB::table($table)->where('id', '<', $blog_id)->orderBy('id','desc')->first();
        return $result;
    }

    //get previous blog for front end
    public function prevSingleBlogData($table, $blog_id)
    {
        $result = DB::table($table)->where('id', '>', $blog_id)->orderBy('id','asc')->first();
        return $result;
    }

    //get count / total from db
    public function getTotalCount($table,$where = [])
    {
        $result = DB::table($table)->where($where)->count();
        return $result;
    }

    //get total from users table excluded admin
    public function getTotalCountAcceptAdmin()
    {
        $result = DB::table('users')
            ->select('users.id','users.email')
            ->join('user_in_roles as role', 'users.id', '=', 'role.user_id')
            ->where('role.role_id', '!=', '1')
            ->where('users.is_deleted', '=', '0')
            ->where('users.is_verified', '=', '1')
            //->distinct()
            ->groupBy('users.id')
            ->orderBy('users.id')
            ->get();

        return count($result);
    }


    // fetch tutor data from user and become tutor table
    public function getTutorData($user_id)
    {
        $result = DB::table('users as u')
            ->select('u.first_name', 'u.last_name', 'u.id', 'u.email', 'u.phone', 'u.is_social','btutor.education_level', 'btutor.specialization', 'btutor.about', 'btutor.teaching_experience', 'u.profile_pic as profile')
            ->leftjoin('become_tutor as btutor', 'btutor.user_id', '=', 'u.id')
            ->where('u.id', '=', $user_id)
            ->first();   
        return $result;
    }
 

    //get Student & Tutor only
    public function getStudentAndTutor()
    {
        //DB::enableQueryLog();
        $result = DB::table('users')
            ->select('users.first_name', 'users.last_name', 'users.id')
            ->join('user_in_roles as role', 'role.user_id', '=', 'users.id')
            ->where('users.is_active', '=', '1')
            ->where('users.is_verified', '=', '1')
            ->where('users.is_deleted', '=', '0')
            ->where('role.role_id', '!=', '1')
            ->where('role.role_id', '!=', '2')
            //->distinct()
            ->groupBy('users.id')
            ->orderBy('users.first_name')
            ->get();
        //print_r(DB::getQueryLog());die();

        return $result;
    }

    public function getLimitData($table, $where, $limit, $last_id=0, $data=0, $select='*')
    {
        $query = DB::table($table)->select($select)->where($where);

        if($last_id != 0) {
            $query = $query->where('id', '<', $last_id);
        }

        if($data != 0) {
            $query = $query->where('end_date', '>', $data);
        }

        $query = $query->orderBy('id', 'DESC')->limit($limit)->get();

        return $query;
    }   

    public function getOrData($table, $where = [], $orwhere =[])
    {
        $query = DB::table($table)
                ->where($where)
                ->orWhere($orwhere)
                ->orderBy('id','DESC')->get();

        return $query;
    }
}
