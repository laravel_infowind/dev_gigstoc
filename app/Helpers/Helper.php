<?php

use App\Models\AdminEarning;
use App\Models\Category;
use App\Models\ChatMessage;
use App\Models\ChatRoom;
use App\Models\JobRequest;
use App\Models\Notification;
use App\Models\OffersOnJob;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\SellerRating;
use App\Models\Service;
use App\Models\ServiceRating;
use App\Models\WithdrawalRequest;
use App\User;
use Illuminate\Support\Facades\Mail;
//use DB;

function getUserProfileImage($userId)
{
    $user = User::where(['id' => $userId])->first();
    if (strpos($user['profile_pic'], 'http') !== false) {
        return $user['profile_pic'];
    }
    $url = url('public/images/profile_pic/' . $user['profile_pic']);
    if (file_exists(public_path() . '/images/profile_pic/' . $user['profile_pic']) && !empty($user['profile_pic'])) {
        $url = url('public/images/profile_pic/' . $user['profile_pic']);
    } else {
        $url = url('public/images/avatar-default.jpg');
    }
    return $url;
}

function getSettings($meta_key)
{
    $setting = App\Models\ConfigSetting::select('meta_value')->where('meta_key', '=', $meta_key)->first();

    return (isset($setting->meta_value) && !empty($setting->meta_value)) ? $setting->meta_value : 0;
}

function getParentCategoryName($id)
{
    $parentcategoryName = '-';
    if ($id) {
        $parent = Category::where(['id' => $id])->first();
        $parentcategoryName = ($parent) ? $parent->name : '-';
    }
    return $parentcategoryName;
}
function getCategoryImage($id)
{
    $cate = App\Models\Category::where(['id' => $id])->first();

    $url = url('public/images/category/' . $cate['cat_image']);
    if (file_exists(public_path() . '/images/category/' . $cate['cat_image']) && !empty($cate['cat_image'])) {
        $url = url('public/images/category/' . $cate['cat_image']);
    } else {
        $url = url('public/images/avatar-default.jpg');
    }
    return $url;
}

// get user preferred method
function getUserPrefereMethod($userId)
{

    $method = DB::table('user_payment_methods')
        ->where('user_id', '=', $userId)->where('preferred_method', '=', 1)
        ->first();
    if (!empty($method)) {
        return ($method->payment_type == 1) ? 'Bank' : 'Paypal';
    } else {
        return '-';
    }
}

function getTransferringAmount($request)
{
    $stripeAmountSum = 0;
    $paypalAmountSum = 0;
    $data = [];
    $customers = [];
    $query = "SELECT t1.*
    FROM user_wallets t1
    WHERE t1.id = (SELECT MAX(t2.id)
               FROM user_wallets t2
               WHERE t2.user_id = t1.user_id) and is_withdrawal =0";
    if ($request['type'] == 'all') {
        $customers = DB::select($query);
    } elseif (isset($request['ids']) && count($request['ids']) > 0) {
        $userIds = implode(',', $request['ids']);
        $customers = DB::select($query . " and t1.user_id in ($userIds)");
    }

    foreach ($customers as $val) {
        $val = (array) $val;
        if (getUserPrefereMethod($val['user_id']) == 'Paypal') {
            $paypalAmountSum = $paypalAmountSum + $val['current_balance'];
        }
    }
    $data['paypal_amount'] = $paypalAmountSum;

    return $data;
}

function getPayPalTransferringAmount($request)
{
    $userIds = $request['ids'];
    $paypalAmountSum = 0;
    if (!empty($userIds) && count($userIds) > 0) {
        $withdrawRequests = WithdrawalRequest::whereIn('user_id', $userIds)->where(['selected_method' => 'paypal', 'status' => 'pending'])->get();
        foreach ($withdrawRequests as $val) {
            $paypalAmountSum = $paypalAmountSum + $val['amount'];
        }
    }
    $data['paypal_amount'] = $paypalAmountSum;
    return $data;
}
//get comma saparated value
function findInCollection(Illuminate\Support\Collection $collection, $key, $value)
{
    foreach ($collection as $item) {
        if (isset($item->$key) && $item->$key == $value) {
            return true;
        }
    }
    return false;
}

function getratingStar($rating)
{
    $output = '';
    $class = '';
    $output .= '
    <div class="reviews-stars">
    ';
    for ($count = 1; $count <= 5; $count++) {
        if ($count <= $rating) {
            $class = 'active';
        } else {
            $class = '';
        }
        $output .= '<i class="fa fa-star ' . $class . '"></i>';
    }
    $output .= '
    </div>';
    return $output;
}

function determineVideoUrlType($url)
{
    $yt_rx = '/^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/';
    $has_match_youtube = preg_match($yt_rx, $url, $yt_matches);

    $vm_rx = '/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([‌​0-9]{6,11})[?]?.*/';
    $has_match_vimeo = preg_match($vm_rx, $url, $vm_matches);

    //Then we want the video id which is:
    $data = [];
    $parts = explode(".", $url);
    if ($has_match_youtube) {
        $video_id = $yt_matches[5];
        $type = '1';
    } elseif ($has_match_vimeo) {
        $video_id = $vm_matches[5];
        $type = '2';
    } elseif (strtolower(end($parts)) == "mp4") {
        $video_id = 'mp4';
        $type = '3';
    } else {
        $video_id = false;
        $type = false;
    }

    $data['video_id'] = $video_id;
    $data['video_type'] = $type;

    return $data;
}

function sendMail($data, $blade)
{
    try {
        Mail::send('emails.' . $blade, ['data' => $data], function ($message) use ($data) {
            $message->from('ved.infowind@gmail.com', 'Gigstoc');
            $message->subject($data['subject']);
            $message->to($data['to_email']);
        });
    } catch(\Exception $e){
        // Get error here
    }
    

}

function uniqueData($table, $where)
{
    $query = \DB::table($table)->where($where)->first();

    return $query;
}

function getServicerating($serviceId)
{
    $serviceRating = ServiceRating::where(['service_id' => $serviceId, 'is_published' => 1])->get();
    $count = count($serviceRating);
    $sum = 0;
    if ($count > 0) {
        foreach ($serviceRating as $val) {
            $sum = $sum + $val->rating;
        }
        return round(number_format(($sum / $count), 2));
    }
    return 0;

}

function getUserRating($userId)
{
    $userRating = SellerRating::where(['to_id' => $userId, 'is_published' => 1])->get();
    $count = count($userRating);
    $sum = 0;
    if ($count > 0) {
        foreach ($userRating as $val) {
            $sum = $sum + $val->rating;
        }
        return number_format(($sum / $count), 2);
    }
    return 0;

}

function getDateMonthString($datetime)
{
    return date('F d, Y', strtotime($datetime));
}

function getEmbedUrl($url)
{
    // function for generating an embed link
    $finalUrl = '';
    if (strpos($url, 'vimeo.com/') !== false) {
        // Vimeo video
        $videoId = isset(explode("vimeo.com/", $url)[1]) ? explode("vimeo.com/", $url)[1] : null;
        if (strpos($videoId, '&') !== false) {
            $videoId = explode("&", $videoId)[0];
        }
        $finalUrl .= 'https://player.vimeo.com/video/' . $videoId;

    } else if (strpos($url, 'youtube.com/') !== false) {
        // Youtube video
        $videoId = isset(explode("v=", $url)[1]) ? explode("v=", $url)[1] : null;
        if (strpos($videoId, '&') !== false) {
            $videoId = explode("&", $videoId)[0];
        }
        $finalUrl .= 'https://www.youtube.com/embed/' . $videoId;

    } else {
        $finalUrl .= $url;
    }

    return $finalUrl;
}

function getVideo($url, $width = '100%', $height = '100%', $autoplay = 0)
{
    if (preg_match('/https:\/\/(www\.)*youtube\.com\/.*/', $url)) {
        preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $iframe);
        ob_start();
        ?>
            <iframe width="<?php echo $width ?>" height="<?php echo $height; ?>" src="https://www.youtube.com/embed/<?php echo $iframe[1]; ?>?showinfo=0&controls=1&autoplay=<?php echo $autoplay; ?>" frameborder="0" allowfullscreen></iframe>
            <?php
return ob_get_clean();
    }

    if (preg_match('/https:\/\/(www\.)*vimeo\.com\/.*/', $url)) {
        ob_start();
        $rest_url = 'https://vimeo.com/api/oembed.json?url=' . $url;
        $json = json_decode(file_get_contents($rest_url), true);
        ?>
                <iframe src="https://player.vimeo.com/video/<?php echo $json['video_id'] ?>?showinfo=0&controls=1&title=0&byline=0&portrait=0" width="<?php echo $width ?>" height="<?php echo $height; ?>" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            <?php
return ob_get_clean();
    }

    if (preg_match('/.mp4.*/', $url)) {
        ob_start();
        ?>
            <video width="<?php echo $width ?>" height="<?php echo $height; ?>" controls>
              <source src="<?php echo $url; ?>" type="video/mp4">
            </video>
            <?php
return ob_get_clean();
    }
    return '';
}

function searchCommaValue($my_array, $value)
{
    if (in_array($value, $my_array)) {
        return 'selected';
    } else {
        return '';
    }
}
function displayRating($rating)
{
    $output = '';
    $class = '';
    $output .= '
    <span class="pro-rating">
    ';
    for ($count = 1; $count <= 5; $count++) {
        if ($count <= $rating) {
            $class = 'fas fa-star';
        } else {
            $class = 'far fa-star';
        }
        $output .= '<i class="' . $class . '"></i>';
    }
    $output .= '
    </span>';
    echo $output;

}

function getNextOrderNumber()
{
    // Get the last created order
    $lastOrder = DB::table('orders')->orderBy('created_at', 'desc')->first();

    if (!$lastOrder)
    // We get here if there is no order at all
    // If there is no number set it to 0, which will be 1 at the end.

    {
        $number = 0;
    } else {
        $number = substr($lastOrder->order_id, 3);
    }

    // If we have ORD000001 in the database then we only want the number
    // So the substr returns this 000001

    // Add the string in front and higher up the number.
    // the %05d part makes sure that there are always 6 numbers in the string.
    // so it adds the missing zero's when needed.

    return 'GIG' . sprintf('%09d', intval($number) + 1);

}

function getTimeago($datetime, $full = false)
{
    $now = new \DateTime;
    $ago = new \DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) {
        $string = array_slice($string, 0, 1);
    }

    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function getViewCount($serviceId)
{
    return \App\Models\ServiceView::where(['service_id' => $serviceId])->count();
}

function getReviewCount($serviceId)
{
    return ServiceRating::where(['service_id' => $serviceId, 'is_published' => 1])->count();
}

function changeDateformat($datetime)
{

    $format = App\Models\ConfigSetting::where(['meta_key' => 'date_formate'])->first();
    if (!empty($format)) {
        return date($format->meta_value, strtotime($datetime));
    }
}

function alphaDateFormat($date)
{
    return date('F d, Y', strtotime($date));
}

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

// get amount received after deduct commission
function getReceivedAmt($commission_rate, $amount, $discount = 0)
{
    //$amount = number_format($amount, 2) - number_format($discount, 2);
    $commission = ($amount * $commission_rate) / 100;
    $amount = number_format($amount, 2) - number_format($commission, 2);
    return $amount;
}

// get existing balance of user
function getExistingBlanace($user_id)
{
    $get_balance = DB::table('user_wallets as uw')
        ->select('uw.current_balance')
        ->whereRaw("id = (SELECT MAX(id) FROM `user_wallets` where `user_id` = " . $user_id . ")")
        ->where('user_id', '=', $user_id)
        ->first();

    if (!empty($get_balance)) {
        $current_balance = $get_balance->current_balance;
        return number_format($current_balance, 2);
    } else {
        return 0;
    }
}

function storeTransactions($senderId, $orderId, $discount, $transactionId, $totalAmount, $serviceCharge, $subtotal, $total_pay, $use_wallet)
{
    $commission_rate = number_format(getSettings('commission_rate'), 2);

    // get owners of prodcut
    $items = DB::table('orders as o')
        ->select('od.product_id', 'od.extra_services', 'od.price', 's.created_by as user_id', 'o.tax', 'o.discount')
        ->selectRaw('sum(od.price) as total_price')
        ->join('order_details as od', 'od.order_id', '=', 'o.id')
        ->join('services as s', 's.id', '=', 'od.product_id')
        ->where('o.id', '=', $orderId)
        ->groupBy('s.created_by')
        ->first();

    $extra_services = json_decode($items->extra_services);

    $total_price = 0;
    if (!empty($extra_services)) {
        foreach ($extra_services as $key => $value) {
            $total_price = number_format($value->price, 2);
        }
    }

    $total_price = number_format($total_price, 2) + number_format($items->total_price, 2);
    $user_received_amt = number_format(getReceivedAmt($commission_rate, $subtotal, $discount), 2);
    $existing_balance = getExistingBlanace($items->user_id);
    $current_balance = number_format($existing_balance, 2) + number_format($user_received_amt, 2);
    $current_balance = number_format($current_balance, 2);

    // prepare data for store in site transactions
    $site_transaction = [
        'sender_id' => $senderId,
        'receiver_id' => $items->user_id,
        'amount' => $total_price,
        'sc' => $serviceCharge,
        'cr' => $commission_rate,
        'discount' => $items->discount,
        'user_received_amount' => $user_received_amt,
        'order_id' => $orderId,
        'transaction_id' => $transactionId,
    ];

    // store data inside site transations
    $site_transaction_id = DB::table('site_transactions')->insertGetId($site_transaction);

    // prepare data for user wallet credit
    $user_wallet_credit = [
        'site_transaction_id' => $site_transaction_id,
        'user_id' => $items->user_id,
        'credit' => $user_received_amt,
        'debit' => 0,
        'amount' => $user_received_amt,
        'current_balance' => $current_balance,
    ];

    // store data inside wallet credits
    $user_wallet_id = DB::table('user_wallets')->insertGetId($user_wallet_credit);

    $userData = User::find($senderId);
    // Event notification
    $namee = $userData->name;

    $title = $namee . " has purchased your service and your wallet has been credited...go and check it out";

    //sendNotification($title, 'Wallet Entry', 1, $value->user_id);

    if ($use_wallet == true) {
        if ($total_pay > 0) {
            $debit = number_format($totalAmount, 2) - number_format($total_pay, 2);
        } else {
            $debit = number_format($totalAmount, 2);
        }
        $existing_balance = getExistingBlanace($senderId);
        $current_balance = number_format($existing_balance, 2) - number_format($debit, 2);
        $current_balance = number_format($current_balance, 2);

        // prepare data for user wallet debit
        $user_wallet_debit = [
            'site_transaction_id' => $site_transaction_id,
            'user_id' => $senderId,
            'credit' => 0,
            'debit' => number_format($debit, 2),
            'amount' => number_format($debit, 2),
            'current_balance' => $current_balance,
        ];

        // store data inside site transations
        $user_wallet_id = DB::table('user_wallets')->insertGetId($user_wallet_debit);
    }

    // prepare data for store in physical transactions
    if ($use_wallet == true) {
        if ($total_pay > 0) {
            $physical_transaction = [
                'user_id' => $senderId,
                'amount' => number_format($total_pay, 2),
                'order_id' => $orderId,
                'transaction_id' => $transactionId,
                'created_at' => date('Y-m-d H:i:s'),
            ];

            // store in physical payment
            $physical_payment_id = DB::table('physical_payments')->insertGetId($physical_transaction);
        }
    } else {
        $physical_transaction = [
            'user_id' => $senderId,
            'amount' => number_format($total_pay, 2),
            'order_id' => $orderId,
            'transaction_id' => $transactionId,
            'created_at' => date('Y-m-d H:i:s'),
        ];

        // store in physical payment
        $physical_payment_id = DB::table('physical_payments')->insertGetId($physical_transaction);
    }

    // prepare data for store in admin earnings
    $earning_amount = number_format(0, 2);
    $commission = (number_format($subtotal, 2) * $commission_rate) / 100;
    $earning_amount = (number_format($commission, 2) + number_format($items->tax, 2)) - number_format($items->discount, 2);

    $admin_earning = [
        'user_id' => $senderId,
        'total_amount' => number_format($totalAmount, 2),
        'sc' => $serviceCharge,
        'discount' => number_format($items->discount, 2),
        'cr' => $commission_rate,
        'earning_amount' => number_format($earning_amount, 2),
        'transaction_id' => $transactionId,
        'order_id' => $orderId,
    ];

    // store in admin earnings
    $admin_earning_id = DB::table('admin_earnings')->insertGetId($admin_earning);

}

function checkServiceRating($serviceId, $custom_offer = false)
{
    if ($custom_offer == false) {
        $rating = ServiceRating::where(['service_id' => $serviceId, 'custom_offer' => 1, 'user_id' => Auth::user()->id])->first();
    } else {
        $rating = ServiceRating::where(['service_id' => $serviceId, 'custom_offer' => 1, 'user_id' => Auth::user()->id])->first();
    }
    if (!empty($rating)) {
        return true;
    }
    return false;
}

function checkuserrating($toId, $serviceId, $custom_offer = false)
{
    if ($custom_offer == false) {
        $rating = SellerRating::where(['service_id' => $serviceId, 'custom_offer' => 0, 'from_id' => Auth::user()->id, 'to_id' => $toId])->first();
    } else {
        $rating = SellerRating::where(['service_id' => $serviceId, 'custom_offer' => 1, 'from_id' => Auth::user()->id, 'to_id' => $toId])->first();
    }

    if (!empty($rating)) {
        return true;
    }
    return false;
}

function getUserOrderCount($userId)
{
    return Order::where(['user_id' => $userId])->count();
}

// to get admin total earning
function getAdminTotalEraning()
{
    $sum = 0;
    $adminEarnings = DB::table('admin_earnings')->get();
    foreach ($adminEarnings as $val) {
        $sum = $sum + $val->earning_amount;
    }
    return $sum;
}

function getCurrentYearEarning()
{

    $result = [];
    for ($i = 1; $i <= 12; $i++) {
        $row = [];
        $earnig = AdminEarning::select('sc', 'cr',
            DB::raw('sum(earning_amount) as sums'),
            DB::raw("DATE_FORMAT(created_at,'%M') as months"),
            DB::raw("DATE_FORMAT(created_at,'%m') as monthKey")
        )->whereMonth('created_at', $i)->whereYear('created_at', date('Y'))->first();

        if (isset($earnig['sums']) && $earnig['sums'] > 0) {
            $row['index'] = $i - 1;
            $row['sum'] = number_format($earnig['sums'], 2);
            $row['month_name'] = $earnig['months'];
        } else {
            $row['index'] = $i - 1;
            $row['sum'] = 0;
            $dateObj = \DateTime::createFromFormat('!m', $i);
            $monthName = $dateObj->format('F');
            $row['month_name'] = $monthName;
        }
        $result[] = $row;
    }
    return $result;

}

function adminCurrentYearEarning()
{
    $sum = 0;
    $firstDate = date('Y-01-01');
    $lastdate = date('Y-12-t');
    $adminEarnings = DB::table('admin_earnings')->whereBetween(DB::raw('DATE(created_at)'), [$firstDate, $lastdate])->get();
    foreach ($adminEarnings as $val) {
        $sum = $sum + $val->earning_amount;
    }
    return $sum;
}

function getTotalUser()
{
    $users = User::with(['userInRole.role'])->latest();
    $users->whereHas('userInRole.role', function ($q) {
        $q->where('name', '!=', 'admin');
    });
    return $users = $users->count();
}

function getServicesalesCount($serviceId)
{
    return OrderDetail::where(['product_id' => $serviceId])->count();
}

function getofferCount($jobId)
{
    return OffersOnJob::where(['job_request_id' => $jobId])->count();
}

function getPendingAmount($userId)
{
    $sum = WithdrawalRequest::where(['user_id' => $userId, 'status' => 'pending'])->sum('amount');
    return number_format($sum, 2);
}

function getAvailableAmount($userId)
{
    $pendingAmount = WithdrawalRequest::where(['user_id' => $userId, 'status' => 'pending'])->sum('amount');
    $getExistingAmount = getExistingBlanace($userId);
    return number_format(($getExistingAmount - $pendingAmount), 2);
}

function getUsername($user_id)
{
    return User::where('id', $user_id)->first()->name;
}

function getUserImage($user_id)
{
    return User::where('id', $user_id)->first()->user_image;
}

function getUnreadChat($user_id)
{
    $check_unread = DB::select('SELECT c1.*, c2.unread_count FROM chat_messages c1 INNER JOIN ( SELECT chat_room_id, MAX(created_at) AS max_created_at, COUNT(*) AS unread_count FROM chat_messages where ( to_id = ' . $user_id . ' OR from_id = ' . $user_id . ') GROUP BY chat_room_id ) c2 ON c1.chat_room_id = c2.chat_room_id AND c1.created_at = c2.max_created_at order by c1.id desc limit 5');

    $unread_count = 0;
    $messages = [];
    $timezone = (Auth::user()->timezone) ? Auth::user()->timezone : 'UTC';
    date_default_timezone_set($timezone);
    if (!empty($check_unread)) {
        foreach ($check_unread as $key => $value) {
            $chat_time = getTimeago(date('Y-m-d H:i:s', $value->time));
            $value->message = $value->message;
            $value->chat_time = $chat_time;
            if ($value->from_id == $user_id) {
                $value->name = getUsername($value->to_id);
                $value->userimage = getUserImage($value->to_id);
            } else {
                $value->name = getUsername($value->from_id);
                $value->userimage = getUserImage($value->from_id);
            }
            $messages[] = $value;

            if ($value->from_id != $user_id) {
                if ($value->is_read == 0) {
                    $unread_count++;
                }
                // update is_shown
                ChatMessage::where('id', '=', $value->id)->update(['is_shown' => 1]);
            }
        }
    }

    //print_r($messages);die;
    return ['messages' => $messages, 'unread_count' => $unread_count];
}

function converDateTimeToTz($time = "", $toTz = '', $type = '', $fromTz = 'UTC')
{
    $date = new \DateTime($time, new \DateTimeZone($fromTz));
    $date->setTimezone(new DateTimeZone($toTz));
    $form = App\Models\ConfigSetting::where(['meta_key' => 'date_formate'])->first();
    $time = $date->format($form->meta_value . ' H:i A');
    return $time;
}

function converToTz($time = "", $toTz = '', $type = '', $fromTz = 'UTC')
{
    $date = new \DateTime($time, new \DateTimeZone($fromTz));
    $date->setTimezone(new DateTimeZone($toTz));
    $format = App\Models\ConfigSetting::where(['meta_key' => 'date_formate'])->first();
    if ($type == 'short') {
        $time = $date->format($format->meta_value . ' h:i A');
    } elseif ($type == 'date') {
        $time = $date->format($format->meta_value);
    } else {
        $time = $date->format($format->meta_value . ' h:i A');
    }

    return $time;
}

function getService($service_id)
{
    $services = Service::with('images')->where('id', '=', $service_id)->first();
    return $services;
}

function getCustomOrder($job_request_id)
{
    $custom_orders = JobRequest::with(['files'])->where('id', '=', $job_request_id)->first();
    return $custom_orders;
}

function getUserRole($userId)
{
    $user = User::where('id', '=', $userId)->first();
    if (!empty($user)) {
        return $user->userInRole[0]->role->name;
    }
}

function getAdminInfo()
{
    $data = User::with(['userInRole.role'])->latest();
    $data = $data->whereHas('userInRole.role', function ($q) {
        $q->where('name', '=', 'admin');
    })->get();
    return $data[0];
}

function checkNotificationRead($userId, $notificationId)
{
    $notification = Notification::whereRaw("NOT FIND_IN_SET($userId,read_by)")->where('id', $notificationId)->first();
    return (!empty($notification)) ? true : false;
}

function saveNotification($message, $url, $toId, $fromId)
{
    $notification = new Notification();
    $notification->title = $message;
    $notification->url = $url;
    $notification->from_id = $fromId;
    $notification->to_id = $toId;
    $notification->read_by = $fromId;
    $notification->save();
}

function getTotalOffers($jobId)
{
    return OffersOnJob::where(['job_request_id' => $jobId])->count();
}

function checkChatRoom($from_id, $to_id)
{
    $q = ChatRoom::where(['from_id' => $from_id, 'to_id' => $to_id]);

    $q->orWhere(function ($query) use ($to_id, $from_id) {
        $query->where('to_id', '=', $from_id)
            ->where('from_id', '=', $to_id);
    });
    $check = $q->get();

    if ($check->count() < 1) {
        return false;
    } else {
        $chat_id = $check[0]->id;
        $chat_id;
    }

    return $chat_id;
}

function replaceBadWord($msg)
{
    $filter_bad_words = getSettings('filter_bad_words');
    $replace_bad_word = getSettings('replace_bad_word');
    $filter_bad_words = explode(',', $filter_bad_words);
    $filter_bad_words = array_map('trim', $filter_bad_words);

    $split_msg = explode(' ', $msg);
    $message = [];
    foreach ($split_msg as $key => $value) {
        if (in_array($value, $filter_bad_words)) {
            $message[$key] = str_replace($value, $replace_bad_word, $value);
        } else {
            $message[$key] = $value;
        }
    }
    $message = implode(' ', $message);
    return $message;
}

function getPayPalWithDrawalBlanace($userId)
{
    $request = WithdrawalRequest::where(['user_id' => $userId, 'status' => 'pending', 'selected_method' => 'paypal'])->first();
    return $request['amount'];
}

function getParentService($parent_service_id)
{
    $parent_service = Service::with(['category', 'images', 'user', 'rating'])->where('id', $parent_service_id)->first();

    return $parent_service;
}

function getMetaTag($title, $description = '', $keyword = '')
{
    return \MetaTag::setTags([
        'title' => $title . ' | ' . env('APP_NAME'),
        'description' => $description,
        'keywords' => $keyword,
    ]);

}

function getPendingPaypalAmount()
{
    return $request = WithdrawalRequest::where(['status' => 'pending', 'selected_method' => 'paypal'])->sum('amount');
}

?>