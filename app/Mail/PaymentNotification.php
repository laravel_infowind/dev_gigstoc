<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public $order_id;
    public $payment_status;
    public $trans_id;
    public $payment_type;
    public function __construct($user, $order_id, $payment_status, $trans_id, $payment_type)
    {
        $this->user = $user;
        $this->order_id = $order_id;
        $this->payment_status = $payment_status;
        $this->trans_id = $trans_id;
        $this->payment_type = $payment_type;
    }

    public function build()
    {
        return $this->subject('Order Received')->view('emails.paymentNotification');
    }
}
