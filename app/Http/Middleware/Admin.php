<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->userInRole[0]->role->name == 'admin') {
           return $next($request);
        }
            
        return redirect('/admin/login');
    
    }
}
