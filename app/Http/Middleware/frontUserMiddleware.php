<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class frontUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    function handle($request, Closure $next)
    {
        if (Auth::check()) {
           return $next($request);
        }
            
        return redirect('/')->with( ['message' => 'Please Login.','type'=>'error'] );;
    
    }
}
