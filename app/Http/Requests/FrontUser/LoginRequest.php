<?php

namespace App\Http\Requests\FrontUser;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|account_verify',
            'password' => 'required',
        ];
    }

    public function messages(){
        return [
            'email.required' => 'The username or email field is required.',
            'email.account_verify' => 'Your account is not verified.',
            'password.only_space_not_allowed' => 'Only space not allowed.',
        ];
    }
}
