<?php

namespace App\Http\Requests\FrontUser;

use Illuminate\Foundation\Http\FormRequest;

class ForgotPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'email' => 'required|string|validate_email',
        ];
    }

    public function messages()
    {
        return [
            'email.validate_email' => 'Please fill valid email Id.',
        ];
    }
}
