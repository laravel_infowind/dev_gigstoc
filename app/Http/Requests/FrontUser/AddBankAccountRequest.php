<?php

namespace App\Http\Requests\FrontUser;

use Illuminate\Foundation\Http\FormRequest;

class AddBankAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'first_name' => 'required|only_space_not_allowed',
            'middle_name' => 'required|only_space_not_allowed',
            'last_name' => 'required|only_space_not_allowed',
            'bank_name' => 'required|string|only_space_not_allowed',
			'swift_code'=>'required|string|only_space_not_allowed',
			'account_number'=>'required|string|only_space_not_allowed'
        ];
    }

    public function messages()
    {
        return [
            'first_name.only_space_not_allowed' => 'Only space not allowed.',
            'middle_name.only_space_not_allowed' => 'Only space not allowed.',
            'last_name.only_space_not_allowed' => 'Only space not allowed.',
			'bank_name.only_space_not_allowed' => 'Only space not allowed.',
			'swift_code.only_space_not_allowed' => 'Only space not allowed.',
			'account_number.only_space_not_allowed' => 'Only space not allowed.',
        ];
    }
}
