<?php

namespace App\Http\Requests\FrontUser;

use Illuminate\Foundation\Http\FormRequest;

class SignupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'fullname' => 'required|max:255|only_space_not_allowed',
            'email' => 'required|email|max:255|unique:users',
            'username' => 'required|string|max:255|unique:users|only_space_not_allowed',
            'password' => 'required|min:6|only_space_not_allowed',
            'confirmed' => 'required|min:6|same:password',
            'terms' => 'required'
        ];
		
    }
	
	public function messages()
    {
        return [
            'fullname.only_space_not_allowed' => 'Only space not allowed.',
			'username.only_space_not_allowed' => 'Only space not allowed.',
            'password.only_space_not_allowed' => 'Only space not allowed.',
        ];
    }
}
