<?php

namespace App\Http\Requests\FrontUser;

use Illuminate\Foundation\Http\FormRequest;

class AddStripeSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
		
            'country' => 'required|only_space_not_allowed',
            'state' => 'required|only_space_not_allowed',
            'city' => 'required|only_space_not_allowed',
            'line1' => 'required|only_space_not_allowed',
			'line2'=>'required|only_space_not_allowed',
			'postal_code'=>'required|numeric',
			'date_of_birth'=>'required',
			'ssn_number'=>'required',
			'routing_number'=>'required',
			'account_number'=>'required',
			'phone_number'=>'required',
			'id_proof'=>'required'
			
        ];
    }

    public function messages()
    {
        return [
            'job_name.only_space_not_allowed' => 'Only space not allowed.',
            'budgets.only_space_not_allowed' => 'Only space not allowed.',
            'time_of_delivery.only_space_not_allowed' => 'Only space not allowed.',
        ];
    }
}
