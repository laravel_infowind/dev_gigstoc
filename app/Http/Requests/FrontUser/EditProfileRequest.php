<?php

namespace App\Http\Requests\FrontUser;

use Illuminate\Foundation\Http\FormRequest;

class EditProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'bio' => 'required|only_space_not_allowed',
            'country' => 'required',
            'fullname' => 'required|only_space_not_allowed',
            'language' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'bio.only_space_not_allowed' => 'Only space not allowed.',
            'fullname.only_space_not_allowed' => 'Only space not allowed.',
            
       ];
    }
}
