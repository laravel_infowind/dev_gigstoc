<?php

namespace App\Http\Requests\FrontUser;

use Illuminate\Foundation\Http\FormRequest;

class SaveRatingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'rating' => 'required|only_space_not_allowed',
            'review' => 'required|string',
           
        ];
    }

    public function messages()
    {
        return [
            'job_name.only_space_not_allowed' => 'Only space not allowed.',
            'budgets.only_space_not_allowed' => 'Only space not allowed.'
        ];
    }
}
