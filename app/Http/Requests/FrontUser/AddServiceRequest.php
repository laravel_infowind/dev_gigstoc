<?php

namespace App\Http\Requests\FrontUser;

use Illuminate\Foundation\Http\FormRequest;

class AddServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
		    'title' => 'required|only_space_not_allowed',
            'price' => 'required|integer|between:5.00,50000.00',
            'deadline' => 'required|integer|min:1',
            'category_id' => 'required',
			//'opening_message'=>'required',
			'images'=>'required',
			'description'=>'required',
			'video'=>'nullable|verify_video_url'
         
        ];
    }

    public function messages()
    {
        return [
            'title.only_space_not_allowed' => 'Only space not allowed.',
			'video.verify_video_url' => 'Please enter valid youtube or vimeo or mp4 video url.',
            'description.only_space_not_allowed' => 'Only space not allowed.',
        ];
    }
}
