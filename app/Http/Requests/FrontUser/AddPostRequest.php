<?php

namespace App\Http\Requests\FrontUser;

use Illuminate\Foundation\Http\FormRequest;

class AddPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'title' => 'required|only_space_not_allowed',
            'budget' => 'required|integer|between:5.00,50000.00',
            'deadline' => 'required|integer|min:1',
            'category_id' => 'required',
			'description'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'title.only_space_not_allowed' => 'Only space not allowed.',
            'description.only_space_not_allowed' => 'Only space not allowed.',
            'time_of_delivery.only_space_not_allowed' => 'Only space not allowed.',
        ];
    }
}
