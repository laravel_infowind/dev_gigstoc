<?php

namespace App\Http\Requests\FrontUser;

use Illuminate\Foundation\Http\FormRequest;

class AddPreviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'account_type' => 'required',
            'amount' => 'required|integer|min:'.getSettings('minimum_withdrawal_amount').'|check_current_wallet|check_max_wallet|check_account_exist'
        ];
    }

    public function messages()
    {
        return [
            'amount.check_current_wallet' => 'Your available balance is not enough to withdraw.',
			'amount.check_max_wallet' => 'Your requested amount is exceeded from available balance.',
			'amount.check_account_exist' => 'Please add this type of config account first.',
        ];
    }
}
