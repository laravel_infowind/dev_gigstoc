<?php

namespace App\Http\Requests\FrontUser;

use Illuminate\Foundation\Http\FormRequest;

class ContactUsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|only_space_not_allowed',
            'email_id' => 'required|validate_email',
            'phone_no' => 'required|numeric',
            'message' => 'required|only_space_not_allowed',
        ];
    }

    public function messages()
    {
        return [
           
            'name.only_space_not_allowed' => 'Only space not allowed.',
            'message.only_space_not_allowed' => 'Only space not allowed.',
            'email_id.validate_email' => 'Please fill valid email Id.',
            
        ];
    }
}
