<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'site_email'=>'required|email',
            'contact_number'=>'required',
            'contact_address'=>'required',
            'facebook_url' => 'required',
            'twitter_url' => 'required',
            'linkedin_url' => 'required',
            'instagram_url' => 'required',
            'copyright_text' => 'required',
            'cookie_privacy_policy' => 'required',
            'commission_rate' => 'required',
            'service_charge' => 'required',
            'date_formate' => 'required',
            'minimum_withdrawal_amount' => 'required',
            'filter_bad_words' => 'required',
            'replace_bad_word' => 'required',

        ];
    }

    public function messages()
    {
        return [
            'password_hash_check'=>'Current Password does not match'
        ];
    }
}
