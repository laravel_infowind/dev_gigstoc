<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddBlogCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_name' => 'required|only_space_not_allowed|unique:blog_categories,category_name,' . ($this->id ? "$this->id" : ''),
            'description' => 'required',
        ];
    }
    
    public function messages(){
        return [
            'category_name.only_space_not_allowed' => 'Only space not allowed.',
        ];
    }
}
