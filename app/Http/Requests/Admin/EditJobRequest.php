<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class EditJobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|only_space_not_allowed',
            'budget' => 'required|numeric',
            'deadline' => 'required|numeric',
            'category_id' => 'required',
          
        ];
    }

    public function messages(){
        return [
            'title.only_space_not_allowed' => 'Only space not allowed.',
        ];
    }

}
