<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class DeclineRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'reason' => 'required|only_space_not_allowed',
        ];
    }

    public function messages()
    {
        return [
            'reason.only_space_not_allowed' => 'Only space not allowed.',
        ];
    }
}
