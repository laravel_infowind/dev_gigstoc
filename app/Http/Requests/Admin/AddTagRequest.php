<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddTagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|only_space_not_allowed|unique:tags,name,' . ($this->id ? "$this->id" : ''),
        ];
    }

    public function messages()
    {
        return [
            'name.only_space_not_allowed' => 'Only space not allowed.',
        ];
    }
}
