<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users,email,' . ($this->id ? "$this->id" : ''),
            'fullname' => 'required|only_space_not_allowed',
            //'password' => 'required_without|only_space_not_allowed|'. ($this->id ? "$this->id" : ''),
            'username' => 'required|only_space_not_allowed',
           // 'confirm_password' =>($this->id) ? '' : 'required|same:password',
            'role_id' => 'required',
            //'profile_pic' => ($this->id) ? '' : 'required',
        ];
    }

    public function messages()
    {
        return [
            'required_without'=>'Category Image is required.',
            'fullname.only_space_not_allowed' => 'Only space not allowed.',
            'password.only_space_not_allowed' => 'Only space not allowed.',
            'username.only_space_not_allowed' => 'Only space not allowed.',
        ];
    }
}
