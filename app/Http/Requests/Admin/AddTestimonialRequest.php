<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddTestimonialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'author' => 'required|only_space_not_allowed|unique:testimonials,author,' . ($this->id ? "$this->id" : ''),
            'comment'=>'required|only_space_not_allowed|max:200',
            'image'=>'nullable|mimes:jpeg,jpg,png',
            'rating'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'author.only_space_not_allowed' => 'Only space not allowed.',
            'comment.only_space_not_allowed' => 'Only space not allowed.'
        ];
    }
}
