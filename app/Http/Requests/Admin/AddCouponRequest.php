<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddCouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
            'coupon' => 'required|only_space_not_allowed|unique:coupons,coupon,' . ($this->id ? "$this->id" : ''),
			'type'=>'required|only_space_not_allowed',
			'amount'=>'required|only_space_not_allowed|numeric|check_percentage|regex:/^\d*(\.\d{1,2})?$/',
			'start_date'=>'required',
			'end_date'=>'required',
			'total_usage'=>'required|integer'
        ];
	
    }

    public function messages()
    {
        return [
            'name.only_space_not_allowed' => 'Only space not allowed.',
			'amount.regex' => 'Only decimal two places allowed.',
			'amount.check_percentage' => 'Percentage should not be greater than 100%.',
        ];
    }
}
