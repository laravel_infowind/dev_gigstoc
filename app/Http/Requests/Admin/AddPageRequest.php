<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddPageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|only_space_not_allowed|unique:pages,name,' . ($this->id ? "$this->id" : ''),
            'content' => 'required',
            'meta_title' => 'required|only_space_not_allowed',
            'meta_desc' => 'required|only_space_not_allowed',
            "meta_keywords"=>'required|only_space_not_allowed',
        ];
    }
    public function messages(){
        return [
            'name.only_space_not_allowed' => 'Only space not allowed.',
            'meta_title.only_space_not_allowed' => 'Only space not allowed.',
            'meta_desc.only_space_not_allowed' => 'Only space not allowed.',
            'meta_keywords.only_space_not_allowed' => 'Only space not allowed.',
        ];
    }

}
