<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|only_space_not_allowed|unique:categories,name,' . ($this->id ? "$this->id" : ''),
            'description' => 'required|only_space_not_allowed',
            'cat_image'=> (!$this->id && !$this->parent_cat_id)?'required_without:parent_cat_id':''
        ];
    }

    public function messages()
    {
        return [
            'required_without'=>'Category Image is required.',
            'name.only_space_not_allowed' => 'Only space not allowed.',
            'description.only_space_not_allowed' => 'Only space not allowed.',
        ];
    }
}
