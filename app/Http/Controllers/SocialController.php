<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Socialite;
use App\Models\SocialAccount;
use App\User;
use App\Models\VerifyUser;
use App\Models\UserInRole;
use DB;
class SocialController extends Controller
{
    public function redirectToProvider($provider)
    {
		 $previousUrl = \URL::previous();
		\Session::put('previous_url', $previousUrl);
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $socialiteUser = Socialite::driver($provider)->user();
        $user = $this->findOrCreateUser($provider, $socialiteUser);
        auth()->login($user, true);
		$previousUrl = \Session::get('previous_url');
		$baseUrl = \URL::to('/');
		
		if(($previousUrl==$baseUrl.'/login/google')||($previousUrl==$baseUrl.'/login'))
		{
			$url = '/dashboard';
        }
		else
		{ 
		$url = str_replace($baseUrl, '', $previousUrl);
		}
		return redirect($url);	
    }
    public function findUserBySocialId($provider, $id)
    {
        $socialAccount = SocialAccount::where('provider', $provider)
            ->where('provider_id', $id)
            ->first();

        return $socialAccount ? $socialAccount->user : false;
    }

    public function findUserByEmail($provider, $email)
    {
        return User::where('email', $email)->first();
    }

    public function addSocialAccount($provider, $user, $socialiteUser)
    {
        SocialAccount::create([
            'user_id' => $user->id,
            'provider' => $provider,
            'provider_id' => $socialiteUser->getId(),
            'token' => $socialiteUser->token,
        ]);
    }

    public function findOrCreateUser($provider, $socialiteUser)
    {
        if ($user = $this->findUserBySocialId($provider, $socialiteUser->getId())) {
            return $user;
        }

        if ($user = $this->findUserByEmail($provider, $socialiteUser->getEmail())) {
            $this->addSocialAccount($provider, $user, $socialiteUser);

            return $user;
        }
    
        $user = User::create([
            'fullname' => $socialiteUser->getName(),
            'email' => $socialiteUser->getEmail(),
            'profile_pic' => $socialiteUser->getAvatar(),
            'password' => bcrypt(str_random(25)),
            'is_active' => '1',
            'is_social' => '1',
            'is_verified' => '1'
        ]);

        $token = str_random(40);
        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => $token
        ]);
        /*---Insert Role-----*/
        $user_in_roles = [
            'role_id' => '2',
            'user_id' => $user->id,
        ];

        UserInRole::create($user_in_roles);
        /*---Insert Role-----*/

     


        $this->addSocialAccount($provider, $user, $socialiteUser);

        return $user;
    }
}
