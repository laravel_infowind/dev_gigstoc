<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Mail;
use Auth;
use Cache;
use App\Models\ConfigSetting;
use App\User;
use App\Models\BlogPost;
use App\Models\BlogCategory;
use Response;
use View;
use App\Models\BookMark;

class BookMarkController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {   
        getMetaTag('My Bookmark');
        return view('bookmark.index');
            
    }
    
    public function getBookMarkList() {
		
        $bookmarks = BookMark::with('user', 'service')
        ->where('is_bookmarked', '=', 1)
		->where('user_id',Auth::user()->id)
        ->latest()->paginate(10);
        $html = \View::make('bookmark.bookmark-list', ['bookmarks'=>$bookmarks])->render();
        return Response::json(['success'=>true,'html' => $html]);
    }
	
	public function removeFromBookMark(Request $request){
		BookMark::where(['id'=>$request->id])->delete();
		return Response::json(['success' => true, 'message' => 'Removed from bookmark.']);
	}
}
