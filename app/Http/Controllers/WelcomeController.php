<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Common;
use Validator;
use Mail;
use Auth;
use Cache;
use App\Models\Slider;
use App\Models\Testimonial;
use App\Models\HomeManagement;
use App\Models\Category;
use App\Models\JobRequest;
use App\Models\ContactUs;
use App\Http\Requests\FrontUser\ContactUsRequest;
use App\Models\ConfigSetting;
use App\User;
use Response;
use App\Models\Page;
use App\Models\Faq;
use App\Models\AboutUs;
use Newsletter;
use App\Models\Service;
use App\Models\OrderDetail;


class WelcomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   $this->common = new Common;
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {   
	    getMetaTag('Gigstoc - Find the Perfect Freelancer for any Job');
        $slider = Slider::latest()->get();
        $testimonials = Testimonial::latest()->get();
        $content = HomeManagement::with(['keyFeatures', 'findingCosts'])->latest()->get();
        $category = Category::with('categories')
                    ->where('is_active', '=', 1)
                    ->whereNull('category_id')
                    ->latest()
                    ->get();
        $job_request = JobRequest::latest()
                        ->where('is_active', '=', 1)
                        ->where('is_completed', '=', 0)
						->where('is_custom', '=', 0)
                        ->take(5)
                        ->get();
		$recentAddedServices = Service::where('is_custom', '=', 0)->take(8)->orderBy('id','desc')->get();
        $data = [];
        foreach ($content as $key => $value) {
            $data[str_replace(' ', '_', strtolower($value->section))] = $value;
        }
		$mostSalesServices = OrderDetail::with(['service','service'])->whereHas('service', function($q){
                 $q->where('is_custom', '=', 0);
              })
            ->select('order_details.*')
            ->selectRaw('count(order_details.id) AS sales')
            ->groupBy('product_id')->take(5)
			->orderBy('sales','desc')
			->get(); 
        return view('welcome', compact('slider', 'testimonials', 'data', 'category', 'job_request','recentAddedServices','mostSalesServices'));
            
    }

    public function staticPage($page)
    {
      $page = Page::where(['slug'=>$page])->first();
      if(empty($page))
      {
       return view('errors.404');
      }
   
      getMetaTag($page->meta_title,$page->meta_desc ,$page->meta_keywords);
      if($page->slug == 'about-us'){
       $aboutUs = AboutUs::latest()->first();
       return view('page.about-us', ['about'=>$aboutUs]); 
       }
      return view('page.index',['page'=>$page]);
    }
    
    public function contactUs()
    {
		getMetaTag('Contact Us');
        $data = [];
        $data['contact_address'] = getSettings('contact_address');
        $data['contact_email'] = getSettings('site_email');
        $data['contact_no'] = getSettings('contact_number');
        return view('contact-us',['data'=>$data]);
    }
	
	
	  //sitemap page
    public function sitemap(){
        getMetaTag('Sitemap');
        return view('sitemap');
    }
	

    public function saveContactUs(ContactUsRequest $request)
    {

        try{
               // save contact us
                $contactObj = new ContactUs();
                $contactObj->create($request->all());
                // sending mail
                $admin = User::with(['userInRole.role'])->latest();
                $admin = $admin->whereHas('userInRole.role', function($q)
                    {
                        $q->where('name','=', 'admin');    
                    })->first();
           
                $data['name'] = $request->name;
                $data['email_id'] = $request->email_id;
                $data['subject'] = 'Contact us mail';
                $data['phone_no'] = $request->phone_no;
                $data['message'] = $request->message;
                $data['to_email'] = $admin->email;
                sendMail($data,'contact-us');
                return Response::json(['success'=>true,'message' => 'Contact request sent successfully.']);
           
        }catch(Exception $ex){
            return Response::json(['success'=>false,'message' => $ex->getMessage()]);
        }
  
    }
	
	public function getSideLink(Request $request){
		$html = \View::make('common.side-menu')->render();
        return Response::json(['success'=>true,'html' => $html]);
    }
    
    public function newsletter_store(Request $request)
    {
        if (empty($request->user_email)) {
            echo json_encode(array('success'=> false,'msg'=> '<p class="newsletter-danger">Please enter your Email ID.</p>'));
        } else {
        // check if already subscribe or not
            if ( ! Newsletter::isSubscribed($request->user_email) ) {
                // check has already member either subscribe or unsubscribe
                if (! Newsletter::hasMember($request->user_email)) { //returns a boolean
                    // do subscribe
                    Newsletter::subscribe($request->user_email);
                } else {
                    // Resend confirmation email
                    Newsletter::subscribeOrUpdate($request->user_email, [], 'subscribers', ['status' => 'pending']);
                }
                echo json_encode(array('success'=> true,'msg'=> '<p class="newsletter-success">Thank you for your subscription.</p>'));
                //echo "successfully subscribed";
            } else {
                echo json_encode(array('success'=> false,'msg'=> '<p class="newsletter-danger">This email address is already exist.</p>'));
                //echo "Already subscribed";
            }
        }

    }
	
	public function faqList(Request $request){
		$faqs = Faq::where(['is_active'=>1])->get();
		 return view('page.faq',['faqs'=>$faqs]);
	}
	public function checkTemplate(){
		 $user = [];
		 $user['fullname'] = 'test';
		 $user['email'] = 'yunush@mailinator.com';
		 $user['username'] = 'test';
		 $user['password'] = 'test';
		 
		 return view('emails.verifyUser',['user'=>$user]);
	}
	
	public function getip(){
		return view('ip');
	}
}
