<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Socialite;
use App\Models\SocialAccount;
use App\User;
use App\Models\VerifyUser;
use App\Models\UserInRole;
use DB;
use App\Models\UserPaymentMethod;
use Auth;
use Illuminate\Http\Request;
use Response;
use App\Http\Requests\FrontUser\AddPreviewRequest;
use App\Models\WithdrawalRequest;

class WithdrawalController extends Controller
{
    public function index()
	{
		getMetaTag('Withdrawing request');
		$paymentmethod = UserPaymentMethod::where(['user_id'=>Auth::user()->id])->first();
		return view('withdrawal.index',['paymentmethod'=>$paymentmethod]);
	}
	
	public function getWithDrawalAccount(Request $request)
	{
		if($request->account_type == 'bank'){
			$account = 1;
		}else{
			$account = 2;
		}
	    $paymnetMethod = UserPaymentMethod::where(['user_id'=>Auth::user()->id,'payment_type'=>$account])->first();
		
		return Response::json(['success' => true, 'message' => '','data'=>($paymnetMethod)?$paymnetMethod:[],'account_type'=>$request->account_type]);
	}
	
	public function checkAccount(AddPreviewRequest $request)
	{
	
		$message = '';
		$currentWalletamount = getExistingBlanace(Auth::user()->id);
		$paymnetMethod = UserPaymentMethod::where(['id'=>$request->method_id])->first();
		$withdrawalRequest = WithdrawalRequest::where(['user_id'=>Auth::user()->id,'status'=>'pending'])->first();
		if(!empty($withdrawalRequest)){
			if($request->account_type == $withdrawalRequest->selected_method){
			 $message = 'You have already requested for withdrawal with this type of account.'	;
			}elseif($request->account_type != $withdrawalRequest->selected_method && ($currentWalletamount-$withdrawalRequest->amount)< $request->amount){
			 $message = 'You have already sent a request so, there is insufficient available amount.'	;
			}
		}
		if(!empty($withdrawalRequest) && $message!=''){
		  return Response::json(['success' => false, 'message' => $message]);	
		}else{
		 return Response::json(['success' => true, 'message' => '','data'=>$paymnetMethod,'account_type'=>$request->account_type,'amount'=>$request->amount,'payment_method_id'=>$request->method_id]);	
		}
	   
	}
	
	public function saveWithdrawalRequest(Request $request)
	{
		
		try{
			$obj = new WithdrawalRequest();
			$obj->user_id = Auth::user()->id;
			$obj->amount = $request->amount;
			$obj->payment_method_id = $request->payment_method_id;
			$obj->selected_method = $request->selected_method;
		    if($obj->save()){
			    $admin = User::with(['userInRole.role'])->latest();
                $admin = $admin->whereHas('userInRole.role', function($q)
                    {
                        $q->where('name','=', 'admin');    
                    })->first();
				$data['username'] = $admin->name;
                $data['subject'] = 'Withdrawal Request Mail';
                $data['to_email'] = $admin->email;
				$data['from_user'] = Auth::user()->name;
				
                sendMail($data,'withdrawal-to-admin');
			}
			return Response::json(['success' => true, 'message' => 'Request has been sent successfully.']);
		}catch(\Exception $ex){
			return Response::json(['success' => false, 'message' =>$ex->getMessage()]);
		}
	}
	
	public function myWithdrawal(){
		getMetaTag('My Withdrawal');
		return view('withdrawal.my-withdrawal');
	}
	
	public function getWithDrawalRequestList(Request $request){
			//print_r($request->all());die;
		   $requestList = WithdrawalRequest::where(['user_id'=>Auth::user()->id ,'status'=>'pending']);
		   if(!empty($request->account_type)){
			  
			   $requestList->where('selected_method','=',$request->account_type);
		   }
		   if(!empty($request->status)){
			   $requestList->where('status','=',$request->status);
		    }
		   $requestList = $requestList->paginate(10);
		   $html = \View::make('withdrawal._request-list', ['requestList' => $requestList])->render();
           return Response::json(['success' => true, 'html' => $html]);
	}
	
	public function getWithDrawalHistoryList(Request $request){
		   $historyList = WithdrawalRequest::where(['user_id'=>Auth::user()->id])->where('status','!=','pending');
		    if(!empty($request->account_type)){
			   $historyList->where('selected_method','=',$request->account_type);
		    }
		   if(!empty($request->status)){
			   $historyList->where('status','=',$request->status);
		    }
		   $historyList = $historyList->paginate(10);
		   $html = \View::make('withdrawal._history-list', ['historyList' => $historyList])->render();
           return Response::json(['success' => true, 'html' => $html]);
	}
}
