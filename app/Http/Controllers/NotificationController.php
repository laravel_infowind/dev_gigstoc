<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Common;
use Validator;
use Mail;
use Auth;
use Cache;
use App\Models\Slider;
use App\Models\Testimonial;
use App\Models\HomeManagement;
use App\Models\Category;
use App\Models\JobRequest;
use App\Models\ContactUs;
use App\Http\Requests\FrontUser\ContactUsRequest;
use App\Models\ConfigSetting;
use App\User;
use Response;
use App\Models\Notification;
use Newsletter;


class NotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   $this->common = new Common;
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
	 
	public function getUnreadCount(){
		$userId = Auth::user()->id;
		$notificationCount = Notification::whereRaw("NOT FIND_IN_SET($userId,read_by)")->whereRaw("FIND_IN_SET($userId,to_id)")->count();
		return Response::json(['success'=>true,'count' => $notificationCount]);
	}
	
	public function getNotification(Request $request){
		getMetaTag('Notification');
		return view('notification.index');
	}
	
	public function getNotificationList(Request $request){
		
		$toId = Auth::user()->id;
		$notifications = Notification::whereRaw("FIND_IN_SET($toId,to_id)")->get();
		foreach($notifications as $notifi){
		$oldReadBy = $notifi->read_by;
		$oldArray = explode(',',$oldReadBy);
			if(!in_array($toId,$oldArray)){
			$newReadBy = $oldReadBy.','.$toId;
			$notifi = Notification::whereRaw("FIND_IN_SET($toId,to_id)")->update(['read_by'=>$newReadBy]);		
			}	
		}
		$notificationList = Notification::whereRaw("FIND_IN_SET($toId,read_by)")->whereRaw("FIND_IN_SET($toId,to_id)")->orderBy('id','desc')->paginate(10);
		$html = \View::make('notification._notification_list', ['notifications'=>$notificationList])->render();
        return Response::json(['success'=>true,'html' => $html]);
	}
}
