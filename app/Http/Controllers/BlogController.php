<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Mail;
use Auth;
use Cache;
use App\Models\ConfigSetting;
use App\User;
use App\Models\BlogPost;
use App\Models\BlogCategory;
use Response;
use View;

class BlogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {   
      
        
        //print_r($blogs);die;
        return view('blog.index');
            
    }
    
    public function getBlogList() {
        $blogs = BlogPost::with('user', 'categories')
        ->where('is_active', '=', 1)
        ->latest()->paginate(10);
        $html = \View::make('blog.blog-list', ['blogs'=>$blogs])->render();
        return Response::json(['success'=>true,'html' => $html]);
    }

    public function blogDetail($slug){
        $blog = BlogPost::with('user', 'categories')
                        ->where('slug', '=', $slug)
                        ->first();
        return view('blog.single', ['blog'=>$blog]);
    }
    
    public function singleBlogDetail($id)
    {   
        
        $blog = BlogPost::with('user', 'categories')
                        ->where('id', '=', $id)
                        ->first();

                      
        // get previous blog id
        $previous = BlogPost::where('id', '<', $blog->id)->max('id');
    
        // get next blog id
        $next = BlogPost::where('id', '>', $blog->id)->min('id');
    
        $html = \View::make('blog.single-page-view',['blog'=>$blog,'previous'=>$previous,'next'=>$next])->render();              
         return Response::json(['success'=>true,'html' => $html]);
            
    }
}
