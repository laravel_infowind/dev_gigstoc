<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\User;
use Auth;
use App\Models\Service;
use App\Models\Order;

use Response;


class DashBoardController extends Controller
{
	
   public function dashboard()
   {
	   getMetaTag('Dashboard');
	   $services =Service::where(['created_by'=>Auth::user()->id])->where('is_custom','=',0)->take(5)->latest()->get();
	   $orders = Order::where(['user_id'=>Auth::user()->id])->take(5)->latest()->get();
	   
       return View('dashboard.index',['services'=>$services,'orders'=>$orders]);
   }
   
   public function getOrderStatics(){
	   
	 $weekFirstDate = date('Y-m-d', strtotime('-6 days'));
	 $orders = Order::where('user_id',Auth::user()->id)->whereDate('created_at','>=',date('Y-m-d',strtotime($weekFirstDate)))
	        ->whereDate('created_at','<=',date('Y-m-d'))->get();
	   $data = [];
	   
	   for($i=1;$i<=7;$i++){
		   $row = [];
		   $successCount = Order::where('user_id',Auth::user()->id)->whereDate('created_at','=',date('Y-m-d',strtotime($weekFirstDate)))
	       ->where('status',1)->count();
		   $cancelledCount = Order::where('user_id',Auth::user()->id)->whereDate('created_at','=',date('Y-m-d',strtotime($weekFirstDate)))
	       ->where('status',2)->count();
		   $pendingCount = Order::where('user_id',Auth::user()->id)->whereDate('created_at','=',date('Y-m-d',strtotime($weekFirstDate)))
	       ->where('status',0)->count();
		   $row['date'] = date('F d, Y', strtotime($weekFirstDate));
		   $row['success_count'] = $successCount;
		   $row['cancelled_count'] = $cancelledCount;
		   $row['pending_count'] = $pendingCount;
		   $weekFirstDate = date("Y-m-d",strtotime("+1 day", strtotime($weekFirstDate)));
		   $data[] = $row; 
	   }
	   return Response::json(['success' => true, 'message' => '','res'=>$data]);
   }

   
}
