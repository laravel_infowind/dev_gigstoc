<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\Http\Requests\FrontUser\LoginRequest;
use Illuminate\Support\Facades\Redirect;
use App\User;
use Socialite;
use App\Services\SocialFacebookAccountService;
use Mail;
use App\Mail\VerifyMail;
use App\Models\VerifyUser;
use Illuminate\Http\Request;
use Response;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request, $user)
    {
        if (!$user->is_verified) {
            auth()->logout();
            return back()->with( ['message' => 'You need to confirm your account. We have sent you an activation code, please check your email.', 'type' => 'warning']);
        }
        return redirect()->intended($this->redirectPath());
    }

    public function index()
    {
        return view('login');
    }

    public function login(LoginRequest $request)
    {
    
	//echo "<pre>";
	//print_r($request->all());die;
        $user = User::where(['email'=>trim($request->email)])->orWhere(['username'=>trim($request->email)])->first();
        if(!empty($user)&& $user['username']==$request->email){
            $credentials = ['username'=>trim($request->email),'password'=>$request->password];
        }elseif(!empty($user)){
            $credentials = ['email'=>trim($request->email),'password'=>$request->password];
        }
        if(!empty($user) && $user['is_active']!=1){
            return Redirect('login')->with( ['message' => 'Your account is not active.','type'=>'error'] );
        }
      
        $remember_me = $request->get('remember')==1 ? true : false;
       
        if (!empty($user) && ($user->userInRole[0]->role->name != 'admin') && Auth::attempt($credentials)) {
            if ($remember_me) {
                setcookie("email", "$request->email", time()+7 * 24 * 60 * 60);  
                setcookie("password", "$request->password", time()+7 * 24 * 60 * 60);
            }else{
                setcookie("email", " ");  
                setcookie("password", " "); 
            }
            if(!empty($request->type)&& ($request->type == 'modal'))
			{
				 return Response::json(['success'=>true,'message' => 'Login successfully.']);
			}else
			{
				return Redirect('dashboard')->with( ['message' => 'Login successfully.','type'=>'success'] );
			}
            
          
        }
		if(!empty($request->type)&& ($request->type == 'modal'))
		{
		 return Response::json(['success'=>false,'message' => 'Invalid Credential.']);		
		}else
		{
        return Redirect('login')->with( ['message' => 'Invalid Credential.','type'=>'error'] );
		}

    }

    public function resendVerificationMail(Request $request){
     
        $user = User::where('email',$request->username)->orWhere('username',$request->username)->first();
        if(!empty($user)){
            $verifyObj = VerifyUser::where(['user_id'=>$user->id])->first();
            $token = str_random(40);
            if(!empty($verifyObj)){
                $verifyObj->token = $token;
            }else{
                $verifyObj = new VerifyUser();
                $verifyObj->token = $token;
                $verifyObj->user_id = $user->id;
            }
            $verifyObj->save();
            Mail::to($user->email)->send(new VerifyMail($user));
            return Response::json(['success'=>true,'message' => 'Mail sent you , please check and verify account.']);
        }
       
    }

    public function checkVerification(Request $request){

        $user = User::where(['email'=>$request->username])->orWhere(['username'=>$request->username])->first();
        if(!empty($user) && $user->is_verified!='1'){
            return Response::json(['success'=>true,'message' => '']);
        }
        return Response::json(['success'=>false,'message' => '']);

    }

    public function logout(){
         Auth::logout();
        \Session::flush();
        return redirect('/');
    }

    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }
    /**
     * Return a callback method from facebook api.
     *
     * @return callback URL from facebook
     */
    public function callback(SocialFacebookAccountService $service)
    {
        $user = $service->createOrGetUser(Socialite::driver('facebook')->user());
        auth()->login($user);
        return redirect()->to('/home');
    }
}
