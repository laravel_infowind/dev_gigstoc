<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Http\Requests\FrontUser\ForgotPasswordRequest;
use App\Http\Requests\FrontUser\ResetPasswordRequest;
use App\Models\PasswordReset;
use App\User;
use Response;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
    }

    public function forgotPassword(){
        return view('forgot');
    }

    public function resetForm($token){
       $passToken =  PasswordReset::where(['token'=>$token])->first();
       if(!empty($passToken)){
        return view('reset-password',['user'=>$passToken]);
       }
       return redirect('/login')->with(['message' => 'Token expired.', 'type' => 'error']);
    }

    public function sendPasswordMail(ForgotPasswordRequest $request){

       $user =  User::with('userInRole.role')->where(['email'=>$request->email]);
            $user->whereHas('userInRole.role', function($q)
            {
                $q->where('name','!=', 'admin');    
            });
            $user = $user->first();
            if(!empty($user)){
                $token = str_random(40);
                $resetObj = PasswordReset::where(['email'=>$user->email])->first();
                if(!empty($resetObj)){
                    $resetObj->token = $token;
                }else{
                    $resetObj = new PasswordReset();
                    $resetObj->token = $token;
                    $resetObj->email = $user->email;
                }
                $resetObj->save();
                // sending mail
                $data['type'] ='forgot';
                $data['fullname'] = $user->fullname;
                $data['to_email'] = $request->email;
                $data['subject']='Forgot Password Mail';
                $data['token']=$token;
                sendMail($data,'forgotPasswordUser');
                return Response::json(['success'=>true,'message' => 'Mail sent you , please check and reset password.']);
            }else{
                return Response::json(['success'=>false,'message' => 'Entered email not found in my record.']);
            }
        
    }

    public function resetPassword(ResetPasswordRequest $request){

        $passReset = PasswordReset::where(['token'=>$request->token])->first();
        if(!empty($passReset)){
            $user = User::where(['email'=>$passReset->email])->first();
            $user->password = bcrypt($request->password);
            $user->save();
            PasswordReset::where(['token'=>$request->token])->delete();
            return Response::json(['success'=>true,'message' => 'Password has been changed.']);
        }
        return Response::json(['success'=>false,'message' => 'Token has been expired.']);

    }
}
