<?php

namespace App\Http\Controllers;

use App\Http\Requests\FrontUser\AddPostRequest;
use App\Models\Category;
use App\Models\JobRequest;
use App\Models\OffersOnJob;
use App\Models\Service;
use App\Models\Tag;
use App\Models\TagInJobRequest;
use Auth;
use Illuminate\Http\Request;
use Response;
use View;

class JobController extends Controller
{
    public function getHome()
    {
        getMetaTag('post-recruit');
        $tags = Tag::all();
        return View('job.post-recuit', ['tags' => $tags]);
    }

    public function savePost(AddPostRequest $request)
    {
        try {
            $jobRequest = new JobRequest();
            $jobRequest->title = $request->title;
            $jobRequest->budget = $request->budget;
            $jobRequest->deadline = $request->deadline;
            $jobRequest->category_id = $request->category_id;
            $jobRequest->description = $request->description;
            $jobRequest->created_by = Auth::user()->id;
            if ($jobRequest->save()) {
                if (!empty($request->tag_id)) {
                    foreach ($request->tag_id as $tagId) {
                        $tagJob = new TagInJobRequest();
                        $tagJob->job_request_id = $jobRequest->id;
                        $tagJob->tag_id = $tagId;
                        $tagJob->save();
                    }
                }
                $userList = [];
                $services = Service::where('category_id', '=', $request->category_id)->get();
                foreach ($services as $val) {
                    if ($val->user->notification_subscription == 1) {
                        $userList[] = $val->user->id;
                    }
                }
                $userList = array_unique($userList);
                $message = '<strong>' . Auth::user()->name . '</strong> has requested for <strong>' . $request->title . '</strong>';
                $url = 'job-detail/' . base64_encode($jobRequest->id);
                $toid = implode(",", $userList);
                $fromId = Auth::user()->id;
                saveNotification($message, $url, $toid, $fromId);

            }
            return Response::json(['success' => true, 'message' => 'Job Added Successfully.']);
        } catch (Exception $ex) {
            return Response::json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }

    public function getCategory(Request $request)
    {

        $list = [];
        $categorys = Category::with('categories')->where('category_id', '=', null)->get();
        $list[] = "<option value=''>Select Category</option>";
        foreach ($categorys as $cat) {
            if (count($cat->categories) > 0) {
                $innerOpt = '';
                $innerArray = [];
                foreach ($cat->categories as $val) {
                    $innerOpt = $innerOpt . "<option value='" . $val->id . "' style='margin-left: 30px;' class='inner-option'>$val->name</option>";
                }
                $option = "<option value='" . $cat->id . "'>$cat->name</option>" . $innerOpt;

            } else {
                $option = "<option value='" . $cat->id . "' >$cat->name</option>";
            }
            if (!empty($request->id) && $cat->id == $request->id) {
                $option = "<option value='" . $cat->id . "' selected='selected'>$cat->name</option>";

            }
            $list[] = $option;
        }
        return Response::json(['success' => true, 'message' => '', 'list' => $list]);

    }

    public function getJobs(Request $request)
    {
        getMetaTag('recruit');
        return View('job.jobs');
    }

    public function getJobList(Request $request)
    {
        $jobs = JobRequest::with('user')
            ->where('is_active', '=', 1)->where('is_custom', '=', 0);
        if (!empty($request->sorting)) {
            if ($request->sorting == 'asc') {
                $jobs->orderBy('id', 'asc');
            } else {
                $jobs->orderBy('id', 'desc');
            }
        }
        $jobs = $jobs->latest()->paginate(10);
        $html = \View::make('job._job-list', ['jobs' => $jobs])->render();
        return Response::json(['success' => true, 'html' => $html]);
    }

    public function getJobDetail($id)
    {
        getMetaTag('Job Request Detail');
        $jobRequest = JobRequest::where(['id' => base64_decode($id)])->first();
        if (!empty(Auth::user())) {
            $myServices = Service::where(['created_by' => Auth::user()->id])->get();
        }
        $user_id = (Auth::check()) ? Auth::user()->id : '';
        return View('job.job-detail', ['jobDetail' => $jobRequest, 'myServices' => (!empty($myServices)) ? $myServices : [], 'user_id' => $user_id]);
    }

    public function myJob(Request $request)
    {
        getMetaTag('My requirements');
        return View('job.my-job');
    }

    public function myJobList()
    {
        $myJobs = JobRequest::with('user')
            ->where('is_active', '=', 1)->where('is_custom', '=', 0)->where('created_by', Auth::user()->id);
        $myJobs = $myJobs->latest()->paginate(10);
        $html = \View::make('job._my-job-list', ['myJobs' => $myJobs])->render();
        return Response::json(['success' => true, 'html' => $html]);
    }

    public function deleteJob($id)
    {
        JobRequest::where(['id' => $id])->delete();
        return Response::json(['success' => true, 'message' => 'Job deleted successfully.']);
    }

    public function submitOffer(Request $request)
    {

        try {
            $offerOnJob = OffersOnJob::where(['requested_by' => Auth::user()->id, 'job_request_id' => $request->request_id, 'service_id' => $request->service_id])->first();
            if (empty($offerOnJob)) {
                $offerOnJob = new OffersOnJob();
                $offerOnJob->requested_by = Auth::user()->id;
                $offerOnJob->job_request_id = $request->request_id;
                $offerOnJob->service_id = $request->service_id;
                if ($offerOnJob->save()) {
                    if ($offerOnJob->jobDetail->user->notification_subscription == 1) {
                        $message = '<strong>' . Auth::user()->name . '</strong> has submitted offer for ' . $offerOnJob->jobDetail->title;
                        $url = 'job-detail/' . base64_encode($offerOnJob->jobDetail->id);
                        $toid = $offerOnJob->jobDetail->created_by;
                        $fromId = Auth::user()->id;
                        saveNotification($message, $url, $toid, $fromId);
                    }
					$data['name'] = $offerOnJob->jobDetail->user->name;
					$data['subject'] = 'Offer on job';
					$data['title'] = $offerOnJob->jobDetail->title;
					$data['url'] = 'job-detail/' . base64_encode($offerOnJob->jobDetail->id);
					$data['to_email'] = $offerOnJob->jobDetail->user->email;
					sendMail($data,'job-offer');
                }
                return Response::json(['success' => true, 'message' => 'Offer Submitted Successfully.']);
            } else {
                return Response::json(['success' => false, 'message' => 'You have already submitted offer on this job request.']);
            }
        } catch (Exception $ex) {
            return Response::json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }

}
