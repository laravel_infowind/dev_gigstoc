<?php

namespace App\Http\Controllers;

use App\Http\Requests\FrontUser\AddBankAccountRequest;
use App\Http\Requests\FrontUser\ChangePasswordRequest;
use App\Http\Requests\FrontUser\EditProfileRequest;
use App\Models\Country;
use App\Models\Language;
use App\Models\UserLanguage;
use App\Models\UserPaymentMethod;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Response;
use App\Http\Requests\FrontUser\SaveRatingRequest;
use App\Models\SellerRating;
use App\Models\Notification;

class UserController extends Controller
{

    public function getChangePassword()
    {
		getMetaTag('Change Password');
        return view('user.change-password');
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        try {
            $admin = User::where(['id' => Auth::user()->id])->first();
            if ($admin) {
                $admin->password = Hash::make($request->new_password);
                $admin->save();
                return Response::json(['success' => true, 'message' => 'Password changed successfully.']);
            }
        } catch (Exception $ex) {
            return Response::json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }

    public function setUserTimeZone(Request $request)
    {
        try {

            $user = User::where(['id' => Auth::user()->id])->first();
            $user->timezone = $request['timezone'];
            $user->save();
            return Response::json(['success' => true, 'html' => '']);
        } catch (\Exception $ex) {
            return Response::json(['success' => false, 'message' => $ex->getMessage(), 'html' => '']);
        }
    }

    public function editProfile()
    {
		getMetaTag('Edit Profile');
        $languages = Language::get();
        return view('user.edit-profile', ['languages' => $languages]);
    }

    public function updateProfile(EditProfileRequest $request)
    {

        $user = User::where(['id' => \Auth::user()->id])->first();
        $user->bio = $request->bio;
        $user->country_id = $request->country;
        $user->fullname = $request->fullname;
        if ($user->save()) {
            if (count($request->language) > 0) {
                UserLanguage::where(['user_id' => \Auth::user()->id])->delete();
                foreach ($request->language as $lan) {
                    $language = new UserLanguage();
                    $language->user_id = \Auth::user()->id;
                    $language->language_id = $lan;
                    $language->save();
                }
            }
            return Response::json(['success' => true, 'message' => 'Profile Updated Successfully.']);
        }
    }

    public function updatePicture(Request $request)
    {

        $dir = url('public/images/profile_pic');
        if (is_dir($dir) === false) {
            mkdir($dir);
        }
        $profile = $request->profile;
        $name = time();
        $imageName = $name . '.' . $profile->getClientOriginalExtension();
        $destinationPath = public_path('images/profile_pic/');
        $profile->move($destinationPath, $imageName);
        $user = User::where(['id' => \Auth::user()->id])->first();
        $user->profile_pic = $imageName;
        $user->save();
        return Response::json(['success' => true, 'message' => '', 'url' => url('public/images/profile_pic') . '/' . $imageName]);

    }

    public function getCountry(Request $request)
    {
        $countryId = ($request->countryId) ? $request->countryId : '';
        $list = [];
        $countrys = Country::get();
        $list[] = "<option value=''>Select Country</option>";
        foreach ($countrys as $country) {
            $option = "<option value='" . $country->id . "' >$country->name</option>";
            if (!empty($countryId) && $country->id == $countryId) {
                $option = "<option value='" . $country->id . "' selected='selected'>$country->name</option>";

            }
            $list[] = $option;
        }
        return Response::json(['success' => true, 'message' => '', 'list' => $list]);
    }

    public function viewProfile($id = '')
    {
		$userId = base64_decode($id);
        $user = User::where(['id' => $userId])->first();
	    return view('user.view-profile', ['user' => $user, 'userId' => $userId]);
    }

    public function myProfile($id = '')
    {
		getMetaTag('My Profile');
        $userId = ($id) ? $id : Auth::user()->id;
        $user = User::where(['id' => $userId])->first();
        return view('user.view-profile', ['user' => $user, 'userId' => $userId]);
    }

    public function paymentMethod()
    {
		getMetaTag('Payment Method');
        $paypalPaymentMethod = UserPaymentMethod::where(['user_id' => Auth::user()->id, 'payment_type' => 2])->first();
        $bankAccount = UserPaymentMethod::where(['user_id' => Auth::user()->id, 'payment_type' => 1])->first();
        return view('setting.index', ['paypal_method' => $paypalPaymentMethod, 'bankAccount' => $bankAccount]);
    }

    public function savePaymentMethod(Request $request)
    {
        if (!empty($request->id)) {
            $methodObj = UserPaymentMethod::where(['id' => $request->id])->first();
        } else {
            $methodObj = new UserPaymentMethod();
        }
        $methodObj->user_id = Auth::user()->id;
        $methodObj->account_info = $request->email;
        $methodObj->payment_type = ($request->type == 'paypal') ? 2 : 1;
        $methodObj->save();
        return Response::json(['success' => true, 'message' => 'Payment method added successfully.']);
    }

    public function selectMethod(Request $request)
    {

        UserPaymentMethod::where('user_id', '=', Auth::user()->id)->update(['preferred_method' => 0]);
        $methodObj = UserPaymentMethod::where(['user_id' => Auth::user()->id, 'payment_type' => $request->preferred_method])->first();
        if (!empty($methodObj)) {
            $methodObj->preferred_method = 1;
            $methodObj->save();
            return Response::json(['success' => true, 'message' => 'Payment method preferred successfully.']);
        } else {
            return Response::json(['success' => false, 'message' => 'Please add payment method first.']);
        }
    }

    public function subscriptionSetting(Request $request)
    {
		getMetaTag('Subscription Setting');
        return view('user.subscription');
    }

    public function setSubscribtion(Request $request)
    {
        $userId = Auth::user()->id;
        $user = User::where(['id' => $userId])->first();
        if (!empty($user) && $user->notification_subscription == 1) {
            $user->notification_subscription = 0;
        } else {
            $user->notification_subscription = 1;
        }
        $user->save();
        return Response::json(['success' => true, 'message' => 'Notification status updated successfully.']);
    }

    public function addBankAccount(AddBankAccountRequest $request)
    {
        $info = json_encode($request->all());
        if (!empty($request->id)) {
            $methodObj = UserPaymentMethod::where(['id' => $request->id])->first();
        } else {
            $methodObj = new UserPaymentMethod();
        }
        $methodObj->user_id = Auth::user()->id;
        $methodObj->account_info = $info;
        $methodObj->payment_type = ($request->type == 'paypal') ? 2 : 1;
        $methodObj->save();
        return Response::json(['success' => true, 'message' => 'Bank account added successfully.']);
    }
	
	public function saveUserRating(SaveRatingRequest $request){
		$rating = new SellerRating();
		$rating->from_id = Auth::user()->id;
		$rating->to_id  = $request->to_id;
		$rating->rating = $request->rating;
		$rating->review = $request->review;
		$rating->service_id = $request->service_id;
		$rating->custom_offer = $request->user_custom_offer;
		if($rating->save()){
			if($rating->service->user->notification_subscription == 1){
			$message = '<strong>'.Auth::user()->name .'</strong> has given seller rating for <strong>'.$rating->service->title.'</strong>';
			$url = 'service/'.$rating->service->id;
			$toid = $rating->service->created_by;
			$fromId = Auth::user()->id;
			saveNotification($message,$url,$toid,$fromId);
			}
		}
	    return Response::json(['success' => true, 'message' => 'Rating Given Successfully.']);
	}
}
