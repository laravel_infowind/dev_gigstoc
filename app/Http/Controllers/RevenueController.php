<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Socialite;
use App\Models\SocialAccount;
use App\User;
use App\Models\VerifyUser;
use App\Models\UserInRole;
use App\Models\SiteTransaction;
use DB;
use Response;
use Illuminate\Http\Request;
use Auth;
use App\Models\WithdrawalRequest;

class RevenueController extends Controller
{
    public function index()
    {
		getMetaTag('Revenues');
        return view('revenue.index');
    }

    public function handleProviderCallback($provider)
    {
         $socialiteUser = Socialite::driver($provider)->user();
        $user = $this->findOrCreateUser($provider, $socialiteUser);
        auth()->login($user, true);

        return redirect('/dashboard');
    }
	
	public function revenueList(Request $request)
	{
		$revenueList = WithdrawalRequest::where(['user_id'=>Auth::user()->id ,'status'=>'complete'])->paginate(10);
		$html = \View::make('revenue.revenue-list',['revenueList'=>$revenueList])->render();
        return Response::json(['success'=>true,'html' => $html]);
	}
    
}
