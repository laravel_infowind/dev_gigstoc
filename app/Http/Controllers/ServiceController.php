<?php

namespace App\Http\Controllers;

use App\Http\Requests\FrontUser\AddServiceRequest;
use App\Http\Requests\FrontUser\SaveRatingRequest;
use App\Mail\PaymentNotification;
use App\Models\BookMark;
use App\Models\Category;
use App\Models\Country;
use App\Models\Coupon;
use App\Models\ExtraInService;
use App\Models\ImageInService;
use App\Models\JobRequest;
use App\Models\Order;
use App\Models\OrderCoupon;
use App\Models\OrderDetail;
use App\Models\Service;
use App\Models\ServiceRating;
use App\Models\ServiceView;
use App\Models\Tag;
use App\Models\TagInService;
use App\User;
use Auth;
use DB;
use File;
use Illuminate\Http\Request;
use Image;
use Mail;
use Response;
use Session;
use Srmklive\PayPal\Services\ExpressCheckout;
use Stripe\Customer;
use Stripe\PaymentIntent;
use Stripe\PaymentMethod;
use Stripe\Stripe;
use Stripe\Token;
use Validator;
use View;

class ServiceController extends Controller
{
    public $ipAddress;
    public function addService()
    {
        getMetaTag('Post a job request');
        $tags = Tag::all();
        return View('service.post-service', ['tags' => $tags]);
    }

    public function saveService(AddServiceRequest $request)
    {

        try {

            $video_url = ($request->video) ? $request->video : '';
            $is_featured = ($request->is_featured) ? $request->is_featured : '';
            $service = new Service();
            $service->title = $request->title;
            $service->price = $request->price;
            $service->deadline = $request->deadline;
            $service->category_id = $request->category_id;
            $service->description = $request->description;
            $service->created_by = \Auth::user()->id;
            $service->is_active = 1;

            if (!empty($video_url)) {

                $video_type = determineVideoUrlType($video_url);

                if (!$video_type['video_type']) {
                    return Response::json(['success' => false, 'video_error' => true, 'message' => 'The vedio type is not valid']);
                }

                $service->is_video = 1;
                $service->video_type = $video_type['video_type'];
                $service->video_link = $video_url;
            }

            $service->opening_message = ($request->opening_message) ? $request->opening_message : '';
            //print_r($service);die;
            if ($service->save()) {
                //tags in services
                if (!empty($request->tag_id)) {

                    foreach ($request->tag_id as $value) {
                        $tag_in_service = new TagInService();
                        $tag_in_service->service_id = $service->id;
                        $tag_in_service->tag_id = $value;
                        $tag_in_service->save();
                    }
                }

                //extras in services
                if (!empty($request->extra)) {
                    foreach ($request->extra as $value) {
                        //print_r($value);die;
                        $extra_in_service = new ExtraInService();
                        $extra_in_service->service_id = $service->id;
                        $extra_in_service->service_name = $value['service'];
                        $extra_in_service->price = $value['extra_price'];
                        $extra_in_service->save();

                    }
                }
                if (!empty($request->gallery_image)) {
                    foreach ($request->gallery_image as $value) {
                        $value = (array) json_decode($value);

                        $extra_in_service = new ImageInService();
                        $extra_in_service->service_id = $service->id;
                        $extra_in_service->thumbnail = $value['name'] . '.' . $value['type'];
                        $extra_in_service->image = $value['name'] . '.' . $value['type'];
                        $extra_in_service->save();

                    }
                }
                // update featured image
                ImageInService::where('service_id', '=', $service->id)->update(['is_featured' => 0]);
                if (!empty($is_featured)) {
                    ImageInService::where('image', 'like', '%' . $is_featured . '%')->where('service_id', $service->id)->update(['is_featured' => 1]);
                }
                // send notification to all user
                $userList = [];
                $users = User::where('is_active', '=', 1)->where('notification_subscription', 1)->get();
                foreach ($users as $val) {
                    $userList[] = $val->id;
                }
                $message = 'A new Job <strong>' . $request->title . '</strong> has been posted by ' . '<strong>' . $service->user->name . '</strong>';
                $url = 'service/' . $service->id;
                $toid = implode(",", $userList);
                $fromId = Auth::user()->id;
                saveNotification($message, $url, $toid, $fromId);

                // send mail to admin
                $admin = getAdminInfo();

                $data['username'] = $service->user->name;
                $data['service'] = $service->title;
                $data['service_link'] = 'service/' . $service->id;
                $data['subject'] = 'new Service Mail';
                $data['to_email'] = $admin->email;
                sendMail($data, 'admin-service-mail');

            }
            return Response::json(['success' => true, 'message' => 'Service Added Successfully.']);
        } catch (Exception $ex) {
            return Response::json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }

    public function imageUpload(Request $request)
    {

        $data = [];
        $service_id = $request->service_id;
        if ($request->hasFile('images')) {

            $rules = [
                'images.*' => 'mimes:gif,jpg,jpeg,png|max:20000|dimensions:min_height=475',
            ];

            $messages = [
                'images.*.mimes' => 'Only jpg, jpeg and png images are allowed',
                'images.*.dimensions' => 'Minimum 475 height image allowed.',
                'images.*.max' => 'Sorry! Maximum allowed size for an image is 20MB',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                //echo "in";die;
                return Response::json(['success' => false, 'message' => $validator->errors()->all()]);
            }
            $res = [];
            foreach ($request->file('images') as $key => $value) {
                $name = time() . $key;
                $imageName = $name . '.' . $value->getClientOriginalExtension();

                $destinationPath = public_path('images/services');

                $thumb_img = Image::make($value->getRealPath())->resize(215, 127);

                $thumb_img->save($destinationPath . '/thumbnail/' . $imageName);

                $main_img = Image::make($value->getRealPath())->resize(781, 475);
                $main_img->save($destinationPath . '/' . $imageName);
                // $value->move($destinationPath, $imageName);

                $data['name'] = $name;
                $data['type'] = $value->getClientOriginalExtension();
                $data['url'] = url('public/images/services') . '/thumbnail/' . $imageName;
                $res[] = $data;

            }
        }

        return Response::json(['success' => true, 'data' => $res]);
    }

    public function deleteImage($imageName, $imageType)
    {

        $image = $imageName . '.' . $imageType;
        $directoryPath = public_path() . '/images/services/' . $image;
        $directoryPathThumbnail = public_path() . '/images/services/thumbnail/' . $image;
        $res = 0;
        //echo $directoryPath;die;
        if (file_exists($directoryPath)) {
            File::delete($directoryPath);
            $res = 1;
        }

        if (file_exists($directoryPathThumbnail)) {
            File::delete($directoryPathThumbnail);
            $res = 1;
        }

        if ($res) {
            return Response::json(
                [
                    'success' => true,
                    'message' => 'The image has been removed.',
                ]
            );
        } else {
            return Response::json(
                [
                    'success' => false,
                    'message' => 'Some problem occurred, please try again.',
                ]
            );
        }
    }

    public function getService(Request $request, $id = '')
    {
        getMetaTag('microjobs');
        $userId = request()->get('user_id');
        $categories = Category::where('is_active', '=', 1)->where('category_id', null)->get();
        return View('service.service', ['categories' => $categories, 'category_id' => $id, 'search' => ($request->search) ? $request->search : '',
            'tag' => ($request->tag) ? $request->tag : '', 'user_id' => $userId]);
    }

    public function getUserServiceList(Request $request)
    {

        $services = Service::with('user', 'category', 'tags')->where('is_custom', '=', 0)->where('created_by', $request->user_id);
        $services = $services->paginate(3);
        $html = \View::make('service.service-list', ['services' => $services])->render();
        return Response::json(['success' => true, 'html' => $html, 'count' => count($services)]);
    }
    /*
    get service list
     */
    public function getServiceList(Request $request)
    {
        $perpage = 6;
        $services = Service::with('user', 'category', 'tags')
            ->leftjoin('service_ratings', 'services.id', '=', 'service_ratings.service_id')
            ->leftjoin('service_views', 'services.id', '=', 'service_views.service_id')
            ->select('services.*', DB::raw('COUNT(service_views.service_id) as service_viewer'))
            ->selectRaw('AVG(service_ratings.rating) AS average_rating')
            ->groupBy('services.id')
            ->where('is_custom', '=', 0);
        //->where('is_active', '=', 1);
        if (!empty($request->category_id)) {
            $services->where('category_id', $request->category_id);
        }

        if (!empty($request->search)) {
            $services->where('title', 'like', '%' . $request->search . '%');
        }
        if (!empty($request->tag)) {
            $services->whereHas('tags', function ($q) use ($request) {
                $q->where('name', 'like', '%' . $request->tag . '%');
            });
        }

        if (!empty($request->user_id)) {
            $perpage = 3;
            $services->where('created_by', $request->user_id);
        }

        if (!empty($request->sorting)) {

            if ($request->sorting == 'newest') {
                $services->orderBy('id', 'desc');
            } elseif ($request->sorting == 'oldest') {
                $services->orderBy('id', 'asc');
            } elseif ($request->sorting == 'hightest_rating') {
                $services->orderByDesc('average_rating');
            } elseif ($request->sorting == 'best_seller') {

            } elseif ($request->sorting == 'featured') {

            } elseif ($request->sorting == 'hightest_views') {
                $services->orderBy('service_viewer', 'desc');
            } elseif ($request->sorting == 'low_to_high') {
                $services->orderBy('price', 'asc');
            } else {
                $services->orderBy('price', 'desc');
            }
        } else {
            $services->orderBy('id', 'desc');
        }
        $services = $services->paginate($perpage);

        $html = \View::make('service.service-list', ['services' => $services])->render();
        return Response::json(['success' => true, 'html' => $html, 'count' => count($services)]);
    }

    public function myGig()
    {
        getMetaTag('My Gig');
        return View('myGig.index');
    }

    public function myGigList(Request $request)
    {
        $services = Service::with('user', 'category', 'tags')->where('is_custom', '=', 0);
        // ->where('is_active', '=', 1);

        $services->where('created_by', Auth::user()->id);
        $services = $services->latest()->paginate(6);
        $html = \View::make('myGig.gig-list', ['services' => $services])->render();
        return Response::json(['success' => true, 'html' => $html, 'count' => count($services)]);
    }

    public function saveTag(Request $request)
    {

        $tag = Tag::where('name', $request->name)->first();
        if (!empty($tag)) {
            return Response::json(['success' => false, 'message' => 'Tag Already Exist.']);
        }
        $tagModel = new Tag();
        $tagModel->name = $request->name;
        if ($tagModel->save()) {
            return Response::json(['success' => true, 'message' => 'Tag Added Successfully.', 'data' => $tagModel]);
        }
    }

    public function serviceDetail($service_id)
    {

        $service = Service::with(['category', 'extras', 'images', 'tags', 'user', 'rating'])->where('id', $service_id)->first();
        getMetaTag($service->title);
        return view('service.service-detail', ['service' => $service]);
    }

    public function checkoutDetail(Request $request)
    {
        if (Auth::check()) {
            $sid = request()->sid;
            if (request()->has('extra_ids')) {
                $extra_ids = request()->extra_ids;
            } else {
                $extra_ids = [];
            }
            $service = Service::with(['category', 'extras', 'images', 'tags', 'user', 'rating'])->where('id', $sid)->first();
            $sc = number_format((getSettings('service_charge') / 100), 2);
            //echo '<pre>';print_r($service);die;
            return view('service.checkout-detail', ['service' => $service, 'extra_ids' => $extra_ids, 'sc' => $sc]);
        } else {
            return redirect('login');
        }

    }

    public function directCheckout(Request $request)
    { //print_r($request->all());die;
        $user = Auth::user();
        $subtotal = number_format($request->subtotal, 2);
        $tax = number_format($request->tax, 2);
        $total_amount = number_format($request->total_amount, 2);
        $total_pay = number_format($request->total_pay, 2);
        $use_wallet = $request->use_wallet;
        $sid = $request->sid;
        $is_discount = $request->is_discount;
        $discount_amt = number_format($request->discount_amt, 2);
        $coupon_id = $request->coupon_id;
        $is_custom_offer = $request->is_custom_offer;
        $custom_job_id = $request->custom_job_id;

        if (!empty($request->extra_ids)) {
            $extra_id = explode(',', $request->extra_ids);
        } else {
            $extra_id = [];
        }

        $order_id = getNextOrderNumber();
        $transaction_id = generateRandomString(24);
        // prepare order data
        $order = [
            'order_id' => $order_id,
            'user_id' => $user->id,
            'subtotal' => $subtotal,
            'tax' => $tax,
            'total' => $total_amount,
            'status' => 1,
            'transaction_id' => $transaction_id,
            'payment_type' => 'wallet',
        ];

        // store order
        $orders = Order::create($order);

        // get service detail and extra services
        $service = Service::find($sid);

        $extra_services = [];
        if (!empty($service->extras)) {
            foreach ($service->extras as $key => $value) {
                if (in_array($value->id, $extra_id)) {
                    $extra_services[] = [
                        'id' => $value->id,
                        'price' => number_format($value->price, 2),
                    ];
                }
            }
        }

        // prepare order details
        $order_details = [
            'user_id' => $service->created_by,
            'order_id' => $orders->id,
            'product_id' => $service->id,
            'extra_services' => json_encode($extra_services),
            'quantity' => 1,
            'price' => number_format($service->price, 2),
            'created_at' => date('Y-m-d H:i:s'),
        ];

        // store order details
        OrderDetail::create($order_details);

        $service_charge = (is_numeric(getSettings('service_charge')) && !empty(getSettings('service_charge'))) ? getSettings('service_charge') : 0;

        // record for transactions and wallet
        try {
            storeTransactions($user->id, $orders->id, $discount_amt, $transaction_id, $total_amount, $service_charge, $subtotal, $total_pay, $use_wallet);
        } catch (Exception $e) {

        }

        try {
            // Mail
            Mail::to($user->email)->send(new PaymentNotification($user, $orders->id, '1', $transaction_id, 'wallet'));
        } catch(\Exception $e){
            // Get error here
        }
        
        // Mail to admin
        $admin = getAdminInfo();
        $order = Order::where('id', $orders->id)->first();
        $service = $order->detail->service->title;
        $data['username'] = $user->name;
        $data['service'] = $service;
        $data['order_link'] = 'invoice-detail/' . base64_encode($order->id);
        $data['subject'] = 'Order invoice mail';
        $data['to_email'] = $admin->email;
        sendMail($data, 'admin-order-invoice');
        // Mail to owner
        $data['username'] = $order->detail->service->user->name;
        $data['to_email'] = $order->detail->service->user->email;
        sendMail($data, 'owner-service-mail');
        if ($orders->id) {

            // IF CUSTOM OFFER UPDATE CUSTOM JOB STATUS
            if ($is_custom_offer == true) {
                JobRequest::where('id', '=', $custom_job_id)
                    ->update(['custom_offer_status' => 5, 'order_id' => $orders->id]);

                Service::where('id', '=', $sid)
                    ->update(['is_approved' => 2]);
            }

            Session::put('order_id', $orders->id);
            Session::put('use_wallet', $use_wallet);
            Session::put('total_pay', $total_pay);
            Session::put('coupon_id', $coupon_id);
            return response()->json([
                'success' => true,
                'msg' => 'Order saved successfully.',
                'order_id' => encrypt($orders->id),
            ]);
        } else {
            return response()->json([
                'success' => false,
                'msg' => 'Order not saved. There is some problem please try again.',
                'order_id' => '',
            ]);
        }
    }

    public function paypalCheckout(Request $request)
    {
        //print_r($request->all());die;
        $user = Auth::user();
        $subtotal = number_format($request->subtotal, 2);
        $tax = number_format($request->tax, 2);
        $total_amount = number_format($request->total_amount, 2);
        $total_pay = number_format($request->total_pay, 2);
        $use_wallet = $request->use_wallet;
        $sid = $request->sid;
        $is_discount = $request->is_discount;
        $discount_amt = number_format($request->discount_amt, 2);
        $coupon_id = $request->coupon_id;
        $is_custom_offer = $request->is_custom_offer;
        $custom_job_id = $request->custom_job_id;

        if (!empty($request->extra_ids)) {
            $extra_id = explode(',', $request->extra_ids);
        } else {
            $extra_id = [];
        }

        $order_id = getNextOrderNumber();
        // prepare order data
        $order_data = [
            'order_id' => $order_id,
            'user_id' => $user->id,
            'subtotal' => number_format($subtotal, 2),
            'tax' => number_format($tax, 2),
            'discount' => number_format($discount_amt, 2),
            'total' => number_format(($total_amount + $discount_amt), 2),
            'payment_type' => 'paypal',
            'created_at' => date('Y-m-d H:i:s'),
        ];

        // store order
        $orders = Order::create($order_data);

        $data = [];
        $order_items = [];

        // get order items and save itno order detail
        $service = Service::find($sid);

        $order_items[] = [
            'name' => $service->title,
            'price' => number_format($service->price, 2),
            'qty' => 1,
        ];

        $extra_services = [];
        if (!empty($service->extras)) {
            foreach ($service->extras as $key => $value) {
                if (in_array($value->id, $extra_id)) {
                    $order_items[] = [
                        'name' => $value->service_name,
                        'price' => number_format($value->price, 2),
                        'qty' => 1,
                    ];

                    $extra_services[] = [
                        'id' => $value->id,
                        'price' => number_format($value->price, 2),
                    ];
                }
            }
        }

        // prepare order details
        $order_details = [
            'user_id' => $service->created_by,
            'order_id' => $orders->id,
            'product_id' => $service->id,
            'extra_services' => json_encode($extra_services),
            'quantity' => 1,
            'price' => number_format($service->price, 2),
            'created_at' => date('Y-m-d H:i:s'),
        ];

        // store order details
        OrderDetail::create($order_details);

        if ($use_wallet == true) {
            if ($total_pay > 0) {
                $discount = $total_amount - $total_pay;
                $order_items[] = [
                    'name' => 'Gigstoc Wallet Discount',
                    'price' => '-' . $discount,
                    'qty' => '1',
                ];
            }
        }

        if ($is_discount) {
            $order_items[] = [
                'name' => 'Gigstoc Coupon Discount',
                'price' => '-' . $discount_amt,
                'qty' => '1',
            ];
        }

        $data['items'] = $order_items;

        $data['invoice_id'] = 'GIG' . sprintf('%09d', $orders->id);
        $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
        $data['return_url'] = route('paypal.success');
        $data['cancel_url'] = route('paypal.cancel');
        $data['notify_url'] = route('paypal.notify');

        $service_charge = (is_numeric(getSettings('service_charge')) && !empty(getSettings('service_charge'))) ? getSettings('service_charge') : 0;

        $total = 0;
        foreach ($data['items'] as $item) {
            $total += $item['price'] * $item['qty'];
        }

        $data['tax'] = ($subtotal * $service_charge) / 100;
        $data['subtotal'] = number_format($total, 2);
        $data['total'] = number_format(($data['tax'] + $data['subtotal']), 2);
        $data['custom'] = $use_wallet . '||' . $coupon_id . '||' . $discount_amt . '||' . $subtotal . '||' . $is_custom_offer . '||' . $custom_job_id;

        //print_r($data);die;
        $cart_data_in_json = json_encode($data);

        // update cart data in order table
        $cart_update = Order::find($orders->id);
        $cart_update->cart_data = $cart_data_in_json;
        $cart_update->save();

        $provider = new ExpressCheckout;

        $response = $provider->setExpressCheckout($data);

        //print_r($response);die;

        if ($response['ACK'] == 'Success' || $response['ACK'] == 'SuccessWithWarning') {
            Session::put('order_id', $orders->id);
            return response()->json([
                'success' => true,
                'msg' => 'Order saved successfully.',
                'paypal_link' => $response['paypal_link'],
            ]);
        } else {
            Session::forget('order_id');
            Order::destroy($orders->id);
            return response()->json([
                'success' => false,
                'msg' => $response['L_LONGMESSAGE0'],
            ]);
        }
    }

    public function success(Request $request)
    {
        $provider = new ExpressCheckout;
        $response = $provider->getExpressCheckoutDetails($request->token);

        $order_id = Session::get('order_id');
        $discount_amt = 0;
        $is_custom_offer = 0;
        $separator = '||';
        if ($response['CUSTOM'] && $response['CUSTOM'] != '') {
            $explodedCustomValue = explode($separator, $response['CUSTOM']);
            if (($explodedCustomValue[1]) && !empty($explodedCustomValue[1])) {
                $coupon_id = $explodedCustomValue[1];
            }

            if (($explodedCustomValue[2]) && !empty($explodedCustomValue[2])) {
                $discount_amt = number_format($explodedCustomValue[2], 2);
            }

            if (($explodedCustomValue[3]) && !empty($explodedCustomValue[3])) {
                $subtotal_amt = number_format($explodedCustomValue[3], 2);
            }

            if (($explodedCustomValue[4]) && !empty($explodedCustomValue[4])) {
                $is_custom_offer = $explodedCustomValue[4];
            }

            if (($explodedCustomValue[5]) && !empty($explodedCustomValue[5])) {
                $custom_job_id = $explodedCustomValue[5];
            }
        }

        $cart = Order::find($order_id);
        $data = json_decode($cart->cart_data, true);

        if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
            $payer_id = $response['PAYERID'];
            $token = $response['TOKEN'];
            $transaction_detail = $provider->doExpressCheckoutPayment($data, $token, $payer_id);
            $request_payment = $response['L_PAYMENTREQUEST_0_AMT0'];

            $total_pay = number_format($response['AMT'], 2);
            $tax = number_format($response['TAXAMT'], 2);
            $cart_total = number_format($subtotal_amt, 2) + number_format($tax, 2);
            $use_wallet = $response['CUSTOM'];
            if ($use_wallet == true) {
                $subtotal = $subtotal_amt;
            } else {
                $subtotal = $subtotal_amt;
            }
            //$service_charge = number_format(($tax / $request_payment) * 100, 2);
            $service_charge = (is_numeric(getSettings('service_charge')) && !empty(getSettings('service_charge'))) ? getSettings('service_charge') : 0;

            // update order status and transaction id
            $payment_status = $transaction_detail['ACK'];
            $transaction_id = $transaction_detail['PAYMENTINFO_0_TRANSACTIONID'];

            $status = ($payment_status == 'Success') ? '1' : '2';
            $order_data = [
                'status' => $status,
                'transaction_id' => $transaction_id,
                'payment_type' => 'paypal',
                'payment_history' => json_encode($transaction_detail),
            ];

            // record for transactions and wallet
            try {
                storeTransactions($cart->user_id, $order_id, $discount_amt, $transaction_id, $cart_total, $service_charge, $subtotal, $total_pay, $use_wallet);
            } catch (Exception $e) {

            }

            if ($discount_amt > 0) {
                // Cpupon applied in order store
                OrderCoupon::create(['order_id' => $order_id, 'coupon_id' => $coupon_id, 'discount_amount' => $discount_amt]);

                //coupon usage count
                Coupon::where('id', $coupon_id)
                    ->update([
                        'current_usage' => DB::raw('current_usage+1'),
                    ]);
            }

            // Mail
            $postedUser = User::find($cart->user_id);
            
            try {
                Mail::to($postedUser->email)->send(new PaymentNotification($postedUser, $order_id, $status, $transaction_id, 'paypal'));
            } catch(\Exception $e){
                // Get error here
            }

            // Mail to admin
            $admin = getAdminInfo();
            $order = Order::where('id', $order_id)->first();
            $service = $order->detail->service->title;
            $data['username'] = $postedUser->name;
            $data['service'] = $service;
            $data['order_link'] = 'invoice-detail/' . base64_encode($order->id);
            $data['subject'] = 'Order invoice mail';
            $data['to_email'] = $admin->email;
            sendMail($data, 'admin-order-invoice');

            // Mail to owner
            $data['username'] = $order->detail->service->user->name;
            $data['to_email'] = $order->detail->service->user->email;
            sendMail($data, 'owner-service-mail');

            // IF CUSTOM OFFER UPDATE CUSTOM JOB STATUS
            if ($is_custom_offer == true) {
                JobRequest::where('id', '=', $custom_job_id)
                    ->update(['custom_offer_status' => 5, 'order_id' => $order->id]);

                Service::where('id', '=', $order->detail->service->id)
                    ->update(['is_approved' => 2]);
            }
        } else {
            // update order status and transaction id
            $payment_status = $transaction_detail['ACK'];
            $transaction_id = $transaction_detail['PAYMENTINFO_0_TRANSACTIONID'];

            $status = ($payment_status == 'Success') ? '1' : '2';
            $order_data = [
                'status' => $status,
                'transaction_id' => $transaction_id,
                'payment_type' => 'paypal',
                'payment_history' => json_encode($response),
            ];

            // Mail
            $postedUser = User::find($cart->user_id);
            try {
                Mail::to($postedUser->email)->send(new PaymentNotification($postedUser, $order_id, $status, $transaction_id, 'paypal'));
            } catch(\Exception $e){
                // Get error here
            }
        }

        // update order status
        $order_update = Order::find($order_id)->update($order_data);
        return redirect('thank-you/' . encrypt($order_id));
    }

    public function thank_you($order_id)
    {
        $order_id = decrypt($order_id);
        $order_detail = Order::find($order_id);

        // send notification
        if ($order_detail->detail->service->user->notification_subscription == 1) {
            $message = '<strong>' . Auth::user()->name . '</strong> ordered your Job <strong>' . $order_detail->detail->service->title . '</strong>';
            $url = 'invoice-detail/' . base64_encode($order_id);
            $fromId = Auth::user()->id;
            $toId = $order_detail->detail->service->created_by;
            saveNotification($message, $url, $toId, $fromId);
        }
        if (!empty($order_detail->transaction_id)) {
            $transaction_id = $order_detail->transaction_id;
        } else {
            $transaction_id = '';
        }

        $status = $order_detail->status;
        Session::forget('order_id');
        Session::forget('use_wallet');
        Session::forget('total_pay');
        Session::forget('coupon_id');
        return view('thank-you', compact('order_id', 'transaction_id', 'status'));
    }

    /**
     * Retrieve IPN Response From PayPal
     *
     * @param \Illuminate\Http\Request $request
     */
    public function notify(Request $request)
    {
        $provider = new ExpressCheckout;

        $request->merge(['cmd' => '_notify-validate']);
        $post = $request->all();

        $response = (string) $provider->verifyIPN($post);

        if ($response === 'VERIFIED') {
            // Your code goes here ...
            file_put_contents(__DIR__ . '/notify.txt', print_r($post, true));
        }
    }

    public function cancel(Request $request)
    {
        //$provider = new ExpressCheckout;
        //$response = $provider->getExpressCheckoutDetails($request->token);
        //echo '<pre>';
        //print_r($response);die;
        $order_id = Session::get('order_id');

        Order::where('id', $order_id)->update(['status' => 2]);
        Session::forget('order_id');
        Session::forget('use_wallet');
        Session::forget('total_pay');
        Session::forget('coupon_id');
        Session::forget('is_custom_offer');
        Session::forget('custom_job_id');
        return view('cancel');
    }

    public function couponApply(Request $request)
    {
        $user = Auth::user();
        $subtotal = $request->subtotal;
        $tax = $request->tax;
        $total_amount = $request->total_amount;
        $use_wallet = $request->use_wallet;
        $sid = $request->sid;
        $coupon_code = $request->coupon_code;

        $coupon = Coupon::where('is_active', '=', 1)->where('coupon', '=', $coupon_code)->first();
        //print_r($coupon);die;
        $discount_amount = 0;
        $coupon_id = 0;
        $success = false;
        $msg = 'Invalid Code';
        if (empty($coupon)) {
            $msg = 'Invalid Code';
            $success = false;
        } else if (date('Y-m-d') > $coupon->end_date) {
            $msg = 'Code has been expired';
            $success = false;
        } else if ($coupon->total_usage <= $coupon->current_usage) {
            $msg = 'Code does not exist';
            $success = false;
        } else {
            if ($coupon->type == 'flat') {
                $discount_amount = number_format($coupon->amount, 2);
                $total_amount = $total_amount - $discount_amount;
                $subtotal = number_format(($subtotal - $discount_amount), 2);
            } else if ($coupon->type == 'percenatge') {
                $discount_amount = number_format(($total_amount * $coupon->amount) / 100, 2);
                $total_amount = $total_amount - $discount_amount;
                $subtotal = number_format(($subtotal - $discount_amount), 2);
            }

            $coupon_id = $coupon->id;
            $total_amount = number_format($total_amount, 2);
            $msg = 'Coupon code applied successfully';
            $success = true;
        }

        return Response::json(['success' => $success, 'msg' => $msg, 'total' => (float) $total_amount, 'subtotal' => (float) $subtotal, 'tax' => (float) $tax, 'discount' => (float) (number_format($discount_amount, 2)), 'code' => $coupon_code, 'coupon_id' => $coupon_id]);
    }

    public function saveOrder(Request $request)
    {
        $user = Auth::user();
        $subtotal = $request->subtotal;
        $tax = $request->tax;
        $total_amount = $request->total_amount;
        $total_pay = $request->total_pay;
        $use_wallet = $request->use_wallet;
        $sid = $request->sid;
        $is_discount = $request->is_discount;
        $discount_amt = $request->discount_amt;
        $coupon_id = $request->coupon_id;
        $is_custom_offer = $request->is_custom_offer;
        $custom_job_id = $request->custom_job_id;

        if (!empty($request->extra_ids)) {
            $extra_id = explode(',', $request->extra_ids);
        } else {
            $extra_id = [];
        }

        $order_id = getNextOrderNumber();
        // prepare order data
        $order_data = [
            'order_id' => $order_id,
            'user_id' => $user->id,
            'subtotal' => number_format($subtotal, 2),
            'tax' => number_format($tax, 2),
            'discount' => number_format($discount_amt, 2),
            'total' => number_format(($total_amount + $discount_amt), 2),
            'payment_type' => 'stripe',
            'created_at' => date('Y-m-d H:i:s'),
        ];

        // store order
        $order = Order::create($order_data);

        // get order items and save itno order detail
        $service = Service::find($sid);

        $extra_services = [];
        if (!empty($service->extras)) {
            foreach ($service->extras as $key => $value) {
                if (in_array($value->id, $extra_id)) {
                    $extra_services[] = [
                        'id' => $value->id,
                        'price' => number_format($value->price, 2),
                    ];
                }
            }
        }

        // prepare order details
        $order_details = [
            'user_id' => $service->created_by,
            'order_id' => $order->id,
            'product_id' => $service->id,
            'extra_services' => json_encode($extra_services),
            'quantity' => 1,
            'price' => number_format($service->price, 2),
            'created_at' => date('Y-m-d H:i:s'),
        ];

        // store order details
        OrderDetail::create($order_details);

        if ($order_id) {
            Session::put('order_id', $order->id);
            Session::put('use_wallet', $use_wallet);
            Session::put('total_pay', $total_pay);
            Session::put('coupon_id', $coupon_id);
            Session::put('is_custom_offer', $is_custom_offer);
            Session::put('custom_job_id', $custom_job_id);
            return response()->json([
                'success' => true,
                'msg' => 'Order saved successfully.',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'msg' => 'Order not saved. There is some problem please try again.',
            ]);
        }
    }

    public function stripeCheckout()
    {
        if (Session::has('order_id')) {
            $order_id = Session::get('order_id');
            $use_wallet = Session::get('use_wallet');
            $total_pay = Session::get('total_pay');
            $coupon_id = Session::get('coupon_id');
            $is_custom_offer = Session::get('is_custom_offer');
            $custom_job_id = Session::get('custom_job_id');

            $user = Auth::user();
            $get_order = Order::find($order_id);

            $total_amt = number_format($get_order->total, 2);
            $subtotal = number_format($get_order->subtotal, 2);
            $tax = number_format($get_order->tax, 2);
            $discount = number_format($get_order->discount, 2);
            $service_charge = (is_numeric(getSettings('service_charge')) && !empty(getSettings('service_charge'))) ? getSettings('service_charge') : 0;

            if ($use_wallet == true) {
                $cart_total = number_format($total_pay, 2);
            } else {
                $cart_total = number_format($total_amt, 2) - number_format($discount, 2);
            }
            $cart_total = number_format($cart_total, 2);

            Stripe::setApiKey(env('STRIPE_SECRET'));

            $intent = PaymentIntent::create([
                'amount' => $cart_total * 100,
                'description' => 'Software development services',
                'currency' => 'usd',
                'metadata' => [
                    'user_id' => $user->id,
                    'order_id' => $order_id,
                    'use_wallet' => $use_wallet,
                    'total_pay' => $total_pay,
                    'total_amt' => $total_amt,
                    'discount_amt' => $discount,
                    'coupon_id' => $coupon_id,
                    'subtotal' => $subtotal,
                    'tax' => $tax,
                    'is_custom_offer' => $is_custom_offer,
                    'custom_job_id' => $custom_job_id,
                ],
            ]);

            return view('service.stripe-checkout', compact('intent', 'user', 'cart_total', 'use_wallet', 'total_pay'));
        } else {
            return redirect()->back();
        }
    }

    public function makeStripePayment(Request $request)
    {
        $intent_id = $request->payment_intent_id;
        $payment_mehtod_id = $request->payment_mehtod_id;
        $token = $request->token;

        // get payment intent object for transaction id and metadata like order id
        Stripe::setApiKey(env('STRIPE_SECRET'));
        $payment_intent = PaymentIntent::retrieve($intent_id);

        $cart_total = number_format($payment_intent->amount / 100, 2);

        $service_charge = (is_numeric(getSettings('service_charge')) && !empty(getSettings('service_charge'))) ? getSettings('service_charge') : 0;

        $user_id = $payment_intent->metadata->user_id;
        $total_amt = $payment_intent->metadata->total_amt;
        $tax = $payment_intent->metadata->tax;
        $subtotal = $payment_intent->metadata->subtotal;
        $discount_amt = $payment_intent->metadata->discount_amt;
        $is_custom_offer = $payment_intent->metadata->is_custom_offer;
        $custom_job_id = $payment_intent->metadata->custom_job_id;

        // get payment method object for card data
        $payment_method = PaymentMethod::retrieve($payment_mehtod_id);

        $curl = new \Stripe\HttpClient\CurlClient([CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1]);
        \Stripe\ApiRequestor::setHttpClient($curl);

        // This creates a new Customer
        $customer = Customer::create([
            'email' => $request->email,
            'name' => $request->name,
        ]);

        $customer_id = $customer['id'];

        // store customer_id into user table for future user
        User::find($user_id)->update(['stripe_customer_id' => $customer_id]);

        // new card to the customer
        $card = $customer->sources->create(
            array(
                'source' => $token,
                'metadata' => ['user_id' => $user_id],
            )
        );

        // update order status and transaction id
        $order_id = $payment_intent->metadata->order_id;
        $transaction_id = $payment_intent->charges->data[0]->id;
        $payment_status = $payment_intent->status;
        $use_wallet = $payment_intent->metadata->use_wallet;
        $total_pay = $payment_intent->metadata->total_pay;
        $coupon_id = $payment_intent->metadata->coupon_id;

        $status = ($payment_status == 'succeeded') ? '1' : '2';

        $order_data = [
            'status' => $status,
            'transaction_id' => $transaction_id,
            'payment_type' => 'stripe',
            'payment_history' => $payment_intent->toJSON(),
        ];

        // update Order status
        Order::where('id', $order_id)->update($order_data);

        // record for transactions and wallet
        try {
            storeTransactions($user_id, $order_id, $discount_amt, $transaction_id, $total_amt, $service_charge, $subtotal, $total_pay, $use_wallet);
        } catch (Exception $e) {

        }

        $order_id = $order_id . 'GIG' . sprintf('%09d', $user_id);

        // Mail
        $postedUser = User::find($user_id);
        try {
            Mail::to($postedUser->email)->send(new PaymentNotification($postedUser, $order_id, $status, $transaction_id, 'stripe'));
        } catch(\Exception $e){
            // Get error here
        }

        // Mail to admin
        $admin = getAdminInfo();
        $order = Order::where('id', $order_id)->first();
        $service = $order->detail->service->title;
        $data['username'] = $postedUser->name;
        $data['service'] = $service;
        $data['order_link'] = 'invoice-detail/' . base64_encode($order->id);
        $data['subject'] = 'Order Invoice Mail';
        $data['to_email'] = $admin->email;
        sendMail($data, 'admin-order-invoice');

        // Mail to owner
        $data['username'] = $order->detail->service->user->name;
        $data['to_email'] = $order->detail->service->user->email;
        sendMail($data, 'owner-service-mail');

        // IF CUSTOM OFFER UPDATE CUSTOM JOB STATUS
        if ($is_custom_offer == true) {
            JobRequest::where('id', '=', $custom_job_id)
                ->update(['custom_offer_status' => 5, 'order_id' => $order->id]);

            Service::where('id', '=', $order->detail->service->id)
                ->update(['is_approved' => 2]);
        }

        if ($status == '1') {
            if ($discount_amt > 0) {
                // Cpupon applied in order store
                OrderCoupon::create(['order_id' => $order_id, 'coupon_id' => $coupon_id, 'discount_amount' => $discount_amt]);

                //coupon usage count
                Coupon::where('id', $coupon_id)
                    ->update([
                        'current_usage' => DB::raw('current_usage+1'),
                    ]);
            }

            return response()->json([
                'success' => true,
                'status' => 200,
                'msg' => 'Thank you! Your order has been placed successfully.',
                'order_id' => encrypt($order_id),
            ]);
        } else {
            return response()->json([
                'success' => false,
                'status' => 200,
                'msg' => 'Oops! Your order has been placed canceled because your transaction has been failed with transaction no.' . $transaction_id,
                'order_id' => encrypt($order_id),
            ]);
        }
    }

    public function addRemoveBookmark(Request $request)
    {
        $bookmark = BookMark::where(['service_id' => $request->service_id, 'user_id' => Auth::user()->id])->first();
        if (empty($bookmark)) {
            $bookmark = new BookMark();
            $bookmark->user_id = Auth::user()->id;
            $bookmark->service_id = $request->service_id;
            $bookmark->is_bookmarked = 1;
            $bookmark->save();
            return Response::json(['success' => true, 'message' => "Service added to bookmark.", 'type' => 'remove']);
        } else {
            BookMark::where(['service_id' => $request->service_id, 'user_id' => Auth::user()->id])->delete();
            return Response::json(['success' => true, 'message' => "Service removed from bookmark.", 'type' => 'add']);
        }
    }

    public function getCountryNames(Request $request)
    {
        $list = [];
        $countrys = Country::get();
        $list[] = "<option value=''>Select Country</option>";
        foreach ($countrys as $country) {
            $option = "<option value='" . $country->sortname . "' >$country->name</option>";
            if ($country->sortname == 'US') {
                $option = "<option value='" . $country->sortname . "' selected='selected'>$country->name</option>";

            }
            $list[] = $option;
        }
        return Response::json(['success' => true, 'message' => '', 'list' => $list]);
    }

    public function viewService(Request $request)
    {
        $serviceView = ServiceView::where(['ip_address' => $request->ip_address, 'service_id' => $request->service_id])->first();
        if (empty($serviceView)) {
            $serviceView = new ServiceView();
            $serviceView->ip_address = $request->ip_address;
            $serviceView->service_id = $request->service_id;
            $serviceView->save();
        }
    }

    public function saveServiceRating(SaveRatingRequest $request)
    {
        $serviceRating = new ServiceRating();
        $serviceRating->user_id = Auth::user()->id;
        $serviceRating->service_id = $request->service_id;
        $serviceRating->rating = $request->rating;
        $serviceRating->custom_offer = $request->custom_offer;
        $serviceRating->review = $request->review;
        if ($serviceRating->save()) {
            if ($serviceRating->service->user->notification_subscription == 1) {
                $message = '<strong>' . Auth::user()->name . '</strong> has given rating for your Job <strong>' . $serviceRating->service->title . '</strong>';
                $url = 'service/' . $serviceRating->service->id;
                $toid = $serviceRating->service->created_by;
                $fromId = Auth::user()->id;
                saveNotification($message, $url, $toid, $fromId);
            }
        }
        return Response::json(['success' => true, 'message' => 'Rating Given Successfully.']);
    }

    public function markAsDelivered(Request $request)
    {
        $order_id = $request->order_id;
        try {
            Order::where(['id' => $request->order_id])->update(['delivered' => 1]);

            $order = Order::where(['id' => $order_id])->first();
            // notification
            
            if ($order->detail->service->user->notification_subscription == 1) {
                $message = 'Your order for Job <strong>' . $order->detail->service->title . '</strong> has been delivered to <strong>' . getUsername(Auth::user()->id) . '</strong>.';
                $url = 'invoice-detail/' . base64_encode($order->id);
                $toid = $order->detail->service->created_by;
                $fromId = Auth::user()->id;
                saveNotification($message, $url, $toid, $fromId);
            }

            return Response::json(['success' => true, 'message' => 'Order As Successfully Delivered']);
            
        } catch (Exception $ex) {
            return Response::json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }
}
