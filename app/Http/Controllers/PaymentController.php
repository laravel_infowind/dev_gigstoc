<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Response;
use View;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    { //print_r(\Request::ip());die;
	    getMetaTag('My Invoice');
        return view('invoice.index');

    }

    public function getInvoiceList(Request $request)
    {
        //print_r($request->all());die;
        $myOrders = Order::where(['user_id' => Auth::user()->id]);
        if (!empty($request->status)) {
            $myOrders->where('status', $request->status);
            if ($request->status == 'pending') {
                $myOrders->where('status', 0);
            }
        }
        if (!empty($request->payment_type)) {
            $myOrders->where('payment_type', $request->payment_type);
        }
        if (!empty($request->type) && $request->type == 'date') {
            if ($request->sort == 'desc') {
                $myOrders->orderBy('created_at', 'DESC');
            } else {
                $myOrders->orderBy('created_at', 'ASC');
            }
        }
        if (!empty($request->type) && $request->type == 'total') {
            if ($request->sort == 'desc') {
                $myOrders->orderBy('total', 'DESC');
            } else {
                $myOrders->orderBy('total', 'ASC');
            }
        }
        $myOrders = $myOrders->orderBY('id', 'desc')->paginate(10);
        $html = \View::make('invoice.invoice-list', ['myOrders' => $myOrders])->render();
        return Response::json(['success' => true, 'html' => $html]);
    }

    public function invoiceDetail($id)
    {
		getMetaTag('Invoice Detail');
        $order = Order::where(['id' => base64_decode($id)])->first();
        return view('invoice.detail', ['order' => $order]);
    }
}
