<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\ChatFile;
use App\Models\ChatMessage;
use App\Models\ChatRoom;
use App\Models\JobRequest;
use App\User;
use Auth;
use DB;
use File;
use Illuminate\Http\Request;
use Response;
use Validator;
use View;
use Illuminate\Support\Facades\Crypt;

class ChatController extends Controller
{
    public function index($room_id)
    {
		getMetaTag('Conversation By '.Auth::user()->name);
        //$room_id = base64_decode($room_id);
        $chat_room = ChatRoom::where('id', '=', $room_id)->first();

        $user_id = Auth::user()->id;

        // update read message
        ChatMessage::where('chat_room_id', '=', $room_id)
            ->where('to_id', '=', $user_id)
            ->update(['is_read' => 1]);

        if ($chat_room->from_id == $user_id) {
            $from_id = $user_id;
            $to_id = $chat_room->to_id;
        }

        if ($chat_room->to_id == $user_id) {
            $from_id = $user_id;
            $to_id = $chat_room->from_id;
        }

        // GET CUSTOM ORDERS
        $q = JobRequest::where('is_custom', '=', 1)
            ->where('custom_offer_status', '!=', 3)
            ->where('custom_offer_status', '!=', 4)
            ->where('custom_offer_status', '!=', 5);
        $q = $q->where(function($q) use ($from_id, $to_id) {
                $q->where(function($query) use ($from_id, $to_id){
                    $query->where('created_by', $from_id)
                          ->where('sent_to', $to_id);
                })
                ->orWhere(function($query) use ($to_id, $from_id) {
                    $query->where('created_by', $to_id)
                          ->where('sent_to', $from_id);
                });
            });
        $custom_orders = $q->latest()->get();
        
        return view('chat.index', compact('user_id', 'room_id', 'from_id', 'to_id', 'custom_orders'));
    }

    public function chatBody(Request $request, $room_id)
    {
        //$room_id = $request->get('room_id');
        $page_number = $request->get('page');

        $chat_room = ChatRoom::where('id', '=', $room_id)->first();
        $user_id = Auth::user()->id;

        $messages = ChatMessage::where('chat_room_id', '=', $room_id)
            ->with(['files', 'userTo', 'userFrom'])
            ->latest()->paginate(10);

        $msg_count = $messages->count();

        $timezone = (Auth::user()->timezone) ? Auth::user()->timezone : 'UTC';
        date_default_timezone_set($timezone);
        $messages_date_key = [];
        $all_messages = [];
        $last_chat_id = '';
        $offer = [];
        $custom_order = [];
        if (!empty($messages)) {
            foreach ($messages as $key => $value) {
                $chat_time = getTimeago(date('Y-m-d H:i:s', $value->time));
                $value->message = nl2br($value->message);
                $value->chat_time = $chat_time;

                if ($value->msg_type == 1) {
                    $offer = getService($value->custom_job_offer_id);
                } else if ($value->msg_type == 2) {
                    $custom_order = getCustomOrder($value->custom_job_offer_id);
                }

                $value->offer = $offer;
                $value->custom_order = $custom_order;

                $all_messages[$key] = $value;
                $last_chat_id = $value->id;

            }
            $all_messages = array_reverse($all_messages);
        }

        if ($page_number == '1') {
            $html = View::make('chat.msg-body', ['all_messages' => $all_messages, 'room_id' => $room_id, 'user_id' => $user_id])->render();
        } else {
            $html = View::make('chat.load-more-msg-body', ['all_messages' => $all_messages, 'room_id' => $room_id, 'user_id' => $user_id])->render();
        }

        return Response::json(['success' => true, 'html' => $html, 'page_number' => $page_number, 'last_chat_id' => $last_chat_id, 'msg_count' => $msg_count]);
    }

    public function sendMessage(Request $request)
    {
        try {
            $chat_room_id = $request->chat_room_id;
            $msg = trim($request->message);
            $message = replaceBadWord($msg); 
            $from_id = $request->from_id;
            $to_id = $request->to_id;
            $images = $request->images;

            $user_id = Auth::user()->id;

            $chat_data = [
                'chat_room_id' => $chat_room_id,
                'from_id' => $from_id,
                'to_id' => $to_id,
                'message' => $message,
                'msg_type' => 0,
                'time' => time(),
            ];

            $chat_id = ChatMessage::create($chat_data);

            $images = [];
            if ($chat_id) {
                if (!empty($request->images)) {
                    $files = json_decode($request->images, true);
                    $images[] = $files;
                    foreach ($files as $value) {
                        $files_in_chat = new ChatFile();
                        $files_in_chat->chat_room_id = $chat_room_id;
                        $files_in_chat->chat_message_id = $chat_id->id;
                        $files_in_chat->file_name = $value['name'] . '.' . $value['type'];
                        $files_in_chat->mime = $value['mime'];
                        $files_in_chat->save();
                    }
                }
            }

            // get last sent message
            $sent_message = ChatMessage::where('id', '=', $chat_id->id)
                ->with(['files', 'userTo', 'userFrom'])
                ->first();

            $timezone = (Auth::user()->timezone) ? Auth::user()->timezone : 'UTC';
            date_default_timezone_set($timezone);

            $html = '';

            $html = View::make('chat._sent_message', ['sent_message' => $sent_message, 'from_id' => $from_id, 'user_id' => $user_id, 'to_id' => $to_id, 'chat_room_id' => $chat_room_id, 'images' => $images])->render();

            return Response::json(['success' => true, 'chat_id' => $chat_id->id, 'message' => 'Message send successfully.', 'html' => $html]);
        } catch (Exception $ex) {
            return Response::json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }

    public function fileUpload(Request $request)
    {
        $data = [];
        $chat_room_id = $request->chat_room_id;
        if ($request->hasFile('images')) {

            $rules = [
                'images.*' => 'mimes:gif,jpg,jpeg,png,docx,doc,pdf|max:20000',
            ];

            $messages = [
                'images.*.mimes' => 'File extension invalid',
                'images.*.max' => 'Sorry! Maximum allowed size for an image is 20MB',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return Response::json(['success' => false, 'message' => $validator->errors()->all()]);
            }
            $res = [];
            foreach ($request->file('images') as $key => $value) {
                $name = time() . $key;
                $imageName = $name . '.' . $value->getClientOriginalExtension();

                $destinationPath = public_path('images/chat/' . $chat_room_id);

                $value->move($destinationPath, $imageName);

                $data['name'] = $name;
                $data['type'] = $value->getClientOriginalExtension();
                $data['mime'] = $value->getClientMimeType();
                $data['url'] = url('public/images/chat') . '/' . $chat_room_id . '/' . $imageName;
                $res[] = $data;

            }
        }

        return Response::json(['success' => true, 'data' => $res]);
    }

    public function deleteFile($imageName, $imageType, $chat_room_id)
    {
        $image = $imageName . '.' . $imageType;
        $directoryPath = public_path() . '/images/chat/' . $chat_room_id . '/' . $image;
        $res = 0;
        
        if (file_exists($directoryPath)) {
            File::delete($directoryPath);
            $res = 1;
        }
        if ($res) {
            return Response::json(
                [
                    'success' => true,
                    'message' => 'The file has been removed.',
                ]
            );
        } else {
            return Response::json(
                [
                    'success' => false,
                    'message' => 'Some problem occurred, please try again.',
                ]
            );
        }
    }

    public function checkUnreadCount(Request $request)
    {
        $chat_room_id = $request->chat_room_id;
        $from_id = $request->from_id;
        $to_id = $request->to_id;

        $user_id = Auth::user()->id;

        $check_unread = DB::select('SELECT c1.* FROM chat_messages c1 where to_id = ' . $user_id . ' and is_read = 0 and is_shown = 0 GROUP BY chat_room_id order by c1.id asc');

        $unread_thread = count($check_unread);
        $messages = [];
        $offer = [];
        $custom_order = [];
        $timezone = (Auth::user()->timezone) ? Auth::user()->timezone : 'UTC';
        date_default_timezone_set($timezone);
        if (!empty($check_unread)) {
            foreach ($check_unread as $key => $value) {
                if (($value->chat_room_id == $chat_room_id) && ($value->from_id != $user_id)) {
                    $images = ChatFile::where('chat_room_id', '=', $value->chat_room_id)
                        ->where('chat_message_id', '=', $value->id)
                        ->get();
                    $chat_time = getTimeago(date('Y-m-d H:i:s', $value->time));
                    $value->message = nl2br($value->message);
                    $value->chat_time = $chat_time;
                    $value->name = getUsername($value->from_id);
                    $value->userimage = getUserImage($value->from_id);
                    $value->files = $images;

                    if ($value->msg_type == 1) {
                        $offer = getService($value->custom_job_offer_id);
                    } else if ($value->msg_type == 2) {
                        $custom_order = getCustomOrder($value->custom_job_offer_id);
                    }
    
                    $value->offer = $offer;
                    $value->custom_order = $custom_order;
                    $messages[] = $value;

                    // update is_shown
                    ChatMessage::where('id', '=', $value->id)->update(['is_shown' => 1]);
                }
            }
        }

        $html = '';
        if (!empty($messages)) {
            $images = [];

            $html = View::make('chat._unread_message', ['all_messages' => $messages, 'from_id' => $from_id, 'user_id' => $user_id, 'to_id' => $to_id, 'chat_room_id' => $chat_room_id, 'images' => $images])->render();
        }

        return Response::json(['success' => true, 'unread_thread' => $unread_thread, 'html' => $html, 'from_id' => $from_id, 'to_id' => $to_id, 'user_id' => (string) $user_id, 'chat_room_id' => $chat_room_id]);
    }

    public function ChatNotification(Request $request)
    {
        $user_id = $request->user_id;

        $check_unread = DB::select('SELECT c1.*, c2.unread_count FROM chat_messages c1 INNER JOIN ( SELECT chat_room_id, MAX(created_at) AS max_created_at, COUNT(*) AS unread_count FROM chat_messages where ( to_id = ' . $user_id . ' OR from_id = ' . $user_id . ') GROUP BY chat_room_id ) c2 ON c1.chat_room_id = c2.chat_room_id AND c1.created_at = c2.max_created_at order by c1.id desc limit 5');

        $unread_count = 0;
        $unread_thread = count($check_unread);
        $messages = [];
        $timezone = (Auth::user()->timezone) ? Auth::user()->timezone : 'UTC';
        date_default_timezone_set($timezone);
        if (!empty($check_unread)) {
            foreach ($check_unread as $key => $value) {
                $chat_time = getTimeago(date('Y-m-d H:i:s', $value->time));
                $value->message = $value->message;
                $value->chat_time = $chat_time;
                if ($value->from_id == $user_id) {
                    $value->name = getUsername($value->to_id);
                    $value->userimage = getUserImage($value->to_id);
                    $value->profile_id = $value->to_id;
                } else {
                    $value->name = getUsername($value->from_id);
                    $value->userimage = getUserImage($value->from_id);
                    $value->profile_id = $value->from_id;
                }
                $messages[] = $value;
                
                if ($value->from_id != $user_id) {
                    if($value->is_read == 0) {
                        $unread_count++;
                    }
                }
            }
        }
        
        $html = '';
        if (!empty($messages)) {
            $images = [];

            $html = View::make('common._chat_notification', ['all_messages' => $messages, 'user_id' => $user_id])->render();
        }

        //print_r($messages);die;
        return Response::json(['success' => true, 'unread_thread' => $unread_thread, 'user_id' => $user_id, 'html' => $html, 'unread_count' => $unread_count]);
    }

    public function MyMessageList(Request $request)
    {
		getMetaTag('Chat List');
        if (!Auth::check()) {
            return redirect('login');
        } else {
            $user_id = Auth::user()->id;
            
            $check_unread = DB::select('SELECT c1.*, c2.unread_count FROM chat_messages c1 INNER JOIN ( SELECT chat_room_id, MAX(created_at) AS max_created_at, COUNT(*) AS unread_count FROM chat_messages where ( to_id = ' . $user_id . ' OR from_id = ' . $user_id . ') GROUP BY chat_room_id ) c2 ON c1.chat_room_id = c2.chat_room_id AND c1.created_at = c2.max_created_at order by c1.id desc');

            $unread_thread = count($check_unread);
            $messages = [];
            $timezone = (Auth::user()->timezone) ? Auth::user()->timezone : 'UTC';
            date_default_timezone_set($timezone);
            if (!empty($check_unread)) {
                foreach ($check_unread as $key => $value) {
                    $chat_time = getTimeago(date('Y-m-d H:i:s', $value->time));
                    $value->message = $value->message;
                    $value->chat_time = $chat_time;
                    
                    if ($value->from_id == $user_id) {
                        $value->name = getUsername($value->to_id);
                        $value->userimage = getUserImage($value->to_id);
                        $value->profile_id = $value->to_id;
                    } else {
                        $value->name = getUsername($value->from_id);
                        $value->userimage = getUserImage($value->from_id);
                        $value->profile_id = $value->from_id;
                    }
                    $messages[] = $value;
                }
            }
            
            return view('chat.my-message-list', compact('user_id', 'messages', 'unread_thread'));
        }

    }

    public function markAllAsRead(Request $request)
    {
        try {
            $user_id = Auth::user()->id;
            // update order declined status
            $unread_count = ChatMessage::where('to_id', '=', $user_id)
                ->where('is_read', '=', 0)
                ->count();

            if ($unread_count > 0) {
                // Check and create chat room between users
                ChatMessage::where('to_id', '=', $user_id)
                    ->where('is_read', '=', 0)
                    ->update(['is_read' => 1]);

                return Response::json(['success' => true, 'message' => 'Read Successfully']);
            }
            
            return Response::json(['success' => true, 'message' => 'No unread messages found!']);
        } catch (Exception $ex) {
            return Response::json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }

    public function sendContactMe(Request $request)
    {
        try {
            // Check and create chat room between users
            $from_id = Auth::user()->id;
            $to_id = $request->chat_to_id;
            $chat_room_id = $this->createChatRoom($from_id, $to_id);
            $msg = trim($request->message);
            $message = replaceBadWord($msg);
            
            //chat message entry
            $chat_message = [
                'chat_room_id' => $chat_room_id,
                'from_id' => $from_id,
                'to_id' => $to_id,
                'msg_type' => 0,
                'message' => $message,
                'time' => time()
            ];

            ChatMessage::create($chat_message);
            return Response::json(['success' => true, 'message' => 'Message Sent Successfully.', 'chat_room_id' => $chat_room_id]);
        } catch (Exception $ex) {
            return Response::json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }

    public function createChatRoom($from_id, $to_id)
    {
        $check = ChatRoom::where(function($q) use ($from_id, $to_id) {
                $q->where(function($query) use ($from_id, $to_id){
                    $query->where('from_id', $from_id)
                          ->where('to_id', $to_id);
                })
                ->orWhere(function($query) use ($to_id, $from_id) {
                    $query->where('to_id', $from_id)
                          ->where('from_id', $to_id);
                });
            })->get();

        if($check->count() < 1) {
            $chat_id = ChatRoom::create(['from_id' => $from_id, 'to_id' => $to_id]);
            $chat_id = $chat_id->id;
        } else {
            $chat_id = $check[0]->id;
            $chat_id;
        }
        
        return $chat_id;
    }
}
