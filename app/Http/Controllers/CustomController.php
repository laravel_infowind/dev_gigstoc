<?php
namespace App\Http\Controllers;

use File;
use Illuminate\Http\Request;
use Image;
use Response;
use Validator;
use App\Models\JobRequest;
use App\Models\ImageInService;
use App\Models\ImageInJobRequest;
use Auth;
use App\Models\Service;
use App\Models\ChatRoom;
use App\Models\ChatMessage;
use View;

class CustomController extends Controller
{
    public function fileUpload(Request $request)
    {
        $data = [];
        $service_id = $request->service_id;
        if ($request->hasFile('images')) {

            $rules = [
                'images.*' => 'mimes:gif,jpg,jpeg,png,docx,doc,pdf|max:20000',
            ];

            $messages = [
                'images.*.mimes' => 'File extension invalid',
                'images.*.max' => 'Sorry! Maximum allowed size for an image is 20MB',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                //echo "in";die;
                return Response::json(['success' => false, 'message' => $validator->errors()->all()]);
            }
            $res = [];
            foreach ($request->file('images') as $key => $value) {
                $name = time() . $key;
                $imageName = $name . '.' . $value->getClientOriginalExtension();

                $destinationPath = public_path('images/job_request/');

                $value->move($destinationPath, $imageName);

                $data['name'] = $name;
                $data['type'] = $value->getClientOriginalExtension();
                $data['url'] = url('public/images/job_request') . '/' . $imageName;
                $res[] = $data;

            }
        }

        return Response::json(['success' => true, 'data' => $res]);
    }

    public function deleteFile($imageName, $imageType)
    {
        $image = $imageName . '.' . $imageType;
        $directoryPath = public_path() . '/images/job_request/' . $image;
        $res = 0;
        //echo $directoryPath;die;
        if (file_exists($directoryPath)) {
            File::delete($directoryPath);
            $res = 1;
        }
        if ($res) {
            return Response::json(
                [
                    'success' => true,
                    'message' => 'The file has been removed.',
                ]
            );
        } else {
            return Response::json(
                [
                    'success' => false,
                    'message' => 'Some problem occurred, please try again.',
                ]
            );
        }
    }

    public function sendCustomOrder(Request $request)
    {
        try {
            $jobRequest = new JobRequest();
            $jobRequest->title = $request->title;
            $jobRequest->budget = $request->budget;
            $jobRequest->deadline = $request->deadline;
            $jobRequest->description = $request->description;
            $jobRequest->created_by = Auth::user()->id;
            $jobRequest->is_custom = 1;
            $jobRequest->custom_offer_status = 1;
            $jobRequest->service_id = $request->service_id;
            $jobRequest->sent_to = $request->sent_to;

            if($jobRequest->save())
            {
                if (!empty($request->images)) {
                    $files = json_decode($request->images, true);
                    foreach ($files as $value) {
                        $files_in_job = new ImageInJobRequest();
                        $files_in_job->job_request_id = $jobRequest->id;
                        $files_in_job->file = $value['name'] . '.' . $value['type'];
                        $files_in_job->save();

                    }
                }

                // Check and create chat room between users
                $from_id = Auth::user()->id;
                $to_id = $request->sent_to;
                $chat_room_id = $this->createChatRoom($from_id, $to_id);
                $msg = trim($jobRequest->description);
                $message = replaceBadWord($msg);

                //chat message entry
                $chat_message = [
                    'chat_room_id' => $chat_room_id,
                    'from_id' => $from_id,
                    'to_id' => $to_id,
                    'msg_type' => 2,
                    'message' => $message,
                    'custom_job_offer_id' => $jobRequest->id,
                    'custom_order_id' => $jobRequest->id,
                    'time' => time(),
                    'is_custom' => 1
                ];

                ChatMessage::create($chat_message);
            }
            return Response::json(['success' => true, 'message' => 'Custom Order Sent Successfully.', 'chat_room_id' => $chat_room_id]);
        } catch (Exception $ex) {
            return Response::json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }

    public function createChatRoom($from_id, $to_id)
    {
        $check = ChatRoom::where(function($q) use ($from_id, $to_id) {
                $q->where(function($query) use ($from_id, $to_id){
                    $query->where('from_id', $from_id)
                          ->where('to_id', $to_id);
                })
                ->orWhere(function($query) use ($to_id, $from_id) {
                    $query->where('to_id', $from_id)
                          ->where('from_id', $to_id);
                });
            })->get();

        if($check->count() < 1) {
            $chat_id = ChatRoom::create(['from_id' => $from_id, 'to_id' => $to_id]);
            $chat_id = $chat_id->id;
        } else {
            $chat_id = $check[0]->id;
            $chat_id;
        }
        
        return $chat_id;
    }

    public function sendCustomOffer(Request $request)
    {
        try {
            $service = new Service();
            $service->title = $request->title;
            $service->price = $request->budget;
            $service->deadline = $request->deadline;
            $service->description = $request->description;
            $service->created_by = Auth::user()->id;
            $service->is_custom = 1;
            $service->is_approved = 1;
            $service->job_request_id = $request->job_request_id;
            $service->parent_service_id = $request->parent_service_id;

            if ($service->save()) {
                //files in services
                if (!empty($request->images)) {
                    $files = json_decode($request->images, true);
                    foreach ($files as $value) {
                        $files_in_service = new ImageInService();
                        $files_in_service->service_id = $service->id;
                        $files_in_service->image = $value['name'] . '.' . $value['type'];
                        $files_in_service->save();

                    }
                }

                // update offer sent status i job request
                JobRequest::where('id', '=', $request->job_request_id)
                    ->update(['custom_offer_status' => 2, 'offered_service_id' => $service->id]);

                // Check and create chat room between users
                $from_id = Auth::user()->id;
                $to_id = $request->offer_sent_to;
                $chat_room_id = $this->createChatRoom($from_id, $to_id);

                //chat message entry
                $chat_message = [
                    'chat_room_id' => $chat_room_id,
                    'from_id' => $from_id,
                    'to_id' => $to_id,
                    'msg_type' => 1,
                    'message' => $service->description,
                    'custom_job_offer_id' => $service->id,
                    'custom_order_id' => $request->job_request_id,
                    'time' => time(),
                    'is_custom' => 1
                ];

                ChatMessage::create($chat_message);
            }
            return Response::json(['success' => true, 'message' => 'Offer Sent Successfully.', 'chat_room_id' => $chat_room_id]);
        } catch (Exception $ex) {
            return Response::json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }

    public function customOrderDetail(Request $request)
    {
        //print_r($request->all());die;
        try {
            $custom_order_id = $request->custom_order_id;
            $custom_order = JobRequest::with('files')->where('id', '=', $custom_order_id)->first();
            
            if(!empty($custom_order->offered_service_id)) {
                $offer = getService($custom_order->offered_service_id);
                $custom_order->is_offer = true;
                $custom_order->offer = $offer;
            } else {
                $custom_order->is_offer = false;
                $custom_order->offer = [];
            }
            
            //print_r($custom_order);die;
            $html = '';
            $html = View::make('chat._content_custom_order', ['custom_order' => $custom_order,'user_id' => Auth::user()->id])->render();

            return Response::json(['success' => true, 'message' => 'Custom Order Detail Displaying Successfully.', 'html' => $html]);
        } catch (Exception $ex) {
            return Response::json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }

    public function orderDeclined(Request $request)
    {
        try {
            // update order declined status
            JobRequest::where('id', '=', $request->custom_order_id)
            ->update(['custom_offer_status' => 3]);

            // Check and create chat room between users
            $from_id = Auth::user()->id;
            $to_id = $request->sent_to;
            $chat_room_id = $this->createChatRoom($from_id, $to_id);

            //chat message entry
            $chat_message = [
                'chat_room_id' => $chat_room_id,
                'from_id' => $from_id,
                'to_id' => $to_id,
                'msg_type' => 3,
                'message' => $request->why_decline,
                'custom_job_offer_id' => $request->custom_order_id,
                'custom_order_id' => $request->custom_order_id,
                'time' => time(),
                'is_custom' => 1
            ];

            ChatMessage::create($chat_message);

            return Response::json(['success' => true, 'message' => 'Order Declined Successfully']);
        } catch (Exception $ex) {
            return Response::json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }
    
    public function rejectOffer(Request $request)
    {
        try {
            // update order declined status
            JobRequest::where('id', '=', $request->custom_order_id)
            ->update(['custom_offer_status' => 4]);

            // Check and create chat room between users
            $from_id = Auth::user()->id;
            $to_id = $request->sent_to;
            $chat_room_id = $this->createChatRoom($from_id, $to_id);

            //chat message entry
            $chat_message = [
                'chat_room_id' => $chat_room_id,
                'from_id' => $from_id,
                'to_id' => $to_id,
                'msg_type' => 4,
                'message' => $request->why_reject,
                'custom_job_offer_id' => $request->offer_id,
                'custom_order_id' => $request->custom_order_id,
                'time' => time(),
                'is_custom' => 1
            ];

            ChatMessage::create($chat_message);

            return Response::json(['success' => true, 'message' => 'Offer Rejected Successfully']);
        } catch (Exception $ex) {
            return Response::json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }
}
