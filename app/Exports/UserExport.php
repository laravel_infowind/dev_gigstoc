<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;
use Modules\Admin\Http\Controllers\UserController;

class UserExport implements FromCollection,WithHeadings {
  

  public $data;

  function __construct($request) {
    $this->data = $request;
  }

  public function headings(): array {
    return [
       "Full Name","Email","Role","Joining Date","Status"
    ];
  }

  /**
  * @return \Illuminate\Support\Collection
  */
  public function collection() {
    return collect(UserController::allUsersExportData($this->data)); 
  }
}