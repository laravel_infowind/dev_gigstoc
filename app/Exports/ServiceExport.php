<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;
use Modules\Admin\Http\Controllers\ServiceController;

class ServiceExport implements FromCollection,WithHeadings {
  

  public $data;

  function __construct($request) {
    $this->data = $request;
  }

  public function headings(): array {
    return [
       "Title","Price","Deadline","View Count","Category","Created By","Created Date"
    ];
  }

  /**
  * @return \Illuminate\Support\Collection
  */
  public function collection() {
    return collect(ServiceController::allServiceExportData($this->data)); 
  }
}