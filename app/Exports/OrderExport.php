<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;
use Modules\Admin\Http\Controllers\OrderController;

class OrderExport implements FromCollection,WithHeadings {
  

  public $data;

  function __construct($request) {
    $this->data = $request;
  }

  public function headings(): array {
    return [
       "orderId","User Name","Amount","Service Charge","Paid Amount","Discount","Applied Coupon","TransactionId","Order Date"
    ];
  }

  /**
  * @return \Illuminate\Support\Collection
  */
  public function collection() {
    return collect(OrderController::allOrderExportData($this->data)); 
  }
}