<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;
use Modules\Admin\Http\Controllers\AdminController;

class TransactionExport implements FromCollection,WithHeadings {
  

  public $data;

  function __construct($request) {
    $this->data = $request;
  }

  public function headings(): array {
    return [
       "Payer Name","Reciever","Recieved Amount","OrderId","Paid Amount","Service Charge","Comission","TransactionId","Created_date"
    ];
  }

  /**
  * @return \Illuminate\Support\Collection
  */
  public function collection() {
    return collect(AdminController::allTransactionExportData($this->data)); 
  }
}