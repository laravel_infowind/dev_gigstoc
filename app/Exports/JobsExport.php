<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;
use Modules\Admin\Http\Controllers\JobController;

class JobsExport implements FromCollection,WithHeadings {
  

  public $data;

  function __construct($request) {
    $this->data = $request;
  }

  public function headings(): array {
    return [
       "Title","Budget","Deadline","category","CreatedBy","Created Date"
    ];
  }

  /**
  * @return \Illuminate\Support\Collection
  */
  public function collection() {
    return collect(JobController::allJobsExportData($this->data)); 
  }
}