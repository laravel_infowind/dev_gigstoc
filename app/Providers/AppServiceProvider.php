<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Hash ;
use Auth;
use App\User;
use App\Models\UserPaymentMethod;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('password_hash_check', function($attribute, $value, $parameters, $validator) {
            $user = Auth::user()->password;
            return Hash::check($value , Auth::user()->password) ;
        });
        Validator::extend('only_space_not_allowed', function($attribute, $value, $parameters, $validator) {
            return ($value)?true:false ;
        });

        Validator::extend('validate_email', function($attribute, $value, $parameters, $validator) {
            return (filter_var($value, FILTER_VALIDATE_EMAIL))?true:false ;
        });
        Validator::extend('account_verify', function($attribute, $value, $parameters, $validator) {
            $user = User::where(['email'=>$value])->orWhere(['username'=>$value])->first();
            if(!empty($user)){
            return($user->is_verified == '1')?true:false ;
            }
            return true;
        });
		
		Validator::extend('check_current_wallet', function($attribute, $value, $parameters, $validator) {
			$getCurrentWallet = getExistingBlanace(Auth::user()->id);
            if($getCurrentWallet < getSettings('minimum_withdrawal_amount')){
            return false ;
            }
            return true;
        });
		Validator::extend('check_max_wallet', function($attribute, $value, $parameters, $validator) {
			$getCurrentWallet =  (getExistingBlanace(Auth::user()->id)- getPendingAmount(Auth::user()->id));
            if($getCurrentWallet < $value){
            return false ;
            }
            return true;
        });
		
		Validator::extend('check_account_exist', function($attribute, $value, $parameters, $validator) {
			$post = request()->all();
			if($post['account_type']=='paypal'){
			$paymentMothod = UserPaymentMethod::where(['user_id'=>Auth::user()->id,'payment_type'=>2])->first();
			}else{
			$paymentMothod = UserPaymentMethod::where(['user_id'=>Auth::user()->id,'payment_type'=>1])->first();
			}
			if(empty($paymentMothod)){
				return false;
			}return true;
        });
		
		Validator::extend('check_percentage', function($attribute, $value, $parameters, $validator) {
			
			$post = request()->all();
			if($post['type'] == 'percenatge'){
				if(100 < $post['amount']){
					return false;
				}return true;
			}
			return true;
        });
		
		Validator::extend('verify_video_url', function($attribute, $value, $parameters, $validator) {
		   if (preg_match('/https:\/\/(www\.)*youtube\.com\/watch\?v=.*/', $value)) {
			return true;
			}
			if (preg_match('/https:\/\/(www\.)*vimeo\.com\/.*/', $value)) {
				$rest_url = 'https://vimeo.com/api/oembed.json?url=' . $value;
				$json = json_decode(file_get_contents($rest_url), true);
				if (isset($json['video_id'])) {
					return true;
				}
			}
			if (preg_match('/.mp4.*/', $value)) {
				return true;
			}
			return false;

        });
		
    
    }
}
