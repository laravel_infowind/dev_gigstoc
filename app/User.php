<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname', 'username', 'email', 'email_verified_at', 'password', 'profile_pic', 'phone_no', 'country_id', 'bio', 'remember_token', 'is_active', 'is_verified', 'is_online', 'is_social', 'timezone', 'stripe_customer_id', 'otp',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['name', 'user_image'];

    public function getNameAttribute()
    {
        return ucwords(strtolower($this->fullname));
    }

    public function getUserImageAttribute()
    {
        if ($this->is_social == '1') {
            if (strpos($this->profile_pic, 'http') !== false) {
                $profile_pic = $this->profile_pic;
            } else if (file_exists(public_path() . '/images/profile_pic' . '/' . $this->profile_pic)) {
                $profile_pic = url('public/images/profile_pic') . '/' . $this->profile_pic;
            } else {
                $profile_pic = url('public/images/profile_pic/avatar-default.jpg');
            }
        } else if (isset($this->profile_pic) && file_exists(public_path() . '/images/profile_pic/' . $this->profile_pic)) {
            $profile_pic = url('public/images/profile_pic') . '/' . $this->profile_pic;
        } else {
            $profile_pic = url('public/images/profile_pic/avatar-default.jpg');
        }

        return $profile_pic;
    }

    public function userInRole()
    {
        return $this->hasMany('App\Models\UserInRole', 'user_id', 'id');
    }

    public function rating()
    {
        return $this->hasMany('App\Models\SellerRating', 'to_id', 'id');
    }

    public function serviceRating()
    {
        return $this->hasMany('App\Models\ServiceRating', 'user_id', 'id');
    }

    public function userpaymentMethod()
    {
        return $this->hasMany('App\Models\UserPaymentMethod', 'user_id', 'id');
    }
	
    public function verifyUser()
    {
        return $this->hasOne('App\Models\VerifyUser');
    }
    public function languages()
    {
        return $this->hasMany('App\Models\UserLanguage', 'user_id', 'id');
    }
    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'country_id');
    }

    public function messages()
    {
        return $this->hasMany('App\Models\ChatMessage');
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
