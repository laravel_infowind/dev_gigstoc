<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ChatMessage extends Model
{
   
    protected $table = 'chat_messages';

    protected $fillable = [
        'chat_room_id', 'from_id', 'to_id', 'msg_type', 'custom_job_offer_id', 'custom_order_id', 'message', 'is_custom', 'is_approved', 'time', 'is_read'
    ];

    public function messages()
    {
        return $this->belongsTo('App\Models\ChatRoom', 'chat_room_id');
    }

    public function files()
    {
        return $this->hasMany('App\Models\ChatFile', 'chat_message_id');
    }

    public function userTo()
    {
    return $this->belongsTo('App\User', 'to_id');
    }

    public function userFrom()
    {
    return $this->belongsTo('App\User', 'from_id');
    }
   
}
