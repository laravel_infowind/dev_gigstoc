<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ServiceRating extends Model
{
   
    protected $table = 'service_ratings';

    protected $fillable = [
	
        'service_id', 'user_id', 'rating','review','is_published'
    ];
   
 
      public function user()
      {
         return $this->belongsTo('App\User', 'user_id');
      }
     
      public function service()
      {
         return $this->belongsTo('App\Models\Service', 'service_id');
      }

      public function rating()
      {
         return $this->hasManyThrough('App\Models\ServiceRating', 'App\User');
      }
   
}
