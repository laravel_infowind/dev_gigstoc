<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ChatFile extends Model
{
   
    protected $table = 'chat_files';

    protected $fillable = [
        'chat_room_id', 'chat_message_id', 'file_name', 'mime'
    ];
   
}
