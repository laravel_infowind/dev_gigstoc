<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OffersOnJob extends Model
{

    protected $table = 'offers_on_job';

    protected $fillable = [
        'job_request_id', 'service_id', 'requested_by',
    ];

    public function jobDetail()
    {
        return $this->belongsTo('App\Models\JobRequest', 'job_request_id');
    }

    public function serviceDetail()
    {
        return $this->belongsTo('App\Models\Service', 'service_id');
    }

    public function requestedUser()
    {
        return $this->belongsTo('App\User', 'requested_by');
    }

}
