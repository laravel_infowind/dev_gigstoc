<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class RestrictedWord extends Model
{
   
    protected $table = 'restricted_words';

    protected $fillable = [
        'word','is_active'
    ];
   
	
   
}
