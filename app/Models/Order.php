<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
   
    protected $table = 'orders';

    protected $fillable = [
        'order_id', 'user_id', 'subtotal','tax', 'discount', 'total','status','transaction_id','payment_type','payment_history','cart_data'
    ];
	
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
	
	public function detail()
    {
        return $this->hasOne('App\Models\OrderDetail', 'order_id', 'id');
    }
	
	public function orderCoupon()
    {
        return $this->hasOne('App\Models\OrderCoupon', 'order_id', 'id');
    }
 
   
}
