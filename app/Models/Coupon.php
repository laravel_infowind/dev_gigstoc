<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Coupon extends Model
{
   
    protected $table = 'coupons';

    protected $fillable = [
        'id','coupon','type','amount','is_active','total_usage','current_usage'
    ];
   
   
}
