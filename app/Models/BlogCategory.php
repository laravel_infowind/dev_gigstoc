<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class BlogCategory extends Model
{
   
    protected $table = 'blog_categories';

    protected $fillable = [
        'category_name', 'slug','category_description','created_by','is_active',
     ];
   
    public function user(){
        return $this->belongsTo('App\User', 'created_by');
    }

    public function posts()
    {
        return $this->belongsToMany('App\Models\BlogPost', 'blog_category_posts');
    }
   
}
