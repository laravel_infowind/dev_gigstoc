<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ConfigSetting extends Model
{
   
    protected $table = 'config_settings';

    protected $fillable = [
        'meta_key', 'meta_value'
    ];
   
 
   
}
