<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UserWallet extends Model
{
   
    protected $table = 'user_wallets';

    protected $fillable = [
        'site_transaction_id', 'user_id', 'credit','debit','amount','current_balance','is_withdrawal'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
   
 
   
}
