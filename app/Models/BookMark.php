<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class BookMark extends Model
{
   
    protected $table = 'bookmarks';

    protected $fillable = [
        'user_id', 'service_id', 'is_bookmarked'
    ];
   
   public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
	public function service()
    {
        return $this->belongsTo('App\Models\Service', 'service_id');
    }
   
}
