<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    
    protected $table = 'services';

    protected $fillable = [
        'title','price','deadline','category_id','description','is_video','video_link','video_type','views','is_custom', 'job_request_id', 'opening_message', 'is_approved', 'created_by','parent_service_id','is_active'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category')->select(array('id', 'name'));
    }

    public function rating()
    {
        return $this->hasMany('App\Models\ServiceRating', 'service_id', 'id')->where(['is_published'=>1]);
    }

    public function extras()
    {
        return $this->hasMany('App\Models\ExtraInService', 'service_id', 'id');
    }

    public function images()
    {
        return $this->hasMany('App\Models\ImageInService', 'service_id', 'id');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'tag_in_services');
    }
	
	

   
}
