<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class BlogPostCategory extends Model
{
   
    protected $table = 'blog_category_posts';

    protected $fillable = [
    'blog_post_id', 'blog_category_id'
    ];
   
    public function blogPost(){
        return $this->belongsTo('App\Models\BlogPost', 'blog_post_id');
    }

    public function blogCategory(){
        return $this->belongsTo('App\Models\BlogCategory', 'blog_category_id');
    }
    
   
}
