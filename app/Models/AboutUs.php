<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class AboutUs extends Model
{
   
    protected $table = 'about_us';

    protected $fillable = [
        'name', 'slug','main_header','header_content','image','middle_content','footer_header','footer_cotent',
     ];
   
   
}
