<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PhysicalPayment extends Model
{
   
    protected $table = 'physical_payments';

    protected $fillable = [
        'user_id', 'amount', 'transaction_id','order_id'
    ];
   
 
   
}
