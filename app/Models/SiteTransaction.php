<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class SiteTransaction extends Model
{
   
    protected $table = 'site_transactions';

    protected $fillable = [
        'sender_id', 'receiver_id', 'amount','sc','cr','user_received_amount','order_id','transaction_id',
    ];
   
    public function payer(){
        return $this->belongsTo('App\User' ,'sender_id');
    }

    public function receiver(){
        return $this->belongsTo('App\User' ,'receiver_id');
    }

    public function order(){
        return $this->belongsTo('App\Models\Order' ,'order_id');
    }
 
   
}
