<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class BlogPost extends Model
{
   
    protected $table = 'blog_posts';

    protected $fillable = [
        'user_id', 'title','slug','short_description','post_body','posted_at','image','thumbnail','meta_title','meta_desc','meta_keywords','is_active'
     ];
   
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\BlogCategory', 'blog_category_posts');
    }

    public function blogPostCategory()
    {
        return $this->hasMany('App\Models\BlogPostCategory', 'blog_post_id', 'id');
    }
   
}
