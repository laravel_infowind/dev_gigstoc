<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class Slider extends Model
{
    protected $table = 'sliders';

    protected $fillable = [
        'section', 'title', 'description','slider_img'
    ];
}