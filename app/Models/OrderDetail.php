<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class OrderDetail extends Model
{
   
    protected $table = 'order_details';

    protected $fillable = [
        'order_id', 'user_id', 'product_id', 'extra_services', 'quantity','price'
    ];
   
    public function service()
    {
        return $this->belongsTo('App\Models\Service', 'product_id');
    }
   
}
