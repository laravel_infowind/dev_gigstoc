<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ExtraInService extends Model
{
   
    protected $table = 'extra_in_services';

    protected $fillable = [
        'service_id', 'service_name', 'price'
    ];
   
    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }
   
}
