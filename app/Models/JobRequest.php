<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class JobRequest extends Model
{
   
    protected $table = 'job_requests';

    protected $fillable = [
        'title', 'description', 'budget','deadline','category_id','created_by','is_completed', 'is_custom', 'custom_offer_status', 'service_id', 'sent_to', 'is_active'
    ];
   
    public function user()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category')->select(array('id', 'name'));
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'tag_in_job_requests');
    }

	public function offers()
    {
        return $this->hasMany('App\Models\OffersOnJob', 'job_request_id', 'id');
    }

    public function files()
    {
        return $this->hasMany('App\Models\ImageInJobRequest', 'job_request_id', 'id');
    }

    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }
}
