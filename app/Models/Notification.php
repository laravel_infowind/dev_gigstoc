<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Notification extends Model
{
   
    protected $table = 'notifications';

    protected $fillable = [
        'title','type','from_id','to_id'
     ];
	 
	 
    public function fromUser()
    {
        return $this->belongsTo('App\User', 'from_id');
    }
   
}
