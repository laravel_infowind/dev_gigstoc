<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UserPaymentMethod extends Model
{
   
    protected $table = 'user_payment_methods';

    protected $fillable = [
        'user_id', 'account_info', 'payment_type','preferred_method','account_details'
    ];
   
 
   
}
