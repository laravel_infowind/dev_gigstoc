<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ChatRoom extends Model
{
   
    protected $table = 'chat_rooms';

    protected $fillable = [
        'from_id', 'to_id'
    ];

    public function messages()
    {
        return $this->hasMany('App\Models\ChatMessage');
    }
   
}
