<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageInJobRequest extends Model
{
   
    protected $table = 'image_in_job_requests';

    protected $fillable = [
        'job_request_id', 'file'
    ];
}
