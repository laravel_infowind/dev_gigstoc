<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
   
    protected $table = 'categories';

    protected $fillable = [
        'name', 'slug', 'description','cat_image','category_id','created_by','is_featured','is_active','visible_in_menu'
    ];

    public function categories()
    {
        return $this->hasMany(Category::class);
    }
   
    public function services()
    {
        return $this->hasMany('App\Models\Services');
    }
   
}
