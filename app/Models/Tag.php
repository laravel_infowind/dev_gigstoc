<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Tag extends Model
{
   
    protected $table = 'tags';

    protected $fillable = [
        'name'
    ];
   
    public function services()
    {
        return $this->belongsToMany('App\Models\Service', 'tag_in_services');
    }
   
}
