<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class WithdrawalRequest extends Model
{
   
    protected $table = 'withdrawal_requests';

    protected $fillable = [
        'user_id', 'account', 'amount','payment_method_id','status','selected_method'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function paymentMethod()
    {
        return $this->belongsTo('App\Models\UserPaymentMethod', 'payment_method_id');
    }
   
 
   
}
