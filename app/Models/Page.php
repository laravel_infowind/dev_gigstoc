<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Page extends Model
{
   
    protected $table = 'pages';

    protected $fillable = [
        'name', 'slug', 'content','meta_title','meta_desc','meta_keywords','is_active','is_visible'
    ];
   
 
   
}
