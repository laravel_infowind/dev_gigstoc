<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceView extends Model
{
    
    protected $table = 'service_views';

    protected $fillable = [
        
    ];

    public function service()
    {
        return $this->belongsTo('App\Models\Service', 'service_id');
    }

    

   
}
