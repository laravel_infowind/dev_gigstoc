<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class FindingCost extends Model
{
    protected $table = 'finding_costs';
    
    protected $fillable = [
        'section_id', 'heading', 'description', 'image'
    ];

    public function findingCost()
    {
        return $this->belongsTo('App\Models\HomeManagement', 'home_management_id');
    }
}