<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class HomeManagement extends Model
{
    protected $table = 'home_management';
    
    protected $fillable = [
        'section', 'sub_heading', 'heading', 'description', 'image', 'status'
    ];

    public function findingCosts()
    {
        return $this->hasMany('App\Models\FindingCost')->where('finding_costs.home_management_id','=', 10);
    }

    public function keyFeatures()
    {
        return $this->hasMany('App\Models\KeyFeature')->where('key_features.home_management_id','=', 9);
    }
}