<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class SellerRating extends Model
{
   
    protected $table = 'seller_ratings';

    protected $fillable = [
        'from_id', 'to_id', 'rating','review','is_published','service_id'
    ];

    public function fromUser()
    {
        return $this->belongsTo('App\User', 'from_id');
    }

    public function toUser()
    {
        return $this->belongsTo('App\User', 'to_id');
    }
   
    public function service()
    {
        return $this->belongsTo('App\Models\Service', 'service_id');
    }
 
   
}
