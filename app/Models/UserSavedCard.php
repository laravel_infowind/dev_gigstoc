<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UserSavedCard extends Model
{
   
    protected $table = 'user_saved_cards';

    protected $fillable = [
        'user_id', 'customer_id', 'card_id','fingerprint','card_detail',
    ];
   
 
   
}
