<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class OrderCoupon extends Model
{
   
    protected $table = 'order_coupons';

    protected $fillable = [
        'order_id', 'coupon_id', 'discount_amount'
    ];
	
    public function coupon()
    {
        return $this->belongsTo('App\Models\Coupon', 'coupon_id');
    }
 
   
}
