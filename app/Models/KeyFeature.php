<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class KeyFeature extends Model
{
    protected $table = 'key_features';
    
    protected $fillable = [
        'section_id', 'heading', 'description', 'image'
    ];

    public function keyFeature()
    {
        return $this->belongsTo('App\Models\HomeManagement', 'home_management_id');
    }
}