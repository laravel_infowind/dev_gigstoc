<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ImageInService extends Model
{
   
    protected $table = 'image_in_services';

    protected $fillable = [
        'service_id', 'thumbnail', 'image', 'is_featured'
    ];
   
 
   
}
